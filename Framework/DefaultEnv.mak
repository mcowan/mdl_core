#*****************************************
# Common Default Help Menu 
#*****************************************


###################################
# Target - Print the Enviroment
default-env:
	@echo "======================================================================"
	@echo "Enviroment Variables                                                  "
	@echo "======================                                                "
	@echo "Directories                                                           "
	@echo "=============                                                         "
	@echo "BASEDIR       = $(BASEDIR)                                            "
	@echo "SUPERDIR      = $(SUPERDIR)                                           "
	@echo "WORKDIR       = $(WORKDIR)                                            "
	@echo "PACKAGE       = $(PACKAGE)                                            "
	@echo "PACKAGETYPE   = $(PACKAGETYPE)                                        "
	@echo "OBJDSTDIR     = $(OBJDSTDIR)                                          "
	@echo "TESTOBJDSTDIR = $(TESTOBJDSTDIR)                                      "
	@echo "LIBDSTDIR     = $(LIBDSTDIR)                                          "
	@echo "APPDSTDIR     = $(APPDSTDIR)                                          "
	@echo "TESTDSTDIR    = $(TESTDSTDIR)                                         "
	@echo "DOXDSTDIR     = $(DOXDSTDIR)                                          "
	@echo "DOXYFILE      = $(DOXYFILE)                                           "
	@echo "                                                                      "
	@echo "Compiler Flags                                                        "
	@echo "================                                                      "
	@echo "TARGET      = $(TARGET)                                               "
	@echo "OS          = $(COMPILEROS)                                           "
	@echo "LIBEXT      = $(LIBEXT)                                               "
	@echo "CC          = $(CXX)                                                  "
	@echo "CXX         = $(CXX)                                                  "
	@echo "AR          = $(AR)                                                   "
	@echo "======================================================================"
###################################	
#*****************************************
# Common Application Help Menu 
#*****************************************


app-help:
	@echo "======================================================================"
	@echo "                                                                      "
	@echo "Micro Development Libraries                                           "
	@echo "                                                                      "
	@echo "      __ __ __ __ __ __                                               "
	@echo "      __\__\__\__\__\__\_                                             "
	@echo "     /\                  \                                            "
	@echo "     \*\   Micro Devel    \       VERSION: $(VERSION)                 "
	@echo "      \*\__________________\                                          "
	@echo "       \/__\__\__\__\__\__\/                                          "
	@echo "           /  /  /  /  /  /                                           "
	@echo "                                                                      "
	@echo "Help -- Compiling an Application                                      "
	@echo "=================================                                     "
	@echo "'make'                                                                "
	@echo "  Will compile the application.                                       "
	@echo "    Application: $(APPDSTDIR)$(PACKAGE).out                           "
	@echo "                                                                      "
	@echo "'make test'                                                           "
	@echo "  No Implemented!! Will run all the unit test of the Test Linked      "
	@echo "  libraries defined in the application makefile.                      "
	@echo "                                                                      "
	@echo "'make coverage'                                                       "
	@echo "  Not Implemented!! Will generate all of the coverage reports for the "
	@echo "  libraries linked in the Test Linked libraries as defined in the     "
	@echo "  application makefile.                                               "
	@echo "                                                                      "
	@echo "'make help'                                                           "
	@echo "  This menu.                                                          "
	@echo "                                                                      "
	@echo "  Optional TARGET variable                                            "
	@echo "    Allows the user to specify a defined architecture to switch the   "
	@echo "    preconfigured Toolchain parameters.  For example to compile for a "
	@echo "    microcontroller. 																								 "
	@echo "                                                                      "
	@echo "          Supported TARGETs:                           							 "
	@echo "          --------------------------------                            "
	@echo "           X86 	-	Default configuration compiles using standard      "
	@echo "                   compilers gcc/g++.																 "
	@echo "           UNO   - Arduino UNO using AVR compilers.                   "
	@echo "           MEGA  - Arduino MEGA using AVR compilers.                  "
	@cat $(REQUIREDPKGS)
	@echo "======================================================================"

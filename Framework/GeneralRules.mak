#********************
# General make rules
#********************

###################################
# Target - Generate Doxygen Code from Source
doxygen: $(DOXDSTDIR) $(DOXYFILE)
	( cat $(DOXYFILE) ; echo "PROJECT_NUMBER=$(DOXVERSION)"; echo "INPUT=$(DOXINPUT) $(DOXMAINPAGE)"; echo "OUTPUT_DIRECTORY=$(DOXOUTPUT)"; echo "WARN_LOGFILE=$(DOXLOGFILE)"; echo "PROJECT_NAME=$(DOXPROJECT)"; ) | doxygen -
	@ $(MAKE) -C $(DOXDSTDIR)/latex pdf > /dev/null
	@ echo "Doxygen Generation Complete"
	@ echo ""
	@ echo "-----------------------------------------------------------"
	@ echo ""
	@ echo " Warnings File: $(DOXDSTDIR)Warnings.dox"
	@ echo "          HTML: $(DOXDSTDIR)html/index.html"
	@ echo "           PDF: $(DOXDSTDIR)/latex/refman.pdf"
	@ echo ""
	@ echo "-----------------------------------------------------------"
	@ echo ""

	
###################################

###################################
# Target - Remove the Destination Directory	
clobber::  # Double so app/project specific can add additional clean rules
	rm -rf $(BASEDIR)dst

###################################

###################################################################
# Target Rules to generate all Directories
$(OSOBJDIR): %:
	@mkdir -p $@

# Rule - Generate Object directory
$(OBJDSTDIR): %:
	@mkdir -p $@
	
# Rule - Generate Test Object directory
$(TESTOBJDSTDIR): %:
	@mkdir -p $@

# Rule - Generate Library directory
$(LIBDSTDIR): %:
	@mkdir -p $@

# Rule - Generate Application directory
$(APPDSTDIR): %:
	@mkdir -p $@
	
# Rule - Generate Test directory
$(TESTDSTDIR): %:
	@mkdir -p $@
	
# Rule - Generate Doxygen directory
$(DOXDSTDIR): %:
	@mkdir -p $@
	
# Rule - Generate Test Results directory
$(TESTRESULTS): %:
	@mkdir -p $@

# Rule - Generate Coverage test app directory
$(COVERDST): %:
	@mkdir -p $@

# Rule - Generate Coverage Object directory
$(COVERPKG): %:
	@mkdir -p $@
	
$(COVERTEST): %:
	@mkdir -p $@

$(OBJDSTDIRCOMMON): %:
	@mkdir -p $@

###################################################################
# Compile Rules

# Rule - Generate object file from C++ source file
$(OBJDSTDIR)/%.o: %.cpp
	$(CXX) $(CCFLAGS) -c $< -o $@	
	@echo

# Rule - Generate object file from CC source file
$(OBJDSTDIR)/%.o: %.cc
	$(CXX) $(CCFLAGS) -c $< -o $@	
	@echo
	
# Rule - Generate object file from C source file
$(OBJDSTDIR)/%.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@	
	@echo	

# Rule - Library archive
$(LIBDSTDIR)lib$(PACKAGE).a: $(OBJLIST)
	$(AR) -crsv $@ $^
	@echo
	
# Rule - Generate other Libraries
l-%:
	@ LIBDIR="$(firstword $(foreach dir,$(SUPERDIR),$(wildcard $(dir)/src/$(subst l-,,$@))))"; \
	$(MAKE) -C $$LIBDIR/ 
	@echo
	
OSAbstract:
	$(MAKE) -C $(BASEDIR)os/
	
###################################################################


###################################################################
#Clean Rules

app-clean:
	@rm -f $(OBJLIST)
	@rm -rf $(OSOBJDIR)
	@rm -f $(TESTLIBS_OBJECTS)
	@rm -rf $(COVERPKG)
	@rm -rf *.gc*
	@echo Clean Complete!

src-clean:
	@rm -f $(OBJLIST)
	@rm -f $(TESTLIBS_OBJECTS)
	@rm -rf $(COVERPKG)
	@rm -f $(LIBDSTDIR)lib$(PACKAGE)$(LIBEXT)
	@rm -rf $(OSOBJDIR)
	@rm -rf *.gc*
	@echo Clean Complete!

default-clean:
	@rm -rf *.gc*
	@rm -rf $(SUPERDIR)dst
	@echo Clean Complete!
###################################################################


###################################################################
#Common Rules

# Compile Source Lib and Test Code
common-test: \
	$(OBJDSTDIR) \
	$(OSOBJDIR) \
	$(LIBDSTDIR) \
	$(TESTDSTDIR) \
	$(TESTOBJDSTDIR) \
	$(TESTRESULTS) \
	$(COVERDST) \
	$(COVERPKG) \
	$(COVERTEST) \
	$(addprefix l-,$(TESTLIBS))
	@rm -f $(TESTDSTDIR)$(PACKAGE).out
		@ set -o pipefail; $(MAKE) $(TESTDSTDIR)$(PACKAGE).out 2>&1 | grep -v 'sh_link not set for section'
		@echo 
		@echo Test Library $(PACKAGE) done.
		@echo 
		@echo Executing Tests...
		@ $(TESTDSTDIR)$(PACKAGE).out --gtest_output=xml:"$(TESTRESULTS)/$(PACKAGE)-testResults.xml"
		@echo
		@echo 'make test' Complete!
		@echo

# Compile Gtest Tests
gtest-test: \
	$(OBJDSTDIR) \
	$(LIBDSTDIR) \
	$(TESTDSTDIR) \
	$(TESTOBJDSTDIR) \
	$(TESTRESULTS) \
	$(COVERDST) \
	$(COVERPKG) \
	$(COVERTEST) \
	l-gtest
		@rm -f $(TESTDSTDIR)$(PACKAGE).out
		@$(CXX) -I$(SUPERDIR)src -pthread $(TEST_SOURCE_FILES) $(LIBDSTDIR)libgtest.a -o $(TESTDSTDIR)$(PACKAGE).out
		@echo 
		@echo Test Library $(PACKAGE) done.
		@echo 
		@echo Executing Tests...
		@ $(TESTDSTDIR)$(PACKAGE).out --gtest_output=xml:"$(TESTRESULTS)/$(PACKAGE)-testResults.xml"
		@echo
		@echo 'make test' Complete!
		@echo
		
# Compile OS Lib
common-os-lib: \
	$(OBJDSTDIR) \
	$(OSOBJDIR) \
	$(OBJDSTDIRCOMMON) \
	$(LIBDSTDIR) \
	$(LIBDSTDIR)lib$(PACKAGE)$(LIBEXT)
		@echo Compile Library $(PACKAGE) done.
		@echo

#Common source library
common-lib: \
	$(OBJDSTDIR) \
	$(OSOBJDIR) \
	$(LIBDSTDIR) \
	OSAbstract   \
	$(LIBDSTDIR)lib$(PACKAGE)$(LIBEXT)
		@echo Compile Library $(PACKAGE) done.
		@echo

# Compile Appplication
common-default-app: \
  $(OSOBJDIR) \
	$(LIBDSTDIR) \
	$(addprefix l-, $(LINKLIBS)) \
	$(OBJDSTDIR) \
	OSAbstract   \
	$(APPDSTDIR)
		@ set -o pipefail; $(MAKE) $(APPDSTDIR)$(PACKAGE)$(APPEXT) 2>&1 | grep -v 'sh_link not set for section'
		@echo 
		@echo "Compiling Application $(PACKAGE) done. Application located at:"
		@echo $(APPDSTDIR)$(PACKAGE)$(APPEXT)
		@echo

# Upload software to Hardware
common-upload: \
	$(APPDSTDIR) \
	$(APPDSTDIR)$(PACKAGE).hex
		$(LOADER) $(LOADERFLAGS) -C$(LOADERCONFIG) -P$(TARGETDEVICE) -Uflash:w:$(APPDSTDIR)$(PACKAGE).hex:i 

# Generate Source Lib Coverage		
common-coverage: \
	$(OBJDSTDIR) \
	$(OSOBJDIR) \
	$(LIBDSTDIR) \
	$(TESTDSTDIR) \
	$(TESTOBJDSTDIR) \
	$(TESTRESULTS) \
	$(addprefix l-,$(TESTLIBS)) 
		@rm -f $(TESTDSTDIR)$(PACKAGE)_Coverage.out
		@ set -o pipefail; $(MAKE) $(TESTDSTDIR)$(PACKAGE)_Coverage.out 2>&1 | grep -v 'sh_link not set for section'
		@echo 
		@echo Test Library $(PACKAGE) done.
		@echo 
		@echo Executing Tests...
		@ $(TESTDSTDIR)$(PACKAGE)_Coverage.out --gtest_output=xml:"$(TESTRESULTS)/$(PACKAGE)-testResults.xml"
		@echo Generating Coverage.info
		lcov --capture --directory $(WORKDIR) --output-file $(TESTRESULTS)/coverage.info --gcov-tool $(COV) --quiet --no-external # --directory $(COVERDST) 
		@echo Generating HTML Report
		genhtml $(TESTRESULTS)/coverage.info --output-directory $(TESTRESULTS)/HTML
		@rm -r $(WORKDIR)/*.gcda
		@rm -r $(WORKDIR)/*.gcno
		@echo
		@echo 'make coverage' Complete!
		@echo

###################################################################




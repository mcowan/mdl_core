###################################
# Common Targets
###################################

# Compiles a Library in the "os" Directory
ifeq ($(PACKAGE), os)

# Target - Compile Default Library
default:
	@ $(MAKE) common-os-lib 

# Target - test
test:
	@ $(MAKE) common-test

# Target - coverage
coverage:
	@ $(MAKE) common-coverage

# Target - help
help:
	@ $(MAKE) os-help
	@echo ""
	
# Target - Print Enviroment Variables	
env:
	@ $(MAKE) os-env
	
# Target - Clean directories
clean::
	@ $(MAKE) src-clean # Double so app/project specific can add additional clean rules

###################################################################


# Compiles a Library in the "src" Directory
else ifeq ($(PACKAGETYPE), src)

###################################
# Target - Compile Default Library
default:
	@ $(MAKE) common-lib

# Target - Test
test:
ifeq ($(PACKAGE), gtest)
	@ $(MAKE) gtest-test
else
	@ $(MAKE) common-test
endif

# Target - Coverage
coverage:
	@ $(MAKE) common-coverage

# Target - Print Enviroment Variables	
help:
	@ $(MAKE) source-help
	@echo ""

# Target - Print Enviroment Variables	
env:
	@ $(MAKE) source-env
	
# Target - Clean directories
clean::
	@ $(MAKE) src-clean # Double so app/project specific can add additional clean rules

###################################################################
# Compiles an Application in the "app" directory
else ifeq ($(PACKAGETYPE), app)

###################################
default:
	@ echo Compiling Application!!
	@ $(MAKE) common-default-app TARGET=$(TARGET) TARGETDEVICE=$(TARGETDEVICE)

upload:
		@echo Uploading Binary
		@ $(MAKE) common-default-app TARGET=$(TARGET) TARGETDEVICE=$(TARGETDEVICE)
		@ $(MAKE) common-upload TARGET=$(TARGET) TARGETDEVICE=$(TARGETDEVICE)

# Target - Print Help Menu
help:
	@ $(MAKE) app-help
	@echo ""

###################################
# Target - Print Enviroment Variables
env:
	@ $(MAKE) app-env

###################################
# Target - Clean directories
clean::  # Double so app/project specific can add additional clean rules
	@ $(MAKE) app-clean

###################################################################
# General Compile
else
# Target - Print Help Menu
help:
	@ $(MAKE) default-help
	@echo ""

# Target - Print Enviroment Variables
env:
	@ $(MAKE) default-env
	
# Target - Clean directories
clean::  # Double so app/project specific can add additional clean rules
	@ $(MAKE) default-clean
		
endif # End of Default.
###################################################################


###################################################################

# Rule - Executable application
$(APPDSTDIR)$(PACKAGE).out: \
	$(OBJLIST) \
	$(addprefix $(LIBDSTDIR)lib, $(addsuffix .a, $(LINKLIBS)))
		$(CXX) -o $@ $(SRCLIST)  $(addprefix -l, $(LINKLIBS)) $(LDFLAGS) $(INCLUDELIBS) -los -I$(SUPERDIR)os -I$(SUPERDIR)src $(EXTRAINCLUDES)
		
$(APPDSTDIR)$(PACKAGE).hex: \
	$(OBJLIST) \
	$(addprefix $(LIBDSTDIR)lib, $(addsuffix .a, $(LINKLIBS)))
		$(AR) rcsv core.a $(ALLOBJLIST)
		$(CXX) -w -Os -Wl,--gc-sections -mmcu=atmega328p -o $@.elf $(SRCLIST) $(LDFLAGS) core.a -I$(SUPERDIR)os -I$(SUPERDIR)src $(EXTRAINCLUDES)
	#	$(CXX) -w -Os -Wl,--gc-sections -mmcu=atmega328p -o $@.elf $(SRCLIST) $(LDFLAGS) $(INCLUDELIBS) -los $(addprefix -l, $(LINKLIBS)) -I$(SUPERDIR)os -I$(SUPERDIR)src $(EXTRAINCLUDES)
	#	$(CXX) -w -Os -Wl,--gc-sections -mmcu=atmega328p -o $@.elf $(OBJLIST) $(ALLOBJLIST) $(LDFLAGS) -I$(SUPERDIR)os -I$(SUPERDIR)src $(EXTRAINCLUDES)	
		$(OBJ) -O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0 $@.elf $@.eep 
		$(OBJ) -O ihex -R .eeprom $@.elf $@
		
# Rule - Test Executable application
$(TESTDSTDIR)$(PACKAGE).out: \
	$(addprefix $(LIBDSTDIR)lib, $(addsuffix .a, $(PACKAGE)))
		$(CXX) -Wall -g $(LDFLAGS) $(ENABLETESTFLAGS) -o $@ $(SRCLIST) $(TEST_SOURCE_FILES) $(INCLUDELIBS) -los $(addprefix -l, $(PACKAGE)) $(addprefix -l, $(TESTLIBS)) -I$(SUPERDIR)os -I$(SUPERDIR)src

$(TESTDSTDIR)$(PACKAGE)_Coverage.out: \
	$(addprefix $(LIBDSTDIR)lib, $(addsuffix .a, $(PACKAGE)))
		$(CXX) -Wall -g --coverage $(ENABLETESTFLAGS) -o $@ $(SRCLIST) $(TEST_SOURCE_FILES) $(INCLUDELIBS)  -los $(addprefix -l, $(TESTLIBS)) $(addprefix -l, $(PACKAGE))  $(LDFLAGS) -I$(SUPERDIR)os -I$(SUPERDIR)src
	
###################################################################
	

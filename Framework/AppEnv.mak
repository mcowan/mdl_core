#*****************************************
# Common Default Help Menu 
#*****************************************


###################################
# Target - Print the Enviroment
app-env:
	@echo ======================================================================
	@echo Enviroment Variables                                                  
	@echo =====================                                                 
	@echo Package                                                               
	@echo ========                                                              
	@echo PACKAGE       = $(PACKAGE)                                            
	@echo PACKAGETYPE   = $(PACKAGETYPE)                                        
	@echo                                                                       
	@echo Directories                                                           
	@echo ============                                                          
	@echo BASEDIR       = $(BASEDIR)                                            
	@echo SUPERDIR      = $(SUPERDIR)                                           
	@echo WORKDIR       = $(WORKDIR)                                            
	@echo DEPDSTDIR     = $(DEPDSTDIR)                                          
	@echo OBJDSTDIR     = $(OBJDSTDIR)                                          
	@echo TESTOBJDSTDIR = $(TESTOBJDSTDIR)                                      
	@echo LIBDSTDIR     = $(LIBDSTDIR)                                          
	@echo APPDSTDIR     = $(APPDSTDIR)                                          
	@echo TESTDSTDIR    = $(TESTDSTDIR)                                         
	@echo DOXDSTDIR     = $(DOXDSTDIR)                                          
	@echo DOXYFILE      = $(DOXYFILE)                                           
	@echo                                                                       
	@echo Source Lists                                                          
	@echo =============                                                         
	@echo SRCLIST  = $(SRCLIST)                                                 
	@echo HEADLIST = $(HEADLIST)                                                
	@echo DEPLIST  = $(DEPLIST)                                                 
	@echo OBJLIST  = $(OBJLIST)                                                 
	@echo LINKLIBS = $(LINKLIBS)                                                
	@echo                                                                       
	@echo Object Lists                                                          
	@echo =============                                                         
	@echo ALLLIBS = $(ALLLIBS)                                                  
	@echo ALLOBJLIST = $(ALLOBJLIST)                                            
	@echo                                                                       
	@echo Test Link Lists                                                       
	@echo ================                                                      
	@echo TESTLIBS            = $(TESTLIBS)                                     
	@echo TESTLIBS_BASE_FILES = $(TESTLIBS_BASE_FILES)                          
	@echo TESTLIBS_OBJECTS    = $(TESTLIBS_OBJECTS)                             
	@echo TEST_SOURCE_FILES        = $(TEST_SOURCE_FILES)                       
	@echo TEST_BASE_SOURCE_FILES   = $(TEST_BASE_SOURCE_FILES)                  
	@echo TEST_OBJECT_SOURCE_FILES = $(TEST_OBJECT_SOURCE_FILES)                
	@echo                                                                       
	@echo Coverage                                                              
	@echo =========                                                             
	@echo COVERDST      = $(COVERDST)                                           
	@echo COVERPKG      = $(COVERPKG)                                           
	@echo COVERTEST     = $(COVERTEST)                                          
	@echo                                                                       
	@echo Compiler Flags                                                        
	@echo ===============                                                       
	@echo TARGET      = $(TARGET) 																							 
	@echo COMPILEROS  = $(COMPILEROS)                                           
	@echo LIBEXT      = $(LIBEXT)                                               
	@echo APPEXT      = $(APPEXT)                                               
	@echo CC          = $(CXX)                                                  
	@echo CXX         = $(CXX)                                                  
	@echo AR          = $(AR)                                                   
	@echo LD          = $(LD)                                                   	
	@echo ASFLAGS     = $(ASFLAGS)                                              
	@echo CCFLAGS     = $(CCFLAGS)                                              
	@echo CXXFLAGS    = $(CXXFLAGS)                                             
	@echo ARFLAGS     = $(ARFLAGS)                                              
	@echo LDFLAGS     = $(LDFLAGS)                                              
	@echo STDLINKLIBS = $(STDLINKLIBS)                                          
	@echo ======================================================================
###################################	



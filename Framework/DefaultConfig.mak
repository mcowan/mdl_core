#***************************
# Common make configuration
#***************************
SHELL := /bin/bash

VERSION       	= 1.0.0

# General
COMPILEROS    	= $(shell uname -s)
TARGET        	?= X86
TARGETDEVICE  	?=
SUPERDIR      	= $(shell pwd)/$(BASEDIR)
WORKDIR       	= $(shell pwd)
PACKAGE       	?= $(shell basename $(WORKDIR))
PACKAGETYPE   	?= $(shell basename $(shell dirname $(WORKDIR)))
FRAMEWORK     	= $(BASEDIR)Framework/
OBJDSTDIR     	= $(BASEDIR)dst/obj/$(PACKAGE)
OBJDSTDIRCOMMON = $(OBJDSTDIR)/Common/
LIBDSTDIR     	= $(BASEDIR)dst/lib/$(TARGET)/
APPDSTDIR     	= $(BASEDIR)dst/app/
SRCDIR        	= $(SUPERDIR)src
OSDIR         	= $(SUPERDIR)os

# Test
TESTDSTDIR    = $(BASEDIR)dst/test/
TESTOBJDSTDIR = $(BASEDIR)dst/obj/$(PACKAGE)/test
TESTRESULTS   = $(BASEDIR)dst/test-results

# Coverage
COVERDST      = $(BASEDIR)dst/cov
COVERPKG      = $(COVERDST)/$(PACKAGE)
COVERTEST     = $(COVERPKG)/test

# Doxygen
DOXDSTDIR     = $(BASEDIR)dst/dox/
DOXYFILE      = $(BASEDIR)Framework/doxygen.dox

# Doxygen Options -- allows other makefiles to dynamically configure
# these options at compile time
DOXLOGFILE = $(DOXDSTDIR)Warnings.log
DOXINPUT   = $(SRCDIR)
DOXOUTPUT  = $(DOXDSTDIR)
DOXPROJECT = MicroDevelCore
DOXVERSION = $(VERSION)
DOXMAINPAGE = $(FRAMEWORK)defaultMDLMainPage.dox


# DEVICEFLAGS
ifeq ($(ENABLETEST),SERIAL)
		ENABLETESTFLAGS = -DENABLE_$(ENABLETEST)_TEST
else
		ENABLETESTFLAGS =
endif

#Configure OSType
# Include OS Rules
ifeq ($(COMPILEROS),Darwin)
    include $(BASEDIR)Framework/MacOS.toolchain
else ifeq ($(COMPILEROS),Linux)
    include $(BASEDIR)Framework/Linux.toolchain
else ifeq ($(COMPILEROS),CYGWIN_NT-6.3)
    include $(BASEDIR)Framework/Windows.toolchain
else
    $(error "Architecture is Unsupported")
endif


#***************************
# File lists
#***************************

# Generate Test Files
TESTLIBS += gtest gmock gtestmain 
TESTLIBS_BASE_FILES := $(basename $(TESTLIBS))
TESTLIBS_OBJECTS := $(addprefix $(OBJDSTDIR)/,$(addsuffix .o, $(TESTLIBS_BASE_FILES)))

# Test Code Files
TEST_SOURCE_FILES ?= $(wildcard test/TEST_*.c test/TEST_*.cc test/TEST_*.cpp $(OSTYPE)/test/TEST_*.c $(OSTYPE)/test/TEST_*.cc $(OSTYPE)/test/TEST_*.cpp)
TEST_BASE_SOURCE_FILES := $(basename $(TEST_SOURCE_FILES))
TEST_OBJECT_SOURCE_FILES := $(addprefix $(OBJDSTDIR)/,$(addsuffix .o, $(TEST_BASE_SOURCE_FILES)))

# Create Source List
SRCLIST ?= $(filter-out $(TEST_SOURCE_FILES),$(wildcard *.cpp *.c *.cc $(OSTYPE)/*.cpp $(OSTYPE)/*.c $(OSTYPE)/*.cc))

ifeq ($(TARGET),X86)
	SRCLIST += $(filter-out $(TEST_SOURCE_FILES),$(wildcard Common/*.cpp Common/*.c Common/*.cc))
endif

# Create Header List
HEADLIST ?= $(wildcard *.hpp *.h *.hh $(OSTYPE)/*.hpp $(OSTYPE)/*.h $(OSTYPE)/*.hh)

# Create object list from source files
OBJLIST = $(addprefix $(OBJDSTDIR)/, $(subst .c,.o,$(subst .cc,.o,$(subst .cpp,.o, $(SRCLIST)))))

ALLLIBS = $(LINKLIBS) os/$(OSTYPE) 
ALLOBJLIST = $(shell find $(addprefix $(BASEDIR)dst/obj/, $(ALLLIBS)) -name '*.o')

#***************************
# Compiler Configurations
#***************************

.PHONY: test

.PHONY: clean

include $(BASEDIR)Framework/CommonTargets.mak

# Include Help Files
include $(BASEDIR)Framework/AppHelp.mak
include $(BASEDIR)Framework/DefaultHelp.mak
include $(BASEDIR)Framework/SourceHelp.mak
include $(BASEDIR)Framework/OsHelp.mak

# Include Env Files
include $(BASEDIR)Framework/AppEnv.mak
include $(BASEDIR)Framework/DefaultEnv.mak
include $(BASEDIR)Framework/SourceEnv.mak
include $(BASEDIR)Framework/OsEnv.mak

# Include General Rules
include $(BASEDIR)Framework/GeneralRules.mak


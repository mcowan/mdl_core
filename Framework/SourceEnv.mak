#*****************************************
# Common Default Help Menu 
#*****************************************


###################################
# Target - Print the Enviroment
source-env:
	@echo "======================================================================"
	@echo "Enviroment Variables                                                  "
	@echo "=====================                                                 "
	@echo "Package                                                               "
	@echo "========                                                              "
	@echo "PACKAGE       = $(PACKAGE)                                            "
	@echo "PACKAGETYPE   = $(PACKAGETYPE)                                        "
	@echo "                                                                      "
	@echo "Directories                                                           "
	@echo "============                                                          "
	@echo "BASEDIR       = $(BASEDIR)                                            "
	@echo "SUPERDIR      = $(SUPERDIR)                                           "
	@echo "WORKDIR       = $(WORKDIR)                                            "
	@echo "OBJDSTDIR     = $(OBJDSTDIR)                                          "
	@echo "LIBDSTDIR     = $(LIBDSTDIR)                                          "
	@echo "TESTOBJDSTDIR = $(TESTOBJDSTDIR)                                      "
	@echo "TESTDSTDIR    = $(TESTDSTDIR)                                         "
	@echo "                                                                      "
	@echo "Doxygen Variables                                                     "
	@echo "==================                                                    "
	@echo "DOXDSTDIR     = $(DOXDSTDIR)                                          "
	@echo "DOXYFILE      = $(DOXYFILE)                                           "
	@echo "                                                                      "
	@echo "Source Variables                                                      "
	@echo "=================                                                     "
	@echo "SRCLIST  = $(SRCLIST)                                                 "
	@echo "HEADLIST = $(HEADLIST)                                                "
	@echo "OBJLIST  = $(OBJLIST)                                                 "
	@echo "LINKLIBS = $(LINKLIBS)                                                "
	@echo "                                                                      "
	@echo "Test Variables                                                        "
	@echo "===============                                                       "
	@echo "TESTLIBS            = $(TESTLIBS)                                     "
	@echo "TESTLIBS_BASE_FILES = $(TESTLIBS_BASE_FILES)                          "
	@echo "TESTLIBS_OBJECTS    = $(TESTLIBS_OBJECTS)                             "
	@echo "TEST_SOURCE_FILES        = $(TEST_SOURCE_FILES)                       "
	@echo "TEST_BASE_SOURCE_FILES   = $(TEST_BASE_SOURCE_FILES)                  "
	@echo "TEST_OBJECT_SOURCE_FILES = $(TEST_OBJECT_SOURCE_FILES)                "
	@echo "                                                                      "
	@echo "Coverage Variables                                                    "
	@echo "===================                                                   "
	@echo "COVERDST      = $(COVERDST)                                           "
	@echo "COVERPKG      = $(COVERPKG)                                           "
	@echo "COVERTEST     = $(COVERTEST)                                          "
	@echo "                                                                      "
	@echo "Compiler Flags                                                        "
	@echo "===============                                                       "
	@echo "OS          = $(COMPILEROS)                                           "
	@echo "LIBEXT      = $(LIBEXT)                                               "
	@echo "CC          = $(CXX)                                                  "
	@echo "CXX         = $(CXX)                                                  "
	@echo "AR          = $(AR)                                                   "
	@echo "LD          = $(LD)                                                   "
	@echo "ASFLAGS     = $(ASFLAGS)                                              "
	@echo "CCFLAGS     = $(CCFLAGS)                                              "
	@echo "CXXFLAGS    = $(CXXFLAGS)                                             "
	@echo "ARFLAGS     = $(ARFLAGS)                                              "
	@echo "LDFLAGS     = $(LDFLAGS)                                              "
	@echo "STDLINKLIBS = $(STDLINKLIBS)                                          "
	@echo "======================================================================"
###################################	
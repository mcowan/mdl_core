

#include <math.h>
#include <math/LUT_Tan.hpp>
#include <common/Convert.hpp>
#include <stdio.h>
#include <common/Error.hpp>

using namespace ns_MDL;
using namespace ns_Math;

LUT_Tan::LUT_Tan(DegreeOfPrecision precision)
: LUT_TrigTable(precision)
{
  for (size_t index = 0; index < 90 * m_Multiplier; index++) {
    m_pTable[index] = tan(DEGREE_TO_RADIAN * ((double) index / (double) m_Multiplier));
  }
}

LUT_Tan::~LUT_Tan()
{
}

double
LUT_Tan::lookupByDegree(const double degree, ERROR* err) const
{
  double index = (wrapDegree(degree + m_Rounder));
  double toReturn = 0;

  if (index >= 0 && index < 90) {
    toReturn = m_pTable[(size_t) (index * m_Multiplier)];
    SetErr(err, ERR_NONE);
  }
  else if (index == 90) {
    toReturn = 0;
    SetErr(err, ERR_UNDEFINED);
  }
  else if (index > 90 && index <= 180) {
    toReturn = -m_pTable[(size_t) ((180.0 - index) * m_Multiplier)];
    SetErr(err, ERR_NONE);
  }
  else if (index > 180 && index < 270) {
    toReturn = (m_pTable[(size_t) ((index - 180.0) * m_Multiplier)]);
    SetErr(err, ERR_NONE);
  }
  else if (index == 270) {
    toReturn = 0;
    SetErr(err, ERR_UNDEFINED);
  }
  else {
    toReturn = -(m_pTable[(size_t) ((360.0 - index) * m_Multiplier)]);
    SetErr(err, ERR_NONE);
  }

  return toReturn;
}

double
LUT_Tan::lookupByRadian(const double radian, ERROR* err) const
{
  return lookupByDegree(radian*RADIAN_TO_DEGREE, err);
}


/*
 * math.h
 *
 *  Created on: May 18, 2014
 *      Author: simplymac
 */

#ifndef LUT_TRIGTABLE_HPP_
#define LUT_TRIGTABLE_HPP_

#include <Types.hpp>

/*
 * @brief General MDL Library Wrapper.
 */
namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    /// @brief Used to generate the values in the Table.  Zero
    /// DegreesOfPrecision means each degree will have a
    /// calculated value.  One Degree of Precision will correspond
    /// to each tenth of a degree having a calculated value.
    /// Values inbetween these will be extrapolated.

    enum DegreeOfPrecision
    {
      Zero = 0,
      One = 1,
      Two = 2,
      Three = 3,
      Four = 4
    };

    /// @brief A look up table is used to speed up complex numerical
    /// calculations.  Each Trigonemetric calculation requires floating
    /// point hardware to extrapolate the values.  If these are precalculated
    /// trig calculations become instantaneous with a lookup.  This of course
    /// comes at the exspense of memory utilization; hence, why various degrees
    /// of precision are speficied.  One a typical multicore machine, this may
    /// seem unnesscesary; however on an arduino module or other microcontroller
    /// which do not contain floating point hardware, these are exspensive
    /// calculations.

    class LUT_TrigTable
    {
    public:

      LUT_TrigTable(DegreeOfPrecision precision = Zero);

      ~LUT_TrigTable();

      size_t size() const;

    protected:
      double wrapDegree(const double value) const;

      double* m_pTable; ///> Table of Values.
      size_t m_Precision; ///> Number of Elements in the Table
      size_t m_Multiplier; ///> Multiplier value, determined by precision.
      size_t m_Rounder; ///> Rounder Value.
    };
  };
};


#endif 

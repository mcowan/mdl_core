/*
 * TEST_Array.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#include "gtest/gtest.h"
#include <math/LUT_Cos.hpp>
#include <common/Constants.hpp>
#include <common/Convert.hpp>
#include <math.h>

namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    // String Test Fixture

    class LUT_CosTest : public testing::Test
    {
    protected:
      // You should make the members protected s.t. they can be
      // accessed from sub-classes.

      // Called before each test is run.

      virtual void
      SetUp()
      {
        t_Zero = new LUT_Cos(Zero);
        t_One = new LUT_Cos(One);
        t_Two = new LUT_Cos(Two);
        t_Three = new LUT_Cos(Three);
        t_Four = new LUT_Cos(Four);
      }

      // Called after each test is run

      virtual void
      TearDown()
      {
        delete t_Zero;
        delete t_One;
        delete t_Two;
        delete t_Three;
        delete t_Four;
      }

      LUT_Cos* t_Zero;
      LUT_Cos* t_One;
      LUT_Cos* t_Two;
      LUT_Cos* t_Three;
      LUT_Cos* t_Four;
    };

    TEST_F(LUT_CosTest, Zero)
    {
      for (double index = -720; index < 720; index += 1.0) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Zero->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 1.0) {
        ASSERT_NEAR(cos(index), t_Zero->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.1) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Zero->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_CosTest, One)
    {
      for (double index = -720; index < 720; index += 0.1) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_One->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.1) {
        ASSERT_NEAR(cos(index), t_One->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.01) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_One->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_CosTest, Two)
    {
      for (double index = -720; index < 720; index += 0.01) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Two->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.01) {
        ASSERT_NEAR(cos(index), t_Two->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.001) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Two->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_CosTest, Three)
    {
      for (double index = -720; index < 720; index += 0.001) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Three->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.001) {
        ASSERT_NEAR(cos(index), t_Three->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.0001) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Three->lookupByDegree(index), 0.1);
      }

    }

    TEST_F(LUT_CosTest, Four)
    {
      for (double index = -720; index < 720; index += 0.0001) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Four->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.0001) {
        ASSERT_NEAR(cos(index), t_Four->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.00001) {
        ASSERT_NEAR(cos(index * DEGREE_TO_RADIAN), t_Four->lookupByDegree(index), 0.1);
      }
    }
  };
};




/*
 * TEST_Array.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#include "gtest/gtest.h"
#include <math/LUT_TrigTable.hpp>
#include <common/Constants.hpp>

namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    // @brief Test Class to verify Wrap Functions.

    class Test_Table : public LUT_TrigTable
    {
    public:

      Test_Table(DegreeOfPrecision precision)
      : LUT_TrigTable(precision)
      {
      }

      ~Test_Table()
      {
      }

      double
      wrapDegree(const double value) const
      {
        return LUT_TrigTable::wrapDegree(value);
      }
    };


    // String Test Fixture

    class LUT_TrigTableTest : public testing::Test
    {
    protected:
      // You should make the members protected s.t. they can be
      // accessed from sub-classes.

      // Called before each test is run.

      virtual void
      SetUp()
      {
        t = new Test_Table(Zero);
      }

      // Called after each test is run

      virtual void
      TearDown()
      {
        delete t;
      }


      Test_Table* t;
    };

    TEST_F(LUT_TrigTableTest, WrapDegrees)
    {
      ASSERT_EQ(5, t->wrapDegree(5));

      ASSERT_EQ(0, t->wrapDegree(360));

      ASSERT_EQ(5, t->wrapDegree(365));

      ASSERT_EQ(0.5, t->wrapDegree(720.5));

      ASSERT_EQ(359.5, t->wrapDegree(-720.5));

      ASSERT_EQ(359.5, t->wrapDegree(-0.5));
    }
  };
};
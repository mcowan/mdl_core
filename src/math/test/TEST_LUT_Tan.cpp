/*
 * TEST_LUT_Tan.cpp
 *
 * Testing Tangent values is incredibly challenging because of
 * the assymtotic nature of the graphs.  As you approach one of
 * the steep curves your level of error explodes exponentially.
 * therefore these regions are ignored during testing.  Yes,
 * there are values that are returned, but depending on your level
 * of precision, the error varies dramatically.
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#include "gtest/gtest.h"
#include <math/LUT_Tan.hpp>
#include <common/Constants.hpp>
#include <common/Convert.hpp>
#include <common/Error.hpp>
#include <math.h>

namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    static const double DegreeTolerance = 8; // //+/- 10 Degrees around assymtote
    static const double RadianTolerance = DegreeTolerance*DEGREE_TO_RADIAN; // 0.1745329251994329; // +/- 10 degrees in Radians around assymtote
    static const double RadianErrorTolerance = 1.0;
    static const double DegreeErrorTolerance = 1.0;
    // String Test Fixture

    class LUT_TanTest : public testing::Test
    {
    protected:
      // You should make the members protected s.t. they can be
      // accessed from sub-classes.

      // Called before each test is run.

      virtual void
      SetUp()
      {
        t_Zero = new LUT_Tan(Zero);
        t_One = new LUT_Tan(One);
        t_Two = new LUT_Tan(Two);
        t_Three = new LUT_Tan(Three);
        t_Four = new LUT_Tan(Four);
      }

      // Called after each test is run

      virtual void
      TearDown()
      {
        delete t_Zero;
        delete t_One;
        delete t_Two;
        delete t_Three;
        delete t_Four;
      }

      LUT_Tan* t_Zero;
      LUT_Tan* t_One;
      LUT_Tan* t_Two;
      LUT_Tan* t_Three;
      LUT_Tan* t_Four;
    };

    TEST_F(LUT_TanTest, Zero)
    {
      int err = ERR_NONE;
      for (double index = -720; index < 720; index += 0.1) {
        if (((fmod(index, 360.0) <= (90 + DegreeTolerance)) && (fmod(index, 360.0) >= (90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (270 + DegreeTolerance)) && (fmod(index, 360.0) >= (270 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-90 + DegreeTolerance)) && (fmod(index, 360.0) >= (-90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-270 + DegreeTolerance)) && (fmod(index, 360.0) >= (-270 - DegreeTolerance)))) {
          t_Zero->lookupByDegree(index, &err);
        }
        else {
          ASSERT_NEAR(tan(index * DEGREE_TO_RADIAN), t_Zero->lookupByDegree(index, &err), DegreeErrorTolerance);
        }
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.1) {
        if (((fmod(index, TWO_PI) <= ((90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((270 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-270 - DegreeTolerance) * DEGREE_TO_RADIAN))))) {
          t_Zero->lookupByRadian(index, &err);
          //ASSERT_EQ(err, ERR_UNDEFINED);
        }
        else {
          ASSERT_NEAR(tan(index), t_Zero->lookupByRadian(index), RadianErrorTolerance);
        }
      }
    }

    TEST_F(LUT_TanTest, One)
    {
      int err = ERR_NONE;

      for (double index = -720; index < 720; index += 0.01) {
        if (((fmod(index, 360.0) <= (90 + DegreeTolerance)) && (fmod(index, 360.0) >= (90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (270 + DegreeTolerance)) && (fmod(index, 360.0) >= (270 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-90 + DegreeTolerance)) && (fmod(index, 360.0) >= (-90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-270 + DegreeTolerance)) && (fmod(index, 360.0) >= (-270 - DegreeTolerance)))) {
          t_One->lookupByDegree(index, &err);
        }
        else {
          ASSERT_NEAR(tan(index * DEGREE_TO_RADIAN), t_One->lookupByDegree(index, &err), DegreeErrorTolerance);
        }
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.01) {
        if (((fmod(index, TWO_PI) <= ((90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((270 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-270 - DegreeTolerance) * DEGREE_TO_RADIAN))))) {
          t_One->lookupByRadian(index, &err);
          //ASSERT_EQ(err, ERR_UNDEFINED);
        }
        else {
          ASSERT_NEAR(tan(index), t_One->lookupByRadian(index), RadianErrorTolerance);
        }
      }
    }

    TEST_F(LUT_TanTest, Two)
    {
      int err = ERR_NONE;

      for (double index = -720; index < 720; index += 0.001) {
        if (((fmod(index, 360.0) <= (90 + DegreeTolerance)) && (fmod(index, 360.0) >= (90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (270 + DegreeTolerance)) && (fmod(index, 360.0) >= (270 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-90 + DegreeTolerance)) && (fmod(index, 360.0) >= (-90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-270 + DegreeTolerance)) && (fmod(index, 360.0) >= (-270 - DegreeTolerance)))) {
          t_Two->lookupByDegree(index, &err);
        }
        else {
          ASSERT_NEAR(tan(index * DEGREE_TO_RADIAN), t_Two->lookupByDegree(index, &err), DegreeErrorTolerance);
        }
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.001) {
        if (((fmod(index, TWO_PI) <= ((90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((270 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-270 - DegreeTolerance) * DEGREE_TO_RADIAN))))) {
          t_Two->lookupByRadian(index, &err);
          //ASSERT_EQ(err, ERR_UNDEFINED);
        }
        else {
          ASSERT_NEAR(tan(index), t_Two->lookupByRadian(index), RadianErrorTolerance);
        }
      }
    }

    TEST_F(LUT_TanTest, Three)
    {
      int err = ERR_NONE;
      for (double index = -720; index < 720; index += 0.0001) {
        if (((fmod(index, 360.0) <= (90 + DegreeTolerance)) && (fmod(index, 360.0) >= (90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (270 + DegreeTolerance)) && (fmod(index, 360.0) >= (270 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-90 + DegreeTolerance)) && (fmod(index, 360.0) >= (-90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-270 + DegreeTolerance)) && (fmod(index, 360.0) >= (-270 - DegreeTolerance)))) {
          t_Three->lookupByDegree(index, &err);
        }
        else {
          ASSERT_NEAR(tan(index * DEGREE_TO_RADIAN), t_Three->lookupByDegree(index, &err), DegreeErrorTolerance);
        }
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.0001) {
        if (((fmod(index, TWO_PI) <= ((90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((270 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-270 - DegreeTolerance) * DEGREE_TO_RADIAN))))) {
          t_Three->lookupByDegree(index, &err);
          //ASSERT_EQ(err, ERR_UNDEFINED);
        }
        else {
          ASSERT_NEAR(tan(index), t_Three->lookupByRadian(index), RadianErrorTolerance);
        }
      }
    }

    TEST_F(LUT_TanTest, Four)
    {
      int err = ERR_NONE;
      for (double index = -720; index < 720; index += 0.00001) {
        if (((fmod(index, 360.0) <= (90 + DegreeTolerance)) && (fmod(index, 360.0) >= (90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (270 + DegreeTolerance)) && (fmod(index, 360.0) >= (270 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-90 + DegreeTolerance)) && (fmod(index, 360.0) >= (-90 - DegreeTolerance))) ||
            ((fmod(index, 360.0) <= (-270 + DegreeTolerance)) && (fmod(index, 360.0) >= (-270 - DegreeTolerance)))) {
          t_Three->lookupByDegree(index, &err);
        }
        else {
          ASSERT_NEAR(tan(index * DEGREE_TO_RADIAN), t_Four->lookupByDegree(index, &err), DegreeErrorTolerance);
        }
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.00001) {
        if (((fmod(index, TWO_PI) <= ((90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((270 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-90 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-90 - DegreeTolerance) * DEGREE_TO_RADIAN)))) ||
            ((fmod(index, TWO_PI) <= ((-270 + DegreeTolerance) * DEGREE_TO_RADIAN) && (fmod(index, TWO_PI) >= ((-270 - DegreeTolerance) * DEGREE_TO_RADIAN))))) {
          t_Three->lookupByDegree(index, &err);
          //ASSERT_EQ(err, ERR_UNDEFINED);
        }
        else {
          ASSERT_NEAR(tan(index), t_Four->lookupByRadian(index), RadianErrorTolerance);
        }
      }
    }
  };
};




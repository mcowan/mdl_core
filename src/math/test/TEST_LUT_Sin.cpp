/*
 * TEST_Array.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#include "gtest/gtest.h"
#include <math/LUT_Sin.hpp>
#include <common/Constants.hpp>
#include <common/Convert.hpp>
#include <math.h>

namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    // String Test Fixture

    class LUT_SinTest : public testing::Test
    {
    protected:
      // You should make the members protected s.t. they can be
      // accessed from sub-classes.

      // Called before each test is run.

      virtual void
      SetUp()
      {
        t_Zero = new LUT_Sin(Zero);
        t_One = new LUT_Sin(One);
        t_Two = new LUT_Sin(Two);
        t_Three = new LUT_Sin(Three);
        t_Four = new LUT_Sin(Four);
      }

      // Called after each test is run

      virtual void
      TearDown()
      {
        delete t_Zero;
        delete t_One;
        delete t_Two;
        delete t_Three;
        delete t_Four;
      }

      LUT_Sin* t_Zero;
      LUT_Sin* t_One;
      LUT_Sin* t_Two;
      LUT_Sin* t_Three;
      LUT_Sin* t_Four;
    };

    TEST_F(LUT_SinTest, Zero)
    {
      for (double index = -720; index < 720; index += 1.0) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Zero->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 1.0) {
        ASSERT_NEAR(sin(index), t_Zero->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.1) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Zero->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_SinTest, One)
    {
      for (double index = -720; index < 720; index += 0.1) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_One->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.1) {
        ASSERT_NEAR(sin(index), t_One->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.01) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_One->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_SinTest, Two)
    {
      for (double index = -720; index < 720; index += 0.01) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Two->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.01) {
        ASSERT_NEAR(sin(index), t_Two->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.001) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Two->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_SinTest, Three)
    {
      for (double index = -720; index < 720; index += 0.001) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Three->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.001) {
        ASSERT_NEAR(sin(index), t_Three->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.0001) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Three->lookupByDegree(index), 0.1);
      }
    }

    TEST_F(LUT_SinTest, Four)
    {
      for (double index = -720; index < 720; index += 0.0001) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Four->lookupByDegree(index), 0.1);
      }

      for (double index = -4 * PI; index < 4 * PI; index += 0.0001) {
        ASSERT_NEAR(sin(index), t_Four->lookupByRadian(index), 0.1);
      }

      for (double index = -720; index < 720; index += 0.00001) {
        ASSERT_NEAR(sin(index * DEGREE_TO_RADIAN), t_Four->lookupByDegree(index), 0.1);
      }
    }
  };
};




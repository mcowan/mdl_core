

#include <math.h>
#include <math/LUT_TrigTable.hpp>
#include <stdio.h>
#include <common/Constants.hpp>

using namespace ns_MDL;
using namespace ns_Math;

LUT_TrigTable::LUT_TrigTable(DegreeOfPrecision precision)
{
  switch (precision) {
  case Zero: // 0 - 90
  {
    m_pTable = new double[90];
    m_Precision = Zero;
    m_Multiplier = 1;
    m_Rounder = 0.5;
    break;
  }
  case One: // 0.0 - 90.0
  {
    m_pTable = new double[900];
    m_Precision = One;
    m_Multiplier = 10;
    m_Rounder = 0.05;
    break;
  }
  case Two: // 0.00 - 90.00
  {
    m_pTable = new double[9000];
    m_Precision = Two;
    m_Multiplier = 100;
    m_Rounder = 0.005;
    break;
  }
  case Three: // 0.000 - 90.000
  {
    m_pTable = new double[90000];
    m_Precision = Three;
    m_Multiplier = 1000;
    m_Rounder = 0.0005;
    break;
  }
  case Four: // 0.0000 - 90.0000
  {
    m_pTable = new double[900000];
    m_Precision = Four;
    m_Multiplier = 10000;
    m_Rounder = 0.00005;
    break;
  }
  }
}

LUT_TrigTable::~LUT_TrigTable()
{
  delete[] m_pTable;
  m_Precision = 0;
  m_Multiplier = 0;
  m_Rounder = 0;
}

size_t
LUT_TrigTable::size() const
{
  return 90 * m_Multiplier;
}

double
LUT_TrigTable::wrapDegree(const double value) const
{
  // fmod = numer - tquot * denom 

  // Where tquot is the truncated (i.e., rounded towards zero) result of: numer/denom.

  double toReturn = (value - (((int) value / 360) * 360));


  return (toReturn < 0 ? toReturn + 360 : toReturn);
}














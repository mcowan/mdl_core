

#include <math.h>
#include <math/LUT_Sin.hpp>
#include <common/Convert.hpp>
#include <stdio.h>
#include <common/Error.hpp>

using namespace ns_MDL;
using namespace ns_Math;

LUT_Sin::LUT_Sin(DegreeOfPrecision precision)
: LUT_TrigTable(precision)
{
  for (size_t index = 0; index < 90 * m_Multiplier; index++) {
    m_pTable[index] = sin(DEGREE_TO_RADIAN * (index / m_Multiplier));
  }
}

LUT_Sin::~LUT_Sin()
{
}

double
LUT_Sin::lookupByDegree(const double degree, ERROR* err) const
{
  double index = (int) (wrapDegree(degree + m_Rounder));
  double toReturn = 0;
  SetErr(err, ERR_NONE);

  if (index >= 0 && index < 90) {
    toReturn = m_pTable[(size_t) (index * m_Multiplier)];
  }
  else if (index == 90) {
    toReturn = 1.0;
  }
  else if (index > 90 && index <= 180) {
    toReturn = m_pTable[(size_t) ((180.0 - index) * m_Multiplier)];
  }
  else if (index > 180 && index < 270) {
    toReturn = -(m_pTable[(size_t) ((index - 180.0) * m_Multiplier)]);
  }
  else if (index == 270) {
    toReturn = -1.0;
  }
  else {
    toReturn = -(m_pTable[(size_t) ((360.0 - index) * m_Multiplier)]);
  }

  return toReturn;
}

double
LUT_Sin::lookupByRadian(const double radian, ERROR* err) const
{
  return lookupByDegree(radian * RADIAN_TO_DEGREE, err);
}


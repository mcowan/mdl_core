/*
 * math.h
 *
 *  Created on: May 18, 2014
 *      Author: simplymac
 */

#ifndef MATH_HPP_
#define MATH_HPP_

#include <types.hpp>

/*
 * @brief General MDL Library Wrapper.
 */
namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {

  };
};


#endif /* MATH_HPP_ */

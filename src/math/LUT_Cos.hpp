/*
 * math.h
 *
 *  Created on: May 18, 2014
 *      Author: simplymac
 */

#ifndef LUT_COS_HPP_
#define LUT_COS_HPP_

#include <math/LUT_TrigTable.hpp>

/*
 * @brief General MDL Library Wrapper.
 */
namespace ns_MDL
{
  /*
   * @brief This namespace is wrapper for the Math functions.
   */
  namespace ns_Math
  {
    /// @brief A look up table is used to speed up complex numerical
    /// calculations.  Each Trigonemetric calculation requires floating
    /// point hardware to extrapolate the values.  If these are precalculated
    /// trig calculations become instantaneous with a lookup.  This of course
    /// comes at the exspense of memory utilization; hence, why various degrees
    /// of precision are speficied.  One a typical multicore machine, this may
    /// seem unnesscesary; however on an arduino module or other microcontroller
    /// which do not contain floating point hardware, these are exspensive
    /// calculations.

    class LUT_Cos : public LUT_TrigTable
    {
    public:
      LUT_Cos(DegreeOfPrecision precision = Zero);

      ~LUT_Cos();

      double lookupByDegree(const double degree, ERROR* err = 0) const;

      double lookupByRadian(const double radian, ERROR* err = 0) const;
    };
  };
};


#endif 

#include <gtest/gtest.h>

TEST(GTest, AssertTrue)
{
  ASSERT_TRUE(true);
}

TEST(GTest, AssertFalse)
{
  ASSERT_FALSE(false);
}

TEST(GTest, AssertEqual)
{
  ASSERT_EQ(0, 0);
}

TEST(GTest, AssertNotEqual)
{
  ASSERT_NE(0, 1);
}

TEST(GTest, AssertLessThan)
{
  ASSERT_LT(0, 1);
}

TEST(GTest, AssertLessThanEqual)
{
  ASSERT_LE(0, 0);
  ASSERT_LE(0, 1);
}

TEST(GTest, AssertGreaterThan)
{
  ASSERT_GT(1, 0);
}

TEST(GTest, AssertGreaterThanEqual)
{
  ASSERT_GE(0, 0);
  ASSERT_GE(1, 0);
}

TEST(GTest, AssertStringEqualCaseSensitive)
{
  ASSERT_STREQ("EqualString", "EqualString");
}

TEST(GTest, AssertStringNotEqualCaseSensitive)
{
  ASSERT_STRNE("EqualString", "NotEqualString");
}

TEST(GTest, AssertStringEqualCaseInsensitive)
{
  ASSERT_STRCASEEQ("EqualString", "EQUALSTRING");
}

TEST(GTest, AssertStringNotEqualCaseInsensitive)
{
  ASSERT_STRCASENE("EqualString", "NOTEQUALSTRING");
}

class GTestClassFixture : public ::testing::Test
{
protected:

  virtual void
  SetUp()
  {
  }

  virtual void
  TearDown()
  {
  }
};

TEST_F(GTestClassFixture, AssertTrue)
{
  ASSERT_TRUE(true);
}

TEST_F(GTestClassFixture, AssertFalse)
{
  ASSERT_FALSE(false);
}

TEST_F(GTestClassFixture, AssertEqual)
{
  ASSERT_EQ(0, 0);
}

TEST_F(GTestClassFixture, AssertNotEqual)
{
  ASSERT_NE(0, 1);
}

TEST_F(GTestClassFixture, AssertLessThan)
{
  ASSERT_LT(0, 1);
}

TEST_F(GTestClassFixture, AssertLessThanEqual)
{
  ASSERT_LE(0, 0);
  ASSERT_LE(0, 1);
}

TEST_F(GTestClassFixture, AssertGreaterThan)
{
  ASSERT_GT(1, 0);
}

TEST_F(GTestClassFixture, AssertGreaterThanEqual)
{
  ASSERT_GE(0, 0);
  ASSERT_GE(1, 0);
}

TEST_F(GTestClassFixture, AssertStringEqualCaseSensitive)
{
  ASSERT_STREQ("EqualString", "EqualString");
}

TEST_F(GTestClassFixture, AssertStringNotEqualCaseSensitive)
{
  ASSERT_STRNE("EqualString", "NotEqualString");
}

TEST_F(GTestClassFixture, AssertStringEqualCaseInsensitive)
{
  ASSERT_STRCASEEQ("EqualString", "EQUALSTRING");
}

TEST_F(GTestClassFixture, AssertStringNotEqualCaseInsensitive)
{
  ASSERT_STRCASENE("EqualString", "NOTEQUALSTRING");
}

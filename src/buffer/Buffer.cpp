/* 
 * File:   Buffer.cpp
 * Author: Orion
 * 
 * Created on September 25, 2015, 9:21 AM
 */

#include "Buffer.hpp"
using namespace ns_MDL;

Buffer::Buffer(uint8_t* buffer, uint32_t size)
: m_pBuffer(buffer)
, m_pSize(size)
{
}

Buffer::Buffer(const Buffer& orig)
: m_pBuffer(orig.Get())
, m_pSize(orig.Size())
{
}

Buffer::~Buffer()
{
  m_pBuffer = 0;
  m_pSize = 0;
}

uint8_t*
Buffer::Get(ERROR* error) const
{
  if (m_pBuffer == 0) {
    SetErr(error, ERR_INVALID);
    return 0;
  }
  SetErr(error, ERR_NONE);
  return m_pBuffer;
}

uint32_t
Buffer::Size(ERROR* error) const
{
  if (m_pBuffer == 0) {
    SetErr(error, ERR_INVALID);
    return 0;
  }
  SetErr(error, ERR_NONE);
  return m_pSize;
}

/* 
 * File:   Buffer.hpp
 * Author: Orion
 *
 * Created on September 25, 2015, 9:21 AM
 */

#ifndef __BUFFER_HPP__
#define	__BUFFER_HPP__

#include <common/Error.hpp>

namespace ns_MDL
{

  class Buffer
  {
  public:
    Buffer(uint8_t* buffer, uint32_t size);
    Buffer(const Buffer& orig);
    virtual ~Buffer();

    uint8_t* Get(ERROR* error = 0) const;
    uint32_t Size(ERROR* error = 0) const;

  private:
    uint8_t* m_pBuffer;
    uint32_t m_pSize;
  };
}
#endif	/* BUFFER_HPP */


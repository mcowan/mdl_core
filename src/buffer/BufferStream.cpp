/* 
 * File:   BufferStream.cpp
 * Author: Orion
 * 
 * Created on September 26, 2015, 12:19 PM
 */

#include "BufferStream.hpp"
using namespace ns_MDL;
#include <string.h>
#include <common/Endian.hpp>

#include <iostream>


BufferStream::BufferStream(uint32_t size)
: m_pBuffer(new Buffer(new uint8_t[size], size))
, m_internalBuffer(true)
, m_ReadIndex(0)
, m_WriteIndex(0)
{
  Clear();
}

BufferStream::BufferStream(uint8_t* buffer, uint32_t size)
: m_pBuffer(new Buffer(buffer, size))
, m_internalBuffer(false)
, m_ReadIndex(0)
, m_WriteIndex(0)
{
  Clear();
}

BufferStream::BufferStream(const BufferStream& orig)
: m_pBuffer(new Buffer(new uint8_t[orig.Size()], orig.Size()))
, m_internalBuffer(true)
, m_ReadIndex(0)
, m_WriteIndex(0)
{
  Clear();
  Deserialize(orig.Serialize(), orig.Size());
}

BufferStream::~BufferStream()
{
  Clear();
  if (m_internalBuffer) {
    delete[] m_pBuffer->Get();
  }

  delete m_pBuffer;
}

void BufferStream::Clear()
{
  memset(m_pBuffer->Get(), 0, m_pBuffer->Size());
  m_ReadIndex = 0;
  m_WriteIndex = 0;
}

uint32_t BufferStream::Size() const
{
  return m_pBuffer->Size();
}

uint32_t BufferStream::GetReadPos() const
{
  return m_ReadIndex;
}

void BufferStream::SetReadPos(uint32_t pos, ERROR* error)
{
  if(pos < Size())
  {
    m_ReadIndex = pos;
    SetErr(error, ERR_NONE);
  }
  else
  {
    SetErr(error, ERR_RANGE);
  }
}

void BufferStream::SetWritePos(uint32_t pos, ERROR* error)
{
  if(pos < Size())
  {
    m_WriteIndex = pos;
    SetErr(error, ERR_NONE);
  }
  else
  {
    SetErr(error, ERR_RANGE);
  }
}

uint32_t BufferStream::GetWritePos() const
{
  return m_WriteIndex;
}

uint8_t* BufferStream::Serialize(ERROR* error) const
{
  return m_pBuffer->Get(error);
}

void BufferStream::Deserialize(uint8_t* buffer, uint32_t size, ERROR* error)
{
  // Remove the buffer we already have.
  if (m_internalBuffer) 
  {
    delete[] m_pBuffer->Get(error);
  }

  delete m_pBuffer;
  m_ReadIndex = 0;
  m_WriteIndex = 0;
  
  // Instantiate the new buffer
  m_pBuffer = new Buffer(new uint8_t[size], size);
  memcpy(m_pBuffer->Get(), buffer, size);
}

uint8_t BufferStream::Read8(ERROR* error)
{
  uint8_t value = 0;
  
  if((int64_t)m_ReadIndex <= (int64_t)(Size()-sizeof(uint8_t)))
  {
    SetErr(error, Endian::Read_Array_NtoH8(m_pBuffer->Get(), m_ReadIndex, value));
    m_ReadIndex+=sizeof(uint8_t);
  }
  else
  {
    SetErr(error, ERR_READ);
  }
  
  return value;
}

uint16_t BufferStream::Read16(ERROR* error)
{
  uint16_t value = 0;
  
  if((int64_t)m_ReadIndex <= (int64_t)(Size()-sizeof(uint16_t)))
  {
    SetErr(error, Endian::Read_Array_NtoH16(m_pBuffer->Get(), m_ReadIndex, value));
    m_ReadIndex+=sizeof(uint16_t);
  }
  else
  {
    SetErr(error, ERR_READ);
  }
  
  return value;
}

uint32_t BufferStream::Read32(ERROR* error)
{
  uint32_t value = 0;
  
  if((int64_t)m_ReadIndex <= (int64_t)(Size()-sizeof(uint32_t)))
  {
    SetErr(error, Endian::Read_Array_NtoH32(m_pBuffer->Get(), m_ReadIndex, value));
    m_ReadIndex+=sizeof(uint32_t);
  }
  else
  {
    SetErr(error, ERR_READ);
  }
  
  return value;
}

uint64_t BufferStream::Read64(ERROR* error)
{
  uint64_t value = 0;
  
  if((int64_t)m_ReadIndex <= (int64_t)(Size()-sizeof(uint64_t)))
  {
    SetErr(error, Endian::Read_Array_NtoH64(m_pBuffer->Get(), m_ReadIndex, value));
    m_ReadIndex+=sizeof(uint64_t);
    SetErr(error, ERR_NONE);
  }
  else
  {
    SetErr(error, ERR_READ);
  }
  
  return value;
}

void BufferStream::Read(uint8_t* buf, uint32_t size, ERROR* error)
{
  if((int64_t)m_ReadIndex <= (int64_t)(Size()-size))
  {
    memcpy(buf, &(m_pBuffer->Get()[m_ReadIndex]), size);
    m_ReadIndex+=size;
    SetErr(error, ERR_NONE);
  }
  else
  {
    SetErr(error, ERR_READ);
  }
}

void BufferStream::Write8(uint8_t data, ERROR* error)
{ 
  if((int64_t)m_WriteIndex <= (int64_t)(m_pBuffer->Size()-sizeof(uint8_t)))
  {
    SetErr(error, Endian::Write_Array_HtoN8(m_pBuffer->Get(), m_WriteIndex, data));
    m_WriteIndex+=sizeof(uint8_t);
  }
  else
  {
    SetErr(error, ERR_WRITE);
  }
}

void BufferStream::Write16(uint16_t data, ERROR* error)
{
  if((int64_t)m_WriteIndex <= (int64_t)(m_pBuffer->Size()-sizeof(uint16_t)))
  {
    SetErr(error, Endian::Write_Array_HtoN16(m_pBuffer->Get(), m_WriteIndex, data));
    m_WriteIndex+=sizeof(uint16_t);
  }
  else
  {
    SetErr(error, ERR_WRITE);
  }
}

void BufferStream::Write32(uint32_t data, ERROR* error)
{
  if((int64_t)m_WriteIndex <= (int64_t)(m_pBuffer->Size()-sizeof(uint32_t)))
  {
    SetErr(error, Endian::Write_Array_HtoN32(m_pBuffer->Get(), m_WriteIndex, data));
    m_WriteIndex+=sizeof(uint32_t);
  }
  else
  {
    SetErr(error, ERR_WRITE);
  }
}

void BufferStream::Write64(uint64_t data, ERROR* error)
{
  if((int64_t)m_WriteIndex <= (int64_t)(m_pBuffer->Size()-sizeof(uint64_t)))
  {
    SetErr(error, Endian::Write_Array_HtoN64(m_pBuffer->Get(), m_WriteIndex, data));
    m_WriteIndex+=sizeof(uint64_t);
  }
  else
  {
    SetErr(error, ERR_WRITE);
  }
}

void BufferStream::Write(uint8_t* buffer, uint32_t size, ERROR* error)
{
  if((int64_t)m_WriteIndex <= (int64_t)(m_pBuffer->Size()-size))
  {
    memcpy(&(m_pBuffer->Get()[m_WriteIndex]), buffer, size);
    m_WriteIndex+=size;
  }
  else
  {
    SetErr(error, ERR_WRITE);
  }
}

/* 
 * File:   BufferStream.hpp
 * Author: Orion
 *
 * Created on September 26, 2015, 12:19 PM
 */

#ifndef BUFFERSTREAM_HPP
#define	BUFFERSTREAM_HPP

#include <buffer/Buffer.hpp>
#include <common/Error.hpp>
#include <Types.hpp>

namespace ns_MDL
{

  class BufferStream
  {
  public:
    BufferStream(uint32_t size);
    BufferStream(uint8_t* buffer, uint32_t size);
    BufferStream(const BufferStream& orig);
    virtual ~BufferStream();

    void Clear();
    uint32_t Size() const;
    uint32_t GetReadPos() const;
    void SetReadPos(uint32_t pos, ERROR* error = 0);
    uint32_t GetWritePos() const;
    void SetWritePos(uint32_t pos, ERROR* error = 0);
    
    
    
    uint8_t* Serialize(ERROR* error = 0) const;
    void Deserialize(uint8_t* buffer, uint32_t size, ERROR* error = 0);

    uint8_t Read8(ERROR* error = 0);
    uint16_t Read16(ERROR* error = 0);
    uint32_t Read32(ERROR* error = 0);
    uint64_t Read64(ERROR* error = 0);
    void Read(uint8_t* buf, uint32_t size, ERROR* error = 0);

    void Write8(uint8_t data, ERROR* error = 0);
    void Write16(uint16_t data, ERROR* error = 0);
    void Write32(uint32_t data, ERROR* error = 0);
    void Write64(uint64_t data, ERROR* error = 0);
    void Write(uint8_t* buffer, uint32_t size, ERROR* error = 0);

  private:
    Buffer* m_pBuffer;
    bool m_internalBuffer;
    int64_t m_ReadIndex;
    int64_t m_WriteIndex;
  };
}
#endif	/* BUFFERSTREAM_HPP */


/* 
 * File:   TEST_BufferStream.cpp
 * Author: Orion
 * 
 * Created on September 27, 2015, 9:00 AM
 */

#define MAX_BUFFER_SIZE 1024

#include "TEST_BufferStream.hpp"
using namespace ns_MDL;

TEST_BufferStream::TEST_BufferStream()
{
}

TEST_BufferStream::~TEST_BufferStream()
{
}

void
TEST_BufferStream::SetUp()
{
  m_pCharBuffer = new uint8_t[MAX_BUFFER_SIZE];
  m_pBuffer = new Buffer(m_pCharBuffer, MAX_BUFFER_SIZE);
  m_pUnitUnderTest1 = new BufferStream(MAX_BUFFER_SIZE);
  m_pUnitUnderTest2 = new BufferStream(m_pCharBuffer, MAX_BUFFER_SIZE);
}

void
TEST_BufferStream::TearDown()
{
  delete m_pUnitUnderTest2;
  delete m_pUnitUnderTest1;
  delete m_pBuffer;
  delete m_pCharBuffer;
}

TEST_F(TEST_BufferStream, ReadWrite8)
{
  int err = ERR_NONE;
  for(unsigned int i = 1; i < MAX_BUFFER_SIZE; i++)
  { 
    // Write Byte
    m_pUnitUnderTest1->Write8(i%256, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, m_pUnitUnderTest1->GetWritePos());
    
    // Read Byte -- Mod the return by a byte max
    ASSERT_EQ(i%256, m_pUnitUnderTest1->Read8(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, m_pUnitUnderTest1->GetReadPos());
    
    ASSERT_EQ(MAX_BUFFER_SIZE, m_pUnitUnderTest1->Size());
  }
}

TEST_F(TEST_BufferStream, ReadWrite16)
{
  int err = ERR_NONE;
  for(unsigned int i = 1; i < MAX_BUFFER_SIZE/sizeof(uint16_t); i++)
  {
    // Write Byte
    m_pUnitUnderTest1->Write16(i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint16_t), m_pUnitUnderTest1->GetWritePos());
    
    // Read Byte -- Mod the return by a byte max
    ASSERT_EQ(i, m_pUnitUnderTest1->Read16(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint16_t), m_pUnitUnderTest1->GetReadPos());
    
    ASSERT_EQ(MAX_BUFFER_SIZE, m_pUnitUnderTest1->Size());
  }
}

TEST_F(TEST_BufferStream, ReadWrite32)
{
  int err = ERR_NONE;
  for(unsigned int i = 1; i < MAX_BUFFER_SIZE/sizeof(uint32_t); i++)
  {
    // Write Byte
    m_pUnitUnderTest1->Write32(i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint32_t), m_pUnitUnderTest1->GetWritePos());
    
    // Read Byte -- Mod the return by a byte max
    ASSERT_EQ(i, m_pUnitUnderTest1->Read32(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint32_t), m_pUnitUnderTest1->GetReadPos());
    
    ASSERT_EQ(MAX_BUFFER_SIZE, m_pUnitUnderTest1->Size());
  }
}

TEST_F(TEST_BufferStream, ReadWrite64)
{
  int err = ERR_NONE;
  for(unsigned int i = 1; i < MAX_BUFFER_SIZE/sizeof(uint64_t); i++)
  {
    // Write Byte
    m_pUnitUnderTest1->Write64(i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint64_t), m_pUnitUnderTest1->GetWritePos());
    
    // Read Byte -- Mod the return by a byte max
    ASSERT_EQ(i, m_pUnitUnderTest1->Read64(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i*sizeof(uint64_t), m_pUnitUnderTest1->GetReadPos());
    
    ASSERT_EQ(MAX_BUFFER_SIZE, m_pUnitUnderTest1->Size());
  }
}


TEST_F(TEST_BufferStream, ReadWriteArray)
{
  int err = ERR_NONE;
  
  uint8_t buf[MAX_BUFFER_SIZE] = {0};
  uint8_t buf2[MAX_BUFFER_SIZE] = {0};
  
  
  for(unsigned int i = 0; i < MAX_BUFFER_SIZE; i++)
  {
    buf[i] = i%256;
  }
  
  m_pUnitUnderTest1->Write(buf, MAX_BUFFER_SIZE, &err);
  ASSERT_EQ(err, ERR_NONE);
  
  m_pUnitUnderTest1->Read(buf2, MAX_BUFFER_SIZE, &err);
  ASSERT_EQ(err, ERR_NONE);
  
  for(unsigned int i = 0; i < MAX_BUFFER_SIZE; i++)
  {
    ASSERT_EQ(buf2[i], i%256);
  }
  
  m_pUnitUnderTest2->Deserialize(m_pUnitUnderTest1->Serialize(), m_pUnitUnderTest1->Size(), &err);
  ASSERT_EQ(err, ERR_NONE);
  
  for(unsigned int i = 0; i < MAX_BUFFER_SIZE; i++)
  {
    ASSERT_EQ(m_pUnitUnderTest1->Serialize()[i], m_pUnitUnderTest2->Serialize()[i]);
  }
  
  ASSERT_EQ(m_pUnitUnderTest1->Size(), MAX_BUFFER_SIZE);
  ASSERT_EQ(m_pUnitUnderTest2->Size(), MAX_BUFFER_SIZE);
  
  m_pUnitUnderTest1->Clear();
  m_pUnitUnderTest2->Clear();

  ASSERT_EQ(m_pUnitUnderTest1->Size(), MAX_BUFFER_SIZE);
  ASSERT_EQ(m_pUnitUnderTest2->Size(), MAX_BUFFER_SIZE);
}


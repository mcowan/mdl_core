/* 
 * File:   TEST_BufferStream.hpp
 * Author: Orion
 *
 * Created on September 27, 2015, 9:00 AM
 */

#ifndef TEST_BUFFERSTREAM_HPP
#define	TEST_BUFFERSTREAM_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <buffer/BufferStream.hpp>

using namespace ns_MDL;

class TEST_BufferStream : public ::testing::Test
{
public:
  TEST_BufferStream();
  virtual ~TEST_BufferStream();

  void SetUp();

  void TearDown();

protected:
  uint8_t* m_pCharBuffer;
  Buffer* m_pBuffer;
  BufferStream* m_pUnitUnderTest1;
  BufferStream* m_pUnitUnderTest2;
};
#endif	/* TEST_BUFFERSTREAM_HPP */


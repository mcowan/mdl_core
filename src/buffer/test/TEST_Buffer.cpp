/* 
 * File:   TEST_Buffer.cpp
 * Author: Orion
 * 
 * Created on September 25, 2015, 9:44 AM
 */

#include "TEST_Buffer.hpp"
#include <common/Error.hpp>

#define MAX_BUFFER_SIZE 100

using ::testing::_;
using ::testing::Return;
using ::testing::NotNull;
using ::testing::AllOf;
using ::testing::Ge;
using ::testing::Le;
using ::testing::MatchesRegex;
using ::testing::StartsWith;

TEST_Buffer::TEST_Buffer()
{
}

TEST_Buffer::~TEST_Buffer()
{
}

void
TEST_Buffer::SetUp()
{
  m_pCharArray = new uint8_t[MAX_BUFFER_SIZE];
  m_pUnitUnderTest = new Buffer(m_pCharArray, MAX_BUFFER_SIZE);
}

void
TEST_Buffer::TearDown()
{
  delete m_pUnitUnderTest;
  delete m_pCharArray;
}

TEST_F(TEST_Buffer, Stack)
{
  ASSERT_THAT(m_pUnitUnderTest, ::testing::NotNull());
  ASSERT_THAT(m_pCharArray, ::testing::NotNull());
}

TEST_F(TEST_Buffer, Get)
{
  ERROR error = ERR_NONE;
  ASSERT_EQ(m_pCharArray, m_pUnitUnderTest->Get(&error));
  ASSERT_EQ(error, ERR_NONE);
}

TEST_F(TEST_Buffer, Size)
{
  ERROR error = ERR_NONE;
  ASSERT_EQ(m_pUnitUnderTest->Size(&error), MAX_BUFFER_SIZE);
  ASSERT_EQ(error, ERR_NONE);
}

TEST_F(TEST_Buffer, CopyConstructor)
{
  Buffer buf(*m_pUnitUnderTest);
  ASSERT_EQ(buf.Get(), m_pUnitUnderTest->Get());
  ASSERT_EQ(buf.Size(), m_pUnitUnderTest->Size());
}

TEST_F(TEST_Buffer, Invalid)
{
  Buffer buf(0, 10);
  ERROR error = ERR_NONE;
  ASSERT_EQ(0, buf.Get(&error));
  ASSERT_EQ(error, ERR_INVALID);
  ASSERT_EQ(buf.Size(&error), 0);
  ASSERT_EQ(error, ERR_INVALID);
}
/* 
 * File:   TEST_Buffer.hpp
 * Author: Orion
 *
 * Created on September 25, 2015, 9:44 AM
 */

#ifndef TEST_BUFFER_HPP
#define	TEST_BUFFER_HPP

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <buffer/Buffer.hpp>

using namespace ns_MDL;

class TEST_Buffer : public ::testing::Test
{
public:
  TEST_Buffer();
  virtual ~TEST_Buffer();

  void SetUp();

  void TearDown();

protected:
  uint8_t* m_pCharArray;
  Buffer* m_pUnitUnderTest;
};

#endif	/* TEST_BUFFER_HPP */


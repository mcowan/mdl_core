


#ifndef ARRAY_SOURCE_HPP_
#define ARRAY_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Container;

#include <Types.hpp>

template <typename T, size_t N>
Array<T, N>::Array()
: Container<T>()
, p_Data(0)
{
  p_Data = new T[N];
};

template <typename T, size_t N>
Array<T, N>::~Array()
{
  delete[] p_Data;
};

template <typename T, size_t N>
Array<T, N>::Array(const Array<T, N> &other)
: Container<T>()
, p_Data(0)
{
  p_Data = new T[N];

  // copy data
  for (size_t i = 0; i < N; i++) {
    p_Data[i] = other[i];
  }
};

template <typename T, size_t N>
size_t Array<T, N>::size() const
{
  return N;
};

template <typename T, size_t N>
T& Array<T, N>::operator[](const size_t index)
{
  if (index < N) {
    return p_Data[index];
  }
  else {
    return p_Data[0];
  }
}

template <typename T, size_t N>
T& Array<T, N>::operator[](const size_t index) const
{
  if (index < N) {
    return p_Data[index];
  }
  else {
    return p_Data[0];
  }
}

template <typename T, size_t N>
void Array<T, N>::swap(size_t index1, size_t index2, ERROR* err)
{
  SetErr(err, ERR_RANGE);
  if ((index1 < N) && (index2 < N) && (index1 != index2)) {
    T tempBuffer = p_Data[index2];
    p_Data[index2] = p_Data[index1];
    p_Data[index1] = tempBuffer;

    SetErr(err, ERR_NONE);
  }
}

template <typename T, size_t N>
void Array<T, N>::set(size_t pos, T elem, ERROR* err)
{
  SetErr(err, ERR_RANGE);
  if (pos < N) {
    p_Data[pos] = elem;
    SetErr(err, ERR_NONE);
  }
}

template <typename T, size_t N>
void Array<T, N>::get(size_t pos, T& elem, ERROR* err)
{
  SetErr(err, ERR_RANGE);
  if (pos < N) {
    elem = p_Data[pos];
    SetErr(err, ERR_NONE);
  }
}

#endif

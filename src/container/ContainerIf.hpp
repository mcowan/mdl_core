/*
 * @file container/ContainerIf.hpp
 *
 * @brief Defines the common interface for collections.
 *
 * @authors mjc
 *
 * @date 05-31-14
 *
 * @details
 * This file contains a set of common methods that all collections will
 * inherit and define.  If they are not defined they will return an
 * implementation error.
 */

#ifndef CONTAINERIF_HPP_
#define CONTAINERIF_HPP_

#include <common/Error.hpp>
#include <Types.hpp>


/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Containers classes.
  ///
  namespace ns_Container
  {
    /// @brief Base class of all container classes.  Forces required functionality
    /// for the common functions to be instantiated.
    /// @details
    /// Allows for common functionality between containers such as sorting and
    /// equality operations.
    /// @tparam T type of data in container.
    /// 

    template <typename T>
    class Container
    {
    public:
      /// @brief The base class constructor.
      ///

      Container()
      {
      };

      /// @brief The base class copy deconstructor.
      ///

      ~Container()
      {
      };

      /// @brief Returns the number of elements in the container.
      ///
      /// @returns Number of Elements.
      ///
      virtual size_t size() const = 0;

      /// @brief Swap to elements in the container.
      ///
      /// @param[in] index1	index of the first element.
      /// @param[in] index2	index of the second element.
      /// @param[out] err		an error code.
      ///
      ///
      virtual void swap(size_t index1, size_t index2, ERROR* err = 0) = 0;


      /// @brief Access Operator.
      ///
      virtual T& operator[](const size_t index) = 0;

      /// @brief Constant Access Operator.
      ///
      virtual T& operator[](const size_t index) const = 0;
    }; // End of Container

    /// @brief Check if Left container is equal to the Right container.
    ///
    /// @param[in] lhs	 	A container to compare.
    /// @param[in] rhs	 	A container to compare.
    ///
    /// @returns True if Left container is equal to the Right container;
    /// otherwise, returns false.
    ///

    template <typename T>
    inline bool operator==(const Container<T>& lhs, const Container<T>& rhs)
    {
      bool toReturn = false;
      if (lhs.size() == rhs.size()) {
        toReturn = true;
        for (size_t i = 0; i < lhs.size(); i++) {
          if (lhs[i] != rhs[i]) {
            toReturn = false;
            break;
          }
        }
      }

      return toReturn;
    };

    /// @brief Check if Left container is not equal to the Right container.
    ///
    /// @param[in] lhs	 	A container to compare.
    /// @param[in] rhs	 	A container to compare.
    ///
    /// @returns True if Left container is not equal to the Right container;
    /// otherwise, returns false.
    ///

    template <typename T>
    inline bool operator!=(const Container<T>& lhs, const Container<T>& rhs)
    {
      return !(lhs == rhs);
    };

  } // End of Container
} // End of MDL

#endif


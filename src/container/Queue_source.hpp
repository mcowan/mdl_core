/// @file container/Queue_source.hpp
///
/// @brief Defines the Queue source.
///
/// @authors mjc
///
/// @date 01-13-2015
///
/// @details An implementation of a templated Queue Container.  Based
///  on the basic container interface.

#ifndef STACK_SOURCE_HPP_
#define STACK_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Container;

template <typename T>
Queue<T>::Queue()
: Container<T>()
{
};

template <typename T>
Queue<T>::Queue(const Queue<T> &other)
: Container<T>()
{
  for (size_t i = 0; i < other.size(); i++) {
    m_Data.push_back(other[i]);
  }
};

template <typename T>
Queue<T>::~Queue()
{
};

template <typename T>
size_t Queue<T>::size() const
{
  return m_Data.size();
};

template <typename T>
bool Queue<T>::empty() const
{
  return m_Data.empty();
};

template <typename T>
T& Queue<T>::operator[](const size_t index)
{
  return m_Data[index];
};

template <typename T>
T& Queue<T>::operator[](const size_t index) const
{
  return m_Data[index];
};

template<typename T>
void Queue<T>::swap(size_t index1, size_t index2, ERROR* err)
{
  m_Data.swap(index1, index2, err);
}

template<typename T>
void Queue<T>::clear(ERROR* err)
{
  m_Data.clear(err);
}

template <typename T>
T Queue<T>::top()
{
  return m_Data[0];
}

template <typename T>
void Queue<T>::push(T elem, ERROR* err)
{
  m_Data.push_back(elem, err);
}

template <typename T>
void Queue<T>::pop(T& elem, ERROR* err)
{
  if (m_Data.size() > 0) {
    m_Data.pop_front(elem, err);
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

#endif
/// @file container/Stack.hpp
///
/// @brief Defines the Stack container.
///
/// @authors mjc
///
/// @date 01-13-14
///
/// @details An implementation of a templated Stack Container.  Based
/// on the basic container interface.


#ifndef Stack_HPP_
#define Stack_HPP_

#include <container/ContainerIf.hpp>
#include <container/Vector.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Containers classes.
  ///
  namespace ns_Container
  {
    /// @details
    /// This class is a basic stack class that functions as a FIFO
    /// container.
    /// @param T type of data in container.
    /// 

    template <typename T>
    class Stack : public Container<T>
    {
    public:
      /// @details Creates a Basic Empty Templated Stack.
      ///
      Stack();

      /// @details Templated Stack Copy Constructor.
      ///
      /// @param[in] other Container to copy.
      ///
      Stack(const Stack<T> &other);

      /// @details Templated Stack Decontructor.
      ///
      ~Stack();

      /// @details Number of Elements in the Stack.
      ///
      /// @return The current number of elements.
      ///
      size_t size() const;

      /// @details Checks if the container is empty.
      ///
      /// @return True, if empty; False, otherwise.
      ///
      bool empty() const;

      /// @details Access Operator.
      ///
      /// @param[in] index Index of object to access.
      ///
      /// @return Access value.
      ///
      T& operator[](const size_t index);

      /// @details Constant Access Operator.
      ///
      /// @param[in] index Index of object to access.
      ///
      /// @return Access value.
      ///
      T& operator[](const size_t index) const;

      ///
      /// @details Swaps two Elements in the Stack.
      /// 
      /// @param [in] index1 Element index one.
      /// @param [in] index2 Element index two.
      /// @param [out] err Optional error code.
      ///
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      /// @brief Removes all elements in the Stack.
      /// 
      /// @param [out] err Optional error code.
      ///
      void clear(ERROR* err = 0);

      /// @brief Returns a copy of the first element in the Stack.
      /// 
      /// @return An element.
      ///
      T top();

      /// @brief Appends an element to the front of the Stack.
      ///
      /// @param [in] elem Element to be appended to the Stack.
      /// @param [out] err Optional error code.
      ///
      void push(T elem, ERROR* err = 0);

      /// @brief Removes and returns the first element in the Stack.
      /// 
      /// @param [in] elem Returns the element being removed.
      /// @param [out] err Optional error code.
      ///
      void pop(T& elem, ERROR* err = 0);

    private:
      Vector<T> m_Data; ///> Underlying Vector class.

    }; // End of Stack
  } /* End of Container */
} /* End of MDL */

// Stack Implementation
#include <container/Stack_source.hpp>

#endif 

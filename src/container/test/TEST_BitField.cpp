
#include <gtest/gtest.h>
#include <container/BitField.hpp>
#include <common/Error.hpp>

using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Container;

class BitField_test : public ::testing::Test
{
protected:
};

TEST_F(BitField_test, Constructor_Default)
{
  // Basic Constructor
  BitField bf;
  ASSERT_EQ((int) bf.size(), 8);
  ASSERT_EQ((int) bf.capacity(), 8);
  ASSERT_TRUE(bf.is_zero());
}

TEST_F(BitField_test, Constructor_GivenSize)
{
  BitField bf(12);
  ASSERT_EQ((int) bf.size(), 12);
  ASSERT_EQ((int) bf.capacity(), 16);
  ASSERT_TRUE(bf.is_zero());
}

TEST_F(BitField_test, Constructor_GivenSizeFillValue)
{
  BitField bf(12, true);
  ASSERT_EQ((int) bf.size(), 12);
  ASSERT_EQ((int) bf.capacity(), 16);
  ASSERT_FALSE(bf.is_zero());

  BitField bf2(12, false);
  ASSERT_EQ((int) bf2.size(), 12);
  ASSERT_EQ((int) bf2.capacity(), 16);
  ASSERT_TRUE(bf2.is_zero());

  BitField bf3(0, false);
  ASSERT_EQ((int) bf3.size(), 0);
  ASSERT_EQ((int) bf3.capacity(), 8);
  ASSERT_TRUE(bf3.is_zero());
}

TEST_F(BitField_test, Constructor_Copy)
{
  BitField bf(12);
  ASSERT_EQ((int) bf.size(), 12);
  ASSERT_EQ((int) bf.capacity(), 16);
  ASSERT_TRUE(bf.is_zero());

  BitField bf_Copy(bf);
  ASSERT_EQ((int) bf_Copy.size(), 12);
  ASSERT_EQ((int) bf_Copy.capacity(), 16);
  ASSERT_TRUE(bf_Copy.is_zero());
}

TEST_F(BitField_test, Size)
{
  BitField bf;
  ASSERT_EQ((int) bf.size(), 8);

  BitField bf2(8);
  ASSERT_EQ((int) bf2.size(), 8);

  BitField bf3(0);
  ASSERT_EQ((int) bf3.size(), 0);

  BitField bf4(16);
  ASSERT_EQ((int) bf4.size(), 16);
}

TEST_F(BitField_test, Capacity)
{
  BitField bf;
  ASSERT_EQ((int) bf.capacity(), 8);

  BitField bf2(8);
  ASSERT_EQ((int) bf2.capacity(), 8);

  BitField bf3(0);
  ASSERT_EQ((int) bf3.capacity(), 8);

  BitField bf4(16);
  ASSERT_EQ((int) bf4.capacity(), 16);

  BitField bf5(12);
  ASSERT_EQ((int) bf5.capacity(), 16);
}

TEST_F(BitField_test, SetGet)
{
  BitField bf; // Default Bitfield
  BitField zero(0); // Zero Size Bitfield
  int iErr = ERR_NONE;


  for (unsigned int i = 0; i < bf.size(); i++) {
    bf.set(i, false);
    ASSERT_FALSE(bf.get(i));
    bf.set(i, true);
    ASSERT_TRUE(bf.get(i));
  }

  // Empty List
  zero.get(0, &iErr);
  ASSERT_EQ(iErr, ERR_EMPTY);
  zero.set(0, false, &iErr);
  ASSERT_EQ(iErr, ERR_EMPTY);

  // Range Error
  bf.get(42, &iErr);
  ASSERT_EQ(iErr, ERR_RANGE);
  bf.set(42, true, &iErr);
  ASSERT_EQ(iErr, ERR_RANGE);

}

TEST_F(BitField_test, fill)
{
  BitField bf;

  bf.fill(true);

  // Verify all true
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_TRUE(bf.get(i));
  }

  bf.fill(false);

  // Verify all true
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_FALSE(bf.get(i));
  }
}

TEST_F(BitField_test, AccessorOperator)
{
  BitField bf;

  bf.fill(true);

  // Verify all true
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_TRUE(bf[i]);
  }

  bf.fill(false);

  // Verify all true
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_FALSE(bf[i]);
  }
}

TEST_F(BitField_test, Clear)
{
  BitField bf;

  bf.fill(true);

  // Verify all true
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_TRUE(bf[i]);
  }

  bf.clear();

  // Verify all false
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_FALSE(bf[i]);
  }
}

TEST_F(BitField_test, swap)
{
  BitField bf;
  BitField zero(0);
  int iErr = 0;

  // Verify all false
  for (unsigned int i = 0; i < bf.size(); i++) {
    ASSERT_FALSE(bf[i]);
  }

  bf.set(4, true);
  // Verify set 4 true
  ASSERT_FALSE(bf.get(0));
  ASSERT_FALSE(bf.get(1));
  ASSERT_FALSE(bf.get(2));
  ASSERT_FALSE(bf.get(3));
  ASSERT_TRUE(bf.get(4));
  ASSERT_FALSE(bf.get(5));
  ASSERT_FALSE(bf.get(6));
  ASSERT_FALSE(bf.get(7));

  bf.set(0, true);
  // Verify set 0 true
  ASSERT_TRUE(bf.get(0));
  ASSERT_FALSE(bf.get(1));
  ASSERT_FALSE(bf.get(2));
  ASSERT_FALSE(bf.get(3));
  ASSERT_TRUE(bf.get(4));
  ASSERT_FALSE(bf.get(5));
  ASSERT_FALSE(bf.get(6));
  ASSERT_FALSE(bf.get(7));


  bf.swap(0, 7); // Swap first and last
  //Verify the values
  ASSERT_FALSE(bf.get(0));
  ASSERT_FALSE(bf.get(1));
  ASSERT_FALSE(bf.get(2));
  ASSERT_FALSE(bf.get(3));
  ASSERT_TRUE(bf.get(4));
  ASSERT_FALSE(bf.get(5));
  ASSERT_FALSE(bf.get(6));
  ASSERT_TRUE(bf.get(7));

  // Check Zero size Errors
  zero.swap(1, 2, &iErr);
  ASSERT_EQ(iErr, ERR_EMPTY);

  // Range Error
  bf.swap(0, 42, &iErr);
  ASSERT_EQ(iErr, ERR_RANGE);
}

TEST_F(BitField_test, EqualityOperator)
{
  BitField bf1;
  BitField bf2;
  BitField bf3;
  BitField bf4(12);

  bf3.fill(true);

  ASSERT_TRUE(bf1 == bf2);
  ASSERT_FALSE(bf1 == bf3);
  ASSERT_FALSE(bf1 == bf4);
}

TEST_F(BitField_test, InEqualityOperator)
{
  BitField bf1;
  BitField bf2;
  BitField bf3;
  BitField bf4(12);

  bf3.fill(true);

  ASSERT_TRUE(bf1 != bf3);
  ASSERT_FALSE(bf1 != bf2);
  ASSERT_TRUE(bf1 != bf4);
}




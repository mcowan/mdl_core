/*
 * TEST_List.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include "gtest/gtest.h"
#include <container/List.hpp>

using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Container;

struct testStruct_t
{
  int x;
  int y;

  bool operator==(const testStruct_t& rhs) const
  {
    return (x == rhs.x) && (y == rhs.y);
  }

  bool operator!=(const testStruct_t& rhs) const
  {
    return !((x == rhs.x) && (y == rhs.y));
  }
};

static int one = 1;
static int two = 2;
static int three = 3;
static int four = 4;
static int five = 5;
static int six = 6;

TEST(List, Constructor)
{
  // Standard Type
  List<double> L;
  ASSERT_EQ(0, (int) L.size());
  ASSERT_TRUE(L.empty());

  // Pointer
  List<int*> L2;
  ASSERT_EQ(0, (int) L2.size());
  ASSERT_TRUE(L2.empty());

  // Pointer
  List<testStruct_t> L3;
  ASSERT_EQ(0, (int) L3.size());
  ASSERT_TRUE(L3.empty());
}

TEST(List, InsertBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i]);
  }

  ASSERT_FALSE(L.empty());
}

TEST(List, InsertPointerType)
{
  List<int*> L;
  int err = ERR_NONE;

  L.insert(0, &one, &err);
  ASSERT_EQ(1, (int) L.size());

  L.insert(0, &two, &err);
  ASSERT_EQ(2, (int) L.size());

  L.insert(2, &three, &err);
  ASSERT_EQ(3, (int) L.size());

  L.insert(1, &four, &err);
  ASSERT_EQ(4, (int) L.size());

  L.insert(2, &five, &err);
  ASSERT_EQ(5, (int) L.size());

  L.insert(4, &six, &err);
  ASSERT_EQ(6, (int) L.size());

  // Verify Not empty
  ASSERT_FALSE(L.empty());

  for (int i = 0; i < 100; i++) {
    L.insert(1, &one, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(one, *L[1]);
  }
}

TEST(List, InsertStructType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.insert(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i].x);
    ASSERT_EQ(i, L[i].y);
  }
}

TEST(List, CopyConstructorPointerType)
{
  List<int*> L;

  int err = ERR_NONE;

  L.insert(0, &one, &err);
  L.insert(0, &two, &err);
  L.insert(2, &three, &err);
  L.insert(1, &four, &err);
  L.insert(2, &five, &err);
  L.insert(4, &six, &err);

  List<int*> LCopy(L);

  // Verify Copy
  ASSERT_EQ(L.size(), LCopy.size());
  for (size_t i = 0; i < L.size(); i++) {
    ASSERT_EQ(*L[i], *LCopy[i]);
  }
}

TEST(List, CopyConstructorBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial list
  for (int i = 0; i < 1000; i++) {
    L.insert(i, i, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  List<int> LCopy(L);

  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[i]);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(List, CopyConstructorStructType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial structure
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.insert(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  List<testStruct_t> LCopy(L);

  // Verify
  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[i].x);
    ASSERT_EQ(i, LCopy[i].y);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(List, EraseBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i]);
  }

  for (int i = 1000; i > 0; i--) {
    L.erase(i - 1);
  }

  ASSERT_TRUE(L.empty());
}

TEST(List, EraseStructType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.insert(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i].x);
    ASSERT_EQ(i, L[i].y);
  }

  for (int i = 1000; i > 0; i--) {
    L.erase(i - 1);
  }

  ASSERT_TRUE(L.empty());
}

TEST(List, ErasePointerType)
{
  int err = ERR_NONE;
  List<int*> L;

  // Erase an empty List
  L.erase(0, &err);
  ASSERT_EQ(err, ERR_EMPTY);

  L.insert(0, &one, &err);
  L.insert(0, &two, &err);
  L.insert(2, &three, &err);
  L.insert(1, &four, &err);
  L.insert(2, &five, &err);
  L.insert(4, &six, &err);

  // Erase First Element
  L.erase(0, &err);
  ASSERT_EQ((int) L.size(), 5);

  // Erase Second Element
  L.erase(1, &err);
  ASSERT_EQ((int) L.size(), 4);

  // Erase Second to Last element
  L.erase(2, &err);
  ASSERT_EQ((int) L.size(), 3);

  // Erase Last element
  L.erase(2, &err);
  ASSERT_EQ((int) L.size(), 2);

  // Clear List
  L.clear();
  ASSERT_EQ((int) L.size(), 0);
}

TEST(List, EraseSectionPointerType)
{
  int err = ERR_NONE;
  List<int*> L;

  // Erase an empty List
  L.erase(0, &err);
  ASSERT_EQ(err, ERR_EMPTY);

  L.insert(0, &one, &err);
  L.insert(1, &two, &err);
  L.insert(2, &three, &err);
  L.insert(3, &four, &err);
  L.insert(4, &five, &err);
  L.insert(5, &six, &err);

  // Erase element indexes 0, 1, 2, 3 
  L.erase(0, 3, &err);
  ASSERT_EQ((int) L.size(), 2);


  ASSERT_EQ(*L[0], five);
  ASSERT_EQ(*L[1], six);

  // Erase begin/end backwards invalid
  L.erase(1, 0, &err);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(err, ERR_INVALID);

  // Erase past end of array
  L.erase(0, 42, &err);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, EraseSectionBasicType)
{
  int err = ERR_NONE;
  List<int> L;

  // Erase an empty List
  L.erase(0, &err);
  ASSERT_EQ(err, ERR_EMPTY);

  // Reset the Error
  err = ERR_NONE;

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i]);
  }

  L.erase(0, 999, &err);

  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(L.empty());
}

TEST(List, EraseSectionStructType)
{
  int err = ERR_NONE;
  List<testStruct_t> L;
  testStruct_t structure;

  // Erase an empty List
  L.erase(0, &err);
  ASSERT_EQ(err, ERR_EMPTY);

  // Reset the Error
  err = ERR_NONE;

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.insert(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i].x);
    ASSERT_EQ(i, L[i].y);
  }

  L.erase(0, 999, &err);

  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(L.empty());
}

TEST(List, SubscriptOperatorBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i]);
  }
}

TEST(List, SubscriptOperatorStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.insert(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i].x);
    ASSERT_EQ(i, L[i].y);
  }

  // invalid access
  L[1000];
}

TEST(List, SubscriptOperatorPointerType)
{
  List<int*> L;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  ASSERT_EQ(*L[0], one);
  ASSERT_EQ(*L[1], two);
  ASSERT_EQ(*L[2], three);
  ASSERT_EQ(*L[3], four);
  ASSERT_EQ(*L[4], five);
  ASSERT_EQ(*L[5], six);
}

TEST(List, EqualityBasicType)
{
  List<int> L;
  List<int> LEqual;
  List<int> LUnequal;
  List<int> LShort;

  // Standard
  for (int i = 0; i < 1000; i++) {
    L.insert(i, i);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    LEqual.insert(i, i);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    LUnequal.insert(i, 1000 - i);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    LUnequal.insert(i, i);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(List, EqualityStructureType)
{
  List<testStruct_t> L;
  List<testStruct_t> LEqual;
  List<testStruct_t> LUnequal;
  List<testStruct_t> LShort;
  testStruct_t structure;

  // Standard
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.insert(i, structure);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    LEqual.insert(i, structure);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LUnequal.insert(i, structure);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LShort.insert(i, structure);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(List, EqualityPointerType)
{
  List<int*> L;
  List<int*> LEqual;
  List<int*> LUnequal;
  List<int*> LShort;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  // 1 2 3 4 5 6
  LEqual.insert(0, &one);
  LEqual.insert(1, &two);
  LEqual.insert(2, &three);
  LEqual.insert(3, &four);
  LEqual.insert(4, &five);
  LEqual.insert(5, &six);

  // 6 5 4 3 2 1
  LUnequal.insert(0, &six);
  LUnequal.insert(1, &five);
  LUnequal.insert(2, &four);
  LUnequal.insert(3, &three);
  LUnequal.insert(4, &two);
  LUnequal.insert(5, &one);

  // 4 6 3
  LShort.insert(0, &four);
  LShort.insert(1, &six);
  LShort.insert(2, &three);


  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(List, SetPointerType)
{
  List<int*> L;
  int err = ERR_NONE;
  int testValue = 42;
  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  for (size_t i = 0; i < L.size(); i++) {
    L.set(i, &testValue, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  ASSERT_EQ(*L[0], testValue);
  ASSERT_EQ(*L[1], testValue);
  ASSERT_EQ(*L[2], testValue);
  ASSERT_EQ(*L[3], testValue);
  ASSERT_EQ(*L[4], testValue);
  ASSERT_EQ(*L[5], testValue);

  //// Range Error
  L.set(42, &one, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, SetBasicType)
{
  List<int> L;
  int err = ERR_NONE;
  int testValue = 42;

  //// Range Error
  L.set(42, one, &err);
  ASSERT_EQ(err, ERR_RANGE);

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i);
  }

  for (size_t i = 0; i < L.size(); i++) {
    L.set(i, testValue, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(L[i], testValue);
  }
}

TEST(List, SetStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;
  int testValue = 42;

  //// Range Error
  L.set(42, structure, &err);
  ASSERT_EQ(err, ERR_RANGE);

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.insert(i, structure);
  }

  for (size_t i = 0; i < L.size(); i++) {
    structure.x = testValue;
    structure.y = testValue;
    L.set(i, structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(L[i].x, testValue);
    ASSERT_EQ(L[i].y, testValue);
  }
}

TEST(List, GetBasicType)
{
  List<int> L;
  int err = ERR_NONE;
  int element = 0;

  // Construct list
  for (int i = 0; i < 1000; i++) {
    L.insert(i, i);
  }

  for (size_t i = 0; i < L.size(); i++) {
    L.get(i, element, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(element, (int) i);
  }
}

TEST(List, GetStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t element;

  // Construct list
  for (int i = 0; i < 1000; i++) {
    element.x = i;
    element.y = i;
    L.insert(i, element);
  }

  for (size_t i = 0; i < L.size(); i++) {
    L.get(i, element, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(element.x, (int) i);
    ASSERT_EQ(element.y, (int) i);
  }
}

TEST(List, GetPointerType)
{
  List<int*> L;
  int err = ERR_NONE;
  int* Element = 0;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  // one
  L.get(0, Element, &err);
  ASSERT_EQ(*Element, one);
  ASSERT_EQ(err, ERR_NONE);

  // two    
  L.get(1, Element, &err);
  ASSERT_EQ(*Element, two);
  ASSERT_EQ(err, ERR_NONE);

  // three
  L.get(2, Element, &err);
  ASSERT_EQ(*Element, three);
  ASSERT_EQ(err, ERR_NONE);

  // four
  L.get(3, Element, &err);
  ASSERT_EQ(*Element, four);
  ASSERT_EQ(err, ERR_NONE);

  // five
  L.get(4, Element, &err);
  ASSERT_EQ(*Element, five);
  ASSERT_EQ(err, ERR_NONE);

  // six
  L.get(5, Element, &err);
  ASSERT_EQ(*Element, six);
  ASSERT_EQ(err, ERR_NONE);


  // Range Error
  L.get(42, Element, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, SwapPointerType)
{
  List<int*> L;
  int err = ERR_NONE;

  L.insert(0, &one);
  L.insert(1, &two);

  L.swap(0, 1, &err);

  ASSERT_EQ(*L[0], two);
  ASSERT_EQ(*L[1], one);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, SwapBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  // 1 2 3 4 5 6
  L.insert(0, 1);
  L.insert(1, 2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[0], 2);
  ASSERT_EQ(L[1], 1);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, SwapStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure1;
  structure1.x = 1;
  structure1.y = 1;

  testStruct_t structure2;
  structure2.x = 2;
  structure2.y = 2;

  // 1 2 3 4 5 6
  L.insert(0, structure1);
  L.insert(1, structure2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[0].x, 2);
  ASSERT_EQ(L[0].y, 2);
  ASSERT_EQ(L[1].x, 1);
  ASSERT_EQ(L[1].y, 1);

  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(List, PushFrontPointerType)
{
  List<int*> L;
  int err = ERR_NONE;

  L.push_front(&six, &err);
  ASSERT_EQ((int) L.size(), 1);
  ASSERT_EQ(*L[0], six);
  ASSERT_EQ(err, ERR_NONE);

  L.push_front(&five, &err);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(*L[0], five);
  ASSERT_EQ(err, ERR_NONE);

  L.push_front(&four, &err);
  ASSERT_EQ((int) L.size(), 3);
  ASSERT_EQ(*L[0], four);
  ASSERT_EQ(err, ERR_NONE);

  L.push_front(&three, &err);
  ASSERT_EQ((int) L.size(), 4);
  ASSERT_EQ(*L[0], three);
  ASSERT_EQ(err, ERR_NONE);

  L.push_front(&two, &err);
  ASSERT_EQ((int) L.size(), 5);
  ASSERT_EQ(*L[0], two);
  ASSERT_EQ(err, ERR_NONE);

  L.push_front(&one, &err);
  ASSERT_EQ((int) L.size(), 6);
  ASSERT_EQ(*L[0], one);
  ASSERT_EQ(err, ERR_NONE);
}

TEST(List, PushFrontBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  for (int i = 0; i < 1000; i++) {
    L.push_front(i, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[0], i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PushFrontStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t element;

  for (int i = 0; i < 1000; i++) {
    element.x = i;
    element.y = i;
    L.push_front(element, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[0].x, i);
    ASSERT_EQ(L[0].y, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PopFrontPointerType)
{
  List<int*> L;
  int err = ERR_NONE;
  int* Element = 0;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  /// Pop front and check next value and size

  // pop 0
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 5);
  ASSERT_EQ(*Element, one);
  ASSERT_EQ(*L[0], two);
  ASSERT_EQ(err, ERR_NONE);

  // pop 1
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 4);
  ASSERT_EQ(*Element, two);
  ASSERT_EQ(*L[0], three);
  ASSERT_EQ(err, ERR_NONE);

  // pop 2
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 3);
  ASSERT_EQ(*Element, three);
  ASSERT_EQ(*L[0], four);
  ASSERT_EQ(err, ERR_NONE);

  // pop 3
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(*Element, four);
  ASSERT_EQ(*L[0], five);
  ASSERT_EQ(err, ERR_NONE);

  // pop 5
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 1);
  ASSERT_EQ(*Element, five);
  ASSERT_EQ(*L[0], six);
  ASSERT_EQ(err, ERR_NONE);

  // pop 6
  L.pop_front(Element, &err);
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_EQ(*Element, six);
  ASSERT_EQ(err, ERR_NONE);

  // pop 7
  L.pop_front(Element, &err);
  ASSERT_EQ(err, ERR_EMPTY);


  //// Pop the List without recovering element, or error
  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);
}

TEST(List, PopFrontBasicType)
{
  List<int> L;
  int err = ERR_NONE;
  int element = 0;

  for (int i = 0; i < 1000; i++) {
    L.push_front(i, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[0], i);
    ASSERT_EQ(err, ERR_NONE);
  }

  for (int i = 999; i >= 0; i--) {
    L.pop_front(element, &err);
    ASSERT_EQ(element, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PopFrontStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t element;

  for (int i = 0; i < 1000; i++) {
    element.x = i;
    element.y = i;
    L.push_front(element, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(err, ERR_NONE);
  }

  for (int i = 999; i >= 0; i--) {
    L.pop_front(element, &err);
    ASSERT_EQ(element.x, i);
    ASSERT_EQ(element.y, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PushBackBasicType)
{
  List<int> L;
  int err = ERR_NONE;

  for (int i = 0; i < 1000; i++) {
    L.push_back(i, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[i], i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PushBackStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t element;

  for (int i = 0; i < 1000; i++) {
    element.x = i;
    element.y = i;
    L.push_back(element, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[i].x, i);
    ASSERT_EQ(L[i].y, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PushBackPointerType)
{
  List<int*> L;
  int err = ERR_NONE;

  L.push_back(&one, &err);
  ASSERT_EQ((int) L.size(), 1);
  ASSERT_EQ(*L[(int) L.size() - 1], one);
  ASSERT_EQ(err, ERR_NONE);

  L.push_back(&two, &err);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(*L[(int) L.size() - 1], two);
  ASSERT_EQ(err, ERR_NONE);

  L.push_back(&three, &err);
  ASSERT_EQ((int) L.size(), 3);
  ASSERT_EQ(*L[(int) L.size() - 1], three);
  ASSERT_EQ(err, ERR_NONE);

  L.push_back(&four, &err);
  ASSERT_EQ((int) L.size(), 4);
  ASSERT_EQ(*L[(int) L.size() - 1], four);
  ASSERT_EQ(err, ERR_NONE);

  L.push_back(&five, &err);
  ASSERT_EQ((int) L.size(), 5);
  ASSERT_EQ(*L[(int) L.size() - 1], five);
  ASSERT_EQ(err, ERR_NONE);

  L.push_back(&six, &err);
  ASSERT_EQ((int) L.size(), 6);
  ASSERT_EQ(*L[(int) L.size() - 1], six);
  ASSERT_EQ(err, ERR_NONE);
}

TEST(List, PopBackBasicType)
{
  List<int> L;
  int err = ERR_NONE;
  int element = 0;

  for (int i = 0; i < 1000; i++) {
    L.push_back(i, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(L[i], i);
    ASSERT_EQ(err, ERR_NONE);
  }

  for (int i = 999; i >= 0; i--) {
    L.pop_back(element, &err);
    ASSERT_EQ(element, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PopBackStructureType)
{
  List<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t element;

  for (int i = 0; i < 1000; i++) {
    element.x = i;
    element.y = i;
    L.push_back(element, &err);
    ASSERT_EQ((int) L.size() - 1, i);
    ASSERT_EQ(err, ERR_NONE);
  }

  for (int i = 999; i >= 0; i--) {
    L.pop_back(element, &err);
    ASSERT_EQ(element.x, i);
    ASSERT_EQ(element.y, i);
    ASSERT_EQ(err, ERR_NONE);
  }
}

TEST(List, PopBackPointerType)
{
  List<int*> L;
  int err = ERR_NONE;
  int* Element = 0;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  L.insert(1, &two);
  L.insert(2, &three);
  L.insert(3, &four);
  L.insert(4, &five);
  L.insert(5, &six);

  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, six);
  ASSERT_EQ((int) L.size(), 5);
  ASSERT_EQ(*L[(int) L.size() - 1], five);
  ASSERT_EQ(err, ERR_NONE);


  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, five);
  ASSERT_EQ((int) L.size(), 4);
  ASSERT_EQ(*L[(int) L.size() - 1], four);
  ASSERT_EQ(err, ERR_NONE);

  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, four);
  ASSERT_EQ((int) L.size(), 3);
  ASSERT_EQ(*L[(int) L.size() - 1], three);
  ASSERT_EQ(err, ERR_NONE);

  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, three);
  ASSERT_EQ((int) L.size(), 2);
  ASSERT_EQ(*L[(int) L.size() - 1], two);
  ASSERT_EQ(err, ERR_NONE);

  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, two);
  ASSERT_EQ((int) L.size(), 1);
  ASSERT_EQ(*L[(int) L.size() - 1], one);
  ASSERT_EQ(err, ERR_NONE);

  // Pop the back items
  L.pop_back(Element, &err);
  ASSERT_EQ(*Element, one);
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_EQ(err, ERR_NONE);
}

TEST(List, FrontBasicType)
{
  List<int> L;

  for (int i = 0; i < 1000; i++) {
    L.insert(0, i);
    //ASSERT_EQ(L.front(), i);
  }
}

TEST(List, FrontStructureType)
{
  List<testStruct_t> L;
  testStruct_t structure;

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.insert(0, structure);
    ASSERT_EQ(L.front().x, i);
    ASSERT_EQ(L.front().y, i);
  }
}

TEST(List, BackBasicType)
{
  List<int> L;

  for (int i = 0; i < 1000; i++) {
    L.insert(i, i);
    ASSERT_EQ(L.back(), i);
  }
}

TEST(List, BackStructureType)
{
  List<testStruct_t> L;
  testStruct_t structure;

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.insert(i, structure);
    ASSERT_EQ(L.back().x, i);
    ASSERT_EQ(L.back().y, i);
  }
}

TEST(List, FrontPointerType)
{
  List<int*> L;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 1);
  L.insert(1, &two);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 2);
  L.insert(2, &three);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 3);
  L.insert(3, &four);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 4);
  L.insert(4, &five);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 5);
  L.insert(5, &six);
  ASSERT_EQ(L.front(), &one);
  ASSERT_EQ((int) L.size(), 6);
}

TEST(List, Back)
{
  List<int*> L;

  // 1 2 3 4 5 6
  L.insert(0, &one);
  ASSERT_EQ(L.back(), &one);
  ASSERT_EQ((int) L.size(), 1);
  L.insert(1, &two);
  ASSERT_EQ(L.back(), &two);
  ASSERT_EQ((int) L.size(), 2);
  L.insert(2, &three);
  ASSERT_EQ(L.back(), &three);
  ASSERT_EQ((int) L.size(), 3);
  L.insert(3, &four);
  ASSERT_EQ(L.back(), &four);
  ASSERT_EQ((int) L.size(), 4);
  L.insert(4, &five);
  ASSERT_EQ(L.back(), &five);
  ASSERT_EQ((int) L.size(), 5);
  L.insert(5, &six);
  ASSERT_EQ(L.back(), &six);
  ASSERT_EQ((int) L.size(), 6);
}



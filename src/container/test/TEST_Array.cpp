/*
 * TEST_Array.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include "gtest/gtest.h"
#include <container/Array.hpp>
#include <stdio.h>


using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Container;

static const int ARRAY_SIZE = 6;

struct structure
{
  int x;
  int y;

  bool operator==(const structure& rhs) const
  {
    return (x == rhs.x) && (y == rhs.y);
  }

  bool operator!=(const structure& rhs) const
  {
    return !((x == rhs.x) && (y == rhs.y));
  }
};

static int one = 1;
static int two = 2;
static int three = 3;
static int four = 4;
static int five = 5;
static int six = 6;

TEST(Array, Constructor)
{
  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;

  // Pointer Array
  Array<int*, ARRAY_SIZE> PointerArray;

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
}

TEST(Array, CopyConstructor)
{
  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;
  ASSERT_EQ(ARRAY_SIZE, (int) IntArray.size());

  // Assignment
  for (int i = 0; i < ARRAY_SIZE; i++) {
    IntArray[i] = i;
    ASSERT_EQ(IntArray[i], i);
  }

  // Copy Constructor
  Array<int, ARRAY_SIZE> CopyIntArray(IntArray);
  for (int i = 0; i < ARRAY_SIZE; i++) {
    ASSERT_EQ(CopyIntArray[i], i);
  }

  // Pointer Array
  Array<int*, ARRAY_SIZE> PointerArray;
  ASSERT_EQ(ARRAY_SIZE, (int) PointerArray.size());

  PointerArray[0] = &one;
  PointerArray[1] = &two;
  PointerArray[2] = &three;
  PointerArray[3] = &four;
  PointerArray[4] = &five;
  PointerArray[5] = &six;

  // Copy Constructor
  Array<int*, ARRAY_SIZE> CopyPointerArray(PointerArray);

  ASSERT_EQ(*CopyPointerArray[0], one);
  ASSERT_EQ(*CopyPointerArray[1], two);
  ASSERT_EQ(*CopyPointerArray[2], three);
  ASSERT_EQ(*CopyPointerArray[3], four);
  ASSERT_EQ(*CopyPointerArray[4], five);
  ASSERT_EQ(*CopyPointerArray[5], six);

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
  ASSERT_EQ(ARRAY_SIZE, (int) StructArray.size());

  // Assignment
  for (int i = 0; i < ARRAY_SIZE; i++) {
    structure s;
    s.x = i;
    s.y = i;

    StructArray[i] = s;
    ASSERT_EQ(StructArray[i].x, i);
    ASSERT_EQ(StructArray[i].y, i);
  }

  Array<structure, ARRAY_SIZE> CopyStructArray(StructArray);

  for (int i = 0; i < ARRAY_SIZE; i++) {
    ASSERT_EQ(CopyStructArray[i].x, i);
    ASSERT_EQ(CopyStructArray[i].y, i);
  }
}

TEST(Array, Size)
{
  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;
  ASSERT_EQ(ARRAY_SIZE, (int) IntArray.size());

  // Pointer Array
  Array<int*, ARRAY_SIZE> PointerArray;
  ASSERT_EQ(ARRAY_SIZE, (int) PointerArray.size());

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
  ASSERT_EQ(ARRAY_SIZE, (int) StructArray.size());
}

TEST(Array, AccessOperator)
{
  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;
  ASSERT_EQ(ARRAY_SIZE, (int) IntArray.size());

  // Assignment
  for (int i = 0; i < ARRAY_SIZE; i++) {
    IntArray[i] = i;
    ASSERT_EQ(IntArray[i], i);
  }

  // Invalid Access
  ASSERT_EQ(IntArray[ARRAY_SIZE], 0); // Return the first value in the array

  // Pointer Array
  Array<int*, ARRAY_SIZE> PointerArray;
  ASSERT_EQ(ARRAY_SIZE, (int) PointerArray.size());

  PointerArray[0] = &one;
  PointerArray[1] = &two;
  PointerArray[2] = &three;
  PointerArray[3] = &four;
  PointerArray[4] = &five;
  PointerArray[5] = &six;

  ASSERT_EQ(*PointerArray[0], one);
  ASSERT_EQ(*PointerArray[1], two);
  ASSERT_EQ(*PointerArray[2], three);
  ASSERT_EQ(*PointerArray[3], four);
  ASSERT_EQ(*PointerArray[4], five);
  ASSERT_EQ(*PointerArray[5], six);

  // Invalid Access
  ASSERT_EQ(*PointerArray[ARRAY_SIZE], one);

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
  ASSERT_EQ(ARRAY_SIZE, (int) StructArray.size());

  // Assignment
  for (int i = 0; i < ARRAY_SIZE; i++) {
    structure s;
    s.x = i;
    s.y = i;

    StructArray[i] = s;
    ASSERT_EQ(StructArray[i].x, i);
    ASSERT_EQ(StructArray[i].y, i);
  }

  // Invalid Access
  ASSERT_EQ(StructArray[ARRAY_SIZE].x, 0);
  ASSERT_EQ(StructArray[ARRAY_SIZE].y, 0);
}

TEST(Array, Swap)
{
  int err = ERR_NONE;

  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;
  ASSERT_EQ(ARRAY_SIZE, (int) IntArray.size());

  // Populate Array
  for (int i = 0; i < ARRAY_SIZE; i++) {
    IntArray[i] = i;
  }

  IntArray.swap(0, 5, &err);
  ASSERT_EQ(IntArray[5], 0);
  ASSERT_EQ(IntArray[0], 5);
  ASSERT_EQ(err, ERR_NONE);

  // Pointer Array
  Array<int*, ARRAY_SIZE> PointerArray;
  ASSERT_EQ(ARRAY_SIZE, (int) PointerArray.size());

  // Populate Array
  PointerArray[0] = &one;
  PointerArray[1] = &two;
  PointerArray[2] = &three;
  PointerArray[3] = &four;
  PointerArray[4] = &five;
  PointerArray[5] = &six;

  PointerArray.swap(0, 5, &err);
  ASSERT_EQ(*PointerArray[5], one);
  ASSERT_EQ(*PointerArray[0], six);
  ASSERT_EQ(err, ERR_NONE);

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
  ASSERT_EQ(ARRAY_SIZE, (int) StructArray.size());

  // Populate Array
  for (int i = 0; i < ARRAY_SIZE; i++) {
    structure s;
    s.x = i;
    s.y = i;

    StructArray[i] = s;
  }

  StructArray.swap(0, 5, &err);
  ASSERT_EQ(StructArray[5].x, 0);
  ASSERT_EQ(StructArray[5].y, 0);
  ASSERT_EQ(StructArray[0].x, 5);
  ASSERT_EQ(StructArray[0].y, 5);
  ASSERT_EQ(err, ERR_NONE);
}

TEST(Array, SetGet)
{
  int err = ERR_NONE;

  // Basic Array
  Array<int, ARRAY_SIZE> IntArray;
  ASSERT_EQ(ARRAY_SIZE, (int) IntArray.size());
  int iGet = 0;

  // Populate Array
  for (int i = 0; i < ARRAY_SIZE; i++) {
    IntArray.set(i, i, &err);
    ASSERT_EQ(IntArray[i], i);
    ASSERT_EQ(err, ERR_NONE);
    IntArray.get(i, iGet, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Pointer Array
  int* pGet;
  Array<int*, ARRAY_SIZE> PointerArray;
  ASSERT_EQ(ARRAY_SIZE, (int) PointerArray.size());

  // Populate Array
  PointerArray.set(0, &one, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[0], one);
  PointerArray.get(0, pGet, &err);
  ASSERT_EQ(*pGet, one);
  ASSERT_EQ(err, ERR_NONE);

  PointerArray.set(1, &two, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[1], two);
  PointerArray.get(1, pGet, &err);
  ASSERT_EQ(*pGet, two);
  ASSERT_EQ(err, ERR_NONE);

  PointerArray.set(2, &three, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[2], three);
  PointerArray.get(2, pGet, &err);
  ASSERT_EQ(*pGet, three);
  ASSERT_EQ(err, ERR_NONE);

  PointerArray.set(3, &four, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[3], four);
  PointerArray.get(3, pGet, &err);
  ASSERT_EQ(*pGet, four);
  ASSERT_EQ(err, ERR_NONE);

  PointerArray.set(4, &five, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[4], five);
  PointerArray.get(4, pGet, &err);
  ASSERT_EQ(*pGet, five);
  ASSERT_EQ(err, ERR_NONE);

  PointerArray.set(5, &six, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(*PointerArray[5], six);
  PointerArray.get(5, pGet, &err);
  ASSERT_EQ(*pGet, six);
  ASSERT_EQ(err, ERR_NONE);

  //Struct Array
  Array<structure, ARRAY_SIZE> StructArray;
  ASSERT_EQ(ARRAY_SIZE, (int) StructArray.size());

  // Populate Array
  for (int i = 0; i < ARRAY_SIZE; i++) {
    structure s;
    structure sGet;
    s.x = i;
    s.y = i;

    StructArray.set(i, s, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(StructArray[i].x, i);
    ASSERT_EQ(StructArray[i].y, i);

    StructArray.get(i, sGet, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(sGet.x, i);
    ASSERT_EQ(sGet.y, i);
  }
}



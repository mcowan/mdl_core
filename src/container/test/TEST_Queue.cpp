/*
 * TEST_Queue.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include "gtest/gtest.h"
#include <container/Queue.hpp>

using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Container;

struct testStruct_t
{
  int x;
  int y;

  bool operator==(const testStruct_t& rhs) const
  {
    return (x == rhs.x) && (y == rhs.y);
  }

  bool operator!=(const testStruct_t& rhs) const
  {
    return !((x == rhs.x) && (y == rhs.y));
  }
};

static int one = 1;
static int two = 2;
static int three = 3;
static int four = 4;
static int five = 5;
static int six = 6;

TEST(Queue, Constructor)
{
  // Standard Type
  Queue<double> S;
  ASSERT_EQ(0, (int) S.size());
  ASSERT_TRUE(S.empty());

  // Pointer
  Queue<int*> S2;
  ASSERT_EQ(0, (int) S2.size());
  ASSERT_TRUE(S2.empty());

  // Pointer
  Queue<testStruct_t> S3;
  ASSERT_EQ(0, (int) S3.size());
  ASSERT_TRUE(S3.empty());
}

TEST(Queue, CopyConstructorPointerType)
{
  Queue<int*> L;

  int err = ERR_NONE;

  L.push(&one, &err);
  L.push(&two, &err);
  L.push(&three, &err);
  L.push(&four, &err);
  L.push(&five, &err);
  L.push(&six, &err);

  Queue<int*> LCopy(L);

  // Serify Copy
  ASSERT_EQ(L.size(), LCopy.size());
  for (size_t i = 0; i < L.size(); i++) {
    ASSERT_EQ(*L[i], *LCopy[i]);
  }
}

TEST(Queue, CopyConstructorBasicType)
{
  Queue<int> L;
  int err = ERR_NONE;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial list
  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  Queue<int> LCopy(L);

  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[i]);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(Queue, CopyConstructorStructType)
{
  Queue<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial structure
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.push(structure, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  Queue<testStruct_t> LCopy(L);

  // Serify
  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[i].x);
    ASSERT_EQ(i, LCopy[i].y);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(Queue, SubscriptOperatorBasicType)
{
  Queue<int> L;
  int err = ERR_NONE;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i]);
  }
}

TEST(Queue, SubscriptOperatorStructureType)
{
  Queue<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.push(structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(i, L[i].x);
    ASSERT_EQ(i, L[i].y);
  }

  // invalid access
  L[1000];
}

TEST(Queue, SubscriptOperatorPointerType)
{
  Queue<int*> L;

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  ASSERT_EQ(*L[0], one);
  ASSERT_EQ(*L[1], two);
  ASSERT_EQ(*L[2], three);
  ASSERT_EQ(*L[3], four);
  ASSERT_EQ(*L[4], five);
  ASSERT_EQ(*L[5], six);
}

TEST(Queue, EqualityBasicType)
{
  Queue<int> L;
  Queue<int> LEqual;
  Queue<int> LUnequal;
  Queue<int> LShort;

  // Standard
  for (int i = 0; i < 1000; i++) {
    L.push(i);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    LEqual.push(i);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    LUnequal.push(1000 - i);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    LUnequal.push(i);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Queue, EqualityStructureType)
{
  Queue<testStruct_t> L;
  Queue<testStruct_t> LEqual;
  Queue<testStruct_t> LUnequal;
  Queue<testStruct_t> LShort;
  testStruct_t structure;

  // Standard
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    LEqual.push(structure);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LUnequal.push(structure);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LShort.push(structure);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Queue, EqualityPointerType)
{
  Queue<int*> L;
  Queue<int*> LEqual;
  Queue<int*> LUnequal;
  Queue<int*> LShort;

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // 1 2 3 4 5 6
  LEqual.push(&one);
  LEqual.push(&two);
  LEqual.push(&three);
  LEqual.push(&four);
  LEqual.push(&five);
  LEqual.push(&six);

  // 6 5 4 3 2 1
  LUnequal.push(&six);
  LUnequal.push(&five);
  LUnequal.push(&four);
  LUnequal.push(&three);
  LUnequal.push(&two);
  LUnequal.push(&one);

  // 4 6 3
  LShort.push(&four);
  LShort.push(&six);
  LShort.push(&three);


  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Queue, SwapPointerType)
{
  Queue<int*> L;
  int err = ERR_NONE;

  L.push(&one);
  L.push(&two);

  L.swap(0, 1, &err);

  ASSERT_EQ(*L[1], one);
  ASSERT_EQ(*L[0], two);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Queue, SwapBasicType)
{
  Queue<int> L;
  int err = ERR_NONE;

  // 1 2 3 4 5 6
  L.push(1);
  L.push(2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[1], 1);
  ASSERT_EQ(L[0], 2);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Queue, SwapStructureType)
{
  Queue<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure1;
  structure1.x = 1;
  structure1.y = 1;

  testStruct_t structure2;
  structure2.x = 2;
  structure2.y = 2;

  // 1 2 3 4 5 6
  L.push(structure1);
  L.push(structure2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[1].x, 1);
  ASSERT_EQ(L[1].y, 1);
  ASSERT_EQ(L[0].x, 2);
  ASSERT_EQ(L[0].y, 2);

  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Queue, ClearBasicType)
{
  Queue<int> L;
  int err = ERR_NONE;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  // Clear the Queue
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Queue, ClearStructureType)
{
  Queue<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  // Clear the Queue
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Queue, ClearPointerType)
{
  Queue<int*> L;
  int err = ERR_NONE;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(L.empty());

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 6);

  // Clear the Queue
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Queue, PushPopBasicType)
{
  Queue<int> L;
  int err = ERR_NONE;
  int iPop;

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  for (int i = 0; i < 1000; i++) {
    L.pop(iPop, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(iPop, i);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}

TEST(Queue, PushPopStructureType)
{
  Queue<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  for (int i = 0; i < 1000; i++) {
    L.pop(structure, &err);
    ASSERT_EQ(structure.x, i);
    ASSERT_EQ(structure.y, i);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}

TEST(Queue, PushPopPointerType)
{
  Queue<int*> L;
  int err = ERR_NONE;
  int* pPop;
  // Verify empty
  ASSERT_TRUE(L.empty());

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 6);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, one);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 5);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, two);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 4);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, three);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 3);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, four);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 2);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, five);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 1);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, six);
  ASSERT_EQ(err, ERR_NONE);


  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}


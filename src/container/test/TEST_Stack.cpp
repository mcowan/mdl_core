/*
 * TEST_Stack.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include "gtest/gtest.h"
#include <container/Stack.hpp>

using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Container;

struct testStruct_t
{
  int x;
  int y;

  bool operator==(const testStruct_t& rhs) const
  {
    return (x == rhs.x) && (y == rhs.y);
  }

  bool operator!=(const testStruct_t& rhs) const
  {
    return !((x == rhs.x) && (y == rhs.y));
  }
};

static int one = 1;
static int two = 2;
static int three = 3;
static int four = 4;
static int five = 5;
static int six = 6;

TEST(Stack, Constructor)
{
  // Standard Type
  Stack<double> S;
  ASSERT_EQ(0, (int) S.size());
  ASSERT_TRUE(S.empty());

  // Pointer
  Stack<int*> S2;
  ASSERT_EQ(0, (int) S2.size());
  ASSERT_TRUE(S2.empty());

  // Pointer
  Stack<testStruct_t> S3;
  ASSERT_EQ(0, (int) S3.size());
  ASSERT_TRUE(S3.empty());
}

TEST(Stack, CopyConstructorPointerType)
{
  Stack<int*> L;

  int err = ERR_NONE;

  L.push(&one, &err);
  L.push(&two, &err);
  L.push(&three, &err);
  L.push(&four, &err);
  L.push(&five, &err);
  L.push(&six, &err);

  Stack<int*> LCopy(L);

  // Serify Copy
  ASSERT_EQ(L.size(), LCopy.size());
  for (size_t i = 0; i < L.size(); i++) {
    ASSERT_EQ(*L[i], *LCopy[i]);
  }
}

TEST(Stack, CopyConstructorBasicType)
{
  Stack<int> L;
  int err = ERR_NONE;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial list
  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  Stack<int> LCopy(L);

  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[LCopy.size() - 1 - i]);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(Stack, CopyConstructorStructType)
{
  Stack<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  // Populate initial structure
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.push(structure, &err);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Copy constructor
  Stack<testStruct_t> LCopy(L);

  // Serify
  for (int i = 0; i < 1000; i++) {
    ASSERT_EQ(i, LCopy[LCopy.size() - 1 - i].x);
    ASSERT_EQ(i, LCopy[LCopy.size() - 1 - i].y);
  }

  ASSERT_FALSE(L.empty());
  ASSERT_FALSE(LCopy.empty());
}

TEST(Stack, SubscriptOperatorBasicType)
{
  Stack<int> L;
  int err = ERR_NONE;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(0, L[i]);
  }
}

TEST(Stack, SubscriptOperatorStructureType)
{
  Stack<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Serify Not empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;

    L.push(structure, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(0, L[i].x);
    ASSERT_EQ(0, L[i].y);
  }

  // invalid access
  L[1000];
}

TEST(Stack, SubscriptOperatorPointerType)
{
  Stack<int*> L;

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  ASSERT_EQ(*L[5], one);
  ASSERT_EQ(*L[4], two);
  ASSERT_EQ(*L[3], three);
  ASSERT_EQ(*L[2], four);
  ASSERT_EQ(*L[1], five);
  ASSERT_EQ(*L[0], six);
}

TEST(Stack, EqualityBasicType)
{
  Stack<int> L;
  Stack<int> LEqual;
  Stack<int> LUnequal;
  Stack<int> LShort;

  // Standard
  for (int i = 0; i < 1000; i++) {
    L.push(i);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    LEqual.push(i);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    LUnequal.push(1000 - i);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    LUnequal.push(i);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Stack, EqualityStructureType)
{
  Stack<testStruct_t> L;
  Stack<testStruct_t> LEqual;
  Stack<testStruct_t> LUnequal;
  Stack<testStruct_t> LShort;
  testStruct_t structure;

  // Standard
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure);
  }

  // Equal
  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    LEqual.push(structure);
  }

  // UnEqual
  for (int i = 0; i < 1000; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LUnequal.push(structure);
  }

  // Short
  for (int i = 0; i < 500; i++) {
    structure.x = 1000 - i;
    structure.y = 1000 - i;
    LShort.push(structure);
  }

  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Stack, EqualityPointerType)
{
  Stack<int*> L;
  Stack<int*> LEqual;
  Stack<int*> LUnequal;
  Stack<int*> LShort;

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // 1 2 3 4 5 6
  LEqual.push(&one);
  LEqual.push(&two);
  LEqual.push(&three);
  LEqual.push(&four);
  LEqual.push(&five);
  LEqual.push(&six);

  // 6 5 4 3 2 1
  LUnequal.push(&six);
  LUnequal.push(&five);
  LUnequal.push(&four);
  LUnequal.push(&three);
  LUnequal.push(&two);
  LUnequal.push(&one);

  // 4 6 3
  LShort.push(&four);
  LShort.push(&six);
  LShort.push(&three);


  // equal
  ASSERT_TRUE(L == LEqual);
  ASSERT_TRUE(L != LUnequal);
  ASSERT_TRUE(L != LShort);

  // unequal
  ASSERT_FALSE(L != LEqual);
  ASSERT_FALSE(L == LUnequal);
  ASSERT_FALSE(L == LShort);
}

TEST(Stack, SwapPointerType)
{
  Stack<int*> L;
  int err = ERR_NONE;

  L.push(&one);
  L.push(&two);

  L.swap(0, 1, &err);

  ASSERT_EQ(*L[1], two);
  ASSERT_EQ(*L[0], one);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Stack, SwapBasicType)
{
  Stack<int> L;
  int err = ERR_NONE;

  // 1 2 3 4 5 6
  L.push(1);
  L.push(2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[1], 2);
  ASSERT_EQ(L[0], 1);


  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Stack, SwapStructureType)
{
  Stack<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure1;
  structure1.x = 1;
  structure1.y = 1;

  testStruct_t structure2;
  structure2.x = 2;
  structure2.y = 2;

  // 1 2 3 4 5 6
  L.push(structure1);
  L.push(structure2);

  L.swap(0, 1, &err);

  ASSERT_EQ(L[1].x, 2);
  ASSERT_EQ(L[1].y, 2);
  ASSERT_EQ(L[0].x, 1);
  ASSERT_EQ(L[0].y, 1);

  ASSERT_EQ(err, ERR_NONE);

  // Range Error
  L.swap(0, 42, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST(Stack, ClearBasicType)
{
  Stack<int> L;
  int err = ERR_NONE;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  // Clear the Stack
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Stack, ClearStructureType)
{
  Stack<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  // Clear the Stack
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Stack, ClearPointerType)
{
  Stack<int*> L;
  int err = ERR_NONE;

  L.clear(&err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(L.empty());

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 6);

  // Clear the Stack
  L.clear(&err);
  ASSERT_EQ((int) L.size(), 0);
}

TEST(Stack, PushPopBasicType)
{
  Stack<int> L;
  int err = ERR_NONE;
  int iPop;

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    L.push(i, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  for (int i = 999; i >= 0; i--) {
    L.pop(iPop, &err);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(iPop, i);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}

TEST(Stack, PushPopStructureType)
{
  Stack<testStruct_t> L;
  int err = ERR_NONE;
  testStruct_t structure;

  // Verify empty
  ASSERT_TRUE(L.empty());

  for (int i = 0; i < 1000; i++) {
    structure.x = i;
    structure.y = i;
    L.push(structure, &err);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 1000);

  for (int i = 999; i >= 0; i--) {
    L.pop(structure, &err);
    ASSERT_EQ(structure.x, i);
    ASSERT_EQ(structure.y, i);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}

TEST(Stack, PushPopPointerType)
{
  Stack<int*> L;
  int err = ERR_NONE;
  int* pPop;
  // Verify empty
  ASSERT_TRUE(L.empty());

  // 1 2 3 4 5 6
  L.push(&one);
  L.push(&two);
  L.push(&three);
  L.push(&four);
  L.push(&five);
  L.push(&six);

  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 6);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, six);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 5);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, five);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 4);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, four);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 3);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, three);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 2);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, two);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((int) L.size(), 1);
  L.pop(pPop, &err);
  ASSERT_EQ(*pPop, one);
  ASSERT_EQ(err, ERR_NONE);


  // Verify Not Empty
  ASSERT_EQ((int) L.size(), 0);
  ASSERT_TRUE(L.empty());
}

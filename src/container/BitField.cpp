/*
 * BitField.cpp
 *
 *  Created on: Dec 13, 2013
 *      Author: simplymac
 */

#include <container/BitField.hpp>
#include <common/Error.hpp>
#include <common/MemOperations.hpp>

using namespace std;
using namespace ns_MDL;
using namespace ns_Container;

static const unsigned char BITMASK[8] = {
  0x01, // Bit 0 mask
  0x02, // Bit 1 mask
  0x04, // Bit 2 mask
  0x08, // Bit 3 mask
  0x10, // Bit 4 mask
  0x20, // Bit 5 mask
  0x40, // Bit 6 mask
  0x80 // Bit 7 mask
};

BitField::BitField()
{
  m_pArray = new unsigned char[1];
  MemSet(m_pArray, 0, 1); // Clear Memory Array
  m_Size = 8;
  m_Capacity = 8;
}

BitField::~BitField()
{
  delete[] m_pArray;
}

BitField::BitField(const BitField& toCopy)
{
  m_Size = toCopy.size();
  m_Capacity = toCopy.capacity();

  m_pArray = new unsigned char[m_Capacity / 8];
  clear();
  for (unsigned int i = 0; i < m_Size; i++) {
    set(i, toCopy[i]);
  }
}

BitField::BitField(unsigned int size)
{
  unsigned char temp_Size = (size + 4) / 8; // Force a Round Up by adding half the number of bits in a byte.

  if (temp_Size == 0) {
    temp_Size = 1;
  }

  m_Size = size;
  m_pArray = new unsigned char[temp_Size];
  MemSet(m_pArray, 0, temp_Size);
  m_Capacity = temp_Size * 8;
}

BitField::BitField(unsigned int size, bool val)
{
  unsigned char temp_Size = size / 8;

  if (temp_Size == 0) {
    temp_Size = 1;
  }

  if (size % 8 != 0) {
    temp_Size += 1;
  }

  m_Size = size;
  m_pArray = new unsigned char[temp_Size];
  m_Capacity = temp_Size * 8;
  fill(val);
}

unsigned int
BitField::size() const
{
  return m_Size;
}

unsigned int
BitField::capacity() const
{
  return m_Capacity;
}

bool
BitField::is_zero() const
{
  bool val = false;

  for (unsigned int i = 0; i < m_Size; i++) {
    val |= get(i);
  }

  return !val;
}


// Access Operator

bool BitField::operator[](const unsigned int index) const
{
  return get(index);
}

bool
BitField::get(const unsigned int index, ERROR* err) const
{
  bool toReturn = false;
  SetErr(err, ERR_NONE);

  if (m_Size != 0) {
    if (index < m_Size) {
      toReturn = ((m_pArray[ArrayIndex(index)] & BITMASK[index % 8]) > 0);
    }
    else {
      SetErr(err, ERR_RANGE);
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }

  return toReturn;
}

void
BitField::set(const unsigned int index, bool val, ERROR* err)
{
  SetErr(err, ERR_NONE);

  if (m_Size != 0) {
    if (index < m_Size) {
      if (val) {
        m_pArray[ArrayIndex(index)] |= BITMASK[index % 8];
      }
      else {
        m_pArray[ArrayIndex(index)] &= ~BITMASK[index % 8];
      }
    }
    else {
      SetErr(err, ERR_RANGE);
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

void
BitField::fill(bool val)
{
  if (val) {
    MemSet(m_pArray, 0xFF, m_Capacity / 8);
  }
  else {
    MemSet(m_pArray, 0, m_Capacity / 8);
  }
}

void
BitField::swap(const unsigned int index1, const unsigned int index2, ERROR* err)
{
  SetErr(err, ERR_NONE);

  if (m_Size != 0) {
    if (index1 < m_Size && index2 < m_Size) {
      bool buffer = get(index1);
      set(index1, get(index2));
      set(index2, buffer);
    }
    else {
      SetErr(err, ERR_RANGE);
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

void
BitField::clear()
{
  fill(false);
}

unsigned int
BitField::ArrayIndex(const unsigned int index) const
{
  return (index == 0) ? 0 : index / 8;
}

bool BitField::operator==(const BitField& other) const
{
  bool toReturn = false;

  if (other.size() == size()) {
    toReturn = true;
    for (unsigned int i = 0; i < other.size(); i++) {
      if (get(i) != other.get(i)) {
        toReturn = false;
        break;
      }
    }
  }
  return toReturn;
}

bool BitField::operator!=(const BitField& other) const
{
  bool toReturn = true;

  if (other.size() == size()) {
    toReturn = false;
    for (unsigned int i = 0; i < other.size(); i++) {
      if (get(i) != other.get(i)) {
        toReturn = true;
        break;
      }
    }
  }
  return toReturn;
}




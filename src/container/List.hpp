/// @file container/List.hpp
///
/// @brief Defines the List interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated List class.

#ifndef LIST_HPP_
#define LIST_HPP_

#include <common/MemOperations.hpp>
#include <common/Error.hpp>
#include <container/ContainerIf.hpp>
#include <Types.hpp>


/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{

  /// @brief This namespace is wrapper for the Containers classes.
  /// 
  namespace ns_Container
  {

    /// @brief This class is the base class for all of the
    /// container classes.
    /// 

    template <typename T>
    class List : public Container<T>
    {
    public:
      /// @details Creates a Basic Empty Templated List.
      ///
      List();

      /// @details Templated List Copy Constructor.
      ///
      /// @param[in] other	The List to copy.
      ///
      List(const List<T> &other);

      /// @details Templated List Decontructor.
      ///
      ~List();

      /// @details Number of Elements in the List.
      ///
      /// @return Number of Elements in the List.
      ///
      size_t size() const;

      /// @details Checks if the container is empty.
      ///
      /// @return True, if the List is empty; False, otherwise.
      ///
      bool empty() const;

      /// @details Access Operator.
      ///
      /// @param[in] index	Index of the object to access.
      ///
      /// @return Returns the obect.
      ///
      T& operator[](const size_t index);

      /// @details Constant Access Operator.
      ///
      /// @param[in] index	Index of the object to access.
      ///
      /// @return Returns the obect.
      ///
      T& operator[](const size_t index) const;

      ///  @brief Swaps two Elements in the List.
      ///  
      ///  @param [in] index1 Element index one.
      ///  @param [in] index2 Element index two.
      ///  @param [out] err Optional error code.
      /// 
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      ///  @brief Set the value of a node in the List at the given
      ///  index with the given value.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being set.
      ///  @param [out] err Optional error code.
      /// 
      void insert(size_t pos, T elem, ERROR* err = 0);

      ///  @brief Set the value of a node in the List at the given
      ///  index with the given value.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being set.
      ///  @param [out] err Optional error code.
      /// 
      void set(size_t pos, T elem, ERROR* err = 0);

      ///  @brief Get the value of a node in the List at the given
      ///  index.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being returned.
      ///  @param [out] err Optional error code.
      /// 				 
      void get(size_t pos, T& elem, ERROR* err = 0);

      ///  @brief Erases a specific element in the list.
      ///  
      ///  @param [in] pos Index of the element to remove.
      ///  @param [out] err Optional error code.
      /// 
      void erase(size_t pos, ERROR* err = 0);

      ///  @brief Erases a range of items in the List.
      ///  
      ///  @param [in] beg Index of the first element to remove from.
      ///  @param [in] end Index of the last element to remove to.
      ///  @param [out] err Optional error code.
      /// 				
      void erase(size_t beg, size_t end, ERROR* err = 0);

      ///  @brief Removes all elements in the list.
      ///  
      ///  @param [out] err Optional error code.
      /// 
      void clear(ERROR* err = 0);

      ///  @brief Returns a copy of the first element in the list.
      ///  
      ///  @return An element at the front of the List.
      /// 
      T front();

      ///  @brief Returns a copy of the last element in the list.
      ///  
      ///  @return An element at the end of the List.
      /// 
      T back();

      ///  @brief Appends an element to the front of the list.
      ///  
      ///  @param [in] elem Element to be appended to the list.
      ///  @param [out] err Optional error code.
      /// 
      void push_front(T elem, ERROR* err = 0);

      ///  @brief Removes and returns the first element in the list.
      ///  
      ///  @param [in] elem Returns the element being removed.
      ///  @param [out] err Optional error code.
      /// 
      void pop_front(T& elem, ERROR* err = 0);

      ///  @brief Appends the given element to the front of the list.
      ///  
      ///  @param [in] elem Element being appended to the end of the list.
      ///  @param [out] err Optional error code.
      /// 
      void push_back(T elem, ERROR* err = 0);

      ///  @brief Removes and returns the last element in the list.
      ///  
      ///  @param [in] elem Returns the element being removed.
      ///  @param [out] err Optional error code.
      /// 
      void pop_back(T& elem, ERROR* err = 0);

    private:

      /// @brief A Template Node for the List. Double Linked List.
      ///  

      struct Node
      {
        T data; ///> Template Data.
        Node* p_Next; ///> Next Element.
        Node* p_Prev; ///>Previous Element.

        ///  @brief Creates a Node object.
        ///  
        ///  @param [in] d Element data to store in Node.
        /// 

        Node(T d)
        {
          data = d;
          p_Next = NULL;
          p_Prev = NULL;
        };

        ///  @brief Node object destructor.
        /// 

        ~Node()
        {
          MemSet(&data, 0, sizeof (T));
          p_Next = NULL;
          p_Prev = NULL;
        }
      };

    private:
      Node* p_Head; ///> Head Pointer.
      Node* p_Tail; ///> Tail Pointer.
      size_t m_Size; ///> Total Number of elements.

    };
  }
}

// List Implementation
#include <container/List_source.hpp>

#endif 

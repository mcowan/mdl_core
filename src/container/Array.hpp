/// @file container/Array.hpp
///
/// @brief Defines the Array interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Array class.

#ifndef ARRAY_HPP_
#define ARRAY_HPP_

#include <common/MemOperations.hpp>
#include <common/Error.hpp>
#include <container/ContainerIf.hpp>
#include <Types.hpp>


/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{

  /// @brief This namespace is wrapper for the Containers classes.
  ///
  namespace ns_Container
  {

    /// @brief A simple class that creates a static array.
    ///
    /// @param[in] T  Array datatype.
    /// @param[in] N  Size of the array.
    ///

    template <typename T, size_t N>
    class Array : public Container<T>
    {
    public:

      /// @details Creates a Basic Empty Templated Array.
      ///
      Array();

      /// @details Templated Array Copy Constructor.
      ///
      /// @param[in] other Array to copy.
      ///
      Array(const Array<T, N> &other);

      /// @details Templated Array Decontructor.
      ///
      ~Array();

      /// @details Number of Elements in the Array.
      ///
      /// @return The number of elements the array can store.
      ///
      size_t size() const;

      /// @details Access Operator.
      ///
      /// @param[in] index	Index of the object to access.
      ///
      /// @return Returns the obect.
      ///
      T& operator[](const size_t index);


      /// @details Constant Access Operator.
      ///
      /// @param[in] index	Index of the object to access.
      ///
      /// @return Returns the obect.
      ///
      T& operator[](const size_t index) const;

      /// @brief Swaps two Elements in the Array.
      /// 
      /// @param [in] index1 Element index one.
      /// @param [in] index2 Element index two.
      /// @param [out] err Optional error code.
      ///
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      /// @brief Set the value of a node in the Array at the given
      /// index with the given value.
      /// 
      /// @param [in] pos Index of the element to retrieve.
      /// @param [in] elem Element being set.
      /// @param [out] err Optional error code.
      ///
      void set(size_t pos, T elem, ERROR* err = 0);

      /// @brief Get the value of a node in the Array at the given
      /// index.
      /// 
      /// @param [in] pos Index of the element to retrieve.
      /// @param [in] elem Element being returned.
      /// @param [out] err Optional error code.
      ///				 
      void get(size_t pos, T& elem, ERROR* err = 0);

    private:
      T* p_Data; ///> Head Pointer.

    }; // End of Array
  }
}

// Array Implementation
#include <container/Array_source.hpp>

#endif 

/// @file container/Stack_source.hpp
///
/// @brief Implementation of the stack container.
///
/// @authors mjc
///
/// @date 01-13-14
///
/// @details An implementation of a templated Stack Container.  Based
/// on the basic container interface.


#ifndef STACK_SOURCE_HPP_
#define STACK_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Container;

template <typename T>
Stack<T>::Stack()
: Container<T>()
{
};

template <typename T>
Stack<T>::Stack(const Stack<T> &other)
: Container<T>()
{
  for (int i = other.size() - 1; i >= 0; i--) {
    push(other[i]);
  }
};

template <typename T>
Stack<T>::~Stack()
{
};

template <typename T>
size_t Stack<T>::size() const
{
  return m_Data.size();
};

template <typename T>
bool Stack<T>::empty() const
{
  return m_Data.empty();
};

template <typename T>
T& Stack<T>::operator[](const size_t index)
{
  return m_Data[m_Data.size() - 1 - index];
};

template <typename T>
T& Stack<T>::operator[](const size_t index) const
{
  return m_Data[m_Data.size() - 1 - index];
};

template<typename T>
void Stack<T>::swap(size_t index1, size_t index2, ERROR* err)
{
  m_Data.swap(m_Data.size() - 1 - index1, m_Data.size() - 1 - index2, err);
}

template<typename T>
void Stack<T>::clear(ERROR* err)
{
  m_Data.clear(err);
}

template <typename T>
T Stack<T>::top()
{
  return m_Data[m_Data.size() - 1];
}

template <typename T>
void Stack<T>::push(T elem, ERROR* err)
{
  m_Data.insert(m_Data.size(), elem, err);
}

template <typename T>
void Stack<T>::pop(T& elem, ERROR* err)
{
  if (m_Data.size() > 0) {
    m_Data.pop_back(elem, err);
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

#endif
/*
 * BitField.hpp
 *
 *  Created on:   December 13, 2013
 *  Author:         mcowan
 *  Description:  A class that allows for the compression of multiple
 *  boolean values into a tightly compressed array of boolean values.  The
 *  user has the ability to get and set specific boolean values within the
 *  array.
 */

#ifndef BitField_HPP_
#define BitField_HPP_

#include <Types.hpp>

namespace ns_MDL
{
  namespace ns_Container
  {

    /**
     * @brief An array of boolean values.  The array allows you to compress multiple boolean
     *  values on systems that would use a full character to store a single boolean value.  All bits
     *  are set to false when constructed, unless specified in the constructor.  The BitField
     *  container is a specialized container, as it does not inherit from the Container
     *  class.  As it is this container cant be sorted, and it cant really be sorted as it
     *  is. 
     */
    class BitField
    {
    public:
      /**
       * @brief A general constructor that generates a BitField with a size of eight.
       */
      BitField();

      /*
       *  @brief BitField destructor.
       */
      ~BitField();

      /**
       * @brief Copy constructor.  Creates a new instance of a BitField with the same values set
       * as the BitField being copied.
       */
      BitField(const BitField& toCopy);

      /**
       * @brief Constructs a BitField of the given size.
       *
       * @param[in] size   the size of the BitField to create.
       */
      BitField(unsigned int size);

      /**
       * @brief Constructs a BitField of the given size and sets all bits
       * to the given value.
       *
       * @param[in] size   The size of the BitField to create.
       * @param[in] val     All bits in BitField will be set to this value.
       */
      BitField(unsigned int size, bool val);

      /**
       * @brief The number of bits the BitField will hold.
       *
       * @return The total number of boolean value the BitField holds.
       */
      unsigned int size() const;

      /**
       * @brief The Maximum number of bits the BitField represents in
       * memory (multiples of 8).
       *
       * @return The total number of bits the BitField represents in memory.
       */
      unsigned int capacity() const;

      /**
       * @brief Returns if the all of the BitField values are set to zero.
       *
       * @return true if all values are zero; false, otherwise.
       */
      bool is_zero() const;

      /**
       * @brief Accessor operator used to access individual bits in the BitField.
       *
       * @param[in] index The array index of the bit to access.
       * @return The boolean value of the accessed bit.
       */
      bool operator[](const unsigned int index) const;

      /**
       * @brief Gets the value of an individual bit in BitField.
       *
       * @param[in] index 	The array index of the bit to access.
       * @param[out]	err		ERR_EMPTY if there are no bits in bitfield.
       * 					ERR_NONE  if there was no error.
       * 					ERR_RANGE if the given index was larger than
       * 							  the bitfield.
       * @return The boolean value of the accessed bit.
       */
      bool get(const unsigned int index, ERROR* err = 0) const;

      /**
       * @brief Sets the value of a specific bit in the BitField.
       *
       * @param[in] index The array index of the bit to set.
       * @param[in] val     The value to set the bit to.
       * @param[out] err		Optional Error return code.
       */
      void set(const unsigned int index, bool val, ERROR* err = 0);

      /**
       * @brief Sets all the bits in a BitField to a specific value.  This sets the number of bits
       * represented by the size not the capacity.
       *
       * @param[in] val The value to set all bits to in the BitField.
       */
      void fill(bool val);

      /**
       * @brief Swaps the values of two bits in a BitField array.
       *
       * @param[in]  index1  First BitField array location.
       * @param[in]  index2  Second BitField array location.
       * @param[out] err		ERR_RANGE if one or both of the indexes is
       * 					greater than the size of the buffer.
       */
      void swap(unsigned int index1, unsigned int index2, ERROR* err = 0);

      /**
       * @brief Sets all of the values in the BitField array to false;
       */
      void clear();

      /**
       * @brief Compares two bitfields to see if they are equal.
       */
      bool operator==(const BitField& other) const;

      /**
       * @brief Compares two bitfields to see if they are not equal.
       */
      bool operator!=(const BitField& other) const;

    private:
      unsigned int ArrayIndex(const unsigned int index) const;
      unsigned char* m_pArray; ///> Array of boolean values
      unsigned int m_Size; ///> Number of boolean values stored
      unsigned int m_Capacity; ///> Capacity of bits the BitField can support.
    };
  } /* namespace Container */
} /* namespace MDL */

#endif

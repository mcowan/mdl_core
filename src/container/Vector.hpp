/// @file container/Vector.hpp
///
/// @brief Defines the Vector interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Vector Container.

#ifndef VECTOR_HPP_
#define VECTOR_HPP_

#include <common/MemOperations.hpp>
#include <common/Error.hpp>
#include <container/ContainerIf.hpp>
#include <Types.hpp>


/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Container classes.
  ///
  namespace ns_Container
  {
    /// @brief This class is the base class for all of the
    /// container classes.
    ///
    /// @param[in] T Type of Vector.
    ///

    template <typename T>
    class Vector : public Container<T>
    {
    private:
      static const size_t DEFAULT_ARRAY_SIZE = 100; ///> Default Array Size.
      static const size_t DEFAULT_INITIAL_SIZE = 100; ///> Default Initial block size.
      static const size_t DEFAULT_EXPANSION_RATE = 2; ///> Default Expansion Rate.

    public:
      /// @details Creates a Basic Empty Templated Vector.
      ///
      /// @param[in] initialSize Initial Size to set the vectors capacity.
      ///
      Vector(size_t initialSize = DEFAULT_INITIAL_SIZE);

      /// @details Templated Vector Copy Constructor.
      ///
      /// @param[in] other	The vector to copy.
      ///
      Vector(const Vector<T> &other);

      /// @details Templated Vector Decontructor.
      ///
      ~Vector();

      /// @details Number of Elements in the Vector.
      ///
      /// @return The current number of elements.
      ///
      size_t size() const;

      /// @details Total Capacity of the Vector.
      ///
      /// @return The current capacity.
      ///
      size_t capacity() const;

      /// @details Gets the Expansion Rate.
      ///
      /// @return Expansion Rate.
      ///
      size_t rate() const;

      /// @details Checks if the container is empty.
      ///
      /// @return True, if empty; False, otherwise.
      ///
      bool empty() const;

      /// @details Access Operator.
      ///
      /// @param[in] index	Index of the object being accessed.
      ///
      /// @return Returns the object.
      ///
      T& operator[](const size_t index);


      /// @details Constant Access Operator.
      ///
      /// @param[in] index	Index of the object being accessed.
      ///
      /// @return Returns the object.
      ///
      T& operator[](const size_t index) const;

      ///  @brief Swaps two Elements in the Vector.
      ///  
      ///  @param [in] index1 Element index one.
      ///  @param [in] index2 Element index two.
      ///  @param [out] err Optional error code.
      ///
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      ///  @brief Set the value of a node in the Vector at the given
      ///  index with the given value.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being set.
      ///  @param [out] err Optional error code.
      ///
      void insert(size_t pos, T elem, ERROR* err = 0);

      ///  @brief Set the value of a node in the Vector at the given
      ///  index with the given value.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being set.
      ///  @param [out] err Optional error code.
      ///
      void set(size_t pos, T elem, ERROR* err = 0);

      ///  @brief Get the value of a node in the Vector at the given
      ///  index.
      ///  
      ///  @param [in] pos Index of the element to retrieve.
      ///  @param [in] elem Element being returned.
      ///  @param [out] err Optional error code.
      ///				 
      void get(size_t pos, T& elem, ERROR* err = 0);

      ///  @brief Erases a specific element in the Vector.
      ///  
      ///  @param [in] pos Index of the element to remove.
      ///  @param [out] err Optional error code.
      ///
      void erase(size_t pos, ERROR* err = 0);

      ///  @brief Erases a range of items in the Vector.
      ///  
      ///  @param [in] beg Index of the first element to remove from.
      ///  @param [in] end Index of the last element to remove to.
      ///  @param [out] err Optional error code.
      ///				
      void erase(size_t beg, size_t end, ERROR* err = 0);

      ///  @brief Removes all elements in the Vector.
      ///  
      ///  @param [out] err Optional error code.
      ///
      void clear(ERROR* err = 0);

      ///  @brief Returns a copy of the first element in the Vector.
      ///  
      ///  @return An element.
      ///
      T front();

      ///  @brief Returns a copy of the last element in the Vector.
      ///  
      ///  @return An element.
      ///
      T back();

      ///  @brief Appends an element to the front of the Vector.
      ///  
      ///  @param [in] elem Element to be appended to the Vector.
      ///  @param [out] err Optional error code.
      ///
      void push_front(T elem, ERROR* err = 0);

      ///  @brief Removes and returns the first element in the Vector.
      ///  
      ///  @param [in] elem Returns the element being removed.
      ///  @param [out] err Optional error code.
      ///
      void pop_front(T& elem, ERROR* err = 0);

      ///  @brief Appends the given element to the front of the Vector.
      ///  
      ///  @param [in] elem Element being appended to the end of the Vector.
      ///  @param [out] err Optional error code.
      ///
      void push_back(T elem, ERROR* err = 0);

      ///  @brief Removes and returns the last element in the Vector.
      ///  
      ///  @param [in] elem Returns the element being removed.
      ///  @param [out] err Optional error code.
      ///
      void pop_back(T& elem, ERROR* err = 0);

    private:

      /// @brief Expands the storage of the vector.
      ///
      void ExpandStorage();

      T** p_Data; ///> Pointer to Array Pointer.
      size_t m_Size; ///> Total number of objects stored in Vector.
      size_t m_ArraySize; ///> Total number of arrays that have been allocated.
      size_t m_TotalCapacity; ///> Total Capacity of the entire Vector.
      size_t m_ArrayCapacity; ///> Total Capacity of the Pointer Array.
      size_t m_ExpansionRate; ///> Number of elements to expand by.
    }; // End of Vector
  }
}

// Vector Implementation
#include <container/Vector_source.hpp>

#endif 

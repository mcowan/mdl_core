/*
 * List_source.hpp
 *
 *  Created on: Feb 16, 2014
 *  Author: simplymac
 *  Description: Implementation of the Templated List class.
 *
 */

#ifndef LIST_SOURCE_HPP_
#define LIST_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Container;

template <typename T>
List<T>::List()
: Container<T>(),
p_Head(NULL),
p_Tail(NULL),
m_Size(0)
{
};

template <typename T>
List<T>::List(const List<T> &other)
: Container<T>(),
p_Head(NULL),
p_Tail(NULL),
m_Size(0)
{
  for (size_t i = 0; i < other.size(); i++) {
    insert(i, other[i]);
  }
};

template <typename T>
List<T>::~List()
{
  clear();
  p_Head = 0;
  p_Tail = 0;
  m_Size = 0;
};

template <typename T>
size_t List<T>::size() const
{
  return m_Size;
};

template <typename T>
bool List<T>::empty() const
{
  return (m_Size == 0);
};

template <typename T>
T& List<T>::operator[](const size_t index) const
{
  Node* curr = p_Head;
  size_t loc = 0;

  if (index < m_Size) {
    if (index < m_Size / 2) {
      curr = p_Head;
      loc = 0;
      while (loc < index) {
        curr = curr->p_Next;
        loc++;
      }
    }
    else {
      curr = p_Tail;
      loc = m_Size - 1;
      while (loc > index) {
        curr = curr->p_Prev;
        loc--;
      }
    }

    return curr->data;
  }

  return curr->data;
}

template <typename T>
T& List<T>::operator[](const size_t index)
{
  Node* curr = p_Head;
  size_t loc = 0;

  if (index < m_Size) {
    if (index < m_Size / 2) {
      curr = p_Head;
      loc = 0;
      while (loc < index) {
        curr = curr->p_Next;
        loc++;
      }
    }
    else {
      curr = p_Tail;
      loc = m_Size - 1;
      while (loc > index) {
        curr = curr->p_Prev;
        loc--;
      }
    }

    return curr->data;
  }

  return curr->data; // return the first element in the list
}

// swap

template <typename T>
void List<T>::swap(size_t index1, size_t index2, ERROR* err)
{
  T elem1;
  T elem2;

  if ((index1 < m_Size) && (index2 < m_Size)) {
    if (index1 != index2) {
      get(index1, elem1);
      get(index2, elem2);

      set(index2, elem1);
      set(index1, elem2);

      SetErr(err, ERR_NONE);
    }
  }
  else {
    SetErr(err, ERR_RANGE);
  }
}


//virtual void swap( size_t index1, size_t index2 );

template <typename T>
void List<T>::insert(size_t pos, T elem, ERROR* err)
{
  Node* curr = NULL;
  Node* prev = NULL;
  Node* next = NULL;

  // Insert First Element
  if (m_Size == 0) {
    curr = new Node(elem);
    p_Head = curr;
    p_Tail = curr;
    m_Size++;
  }
  else if (pos == 0) // Beginning of List
  {
    curr = new Node(elem);
    curr->p_Next = p_Head;
    p_Head->p_Prev = curr;
    p_Head = curr;
    m_Size++;
  }
  else if (pos == m_Size) // End of List
  {
    curr = new Node(elem);
    p_Tail->p_Next = curr;
    curr->p_Prev = p_Tail;
    p_Tail = curr;
    m_Size++;
  }
  else {
    size_t loc = 0;
    curr = new Node(elem);
    if (pos < m_Size / 2) {
      next = p_Head;
      while (pos != loc) {
        next = next->p_Next;
        loc++;
      }
      prev = next->p_Prev;
    }
    else {
      loc = m_Size - 1;
      next = p_Tail;
      while (pos != loc) {
        next = next->p_Prev;
        loc--;
      }
      prev = next->p_Prev;
    }
    // Insert Current
    prev->p_Next = curr;
    curr->p_Prev = prev;
    next->p_Prev = curr;
    curr->p_Next = next;

    m_Size++;
  }

};

template <typename T>
void List<T>::set(size_t pos, T elem, ERROR* err)
{
  int iErr = ERR_NONE;
  Node* curr;
  size_t loc = 0;

  if (pos >= m_Size) {
    iErr = ERR_RANGE;
  }

  if (iErr == ERR_NONE) {
    if (pos < m_Size / 2) {
      curr = p_Head;
      while (pos != loc) {
        curr = curr->p_Next;
        loc++;
      }
    }
    else {
      loc = m_Size - 1;
      curr = p_Tail;
      while (pos != loc) {
        curr = curr->p_Prev;
        loc--;
      }
    }
    curr->data = elem;
  }

  SetErr(err, iErr);
}

template <typename T>
void List<T>::get(size_t pos, T& elem, ERROR* err)
{
  Node* curr;
  size_t loc = 0;

  if (pos < m_Size) {
    if (pos < m_Size / 2) {
      curr = p_Head;
      loc = 0;
      while (loc < pos) {
        curr = curr->p_Next;
        loc++;
      }
    }
    else {
      curr = p_Tail;
      loc = m_Size - 1;
      while (loc > pos) {
        curr = curr->p_Prev;
        loc--;
      }
    }

    //// Set Value
    elem = curr->data;
  }
  else {
    SetErr(err, ERR_RANGE);
  }
}

template <typename T>
void List<T>::erase(size_t pos, ERROR* err)
{
  Node* next = 0;
  Node* curr = 0;
  Node* prev = 0;

  if (m_Size == 0) {
    SetErr(err, ERR_EMPTY);
    return;
  }

  if (pos == 0) {
    curr = p_Head;
    p_Head = p_Head->p_Next;
    m_Size--;
    delete curr;
  }
  else if (pos == (m_Size - 1)) {
    curr = p_Tail;
    p_Tail = p_Tail->p_Prev;
    m_Size--;
    delete curr;
  }
  else {
    size_t loc = 0;

    if (pos < m_Size / 2) {
      curr = p_Head;
      while (pos != loc) {
        curr = curr->p_Next;
        loc++;
      }
      prev = curr->p_Prev;
      next = curr->p_Next;
    }
    else {
      loc = m_Size - 1;
      curr = p_Tail;
      while (pos != loc) {
        curr = curr->p_Prev;
        loc--;
      }
      prev = curr->p_Prev;
      next = curr->p_Next;
    }

    prev->p_Next = next;
    next->p_Prev = prev;
    m_Size--;
    delete curr;
  }
};

template <typename T>
void List<T>::erase(size_t beg, size_t end, ERROR* err)
{
  int iErr = ERR_NONE;

  // Range Check
  if ((end > (m_Size - 1)) ||
      (beg > (m_Size - 1))) {
    iErr = ERR_RANGE;
  }

  if (iErr == ERR_NONE) {
    // End cannot be before the beginning
    if (beg > end) {
      iErr = ERR_INVALID;
    }
    else {
      for (size_t count = 0; count <= (end - beg); count++) {
        erase(beg); // number of items.
      }
    }
  }

  SetErr(err, iErr);
};

template <typename T>
void List<T>::clear(ERROR* err)
{
  while (m_Size > 0) {
    erase(0, err); // erase first element until size is 0
  }
};

template <typename T>
void List<T>::push_front(T elem, ERROR* err)
{
  insert(0, elem, err);
};

template <typename T>
void List<T>::pop_front(T& elem, ERROR* err)
{
  get(0, elem, err);
  erase(0, err);
};

template <typename T>
void List<T>::push_back(T elem, ERROR* err)
{
  insert(m_Size, elem, err);
};

template <typename T>
void List<T>::pop_back(T& elem, ERROR* err)
{
  get(m_Size - 1, elem, err);
  erase(m_Size - 1, err);
};

template<typename T>
T List<T>::front()
{
  return (*this)[0];
}

template<typename T>
T List<T>::back()
{
  return (*this)[m_Size - 1];
}

#endif


/*
 * Vector_source.hpp
 *
 *  Created on: Jan 1, 2015
 *  Author: simplymac
 *  Description: Implementation of the Templated Vector class.
 *
 */

#ifndef VECTOR_SOURCE_HPP_
#define VECTOR_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Container;

#include <math.h>

template <typename T>
Vector<T>::Vector(size_t initialSize)
: Container<T>()
, p_Data(0)
, m_Size(0)
, m_ArraySize(0)
, m_TotalCapacity(initialSize)
, m_ArrayCapacity(DEFAULT_ARRAY_SIZE)
, m_ExpansionRate(initialSize)
{
  p_Data = new T*[DEFAULT_ARRAY_SIZE];
  p_Data[0] = new T[initialSize];
  m_ArraySize = 1;
};

template <typename T>
Vector<T>::~Vector()
{
  for (size_t i = 0; i < m_ArraySize; i++) {
    delete[] p_Data[i];
  }
  delete[] p_Data;
};

template <typename T>
Vector<T>::Vector(const Vector<T> &other)
: Container<T>()
, p_Data(0)
, m_Size(0)
, m_ArraySize(0)
, m_ExpansionRate(other.rate())
{
  m_TotalCapacity = other.capacity();
  m_ArrayCapacity = (m_TotalCapacity / m_ExpansionRate) + 1;
  p_Data = new T*[m_ArrayCapacity];

  for (size_t i = 0; i < m_ArrayCapacity; i++) {
    p_Data[i] = new T[m_ExpansionRate];
    m_ArraySize++;
  }

  // Insert Data
  for (size_t i = 0; i < other.size(); i++) {
    insert(i, other[i]);
  }
};

template <typename T>
size_t Vector<T>::size() const
{
  return m_Size;
};

template <typename T>
size_t Vector<T>::capacity() const
{
  return m_TotalCapacity;
};

template <typename T>
size_t Vector<T>::rate() const
{
  return m_ExpansionRate;
};

template <typename T>
bool Vector<T>::empty() const
{
  return m_Size == 0;
};

template <typename T>
T& Vector<T>::operator[](const size_t index)
{
  if (index < m_Size) {
    return p_Data[index / m_ExpansionRate][index % m_ExpansionRate];
  }
  else {
    return p_Data[0][0];
  }
}

template <typename T>
T& Vector<T>::operator[](const size_t index) const
{
  if (index < m_Size) {
    return p_Data[index / m_ExpansionRate][index % m_ExpansionRate];
  }
  else {
    return p_Data[0][0];
  }
}

template <typename T>
void Vector<T>::swap(size_t index1, size_t index2, ERROR* err)
{
  SetErr(err, ERR_RANGE);

  if ((index1 < m_Size) && (index2 < m_Size) && (index1 != index2)) {
    T tempBuffer = p_Data[index2 / m_ExpansionRate][index2 % m_ExpansionRate];
    p_Data[index2 / m_ExpansionRate][index2 % m_ExpansionRate] = p_Data[index1 / m_ExpansionRate][index1 % m_ExpansionRate];
    p_Data[index1 / m_ExpansionRate][index1 % m_ExpansionRate] = tempBuffer;

    SetErr(err, ERR_NONE);
  }
}

template <typename T>
void Vector<T>::insert(size_t pos, T elem, ERROR* err)
{
  // Check for expansion
  if (m_Size == m_TotalCapacity) {
    ExpandStorage();
  }

  // Insert Element
  if (pos == m_Size) // Last element -- added to the end of the list
  {
    p_Data[pos / m_ExpansionRate][pos % m_ExpansionRate] = elem;
    m_Size++;
    SetErr(err, ERR_NONE);
  }
  else if (pos < m_Size) {
    // Shift Data -- last element first
    for (size_t i = m_Size; i > pos; i--) {
      p_Data[i / m_ExpansionRate][i % m_ExpansionRate] = p_Data[(i - 1) / m_ExpansionRate][(i - 1) % m_ExpansionRate];
    }

    p_Data[pos / m_ExpansionRate][pos % m_ExpansionRate] = elem;
    m_Size++;
    SetErr(err, ERR_NONE);
  }
  else {
    SetErr(err, ERR_RANGE);
  }
}

template <typename T>
void Vector<T>::set(size_t pos, T elem, ERROR* err)
{
  if (m_Size > 0) {
    SetErr(err, ERR_RANGE);
    if (pos < m_Size) {
      p_Data[pos / m_ExpansionRate][pos % m_ExpansionRate] = elem;
      SetErr(err, ERR_NONE);
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

template <typename T>
void Vector<T>::get(size_t pos, T& elem, ERROR* err)
{
  if (m_Size > 0) {
    SetErr(err, ERR_RANGE);
    if (pos < m_Size) {
      elem = p_Data[pos / m_ExpansionRate][pos % m_ExpansionRate];
      SetErr(err, ERR_NONE);
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }
}

template <typename T>
void Vector<T>::erase(size_t pos, ERROR* err)
{
  SetErr(err, ERR_EMPTY);

  if (m_Size > 0) {
    if (pos < m_Size) {
      // Last Element in the Vector
      if (pos == (m_Size - 1)) {
        m_Size--;
      }
      else // beginning and middle shift
      {
        // Shift all of the elements back, to overwrite and erase the element.
        for (size_t i = pos; i < m_Size - 1; i++) {
          p_Data[i / m_ExpansionRate][i % m_ExpansionRate] = p_Data[(i + 1) / m_ExpansionRate][(i + 1) % m_ExpansionRate];
        }
        // adjust size
        m_Size--;
      }
      SetErr(err, ERR_NONE);
    }
    else {
      SetErr(err, ERR_RANGE);
    }
  }
}

template <typename T>
void Vector<T>::erase(size_t beg, size_t end, ERROR* err)
{
  int iErr = ERR_NONE;

  if (m_Size > 0) {
    // Range Check
    if ((end > (m_Size - 1)) ||
        (beg > (m_Size - 1))) {
      iErr = ERR_RANGE;
    }

    if (iErr == ERR_NONE) {
      // End cannot be before the beginning
      if (beg > end) {
        iErr = ERR_INVALID;
      }
      else {
        for (size_t count = 0; count <= (end - beg); count++) {
          erase(beg); // number of items.
        }
      }
    }
  }
  else {
    SetErr(err, ERR_EMPTY);
  }

  SetErr(err, iErr);
};

template <typename T>
void Vector<T>::clear(ERROR* err)
{
  m_Size = 0; // Dont waste time removing elements. 
}

template <typename T>
void Vector<T>::push_front(T elem, ERROR* err)
{
  insert(0, elem, err);
};

template <typename T>
void Vector<T>::pop_front(T& elem, ERROR* err)
{
  get(0, elem, err);
  erase(0, err);
};

template <typename T>
void Vector<T>::push_back(T elem, ERROR* err)
{
  insert(m_Size, elem, err);
};

template <typename T>
void Vector<T>::pop_back(T& elem, ERROR* err)
{
  get(m_Size - 1, elem, err);
  erase(m_Size - 1, err);
};

template<typename T>
T Vector<T>::front()
{
  return p_Data[0][0];
}

template<typename T>
T Vector<T>::back()
{
  return p_Data[(m_Size - 1) / m_ExpansionRate][(m_Size - 1) % m_ExpansionRate];
}

template<typename T>
void Vector<T>::ExpandStorage()
{
  // Expand the Pointer Array if needed.
  if (m_ArrayCapacity == (m_Size / m_ExpansionRate)) {
    T** tempBuffer = p_Data;

    // Create new memory buffer and transfer data.
    p_Data = new T*[m_ArrayCapacity * DEFAULT_EXPANSION_RATE];

    // Copy Array Data
    for (size_t i = 0; i < m_ArrayCapacity; i++) {
      p_Data[i] = tempBuffer[i];
    }

    // Delete old array
    delete[] tempBuffer;
    m_ArrayCapacity *= DEFAULT_EXPANSION_RATE;
  }

  // Add new row of data
  p_Data[m_Size / m_ExpansionRate] = new T[m_ExpansionRate];
  m_ArraySize++;
  m_TotalCapacity += m_ExpansionRate;
}

#endif


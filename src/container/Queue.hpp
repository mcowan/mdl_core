/// @file container/Queue.hpp
///
/// @brief Defines the Queue Stack.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Queue Container.  Based
/// on the basic container interface.


#ifndef Queue_HPP_
#define Queue_HPP_

#include <container/ContainerIf.hpp>
#include <container/List.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Containers classes.
  ///
  namespace ns_Container
  {
    /// @details
    /// This class is a basic Queue class that functions as a FIFO
    /// container.
    /// @param T type of data in container.
    /// 

    template <typename T>
    class Queue : public Container<T>
    {
    public:
      /// @details Creates a Basic Empty Templated Queue.
      ///
      Queue();

      /// @details Templated Queue Copy Constructor.
      ///
      /// @param[in] other The Queue to copy.
      ///
      Queue(const Queue<T> &other);

      /// @details Templated Queue Decontructor.
      ///
      ~Queue();

      /// @details Number of Elements in the Queue.
      ///
      /// @return The current number of elements.
      ///
      size_t size() const;

      /// @details Checks if the container is empty.
      ///
      /// @return True, if empty; False, otherwise.
      ///
      bool empty() const;

      /// @details Access Operator.
      ///
      /// @param[in] index 	Index of object to access.  Zero based access in 
      /// 									the queue with the zero object being the next
      ///                   object to be popped of the queue.
      ///
      /// @return Object being accessed in the Queue.
      ///
      T& operator[](const size_t index);

      /// @details Constant Access Operator.
      ///
      /// @param[in] index  Index of object to access.  Zero based access in the
      /// queue with the zero object being the next object to be popped of the
      /// queue.
      ///
      /// @return Object being accessed in the Queue.
      ///
      T& operator[](const size_t index) const;

      /// @details Swaps two Elements in the Queue.
      ///  
      /// @param [in] index1 Element index one.
      /// @param [in] index2 Element index two.
      /// @param [out] err Optional error code.
      ///
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      /// @details Removes all elements in the Queue.
      ///  
      /// @param [out] err Optional error code.
      ///
      void clear(ERROR* err = 0);

      /// @brief Returns a copy of the first element in the Queue.
      /// 
      /// @return An element.
      ///
      T top();

      /// @brief Appends an element to the front of the Queue.
      /// 
      /// @param [in] elem Element to be appended to the Queue.
      /// @param [out] err Optional error code.
      ///
      void push(T elem, ERROR* err = 0);

      /// @brief Removes and returns the first element in the Queue.
      /// 
      /// @param [in] elem Returns the element being removed.
      /// @param [out] err Optional error code.
      ///
      void pop(T& elem, ERROR* err = 0);

    private:
      List<T> m_Data; ///> Underlysing List used for the Queue.

    }; // End of Queue
  } /* End of Container */
} /* End of MDL */

// Queue Implementation
#include <container/Queue_source.hpp>

#endif 

/* 
 * File:   packet.hpp
 * Author: Orion
 *
 * Created on January 1, 2016, 1:34 PM
 */

#ifndef PACKET_HPP
#define	PACKET_HPP

#include <Types.hpp>
#include <common/Error.hpp>
#include <common/CRC16.hpp>
#include <buffer/BufferStream.hpp>

namespace ns_MDL
{
class Packet
{
public:
  Packet(uint32_t header, uint32_t payloadSize);
  Packet(const Packet& orig);
  virtual ~Packet();
  
  uint8_t* Serialize(ERROR* error = 0) const;
  void Deserialize(uint8_t* buffer, uint32_t size, ERROR* error = 0);
  
  bool Valid() const;
  uint32_t Header() const;
  uint16_t Size() const;
  uint16_t CRC() const;
  
  void SetPayload(uint8_t* buffer, uint32_t size, ERROR* error = 0);
  void GetPayload(uint8_t* buffer, uint32_t size, ERROR* error = 0);
  uint16_t PayloadSize() const;
  
  bool operator==(const Packet& rhs);
  bool operator!=(const Packet& rhs);
  
protected:
  static const uint32_t DEFAULT_SIZE = 8;
  static const uint32_t HEADER_POS = 0;
  static const uint32_t SIZE_POS = 4;
  static const uint32_t CRC_POS = 6;
  static const uint32_t PAYLOAD_POS = 8;
  
  void UpdateCRC();
  
  BufferStream* m_pBufferStream;
  
private:
  uint16_t CalculateCRC() const;
};
}
#endif	/* PACKET_HPP */


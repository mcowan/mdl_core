/* 
 * File:   TEST_packet.hpp
 * Author: Orion
 *
 * Created on January 2, 2016, 10:28 AM
 */

#ifndef TEST_PACKET_HPP
#define	TEST_PACKET_HPP

#include <gtest/gtest.h>
#include <packet/packet.hpp>

using namespace ns_MDL;


class TEST_packet : public ::testing::Test
{
public:
  TEST_packet();
  virtual ~TEST_packet();
  
  virtual void SetUp();
  virtual void TearDown();


  Packet* m_pPacket;
  
};

#endif	/* TEST_PACKET_HPP */


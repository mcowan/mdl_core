/* 
 * File:   TEST_packet.cpp
 * Author: Orion
 * 
 * Created on January 2, 2016, 10:28 AM
 */

#include "TEST_packet.hpp"
#include <gtest/gtest.h>
#include <gmock/gmock.h>


#define PAYLOAD_SIZE 12

using namespace ns_MDL;

TEST_packet::TEST_packet()
{
}

TEST_packet::~TEST_packet()
{
}

void TEST_packet::SetUp()
{
  m_pPacket = new Packet(0xAABBCCDD, PAYLOAD_SIZE);
}

void TEST_packet::TearDown()
{
  delete m_pPacket;
}


TEST_F(TEST_packet, Stack)
{
  ASSERT_TRUE(m_pPacket != 0);
}

TEST_F(TEST_packet, Size)
{
  ASSERT_EQ(m_pPacket->Size(), PAYLOAD_SIZE + 8); // 8 = Header Size
  ASSERT_EQ(m_pPacket->PayloadSize(), PAYLOAD_SIZE);
}

TEST_F(TEST_packet, Header)
{
  ASSERT_EQ(m_pPacket->Header(), 0xAABBCCDD);
}

TEST_F(TEST_packet, CRC)
{
  ASSERT_TRUE(m_pPacket->CRC()!= 0);
}

TEST_F(TEST_packet, Payload)
{
  uint8_t buffer[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  uint8_t tempBuffer[12] ={0};
  ERROR err = ERR_NONE;
  
  m_pPacket->SetPayload(buffer, 12, &err);
  
  ASSERT_EQ(err, ERR_NONE);
  
  m_pPacket->GetPayload(tempBuffer, 12, &err);
  ASSERT_EQ(err, ERR_NONE);
  
  for(unsigned int i = 0; i < 12; i++)
  {
    ASSERT_EQ(buffer[i], tempBuffer[i]);
  }
}

TEST_F(TEST_packet, CopyConstructor)
{
  uint8_t buffer[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  uint8_t tempBuffer[12] ={0};
  ERROR err = ERR_NONE;
  
  m_pPacket->SetPayload(buffer, 12, &err);
  
  Packet copy(*m_pPacket);
  
  ASSERT_TRUE(*m_pPacket == copy);
  ASSERT_FALSE(*m_pPacket != copy);
}

TEST_F(TEST_packet, ValidCRC)
{
  uint8_t buffer[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  uint8_t tempBuffer[12] ={0};
  ERROR err = ERR_NONE;
  m_pPacket->SetPayload(buffer, 12, &err);
  
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(m_pPacket->Valid());
}

TEST_F(TEST_packet, Equality)
{
   uint8_t buffer[12] = {1,2,3,4,5,6,7,8,9,10,11,12};
  uint8_t tempBuffer[12] ={0};
  ERROR err = ERR_NONE;
  Packet packet2(0xAABBCCDD, PAYLOAD_SIZE);
  
  m_pPacket->SetPayload(buffer, 12, &err);
  ASSERT_EQ(err, ERR_NONE);
  
  // verify packets are different
  ASSERT_FALSE(*m_pPacket == packet2);
  
  // Configure 2nd packet with desirializing the first packet
  packet2.Deserialize(m_pPacket->Serialize(), m_pPacket->Size(), &err);
  ASSERT_EQ(err, ERR_NONE);
  
  // Verify the packets are the same now
  ASSERT_TRUE(*m_pPacket == packet2);
  ASSERT_FALSE(*m_pPacket != packet2);
}
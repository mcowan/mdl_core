/* 
 * File:   Packet.cpp
 * Author: Orion
 * 
 * Created on January 1, 2016, 1:34 PM
 */

#include "Packet.hpp"
#include <common/CRC16.hpp>
#include <common/Endian.hpp>

using namespace ns_MDL;

#include <stdio.h>


Packet::Packet(uint32_t header, uint32_t payloadSize)
{
  m_pBufferStream = new BufferStream(payloadSize + DEFAULT_SIZE);
  m_pBufferStream->Write32((header));
  m_pBufferStream->Write16(payloadSize);
  UpdateCRC();
}

Packet::Packet(const Packet& orig)
{
  m_pBufferStream = new BufferStream(orig.Serialize(), orig.Size());
}

Packet::~Packet()
{
  delete m_pBufferStream;
}

uint8_t* Packet::Serialize(ERROR* error) const
{
  return m_pBufferStream->Serialize(error); 
}

void Packet::Deserialize(uint8_t* buffer, uint32_t size, ERROR* error)
{
  m_pBufferStream->Deserialize(buffer, size, error);
}

bool Packet::Valid() const
{
  uint16_t crc = CRC();
  uint16_t calc_crc = this->CalculateCRC();
  
  return (crc == calc_crc);
}

uint16_t Packet::Size() const
{
  return m_pBufferStream->Size();
}

uint32_t Packet::Header() const
{
  uint32_t header = 0;
  Endian::Read_Array_NtoH32(m_pBufferStream->Serialize(), HEADER_POS, header);
  return header;
}

uint16_t Packet::CRC() const
{
  uint16_t crc = 0;
  Endian::Read_Array_NtoH16(m_pBufferStream->Serialize(), CRC_POS, crc);
  return crc;
}

uint16_t Packet::PayloadSize() const
{
  uint16_t size = 0;
  Endian::Read_Array_NtoH16(m_pBufferStream->Serialize(), SIZE_POS, size);
  return size;
}

void Packet::UpdateCRC()
{
  Endian::Write_Array_HtoN16(m_pBufferStream->Serialize(), CRC_POS, CalculateCRC());
}

void Packet::SetPayload(uint8_t* buffer, uint32_t size, ERROR* error)
{
  m_pBufferStream->SetWritePos(PAYLOAD_POS);
  m_pBufferStream->Write(buffer, size, error);
  this->UpdateCRC();
}

void Packet::GetPayload(uint8_t* buffer, uint32_t size, ERROR* error)
{
  m_pBufferStream->SetReadPos(PAYLOAD_POS);
  m_pBufferStream->Read(buffer, size, error);
}

uint16_t Packet::CalculateCRC() const
{
  uint16_t calc_crc = CRC16::CalcCRC16(0, m_pBufferStream->Serialize(), 6); // Header + Size
  calc_crc = CRC16::CalcCRC16(calc_crc, &(m_pBufferStream->Serialize()[PAYLOAD_POS]), Size()-8);
  return calc_crc;
}

bool Packet::operator==(const Packet& rhs)
{
  bool toReturn = false;
  if (this->Size() == rhs.Size()) 
  { 
    toReturn = true;
    for (size_t i = 0; i < this->Size(); i++) {
      if (this->Serialize()[i] != rhs.Serialize()[i]) {
        toReturn = false;
        break;
      }
    }
  }

  return toReturn;
};

bool Packet::operator!=(const Packet& rhs)
{
  return !(*this == rhs);
};
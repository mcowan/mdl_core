/// @file common/MemOperations.hpp
///
/// @brief  General Memory operations that are normally found in the C-String
/// Libraries.
///
/// @authors mjc
/// @date 05-31-14
///

#ifndef MEM_OPERATIONS_HPP_
#define MEM_OPERATIONS_HPP_

#include <Types.hpp>
#include <common/Error.hpp>

/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{
  //class MemOp
  //{
  //	public:
  ///	@brief Sets a memory array of a given length to a specific value.
  ///
  ///	@param[in]	dst			Location of memory array.
  ///	@param[in]	val 		Value to set the memory too.
  ///	@param[in]	length	Number of bytes to set in memory.
  ///	@param[out]	err			Any errors.
  ///
  void MemSet(void* dst, int val, size_t length, ERROR* err = 0);

  ///	@brief Copys a given number of bytes from the source location to
  ///	the given destination.  Memcopy will copy memory from one location
  ///	to another without the use of another buffer.
  ///
  ///	@param[in]	dst			Location to copy the memory to.
  ///	@param[in]  src 		Location of the memory array to copy.
  ///	@param[in]	length	Number of bytes to copy in memory.
  ///	@param[out]	err			Any errors.
  ///
  void MemCopy(void* dst, const void* src, size_t length, ERROR* err = 0);

  ///	@brief Swaps the given number of bytes in the two sources.  This
  ///	implementation utilizes an XOR swap technique to avoid the use of
  ///	another memory buffer.
  ///
  ///	@param[in]	src1		Location to copy the memory to.
  ///	@param[in]  src2 		Location of the memory array to copy.
  ///	@param[in]	length	Number of bytes to copy in memory.
  ///	@param[out]	err			Any errors.
  ///
  void MemSwap(void* src1, void* src2, size_t length, ERROR* err = 0);

  ///	@brief Moves the memory from the source location to a destination
  ///	location with the use of a memory buffer.
  ///
  ///	@param[in]	dst			Location to copy the memory to.
  ///	@param[in]  src 		Location of the memory array to copy.
  ///	@param[in]	length	Number of bytes to copy in memory.
  ///	@param[out]	err			Any errors.
  ///
  void MemMove(void* dst, const void*src, size_t length, ERROR* err = 0);


  int MemCompare(const void* buffer1, const void* buffer2, size_t length, ERROR* err = 0);

  size_t StrLen(const void* str, ERROR* err = 0);

  void StrCopy(void* dst, const char* src, size_t length, ERROR* err = 0);

};


#endif

/// @file LOG.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A basic LOGger that will allow you to LOG to
/// a file, console, or out of a serial connection.

#include <common/Log.hpp>
#include <stdio.h>
#include <Clock.hpp>
#include <common/MemOperations.hpp>

using namespace ns_MDL;
using namespace ns_Datatype;

// Initialize Static Variables
bool LOG::m_EnableTimeStamp = false;
bool LOG::m_EnableDateStamp = false;
Vector<DeviceIF*> LOG::devices;
Clock LOG::m_Clock;

void
LOG::Print(const char* str)
{
  CString temp(str);
  temp += "\n";
  for (size_t i = 0; i < devices.size(); i++) {
    devices[i]->write(temp.c_str(), StrLen(str));
  }
};

void
LOG::Print(CString str)
{
  str += "\n";
  for (size_t i = 0; i < devices.size(); i++) {
    devices[i]->write(str.c_str(), str.size());
  }
};

void
LOG::Message(const char* str)
{
  CString temp("[");
  if (!m_EnableTimeStamp && !m_EnableDateStamp) {
    LOG::Print(str);
  }
  else {
    if (m_EnableTimeStamp && !m_EnableDateStamp) {
      temp += LOG::m_Clock.Time();
    }
    else if (!m_EnableTimeStamp && m_EnableDateStamp) {
      temp += LOG::m_Clock.Date();
    }
    else {
      temp += LOG::m_Clock.DateTime();
    }

    temp += "] ";
    temp += str;
    temp += "\n";

    for (size_t i = 0; i < devices.size(); i++) {
      devices[i]->write(temp.c_str(), temp.size());
    }
  }
};

void
LOG::Message(CString str)
{
  Message(str.c_str());
};

void
LOG::Error(const char* str)
{
  CString temp("[");

  if (!m_EnableTimeStamp && !m_EnableDateStamp) {
    LOG::Print(str);
  }
  else {
    if (m_EnableTimeStamp && !m_EnableDateStamp) {
      temp += LOG::m_Clock.Time();
    }
    else if (!m_EnableTimeStamp && m_EnableDateStamp) {
      temp += LOG::m_Clock.Date();
    }
    else {
      temp += LOG::m_Clock.DateTime();
    }

    temp += "] ERROR: ";
    temp += str;
    temp += "\n";

    for (size_t i = 0; i < devices.size(); i++) {
      devices[i]->write(temp.c_str(), temp.size());
    }
  }
};

void
LOG::Error(CString str)
{
  Error(str.c_str());
};

void
LOG::Warning(const char* str)
{
  CString temp("[");

  if (!m_EnableTimeStamp && !m_EnableDateStamp) {
    LOG::Print(str);
  }
  else {
    if (m_EnableTimeStamp && !m_EnableDateStamp) {
      temp += LOG::m_Clock.Time();
    }
    else if (!m_EnableTimeStamp && m_EnableDateStamp) {
      temp += LOG::m_Clock.Date();
    }
    else {
      temp += LOG::m_Clock.DateTime();
    }

    temp += "] WARNING: ";
    temp += str;
    temp += "\n";

    for (size_t i = 0; i < devices.size(); i++) {
      devices[i]->write(temp.c_str(), temp.size());
    }
  }
};

void
LOG::Warning(CString str)
{
  Warning(str.c_str());
};

void
LOG::AddOutputDevice(DeviceIF* dev, ERROR* error)
{
  // verify the device doesn't already exist on the list.
  for (size_t i = 0; i < devices.size(); i++) {
    if (devices[i] == dev) {
      SetErr(error, ERR_DUPLICATE);
      return;
    }
  }

  devices.push_back(dev, error);
};

void
LOG::RemoveAllDevices()
{
  devices.clear();
};

void
LOG::EnableDateStamp(bool enable)
{
  m_EnableDateStamp = enable;
};

void
LOG::EnableTimeStamp(bool enable)
{
  m_EnableTimeStamp = enable;
};

bool
LOG::IsDateStampEnabled()
{
  return m_EnableDateStamp;
};

bool
LOG::IsTimeStampEnabled()
{
  return m_EnableTimeStamp;
};


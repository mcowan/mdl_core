/*
 *  @file Error.hpp
 * @brief  A class that defines the error codes for the Libraries, function
 * to set error pointers,  and a function that returns the error message for a
 * error value.
 */

#ifndef __ERROR_HPP__
#define __ERROR_HPP__

#include <Types.hpp>

/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{
  /// @brief Enumeration of all Common MDL Error codes

  enum ErrorCode
  {
    ERR_NONE = 0, /*!< No Error. 		*/
    ERR_RANGE = -1, /*!< Range Error.	 	*/
    ERR_TYPE = -2, /*!< Type Error. 		*/
    ERR_UNSPEC = -3, /*!< Unspecified Error. 	*/
    ERR_TIMEOUT = -4, /*!< Timeout Error. 	*/
    ERR_ACCESS = -5, /*!< Access Error. 		*/
    ERR_INVALID = -6, /*!< Invalid Error. 	*/
    ERR_ALIGNMENT = -7, /*!< Alignment Error. 	*/
    ERR_NOT_INITIALIZED = -8, /*!< Error Not Initialized.*/
    ERR_NOT_FOUND = -9, /*!< Error Not Found. 	*/
    ERR_NOT_IMPLEMENTED = -10, /*!< Error Not Implemented.*/
    ERR_HARDWARE = -11, /*!< Hardware Error. 	*/
    ERR_FULL = -12, /*!< Error Full. 		*/
    ERR_EMPTY = -13, /*!< Error Empty. 		*/
    ERR_WRITE = -14, /*!< Write Error. 		*/
    ERR_READ = -15, /*!< Read Error. 		*/
    ERR_END_OF_FILE = -16, /*!< End of File Error. 	*/
    ERR_NO_FILE_OPEN = -17, /*!< No File Open Error. 	*/
    ERR_VERSION = -18, /*!< Version Error. 	*/
    ERR_DUPLICATE = -19, /*!< Duplicate Error. 	*/
    ERR_PERMISSION = -20, /*!< Permission Error. 	*/
    ERR_RESOURCES = -21, /*!< Resource Error. 	*/
    ERR_UNDEFINED = -22, /*!< Undefined Error.      */
    ERR_BUSY = -23 /*!< Busy Error.           */
  };

  /// @brief Set an error code value to an error pointer.
  ///	@param[out]	error	Error Pointer.
  ///	@param[in]	value	Error value to set the pointer too.
  void SetErr(ERROR* error, ERROR value);

  ///	@brief Gets an error message relating to the given error code.
  ///
  ///	@param[in]	err	Error code.
  ///
  ///	@returns Error code string.
  const char* GetErrMsg(ERROR* error);
};

#endif  // ERROR_HPP_

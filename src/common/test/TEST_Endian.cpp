
/*
 * TEST_Endian.cpp
 *
 *  Created on:   May 31, 2013
 *  Author:         mcowan
 *  Description:  Tests for the Error.hpp class.
 */

#include <common/Endian.hpp>
#include <gtest/gtest.h>
#include <netinet/in.h>
using namespace ns_MDL;

static const uint8_t MAX_ARRAY_SIZE = 16;  
	  
// Big Endian and Network are the same
uint8_t NetworkByteOrder_8[MAX_ARRAY_SIZE] =  { 0x00, 0x01, 0x02, 0x03,
                                                0x04, 0x05, 0x06, 0x07,
                                                0x08, 0x09, 0x0A, 0x0B,
                                                0x0C, 0x0D, 0x0E, 0x0F }; 
											  
uint16_t NetworkByteOrder_16[MAX_ARRAY_SIZE/2] =  { 0x0001, 0x0203,
                                                    0x0405, 0x0607,
                                                    0x0809, 0x0A0B,
                                                    0x0C0D, 0x0E0F }; 	

uint16_t LittleEndianHostByteOrder_16[MAX_ARRAY_SIZE/2] = { 0x0100, 0x0302,
                                                            0x0504, 0x0706,
                                                            0x0908, 0x0B0A,
                                                            0x0D0C, 0x0F0E }; 													
											  
uint32_t NetworkByteOrder_32[MAX_ARRAY_SIZE/4] =  { 0x00010203,
                                                    0x04050607,
                                                    0x08090A0B,
                                                    0x0C0D0E0F }; 
													
uint32_t LittleEndianHostByteOrder_32[MAX_ARRAY_SIZE/4] = { 0x03020100, 
                                                            0x07060504, 
                                                            0x0B0A0908, 
                                                            0x0F0E0D0C };  
											 
													
uint64_t NetworkByteOrder_64[MAX_ARRAY_SIZE/8] =  { 0x0001020304050607ULL,
                                                    0x08090A0B0C0D0E0FULL };												  
										 
uint64_t LittleEndianHostByteOrder_64[MAX_ARRAY_SIZE/8] = { 0x0706050403020100ULL, 
                                                            0x0F0E0D0C0B0A0908ULL };


TEST(Endian, IsBigEndian)
{
  printf("System Endianess: %s\n", Endian::IsBigEndian() ? "Big Endian" : 
                                                           "Little Endian");
}

TEST(Endian, Read_Write_8Bit)
{
  uint8_t array[MAX_ARRAY_SIZE] = {0};
  uint8_t value;
  
  // Write the Array
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Write_Array_HtoN8(array, i, NetworkByteOrder_8[i]));
  }

  // Read the array and verify
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Read_Array_NtoH8(array, i, value));
    ASSERT_EQ(value, NetworkByteOrder_8[i]);
  }
}

TEST(Endian, Read_Write_16Bit)
{
  uint8_t array[MAX_ARRAY_SIZE] = {0};
  uint16_t value;
  
  // Write the Array
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/2; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Write_Array_HtoN16(array, i*2, NetworkByteOrder_16[i]));
  }  

  
  // Read the array and verify
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/2; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Read_Array_NtoH16(array, i*2, value));
    ASSERT_EQ(value, NetworkByteOrder_16[i]);
  }
}


TEST(Endian, Read_Write_32Bit)
{
  uint8_t array[MAX_ARRAY_SIZE] = {0};
  uint32_t value;
  
  // Write the Array
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/4; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Write_Array_HtoN32(array, i*4, NetworkByteOrder_32[i]));
  }  

  
  // Read the array and verify
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/4; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Read_Array_NtoH32(array, i*4, value));
    ASSERT_EQ(value, NetworkByteOrder_32[i]);
  }
}

TEST(Endian, Read_Write_64Bit)
{
  uint8_t array[MAX_ARRAY_SIZE] = {0};
  uint64_t value;
  
  // Write the Array
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/8; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Write_Array_HtoN64(array, i*8, NetworkByteOrder_64[i]));
  }
  
  // Read the array and verify
  for(unsigned int i = 0; i < MAX_ARRAY_SIZE/8; i++)
  {
    ASSERT_EQ(ERR_NONE, Endian::Read_Array_NtoH64(array, i*8, value));
    ASSERT_EQ(value, NetworkByteOrder_64[i]);
  }
}

TEST(Endian, _8_BIT_)
{
  for(int i = 0; i < MAX_ARRAY_SIZE; i++)
  {
    // NetworkByteOrder_8[MAX_ARRAY_SIZE]
    ASSERT_EQ(Endian::HtoN8(NetworkByteOrder_8[i]), NetworkByteOrder_8[i]);
    ASSERT_EQ(Endian::NtoH8(NetworkByteOrder_8[i]), NetworkByteOrder_8[i]);
  }
  
}

TEST(Endian, _16_BIT_)
{
  for(int i = 0; i < MAX_ARRAY_SIZE/2; i++)
  {
    // NetworkByteOrder_8[MAX_ARRAY_SIZE]
    ASSERT_EQ(Endian::HtoN16(NetworkByteOrder_16[i]), htons(NetworkByteOrder_16[i]));
    ASSERT_EQ(Endian::NtoH16(NetworkByteOrder_16[i]), ntohs(NetworkByteOrder_16[i]));
  }
}

TEST(Endian, _32_BIT_)
{
  for(int i = 0; i < MAX_ARRAY_SIZE/4; i++)
  {
    // NetworkByteOrder_8[MAX_ARRAY_SIZE]
    ASSERT_EQ(Endian::HtoN32(NetworkByteOrder_32[i]), htonl(NetworkByteOrder_32[i]));
    ASSERT_EQ(Endian::NtoH32(NetworkByteOrder_32[i]), ntohl(NetworkByteOrder_32[i]));
  }
}



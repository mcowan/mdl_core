/*
 * TEST_Error.hpp
 *
 *  Created on:   May 31, 2013
 *  Author:         mcowan
 *  Description:  Tests for the Error.hpp class.
 */

#include <common/Error.hpp>
#include <gtest/gtest.h>
using namespace ns_MDL;

TEST(Error, SetErr)
{
  int err = ERR_NONE;

  // Error None
  SetErr(&err, ERR_NONE);
  ASSERT_EQ(err, ERR_NONE);

  // Error Range
  SetErr(&err, ERR_RANGE);
  ASSERT_EQ(err, ERR_RANGE);

  // Error Type
  SetErr(&err, ERR_TYPE);
  ASSERT_EQ(err, ERR_TYPE);

  // Error Unspecified
  SetErr(&err, ERR_UNSPEC);
  ASSERT_EQ(err, ERR_UNSPEC);

  // Error Timeout
  SetErr(&err, ERR_TIMEOUT);
  ASSERT_EQ(err, ERR_TIMEOUT);

  // Error Access
  SetErr(&err, ERR_ACCESS);
  ASSERT_EQ(err, ERR_ACCESS);

  // Error Invalid
  SetErr(&err, ERR_INVALID);
  ASSERT_EQ(err, ERR_INVALID);

  // Error Alignment
  SetErr(&err, ERR_ALIGNMENT);
  ASSERT_EQ(err, ERR_ALIGNMENT);

  // Error Not Initialized
  SetErr(&err, ERR_NOT_INITIALIZED);
  ASSERT_EQ(err, ERR_NOT_INITIALIZED);

  // Error Not Found
  SetErr(&err, ERR_NOT_FOUND);
  ASSERT_EQ(err, ERR_NOT_FOUND);

  // Error Not Implemented
  SetErr(&err, ERR_NOT_IMPLEMENTED);
  ASSERT_EQ(err, ERR_NOT_IMPLEMENTED);

  // Error Hardware
  SetErr(&err, ERR_HARDWARE);
  ASSERT_EQ(err, ERR_HARDWARE);

  // Error Full
  SetErr(&err, ERR_FULL);
  ASSERT_EQ(err, ERR_FULL);

  // Error Empty
  SetErr(&err, ERR_EMPTY);
  ASSERT_EQ(err, ERR_EMPTY);

  // Error Write
  SetErr(&err, ERR_WRITE);
  ASSERT_EQ(err, ERR_WRITE);

  // Error Read
  SetErr(&err, ERR_READ);
  ASSERT_EQ(err, ERR_READ);

  // Error End Of File
  SetErr(&err, ERR_END_OF_FILE);
  ASSERT_EQ(err, ERR_END_OF_FILE);

  // Error No File Open
  SetErr(&err, ERR_NO_FILE_OPEN);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  // Error Version
  SetErr(&err, ERR_VERSION);
  ASSERT_EQ(err, ERR_VERSION);

  // Error Duplicate
  SetErr(&err, ERR_DUPLICATE);
  ASSERT_EQ(err, ERR_DUPLICATE);

  // Error Permission
  SetErr(&err, ERR_PERMISSION);
  ASSERT_EQ(err, ERR_PERMISSION);

  // Error Resources
  SetErr(&err, ERR_RESOURCES);
  ASSERT_EQ(err, ERR_RESOURCES);
}

TEST(Error, Messages)
{
  int err = 0;

  // Error None
  SetErr(&err, ERR_NONE);
  ASSERT_STREQ(GetErrMsg(&err), "None");

  // Error Range
  SetErr(&err, ERR_RANGE);
  ASSERT_STREQ(GetErrMsg(&err), "Range");

  // Error Type
  SetErr(&err, ERR_TYPE);
  ASSERT_STREQ(GetErrMsg(&err), "Type");

  // Error Unspecified
  SetErr(&err, ERR_UNSPEC);
  ASSERT_STREQ(GetErrMsg(&err), "Unspecified");

  // Error Timeout
  SetErr(&err, ERR_TIMEOUT);
  ASSERT_STREQ(GetErrMsg(&err), "Timeout");

  // Error Access
  SetErr(&err, ERR_ACCESS);
  ASSERT_STREQ(GetErrMsg(&err), "Access");

  // Error Invalid
  SetErr(&err, ERR_INVALID);
  ASSERT_STREQ(GetErrMsg(&err), "Invalid");

  // Error Alignment
  SetErr(&err, ERR_ALIGNMENT);
  ASSERT_STREQ(GetErrMsg(&err), "Alignment");

  // Error Not Initialized
  SetErr(&err, ERR_NOT_INITIALIZED);
  ASSERT_STREQ(GetErrMsg(&err), "Not Initialized");

  // Error Not Found
  SetErr(&err, ERR_NOT_FOUND);
  ASSERT_STREQ(GetErrMsg(&err), "Not Found");

  // Error Not Implemented
  SetErr(&err, ERR_NOT_IMPLEMENTED);
  ASSERT_STREQ(GetErrMsg(&err), "Not Implemented");

  // Error Hardware
  SetErr(&err, ERR_HARDWARE);
  ASSERT_STREQ(GetErrMsg(&err), "Hardware");

  // Error Full
  SetErr(&err, ERR_FULL);
  ASSERT_STREQ(GetErrMsg(&err), "Full");

  // Error Empty
  SetErr(&err, ERR_EMPTY);
  ASSERT_STREQ(GetErrMsg(&err), "Empty");

  // Error Write
  SetErr(&err, ERR_WRITE);
  ASSERT_STREQ(GetErrMsg(&err), "Write");

  // Error Read
  SetErr(&err, ERR_READ);
  ASSERT_STREQ(GetErrMsg(&err), "Read");

  // Error End Of File
  SetErr(&err, ERR_END_OF_FILE);
  ASSERT_STREQ(GetErrMsg(&err), "End of File");

  // Error No File Open
  SetErr(&err, ERR_NO_FILE_OPEN);
  ASSERT_STREQ(GetErrMsg(&err), "No File Open");

  // Error Version
  SetErr(&err, ERR_VERSION);
  ASSERT_STREQ(GetErrMsg(&err), "Version");

  // Error Duplicate
  SetErr(&err, ERR_DUPLICATE);
  ASSERT_STREQ(GetErrMsg(&err), "Duplicate");

  // Error Permission
  SetErr(&err, ERR_PERMISSION);
  ASSERT_STREQ(GetErrMsg(&err), "Permission");

  // Error Resources
  SetErr(&err, ERR_RESOURCES);
  ASSERT_STREQ(GetErrMsg(&err), "Resources");

  // Error Resourses
  SetErr(&err, 100);
  ASSERT_STREQ(GetErrMsg(&err), "Undefined Error Code!!");
}

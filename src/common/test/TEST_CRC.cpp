
/*
 * TEST_CRC.cpp
 * 
 */

#include <gtest/gtest.h>
#include <common/CRC16.hpp>


//
//CRC Name : CRC-16
//Width : 16 Bits
//Polynomial Used : 1189 (hex)
//Seed Value : FFFF (hex)
//Reflected Input/Output : No
//Exclusive OR Output : No
//Test CRC for string "123456789" : 5502 (hex)

TEST(CRC, CRC16)
{ 
  uint8_t val[] = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
  ASSERT_EQ(CRC16::CalcCRC16(0xFFFF, val, 9), 0x5502 );
}
/*
 * TEST_Error.hpp
 *
 *  Created on:   May 31, 2013
 *  Author:         mcowan
 *  Description:  Tests for the Error.hpp class.
 */

#include <common/MemOperations.hpp>
#include <common/Error.hpp>
#include <gtest/gtest.h>
#include <iostream>
using namespace std;

using namespace ns_MDL;

static const unsigned char onesBuffer[] = {1, 1, 1, 1, 1};
static const unsigned char zeroBuffer[] = {0, 0, 0, 0, 0};
static const unsigned char prtlBuffer[] = {0, 1, 1, 1, 0};

// Test a basic Assert.

TEST(MemOperations, MemSet)
{
  int err = ERR_NONE;
  unsigned char MemSetBuffer[5];

  // Zero's buffer
  MemSet(MemSetBuffer, 0, 5, &err);
  ASSERT_EQ(err, ERR_NONE);
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(MemSetBuffer[i], zeroBuffer[i]);
  }

  // Ones Buffer
  MemSet(MemSetBuffer, 1, 5, &err);
  ASSERT_EQ(err, ERR_NONE);
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(MemSetBuffer[i], onesBuffer[i]);
  }

  // Zero's Buffer
  MemSet(MemSetBuffer, 0, 5, &err);
  ASSERT_EQ(err, ERR_NONE);
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(MemSetBuffer[i], zeroBuffer[i]);
  }

  // Partial Buffer
  // Ones Buffer
  MemSet(&MemSetBuffer[1], 1, 3, &err);
  ASSERT_EQ(err, ERR_NONE);
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(MemSetBuffer[i], prtlBuffer[i]);
  }

  // Invalid Memset
  MemSet(0, 0, 5, &err);
  ASSERT_EQ(err, ERR_INVALID);
}

TEST(MemOperations, MemCopy)
{
  unsigned char Buffer[5];
  int err = ERR_NONE;

  // Invalid Arrays
  MemCopy(0, 0, 0, &err);
  ASSERT_EQ(err, ERR_INVALID);

  // copy 1's array
  //memcopy(Buffer, onesBuffer, 5, &err);
  MemCopy(Buffer, onesBuffer, 5, &err);
  ASSERT_EQ(err, ERR_NONE);

  // verify array
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(onesBuffer[i], Buffer[i]);
  }
}

TEST(MemOperations, MemSwap)
{
  unsigned char src1[] = {1, 1, 1, 1, 1};
  unsigned char src2[] = {2, 2, 2, 2, 2};
  int err = ERR_NONE;

  // Swap Invalid Array
  MemSwap(src1, 0, 5, &err);

  // Swap Arrays
  MemSwap(src1, src2, 5, &err);

  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(src1[i], 2);
    ASSERT_EQ(src2[i], 1);
  }

}

TEST(MemOperations, MemMove)
{
  unsigned char Buffer[5];
  int err = ERR_NONE;

  // Invalid Arrays
  MemMove(0, 0, 0, &err);
  ASSERT_EQ(err, ERR_INVALID);

  // copy 1's array
  //memcopy(Buffer, onesBuffer, 5, &err);
  MemMove(Buffer, onesBuffer, 5, &err);
  ASSERT_EQ(err, ERR_NONE);

  // verify array
  for (unsigned int i = 0; i < 5; i++) {
    ASSERT_EQ(onesBuffer[i], Buffer[i]);
  }
}

TEST(MemOperations, StrLen)
{
  const char* test1 = "";
  const char* test2 = "test";
  int err = ERR_NONE;

  ASSERT_EQ(strlen(test1), StrLen(test1, &err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(strlen(test2), StrLen(test2, &err));
  ASSERT_EQ(err, ERR_NONE);

  // Null Value
  StrLen(0, &err);
  ASSERT_EQ(err, ERR_INVALID);
}

TEST(MemOperations, MemCompare)
{
  const char* str1 = "";
  const char* str2 = "test";
  const char* str3 = "TEST";
  int err = ERR_NONE;

  ASSERT_EQ(MemCompare(str1, str1, 0, &err), 0);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str2, str2, 4, &err), 0);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str3, str3, 4, &err), 0);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str1, str2, 4, &err), -1);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str2, str3, 4, &err), 1);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str2, str1, 4, &err), 1);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(MemCompare(str3, str2, 4, &err), -1);
  ASSERT_EQ(err, ERR_NONE);

  // Null Pointer
  ASSERT_EQ(MemCompare(0, str2, 4, &err), 0);
  ASSERT_EQ(err, ERR_INVALID);

  ASSERT_EQ(MemCompare(str3, 0, 4, &err), 0);
  ASSERT_EQ(err, ERR_INVALID);
}

TEST(MemOperations, StrCopy)
{
  const char* str1 = "test";
  char str2[10];
  int err = ERR_NONE;

  // Pointer Error
  StrCopy(0, str1, 4, &err);
  ASSERT_EQ(err, ERR_INVALID);

  StrCopy(str2, 0, 4, &err);
  ASSERT_EQ(err, ERR_INVALID);

  // Copy 0 Bytes -- stupid but possible
  StrCopy(str2, str1, 0, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Copy 4 Bytes to 10 byte buffer
  StrCopy(str2, str1, 10, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ(str2, "test");

}


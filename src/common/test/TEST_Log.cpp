/*
 * log/Log/TEST_Log.cpp
 *
 *  Created on: Mar 10, 2015
 *      Author: mjc
 */

#include "gtest/gtest.h"
#include <common/Log.hpp>
#include <common/Error.hpp>
#include <datatype/CString.hpp>
#include <Console.hpp>

using namespace ns_MDL;
using namespace ns_Datatype;

// CString Test Fixture

class LOG_Test : public testing::Test
{
protected:

  // Called before each test is run.

  virtual void
  SetUp()
  {
    err = ERR_NONE;
    LOG::EnableDateStamp(false);
    LOG::EnableTimeStamp(false);
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    err = ERR_NONE;
    LOG::RemoveAllDevices();
    LOG::EnableDateStamp(false);
    LOG::EnableTimeStamp(false);
  }

  int err;

};

TEST_F(LOG_Test, ConsolePrint)
{
  Console device;
  LOG::AddOutputDevice(&device);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Print("Character CString");
  LOG::Print(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(false);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Print("Character CString");
  LOG::Print(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(false);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Print("Character CString");
  LOG::Print(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Print("Character CString");
  LOG::Print(CString("CString"));
};

TEST_F(LOG_Test, ConsoleMessage)
{
  Console device;
  LOG::AddOutputDevice(&device);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Message("Character CString");
  LOG::Message(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(false);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Message("Character CString");
  LOG::Message(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(false);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Message("Character CString");
  LOG::Message(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Message("Character CString");
  LOG::Message(CString("CString"));
};

TEST_F(LOG_Test, ConsoleWarning)
{
  Console device;
  LOG::AddOutputDevice(&device);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Warning("Character CString");
  LOG::Warning(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(false);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Warning("Character CString");
  LOG::Warning(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(false);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Warning("Character CString");
  LOG::Warning(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Warning("Character CString");
  LOG::Warning(CString("CString"));
};

TEST_F(LOG_Test, ConsoleError)
{
  Console device;
  LOG::AddOutputDevice(&device);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Error("Character CString");
  LOG::Error(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(false);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Error("Character CString");
  LOG::Error(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(false);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Error("Character CString");
  LOG::Error(CString("CString"));
  printf("\n");
  LOG::EnableDateStamp(true);
  LOG::EnableTimeStamp(true);
  printf("DateStamp: %i\n", LOG::IsDateStampEnabled());
  printf("TimeStamp: %i\n", LOG::IsTimeStampEnabled());
  LOG::Error("Character CString");
  LOG::Error(CString("CString"));
};




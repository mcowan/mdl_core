/// @file log/Log.hpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details LOG designed to log to console,
/// file, or to a serial device.


#ifndef LOG_HPP_
#define LOG_HPP_

#include <Types.hpp>
#include <common/Error.hpp>
#include <datatype/CString.hpp>
#include <container/Vector.hpp>
#include <DeviceIF.hpp>
#include <Clock.hpp>

using namespace ns_Datatype;
using namespace ns_Container;

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief A CString class to abstract a signed CCStrings.
  ///

  class LOG
  {
  public:
    // Write Log Messages
    static void Print(const char* str);
    static void Print(CString str);

    static void Message(const char* str);
    static void Message(CString str);

    static void Error(const char* str);
    static void Error(CString str);

    static void Warning(const char* str);
    static void Warning(CString str);

    static void AddOutputDevice(DeviceIF* dev, ERROR* error = 0);
    static void RemoveAllDevices();
    static void EnableDateStamp(bool enable);
    static bool IsDateStampEnabled();
    static void EnableTimeStamp(bool enable);
    static bool IsTimeStampEnabled();

  private:
    static bool m_EnableTimeStamp;
    static bool m_EnableDateStamp;
    static Vector<DeviceIF*> devices;
    static Clock m_Clock;
  };
};

#endif



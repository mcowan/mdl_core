/// @file Conversions.hpp
///
/// @brief  A class that defines the common conversions.
///
/// @authors mjc
/// @date 05-31-14
///

#ifndef CONSTANTS_HPP_
#define CONSTANTS_HPP_

/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{
  // Byte Conversions
  static const double PI = 3.14159265359;

  static const double TWO_PI = 2 * PI;

}

#endif

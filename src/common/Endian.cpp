/* 
 * File:   Endian.cpp
 * Author: Orion
 * 
 * Created on November 8, 2015, 11:55 AM
 */


#include "Endian.hpp"
using namespace ns_MDL;
#include <iostream>

bool Endian::IsBigEndian()
{
   short word = 0x4321;
   if((*(char *)& word) != 0x21 )
     return true;
   else 
     return false;
}

uint8_t Endian::NtoH8(uint8_t value)
{
  return value;
}

uint16_t Endian::NtoH16(uint16_t value)
{
  uint16_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value;
  }
  else
  {
    toReturn = ((value & 0x00FF) << 8 | 
                (value & 0xFF00) >> 8); 
  }

  return toReturn;
}
 
uint32_t Endian::NtoH32(uint32_t value)
{
  uint32_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value;
  }
  else
  {
    toReturn =  ( (((value) >> 24) & 0x000000FF) | 
                  (((value) >>  8) & 0x0000FF00) | 
                  (((value) <<  8) & 0x00FF0000) | 
                  (((value) << 24) & 0xFF000000) );
  }

  return toReturn;
}

uint64_t Endian::NtoH64(uint64_t value)
{
  uint64_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value;
  }
  else
  {
    toReturn = ((((value) >> 56) & 0x00000000000000FF) | 
                (((value) >> 40) & 0x000000000000FF00) |
                (((value) >> 24) & 0x0000000000FF0000) | 
                (((value) >>  8) & 0x00000000FF000000) |
                (((value) <<  8) & 0x000000FF00000000) | 
                (((value) << 24) & 0x0000FF0000000000) |
                (((value) << 40) & 0x00FF000000000000) | 
                (((value) << 56) & 0xFF00000000000000) );
  }

  return toReturn;
}


uint8_t Endian::HtoN8(uint8_t value)
{
  return value;
}

uint16_t Endian::HtoN16(uint16_t value)
{
  uint16_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value; 
  }
  else
  {
    toReturn = ((value & 0x00FF) << 8 | 
                (value & 0xFF00) >> 8);
  }

  return toReturn;
}

uint32_t Endian::HtoN32(uint32_t value)
{
  uint32_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value;
  }
  else
  {

    toReturn =  ( (((value) >> 24) & 0x000000FF) |
                  (((value) >>  8) & 0x0000FF00) |
                  (((value) <<  8) & 0x00FF0000) |
                  (((value) << 24) & 0xFF000000) );
  }

  return toReturn;
}

uint64_t Endian::HtoN64(uint64_t value)
{
  uint64_t toReturn = 0;
  
  if(IsBigEndian())
  {
    toReturn = value;
  }
  else
  {
    toReturn = ((((value) >> 56) & 0x00000000000000FF) | 
                (((value) >> 40) & 0x000000000000FF00) |
                (((value) >> 24) & 0x0000000000FF0000) | 
                (((value) >>  8) & 0x00000000FF000000) |
                (((value) <<  8) & 0x000000FF00000000) | 
                (((value) << 24) & 0x0000FF0000000000) |
                (((value) << 40) & 0x00FF000000000000) | 
                (((value) << 56) & 0xFF00000000000000) );
  }

  return toReturn;
}

ERROR Endian::Write_Array_HtoN8(uint8_t* buffer, uint32_t index, uint8_t value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  buffer[index] = value;
  return ERR_NONE;
}

ERROR Endian::Write_Array_HtoN16(uint8_t* buffer, uint32_t index, uint16_t value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  buffer[index]     =  (uint8_t)(value >> 8);
  buffer[index+1]   =  (uint8_t)(value);
          
  return ERR_NONE;
}

ERROR Endian::Write_Array_HtoN32(uint8_t* buffer, uint32_t index, uint32_t value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  buffer[index]     =  (uint8_t)(value >> 24);
  buffer[index+1]   =  (uint8_t)(value >> 16);
  buffer[index+2]   =  (uint8_t)(value >> 8);
  buffer[index+3]   =  (uint8_t)(value);
  
  return ERR_NONE;
}

ERROR Endian::Write_Array_HtoN64(uint8_t* buffer, uint32_t index, uint64_t value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  buffer[index]     =  (uint8_t)(value >> 56);
  buffer[index+1]   =  (uint8_t)(value >> 48);
  buffer[index+2]   =  (uint8_t)(value >> 40);
  buffer[index+3]   =  (uint8_t)(value >> 32);
  buffer[index+4]   =  (uint8_t)(value >> 24);
  buffer[index+5]   =  (uint8_t)(value >> 16);
  buffer[index+6]   =  (uint8_t)(value >> 8);
  buffer[index+7]   =  (uint8_t)(value);
  
  return ERR_NONE;
}


ERROR Endian::Read_Array_NtoH8(uint8_t* buffer, uint32_t index, uint8_t &value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  value = buffer[index];
  
  return ERR_NONE;
}

ERROR Endian::Read_Array_NtoH16(uint8_t* buffer, uint32_t index, uint16_t &value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }

  value = ((uint64_t)buffer[index] << 8)  |
          ((uint64_t)buffer[index+1]);
  
  return ERR_NONE;
}

ERROR Endian::Read_Array_NtoH32(uint8_t* buffer, uint32_t index, uint32_t &value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  value = ((uint64_t)buffer[index]   << 24) |
          ((uint64_t)buffer[index+1] << 16) |
          ((uint64_t)buffer[index+2] << 8)  |
          ((uint64_t)buffer[index+3]);
  
  return ERR_NONE;
}

ERROR Endian::Read_Array_NtoH64(uint8_t* buffer, uint32_t index, uint64_t &value)
{
  if(buffer == NULL)
  {
    return ERR_INVALID;
  }
  
  value = ((uint64_t)buffer[index]   << 56) |
          ((uint64_t)buffer[index+1] << 48) |
          ((uint64_t)buffer[index+2] << 40) |
          ((uint64_t)buffer[index+3] << 32) |
          ((uint64_t)buffer[index+4] << 24) |
          ((uint64_t)buffer[index+5] << 16) |
          ((uint64_t)buffer[index+6] << 8)  |
          ((uint64_t)buffer[index+7]);
  
  return ERR_NONE;
}


/// @file Error.cpp
///
/// @brief  A class that defines the error codes for the Libraries, function
/// to set error pointers,  and a function that returns the error message for a
/// error value.
///
/// @authors Micheal Cowan
/// @date 05-31-14


#include <common/Error.hpp>

namespace ns_MDL
{

  void SetErr(ERROR* error, ERROR val)
  {
    if (error != 0) {
      *error = val;
    }
  }

  const char* GetErrMsg(ERROR* error)
  {
    const char* toReturn = "Invalid Error Pointer";

    if (error != 0) {
      switch (*error) {
      case ERR_NONE:
      {
        toReturn = "None";
        break;
      }
      case ERR_RANGE:
      {
        toReturn = "Range";
        break;
      }
      case ERR_TYPE:
      {
        toReturn = "Type";
        break;
      }
      case ERR_UNSPEC:
      {
        toReturn = "Unspecified";
        break;
      }
      case ERR_TIMEOUT:
      {
        toReturn = "Timeout";
        break;
      }
      case ERR_ACCESS:
      {
        toReturn = "Access";
        break;
      }
      case ERR_INVALID:
      {
        toReturn = "Invalid";
        break;
      }
      case ERR_ALIGNMENT:
      {
        toReturn = "Alignment";
        break;
      }
      case ERR_NOT_INITIALIZED:
      {
        toReturn = "Not Initialized";
        break;
      }
      case ERR_NOT_FOUND:
      {
        toReturn = "Not Found";
        break;
      }
      case ERR_NOT_IMPLEMENTED:
      {
        toReturn = "Not Implemented";
        break;
      }
      case ERR_HARDWARE:
      {
        toReturn = "Hardware";
        break;
      }
      case ERR_FULL:
      {
        toReturn = "Full";
        break;
      }
      case ERR_EMPTY:
      {
        toReturn = "Empty";
        break;
      }
      case ERR_WRITE:
      {
        toReturn = "Write";
        break;
      }
      case ERR_READ:
      {
        toReturn = "Read";
        break;
      }
      case ERR_END_OF_FILE:
      {
        toReturn = "End of File";
        break;
      }
      case ERR_NO_FILE_OPEN:
      {
        toReturn = "No File Open";
        break;
      }
      case ERR_VERSION:
      {
        toReturn = "Version";
        break;
      }
      case ERR_DUPLICATE:
      {
        toReturn = "Duplicate";
        break;
      }
      case ERR_PERMISSION:
      {
        toReturn = "Permission";
        break;
      }
      case ERR_RESOURCES:
      {
        toReturn = "Resources";
        break;
      }
      case ERR_BUSY:
      {
        toReturn = "Busy";
        break;
      }
      case ERR_UNDEFINED:
      {
        toReturn = "Undefined";
        break;
      }
      default:
      {
        toReturn = "Undefined Error Code!!";
        break;
      }
      }
    }
    return toReturn;
  }

};




/* 
 * File:   Endian.hpp
 * Author: Orion
 *
 * Created on November 8, 2015, 11:55 AM
 */

#ifndef ENDIAN_HPP
#define	ENDIAN_HPP

#include <Types.hpp>
#include <common/Error.hpp>

namespace ns_MDL
{

  class Endian
  {
  public:
    static bool IsBigEndian();
    
    static uint8_t HtoN8(uint8_t value);
    static uint16_t HtoN16(uint16_t value);
    static uint32_t HtoN32(uint32_t value);
    static uint64_t HtoN64(uint64_t value);
    
    static uint8_t NtoH8(uint8_t value);
    static uint16_t NtoH16(uint16_t value);
    static uint32_t NtoH32(uint32_t value);
    static uint64_t NtoH64(uint64_t value);
    
    
    static ERROR Write_Array_HtoN8(uint8_t* buffer, uint32_t index, uint8_t value);
    static ERROR Write_Array_HtoN16(uint8_t* buffer, uint32_t index, uint16_t value);
    static ERROR Write_Array_HtoN32(uint8_t* buffer, uint32_t index, uint32_t value);
    static ERROR Write_Array_HtoN64(uint8_t* buffer, uint32_t index, uint64_t value);
    
    static ERROR Read_Array_NtoH8(uint8_t* buffer, uint32_t index, uint8_t &value);
    static ERROR Read_Array_NtoH16(uint8_t* buffer, uint32_t index, uint16_t &value);
    static ERROR Read_Array_NtoH32(uint8_t* buffer, uint32_t index, uint32_t &value);
    static ERROR Read_Array_NtoH64(uint8_t* buffer, uint32_t index, uint64_t &value);

  private:
  };
};
#endif	/* ENDIAN_HPP */


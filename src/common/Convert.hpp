/// @file Conversions.hpp
///
/// @brief  A class that defines the common conversions.
///
/// @authors mjc
/// @date 05-31-14
///

#ifndef CONVERT_HPP_
#define CONVERT_HPP_

/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{
  // Byte Conversions
  static const double GIBABYTES_TO_BYTES = 1073741824; /*!< Gigabytes to Bytes */
  static const double MEGABYTES_TO_BYTES = 1048576; /*!< Megabytes to Bytes */
  static const double KILOBYTES_TO_BYTES = 1024; /*!< Kilobytes to Bytes */
  static const double BYTES_TO_KILOBYTES = 1.0 / KILOBYTES_TO_BYTES; /*!< Bytes to Kilobytes */
  static const double BYTES_TO_MEGABYTES = 1.0 / MEGABYTES_TO_BYTES; /*!< Bytes to Megabytes */
  static const double BYTES_TO_GIGABYTES = 1.0 / GIBABYTES_TO_BYTES; /*!< Bytes to Gigabytes */


  // Metric Conversions
  static const double METRIC_TO_TERA = 0.000000000001; /*!< 10^-12 */
  static const double METRIC_FROM_TERA = 1000000000000; /*!< 10^12  */
  static const double METRIC_TO_GIGA = 0.000000001; /*!< 10^-9  */
  static const double METRIC_FROM_GIGA = 1000000000; /*!< 10^9   */
  static const double METRIC_TO_MEGA = 0.000001; /*!< 10^-6  */
  static const double METRIC_FROM_MEGA = 1000000; /*!< 10^6   */
  static const double METRIC_TO_KILO = 0.001; /*!< 10^-3  */
  static const double METRIC_FROM_KILO = 1000; /*!< 10^3   */
  static const double METRIC_TO_HECTO = 0.01; /*!< 10^-2  */
  static const double METRIC_FROM_HECTO = 100; /*!< 10^2   */
  static const double METRIC_TO_DECA = 0.1; /*!< 10^-1  */
  static const double METRIC_FROM_DECA = 10; /*!< 10^1   */

  static const double METRIC_TO_DECI = 0.1; /*!< 10^-1  */
  static const double METRIC_FROM_DECI = 10; /*!< 10^1   */
  static const double METRIC_TO_CENTI = 0.01; /*!< 10^-2  */
  static const double METRIC_FROM_CENTI = 100; /*!< 10^2   */
  static const double METRIC_TO_MILLI = 0.001; /*!< 10^-3  */
  static const double METRIC_FROM_MILLI = 1000; /*!< 10^3   */
  static const double METRIC_TO_MICRO = 0.000001; /*!< 10^-6  */
  static const double METRIC_FROM_MICRO = 1000000; /*!< 10^6   */
  static const double METRIC_TO_NANO = 0.000000001; /*!< 10^-9  */
  static const double METRIC_FROM_NANO = 1000000000; /*!< 10^9   */
  static const double METRIC_TO_PICO = 0.000000000001; /*!< 10^-12 */
  static const double METRIC_FROM_PICO = 1000000000000; /*!< 10^12  */

  // Trig conversions
  static const double DEGREE_TO_RADIAN = 0.0174532925; /*!< One degree to Radians */
  static const double RADIAN_TO_DEGREE = 57.2957795; /*!< One Radian to Degrees */
};

#endif

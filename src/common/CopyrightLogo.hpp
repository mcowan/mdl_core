/// @file Error.hpp
///
/// @brief  A class that defines the error codes for the Libraries, function
/// to set error pointers,  and a function that returns the error message for a
/// error value.
///
/// @authors mjc
/// @date 05-31-14
///

#ifndef COPYRIGHTLOGO_HPP_
#define COPYRIGHTLOGO_HPP_

/// @brief General MDL Library Wrapper.
/// 
namespace ns_MDL
{
  /// @brief General Logo for applications
  ///
  /// @param[in] name     Program Name.
  /// @param[in] year     Year the program was written.
  /// @param[in] version  Program Version.
  ///

  void MicroDevelLogo(const char* name, unsigned int year, const char* version = "1.0.0")
  {
    printf("                                                               \n");
    printf("Micro Development Libraries                                    \n");
    printf("                                                               \n");
    printf("      __ __ __ __ __ __                                        \n");
    printf("      __\\__\\__\\__\\__\\__\\_                                      \n");
    printf("     /\\                  \\        %s                           \n", name);
    printf("     \\*\\   Micro Devel    \\       VERSION: %s                  \n", version);
    printf("      \\*\\__________________\\      Copyright: %i                \n", year);
    printf("       \\/__\\__\\__\\__\\__\\__\\/                                   \n");
    printf("           /  /  /  /  /  /                                    \n");
    printf("                                                               \n");
    printf("===============================================================\n");
  }
}

#endif
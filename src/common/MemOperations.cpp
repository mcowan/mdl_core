
#include <common/MemOperations.hpp>

namespace ns_MDL
{

  void
  MemSet(void* dst, int val, size_t length, ERROR* err)
  {
    // Pointer Check
    if (dst == 0) {
      SetErr(err, ERR_INVALID);
    }
    else {
      for (size_t i = 0; i < length; i++) {
        ((unsigned char*) dst)[i] = val;
      }
      SetErr(err, ERR_NONE);
    }
  }

  int
  MemCompare(const void* buffer1, const void* buffer2, size_t length, ERROR* err)
  {
    size_t index = 0;

    if ((((unsigned char*) buffer1) == 0) || (((unsigned char*) buffer2) == 0)) {
      SetErr(err, ERR_INVALID);
    }
    else {
      SetErr(err, ERR_NONE);

      while (index < length) {
        if (((unsigned char*) buffer1)[index] == ((unsigned char*) buffer2)[index]) {
          index++;
        }
        else if (((unsigned char*) buffer1)[index] < ((unsigned char*) buffer2)[index]) {
          return -1;
        }
        else {
          return 1;
        }
      }
    }
    return 0;
  }

  void
  MemCopy(void* dst, const void* src, size_t length, ERROR* err)
  {
    if ((src == 0) || (dst == 0)) {
      SetErr(err, ERR_INVALID);
    }
    else {
      for (size_t i = 0; i < length; i++) {
        ((unsigned char*) dst)[i] = ((unsigned char*) src)[i];
      }
      SetErr(err, ERR_NONE);
    }
  }

  void
  MemSwap(void* src1, void* src2, size_t length, ERROR* err)
  {
    // Verify pointers are valid.
    if ((src1 == 0) || (src2 == 0)) {
      SetErr(err, ERR_INVALID);
    }
    else {
      for (size_t i = 0; i < length; i++) {
        // Assert the memory locations are not the same.
        if (&((unsigned char*) src1)[i] != &((unsigned char*) src2)[i]) {
          // XOR Swap
          ((unsigned char*) src1)[i] ^= ((unsigned char*) src2)[i];
          ((unsigned char*) src2)[i] ^= ((unsigned char*) src1)[i];
          ((unsigned char*) src1)[i] ^= ((unsigned char*) src2)[i];
        }
      }
      SetErr(err, ERR_NONE);
    }
  }

  void
  MemMove(void* dst, const void* src, size_t length, ERROR* err)
  {
    // Pointer Check
    if (dst == 0 || src == 0 || length == 0) {
      SetErr(err, ERR_INVALID);
    }
    else {
      unsigned char* Buffer = new unsigned char[length];
      // Copy Source to Buffer
      for (size_t i = 0; i < length; i++) {
        ((unsigned char*) Buffer)[i] = ((unsigned char*) src)[i];
      }
      for (size_t i = 0; i < length; i++) {
        ((unsigned char*) dst)[i] = ((unsigned char*) Buffer)[i];
      }

      delete[] Buffer;
      SetErr(err, ERR_NONE);
    }
  }

  size_t
  StrLen(const void* str, ERROR* err)
  {
    size_t index = 0;
    unsigned char* buffer = (unsigned char*) str;

    if (buffer != 0) {
      while (buffer[index] != '\0') {
        index++;
      }
    }
    else {
      SetErr(err, ERR_INVALID);
    }
    return index;
  }

  void
  StrCopy(void* dst, const char* src, size_t length, ERROR* err)
  {
    if ((src == 0) || (dst == 0)) {
      SetErr(err, ERR_INVALID);
    }
    else {
      size_t i = 0;
      for (i = 0; i < length; i++) {
        if (((unsigned char*) src)[i] != '\0') {
          ((unsigned char*) dst)[i] = ((unsigned char*) src)[i];
        }
        else {
          break;
        }
      }
      ((unsigned char*) dst)[i++] = '\0';

      // Pad remaining characters of buffer with 0 if src is smaller than dst buffer.
      if (i < length) {
        ((unsigned char*) dst)[i++] = 0;
      }

      SetErr(err, ERR_NONE);
    }
  }

}
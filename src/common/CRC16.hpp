/* 
 * File:   CRC16.hpp
 * Author: Orion
 *
 * Created on January 1, 2016, 2:00 PM
 */

#ifndef CRC16_HPP
#define	CRC16_HPP

#include <Types.hpp>

class CRC16
{
public:
  static uint16_t CalcCRC16(uint16_t crc, uint8_t* buffer, uint32_t size);
  
private:
  static uint16_t CRC16_Table[256];
};

#endif	/* CRC16_HPP */


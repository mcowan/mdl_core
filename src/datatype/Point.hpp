/// @file container/Point.hpp
///
/// @brief Defines the Point interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Point class.

#ifndef POINT_HPP_
#define POINT_HPP_

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Container classes.
  ///
  namespace ns_Datatype
  {
    /// @brief A simple class that binds two data types
    /// as a pair.  A left and a right element that can be of
    /// the same type or different types.  Left and Right values
    /// can be set simulataneously with the constructor and are
    /// left with public access to get and set the values.
    ///
    /// @param[in] L  Left datatype.
    /// @param[in] R  Right datatype.
    ///

    template <typename T>
    class Point
    {
    public:
      /// @brief Default Point constructor.
      ///
      Point();

      /// @brief Constructs Point object and initializes values to given
      /// values.
      ///
      /// @param[in] x  Coordinate Value.
      /// @param[in] y  Coordinate Value.
      ///
      Point(T x, T y);

      /// @brief Constructs Point object and initializes values to given
      /// values.
      ///
      /// @param[in] x  Coordinate Value.
      /// @param[in] y  Coordinate Value.
      /// @param[in] z  Coordinate Value.
      ///
      Point(T x, T y, T z);

      /// @brief Deconstructor.
      ///
      ~Point();

      /// @brief Assignment operator sets the values of the given Point on
      /// the right to the Point on the left.
      ///
      /// @param[in] other  The Point to copy values from.
      ///
      /// @returns A configured Point.
      ///
      Point<T>& operator=(const Point<T>& other);

      /// @brief Equality Operator.
      ///
      /// @param[in]  other Right hand side object.
      ///
      /// @return True, if objects are equal; False, otherwise.
      ///
      bool operator==(const Point<T>& other) const;

      /// @brief In-Equality Operator.
      ///
      /// @param[in]  other Right hand side object.
      ///
      /// @return False, if objects are equal; True, otherwise.
      ///
      bool operator!=(const Point<T>& other) const;

    public:
      T x; /*!< x value.*/
      T y; /*!< y value.*/
      T z; /*!< z value.*/
    };
  }
}

#include <datatype/Point_source.hpp>  // Template Source file

#endif 

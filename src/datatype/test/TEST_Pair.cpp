/*
 * Pair_test.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include <gtest/gtest.h>
#include <datatype/Pair.hpp>


using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Datatype;

struct testStruct_t
{
  int x;
  int y;
};

class Pair_test : public ::testing::Test
{
protected:
};

TEST_F(Pair_test, Constructor)
{
  Pair<int, int> p(1, 2);

  Pair<int, int> p2();

  Pair<void*, void*> p_Pointer();

  Pair<testStruct_t, testStruct_t> p_Struct();
}

TEST_F(Pair_test, AssignmentOperator)
{
  Pair<int, int> p(0, 0);
  Pair<int, int> p2(1, 1);

  ASSERT_EQ(0, p.Left);
  ASSERT_EQ(0, p.Right);
  ASSERT_EQ(1, p2.Left);
  ASSERT_EQ(1, p2.Right);

  p2 = p;

  ASSERT_EQ(0, p2.Left);
  ASSERT_EQ(0, p2.Right);
}

TEST_F(Pair_test, EqualityOperators)
{
  Pair<int, int> p1(0, 0);
  Pair<int, int> p2(0, 1);
  Pair<int, int> p3(1, 0);
  Pair<int, int> p4(1, 1);
  Pair<int, int> p5(0, 0);

  ASSERT_TRUE(p1 == p5);
  ASSERT_FALSE(p1 == p2);
  ASSERT_FALSE(p1 == p3);
  ASSERT_FALSE(p1 == p4);

  ASSERT_FALSE(p1 != p5);
  ASSERT_TRUE(p1 != p2);
  ASSERT_TRUE(p1 != p3);
  ASSERT_TRUE(p1 != p4);
}





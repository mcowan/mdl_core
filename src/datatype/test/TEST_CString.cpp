/*
 * TEST_Array.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#include "gtest/gtest.h"
#include <datatype/CString.hpp>
#include <cstring>
#include <stdio.h>
#include <string.h>


using namespace ns_MDL;
using namespace ns_Datatype;

const char* HelloWorld = "Hello World";
const char* TestCString = "Test CString";

// CString Test Fixture

class CStringTest : public testing::Test
{
protected:
  // You should make the members protected s.t. they can be
  // accessed from sub-classes.

  // Called before each test is run.

  virtual void
  SetUp()
  {
    m_CopyConstructorCCString = new CString(HelloWorld);
    m_CopyConstructorCString = new CString(*m_CopyConstructorCCString);
    m_CString = new CString(TestCString);
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    delete m_CString;
    delete m_CopyConstructorCString;
    delete m_CopyConstructorCCString;
  }

  // Declares the variables your tests want to use.
  CString m_Default;
  CString* m_CopyConstructorCCString;
  CString* m_CopyConstructorCString;
  CString* m_CString;
};

// Default Constructor

TEST_F(CStringTest, DefaultConstructor)
{
  // Default
  EXPECT_EQ(0u, m_Default.size());

  // CCString Copy Constructor
  EXPECT_EQ(strlen(HelloWorld), m_CopyConstructorCCString->size());

  // CString Copy Constructor
  EXPECT_EQ(strlen(HelloWorld), m_CopyConstructorCString->size());
}

// c_str verification

TEST_F(CStringTest, c_str)
{
  ASSERT_STREQ("", m_Default.c_str());
  ASSERT_STREQ(HelloWorld, m_CopyConstructorCCString->c_str());
  ASSERT_STREQ(HelloWorld, m_CopyConstructorCString->c_str());
}

TEST_F(CStringTest, empty)
{
  EXPECT_TRUE(m_Default.empty());
  EXPECT_FALSE(m_CopyConstructorCCString->empty());
  EXPECT_FALSE(m_CopyConstructorCString->empty());
}

TEST_F(CStringTest, clear)
{
  EXPECT_TRUE(m_Default.empty());
  EXPECT_FALSE(m_CopyConstructorCCString->empty());
  EXPECT_FALSE(m_CopyConstructorCString->empty());

  // Clear all strings and verify empty
  m_Default.clear();
  m_CopyConstructorCCString->clear();
  m_CopyConstructorCString->clear();

  // Verify
  EXPECT_TRUE(m_Default.empty());
  EXPECT_TRUE(m_CopyConstructorCCString->empty());
  EXPECT_TRUE(m_CopyConstructorCString->empty());
}

TEST_F(CStringTest, swap)
{
  int err = ERR_NONE;
  EXPECT_TRUE(m_Default.empty());
  ASSERT_STREQ(HelloWorld, m_CopyConstructorCCString->c_str());
  ASSERT_STREQ(HelloWorld, m_CopyConstructorCString->c_str());

  m_Default.swap(0, 1, &err);
  ASSERT_EQ(err, ERR_RANGE);

  m_CopyConstructorCCString->swap(0, 1, &err);
  ASSERT_STREQ("eHllo World", m_CopyConstructorCCString->c_str());
  ASSERT_EQ(err, ERR_NONE);

  m_CopyConstructorCString->swap(0, 1, &err);
  ASSERT_STREQ("eHllo World", m_CopyConstructorCString->c_str());
  ASSERT_EQ(err, ERR_NONE);
}

TEST_F(CStringTest, assign)
{
  int err = ERR_NONE;

  // string
  m_Default.assign(*m_CopyConstructorCCString);
  ASSERT_STREQ(HelloWorld, m_Default.c_str());
  ASSERT_EQ(strlen(HelloWorld), m_Default.size());

  // c string
  m_Default.clear();
  m_Default.assign(HelloWorld);
  ASSERT_STREQ(HelloWorld, m_Default.c_str());
  ASSERT_EQ(strlen(HelloWorld), m_Default.size());

  // character
  m_Default.clear();
  m_Default.assign('c');
  ASSERT_STREQ("c", m_Default.c_str());
  ASSERT_EQ(strlen("c"), m_Default.size());

  // invalid cstring
  m_Default.assign((const char*) 0, &err);
  ASSERT_EQ(err, ERR_INVALID);
}

TEST_F(CStringTest, append)
{
  int err = 0;

  // string
  m_Default.append(*m_CopyConstructorCCString, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ(HelloWorld, m_Default.c_str());

  // cstring
  m_Default.append(HelloWorld, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Hello WorldHello World", m_Default.c_str());

  // character
  m_Default.append('c', &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Hello WorldHello Worldc", m_Default.c_str());

  // invalid cstring
  m_Default.append((const char*) 0, &err);
  ASSERT_EQ(err, ERR_INVALID);
}

TEST_F(CStringTest, SubscriptOperator)
{
  char temp[] = "World Hello";

  for (size_t i = 0; i < m_CopyConstructorCCString->size(); i++) {
    ASSERT_EQ(HelloWorld[i], (*m_CopyConstructorCCString)[i]);
  }

  for (size_t i = 0; i < m_CopyConstructorCCString->size(); i++) {
    (*m_CopyConstructorCCString)[i] = temp[i];
  }

  for (size_t i = 0; i < m_CopyConstructorCCString->size(); i++) {
    ASSERT_EQ(temp[i], (*m_CopyConstructorCCString)[i]);
  }

  // invalid index
  ASSERT_EQ('W', (*m_CopyConstructorCCString)[42]);
}

TEST_F(CStringTest, EraseSingle)
{
  int err = ERR_NONE;
  // erase end
  m_CopyConstructorCCString->erase(0, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ello World", m_CopyConstructorCCString->c_str());

  // erase middle
  m_CopyConstructorCCString->erase(1, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("elo World", m_CopyConstructorCCString->c_str());

  // erase end
  m_CopyConstructorCCString->erase(8, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("elo Worl", m_CopyConstructorCCString->c_str());

  // erase error
  m_CopyConstructorCCString->erase(154, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST_F(CStringTest, EraseSection)
{
  int err = ERR_NONE;
  // erase end
  m_CopyConstructorCCString->erase(0, 0, &err);
  ASSERT_EQ(err, ERR_INVALID);
  ASSERT_STREQ("Hello World", m_CopyConstructorCCString->c_str());

  // erase middle
  m_CopyConstructorCCString->erase(0, 1, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ello World", m_CopyConstructorCCString->c_str());

  // erase end
  m_CopyConstructorCCString->erase(8, 10, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ello Wor", m_CopyConstructorCCString->c_str());

  // erase error
  m_CopyConstructorCCString->erase(8, 154, &err);
  ASSERT_EQ(err, ERR_RANGE);

}

TEST_F(CStringTest, InsertCString)
{
  int err = ERR_NONE;
  // insert beginning
  m_CopyConstructorCCString->insert(0, *m_CString, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Test CStringHello World", m_CopyConstructorCCString->c_str());

  // insert end
  m_CopyConstructorCCString->insert(m_CopyConstructorCCString->size(), *m_CString, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Test CStringHello WorldTest CString", m_CopyConstructorCCString->c_str());

  // insert middle
  m_CopyConstructorCCString->insert(1, *m_CString, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("TTest CStringest CStringHello WorldTest CString", m_CopyConstructorCCString->c_str());

  m_CopyConstructorCCString->insert(1000, *m_CString, &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST_F(CStringTest, InsertCharacter)
{
  int err = ERR_NONE;
  // insert beginning
  m_CopyConstructorCCString->insert(0, 'Z', &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ZHello World", m_CopyConstructorCCString->c_str());

  // insert end
  m_CopyConstructorCCString->insert(m_CopyConstructorCCString->size(), 'Z', &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ZHello WorldZ", m_CopyConstructorCCString->c_str());

  // insert middle
  m_CopyConstructorCCString->insert(1, 'Z', &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("ZZHello WorldZ", m_CopyConstructorCCString->c_str());

  m_CopyConstructorCCString->insert(1000, 'Z', &err);
  ASSERT_EQ(err, ERR_RANGE);
}

TEST_F(CStringTest, PlusEqualsOperator)
{
  int err = ERR_NONE;
  // Add CString
  (*m_CopyConstructorCCString) += (*m_CopyConstructorCString);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Hello WorldHello World", m_CopyConstructorCCString->c_str());

  // Add CCString
  (*m_CopyConstructorCCString) += m_CopyConstructorCString->c_str();
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Hello WorldHello WorldHello World", m_CopyConstructorCCString->c_str());

  // Add Character
  (*m_CopyConstructorCCString) += 'Z';
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_STREQ("Hello WorldHello WorldHello WorldZ", m_CopyConstructorCCString->c_str());
}

TEST_F(CStringTest, PlusOperator)
{
  CString a(TestCString);
  CString b(HelloWorld);
  CString c;

  // Add CStrings
  c = a + b;
  ASSERT_STREQ(c.c_str(), "Test CStringHello World");

  // Add C CString
  c = a + " Test";
  ASSERT_STREQ(c.c_str(), "Test CString Test");

  // Add character
  c = a + 'Z';
  ASSERT_STREQ(c.c_str(), "Test CStringZ");
}

TEST_F(CStringTest, AssigmentOperator)
{
  CString a(TestCString);
  CString b;

  // Assign CString
  b = a;
  ASSERT_STREQ(a.c_str(), b.c_str());

  // Assign C CString
  b = "Testing";
  ASSERT_STREQ("Testing", b.c_str());

  // Assign character
  b = 'Z';
  ASSERT_STREQ("Z", b.c_str());
}

TEST_F(CStringTest, substring)
{

  CString a = m_CString->substr(0, 4);
  ASSERT_EQ(4, a.size());
  ASSERT_STREQ(a.c_str(), "Test");

}

TEST_F(CStringTest, Find)
{
  int err = ERR_NONE;

  ASSERT_EQ(0, m_CString->find("Test CString"));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(0, m_CString->find("Test", 0, &err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(1, m_CString->find("est", 0, &err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(9, m_CString->find("ing", 0, &err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(0, m_CString->find("Not Here", 0, &err));
  ASSERT_EQ(err, ERR_NOT_FOUND);
  err = ERR_NONE;
  ASSERT_EQ(0, m_CString->find(*m_CString, 0, &err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(0, m_CString->find('T'));
  ASSERT_EQ(1, m_CString->find('e'));
  ASSERT_EQ(2, m_CString->find('s'));
  ASSERT_EQ(11, m_CString->find('g'));

  // Range Errors
  ASSERT_EQ(0, m_CString->find('g', 54, &err));
  ASSERT_EQ(err, ERR_RANGE);

  // Search CString is larger than CString
  ASSERT_EQ(0, m_CString->find("Test StringZZZZZZZZZZZ", 0, &err));
  ASSERT_EQ(err, ERR_RANGE);

  // Search position larger than string -- note searching yourself is ok because
  // this is a const function and nondestructive in nature.
  ASSERT_EQ(0, m_CString->find(*m_CString, 154, &err));
  ASSERT_EQ(err, ERR_RANGE);
}



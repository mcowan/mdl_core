/*
 * PointTest.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include <gtest/gtest.h>
#include <datatype/Point.hpp>

/*
using ::testing::Test;
using namespace ns_MDL;
using namespace ns_Datatype;


struct testStruct_t {
    int x;
    int y;
};



class PointTest : public ::testing::Test
{
protected:
};


TEST_F(PointTest, Constructor)
{
  Point<int> p(1,2);
  
  ASSERT_EQ(1, p.x);
  ASSERT_EQ(2, p.y);
  ASSERT_EQ(0, p.z);

  Point<int> p2();
  
  ASSERT_EQ(0, p2.x);
  ASSERT_EQ(0, p2.y);
  ASSERT_EQ(0, p2.z);
  
  Point<int> p3(1,2,3);
  
  ASSERT_EQ(1, p3.x);
  ASSERT_EQ(2, p3.y);
  ASSERT_EQ(3, p3.z);
  
  Point<void*, void*> p_Pointer();

  Point<testStruct_t, testStruct_t> p_Struct();
}

TEST_F(PointTest, AssignmentOperator)
{
  Point<int> p(0,0);
  Point<int> p2(1,1);

  ASSERT_EQ(0, p.Left);
  ASSERT_EQ(0, p.Right);
  ASSERT_EQ(1, p2.Left);
  ASSERT_EQ(1, p2.Right);

  p2 = p;

  ASSERT_EQ(0, p2.Left);
  ASSERT_EQ(0, p2.Right);
}

TEST_F(PointTest, EqualityOperators)
{
  Point<int> p1(0,0);
  Point<int> p2(0,1);
  Point<int> p3(1,0);
  Point<int> p4(1,1);
  Point<int> p5(0,0);

  ASSERT_TRUE( p1 == p5 );
  ASSERT_FALSE( p1 == p2 );
  ASSERT_FALSE( p1 == p3 );
  ASSERT_FALSE( p1 == p4 );

  ASSERT_FALSE( p1 != p5 );
  ASSERT_TRUE( p1 != p2 );
  ASSERT_TRUE( p1 != p3 );
  ASSERT_TRUE( p1 != p4 );
}


 */


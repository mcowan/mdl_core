/// @file System.hpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A header file describing functions to access system variables 
/// pertaining to the hardware.


#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include <Types.hpp>
#include <container/Array.hpp>
#include <common/Error.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief General Datatype namespace
  ///
  namespace ns_Datatype
  {
    /// @brief A CString class to abstract a signed CCStrings.
    ///

    class CString
    {
    public:
      /// @details Default CString Constructor 
      ///
      CString();

      /// @details CString Constructor
      ///
      /// @param[in] other CString to assign to CString
      CString(const CString& other);

      /// @details CCString Constructor
      ///
      /// @param[in] other CCString to assign to CString.
      ///
      CString(const char* other);

      /// @details CString Deconstructor
      ///
      ~CString();

      /// @details Returns the number of characters in
      /// the CString excluding the NULL character.
      ///
      /// @return Number of characters.
      ///
      size_t size() const;

      /// @details Erases content of the CString.
      ///
      void clear();

      /// @details Checks if the CString is empty.
      ///
      /// @return True if empty CString; False, otherwise.
      ///
      bool empty() const;

      /// @details Swaps two characters at the given zero based
      /// index.
      ///
      /// @param[in]	index1	Index of the first character.
      /// @param[in]  index2  Index of the second character.
      /// @param[out]	err			ERR_RANGE if one or both indexes are
      ///											out of range; otherwise, ERR_NONE if
      ///											successful.
      ///
      void swap(size_t index1, size_t index2, ERROR* err = 0);

      /// @details Returns the C-CString within the class.
      ///
      /// @return The underlying C-CString.
      ///
      const char* c_str() const;

      /// @details Append a CString to this CString.
      ///
      /// @param[in]	str		CString to append.
      /// @param[out]	err		Optional Error Code.
      ///
      void append(const CString& str, ERROR* err = 0);

      /// @details Append a C-CString to this CString.
      ///
      /// @param[in]	str		C-CString to append.
      /// @param[out]	err		Optional Error Code.
      ///
      void append(const char* str, ERROR* err = 0);

      /// @details Append a character to this CString.
      ///
      /// @param[in]	str		character to append.
      /// @param[out]	err		Optional Error Code.
      ///
      void append(const char c, ERROR* err = 0);

      /// @details Erases current CString content and
      /// assigns this new CString.
      ///
      /// @param[in]	str		CString to assign.
      /// @param[out]	err		Optional Error Code.
      ///
      void assign(const CString& str, ERROR* err = 0);

      /// @details Erases current CString content and
      /// assigns this C-CString.
      ///
      /// @param[in]	str		C-CString to assign.
      /// @param[out]	err		Optional Error Code.
      ///
      void assign(const char* str, ERROR* err = 0);

      /// @details Erases current CString content and
      /// assigns this character.
      ///
      /// @param[in]	str		Character to assign.
      /// @param[out]	err		Optional Error Code.
      ///
      void assign(const char c, ERROR* err = 0);

      ///  @details Erases a specific element in the list.
      ///  
      ///  @param [in] pos Index of the element to remove.
      ///  @param [out] err Optional error code.
      /// 
      void erase(size_t pos, ERROR* err = 0);

      ///  @details Erases a range of items in the List.
      ///  
      ///  @param [in] beg Index of the first element to remove from.
      ///  @param [in] end Index of the last element to remove to.
      ///  @param [out] err Optional error code.
      /// 				
      void erase(size_t beg, size_t end, ERROR* err = 0);

      /// @details Subscript Operator that access the characters
      /// in the string.  If an invalid range is entered the first
      /// value of the string is returned.
      ///
      /// @return A character from the CString.
      ///
      char& operator[](size_t index);

      /// @details Subscript Operator that access the characters
      /// in the string.  If an invalid range is entered the first
      /// value of the string is returned.
      ///
      /// @return A constant character reference from the CString.
      ///
      const char& operator[](size_t index) const;

      /// @details Inserts a CString at the given position into
      /// the current string.
      ///
      /// @param[in]	pos		Position to insert(zero based).
      /// @param[in]	str		CString to insert.
      /// @param[out]	err		Optional error code.
      ///											ERR_RANGE for invalid position.
      ///											ERR_NONE on success.
      ///
      void insert(size_t pos, const CString& str, ERROR* err = 0);

      /// @details Inserts a C-CString at the given position into
      /// the current string.
      ///
      /// @param[in]	pos		Position to insert(zero based).
      /// @param[in]	str		C-CString to insert.
      /// @param[out]	err		Optional error code.
      ///											ERR_RANGE for invalid position.
      ///											ERR_NONE on success.
      ///											ERR_INVALID for null str pointer.
      ///
      void insert(size_t pos, const char* str, ERROR* err = 0);

      /// @details Inserts a character at the given position into
      /// the current string.
      ///
      /// @param[in]	pos		Position to insert(zero based).
      /// @param[in]	c			Character to insert.
      /// @param[out]	err		Optional error code.
      ///											ERR_RANGE for invalid position.
      ///											ERR_NONE on success.
      ///
      void insert(size_t pos, const char c, ERROR* err = 0);

      /// @details Append this CString and self assign the result.
      ///
      /// @param[in]	str		CString to append to this CString.
      ///
      /// @return This result.
      ///
      CString& operator+=(const CString& str);

      /// @details Append this CCString and self assign the result.
      ///
      /// @param[in]	str		CCString to append to this CString.
      ///
      /// @return This result.
      ///
      CString& operator+=(const char* str);

      /// @details Append this character and self assign the result.
      ///
      /// @param[in]	c			Character to append to this CString.
      ///
      /// @return This result.
      ///
      CString& operator+=(char c);

      /// @details Appends the right hand side CString to the
      /// left hand side CString and returns the resulting
      /// CString.
      ///
      ///	@param[in]		rhs		Right hand side CString.
      ///
      /// @return	The appended result.
      ///
      CString operator+(const CString& rhs);

      /// @details Appends the right hand side C-CString to the
      /// left hand side CString and returns the resulting
      /// CString.
      ///
      ///	@param[in]		rhs		Right hand side CString.
      ///
      /// @return	The appended result.
      ///
      CString operator+(const char* rhs);

      /// @details Appends the right hand side string to the
      /// left hand side string and returns the resulting
      /// string.
      ///
      ///	@param[in]		rhs		Right hand side string.
      ///
      /// @return	The appended result.
      ///
      CString operator+(const char rhs);

      /// @details Assignment the CString on the right hand
      /// side to the CString on the left hand side of the
      /// operator.
      ///
      /// @param[in] 	str		CString to assign
      ///
      /// @return	Reference CString that is assigned.
      CString& operator=(const CString& str);

      /// @details Assignment the C-CString on the right hand
      /// side to the CString on the left hand side of the
      /// operator.
      ///
      /// @param[in] 	str		C-CString to assign
      ///
      /// @return	Reference CString that is assigned.
      CString& operator=(const char* str);

      /// @details Assignment the character on the right hand
      /// side to the CString on the left hand side of the
      /// operator.
      ///
      /// @param[in] 	str		character to assign
      ///
      /// @return	Reference CString that is assigned.
      CString& operator=(char c);

      /// @brief Generates a substring of the CString starting
      /// at the given position and of the given length.
      ///
      /// @param[in]	pos		Position in the CString.
      /// @param[in]	len		length to make the string.
      /// @param[out]	err		Optional error code.
      ///
      /// @return	A substring of the string; or an empty string
      /// if an error occured.
      ///
      CString substr(size_t pos = 0, size_t len = 1, ERROR* err = 0) const;

      /// @brief Finds a given CString of characters within the current
      /// CString.
      ///
      /// @param[in]	str		CString to search for.
      /// @param[in]  pos		An intial position to start the search.
      /// @param[out]	err		An optional Error Code.
      ///
      /// @return	Position of the first instance of the CString being
      /// searched for.
      ///
      size_t find(const CString& str, size_t pos = 0, ERROR* err = 0) const;

      /// @brief Finds a given CString of characters within the current
      /// CString.
      ///
      /// @param[in]	str		C-CString to search for.
      /// @param[in]  pos		An intial position to start the search.
      /// @param[out]	err		An optional Error Code.
      ///
      /// @return	Position of the first instance of the C-CString being
      /// searched for.
      ///
      size_t find(const char* str, size_t pos = 0, ERROR* err = 0) const;

      /// @brief finds a given CString of characters within the current
      /// CString.
      ///
      /// @param[in]	c			Character to search for.
      /// @param[in]  pos		An intial position to start the search.
      /// @param[out]	err		An optional Error Code.
      ///
      /// @return	Position of the first instance of the character being
      /// searched for.
      ///
      size_t find(char c, size_t pos = 0, ERROR* err = 0) const;

    private:
      char* m_pCString; ///> The underlying C-CString.

    };

    // Relational Operators
  };
};

#endif

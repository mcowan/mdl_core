/*
 * Point_source.hpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#ifndef POINT_SOURCE_HPP_
#define POINT_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Datatype;

template <typename T>
Point<T>::Point()
{
  x = T();
  y = T();
  z = T();
}

template <typename T>
Point<T>::Point(T X, T Y)
{
  x = X;
  y = Y;
  z = T();
}

template <typename T>
Point<T>::Point(T X, T Y, T Z)
{
  x = X;
  y = Y;
  z = Z;
}

template <typename T>
Point<T>::~Point()
{
};

template <typename T>
Point<T>& Point<T>::operator=(const Point<T>& other)
{
  x = other.x;
  y = other.y;
  z = other.z;
}

template <typename T>
bool Point<T>::operator==(const Point<T>& other) const
{
  return ((x == other.x) && (y = other.y) && (z == other.z));
}

template <typename T>
bool Point<T>::operator!=(const Point<T>& other) const
{
  return !(*this == other);
}

#endif

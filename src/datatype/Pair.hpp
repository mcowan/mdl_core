/// @file container/Pair.hpp
///
/// @brief Defines the Pair interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Pair class.

#ifndef PAIR_HPP_
#define PAIR_HPP_

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief This namespace is wrapper for the Container classes.
  ///
  namespace ns_Datatype
  {
    /// @brief A simple class that binds two data types
    /// as a pair.  A left and a right element that can be of
    /// the same type or different types.  Left and Right values
    /// can be set simulataneously with the constructor and are
    /// left with public access to get and set the values.
    ///
    /// @param[in] L  Left datatype.
    /// @param[in] R  Right datatype.
    ///

    template <typename L, typename R>
    class Pair
    {
    public:
      /// @brief Default Pair constructor.
      ///
      Pair();

      /// @brief Constructs Pair object and initializes values to given
      /// values.
      ///
      /// @param[in] left  Left object.
      /// @param[in] right  Right object.
      ///
      Pair(L left, R right);

      /// @brief Deconstructor.
      ///
      ~Pair();

      /// @brief Assignment operator sets the values of the given Pair on
      /// the right to the Pair on the left.
      ///
      /// @param[in] other  The Pair to copy values from.
      ///
      /// @returns A configured Pair.
      ///
      Pair<L, R>& operator=(const Pair<L, R>& other);

      /// @brief Equality Operator.
      ///
      /// @param[in]  other Right hand side object.
      ///
      /// @return True, if objects are equal; False, otherwise.
      ///
      bool operator==(const Pair<L, R>& other) const;

      /// @brief In-Equality Operator.
      ///
      /// @param[in]  other Right hand side object.
      ///
      /// @return False, if objects are equal; True, otherwise.
      ///
      bool operator!=(const Pair<L, R>& other) const;

    public:
      L Left; /*!< Left object, left as public to access.*/
      R Right; /*!< Right object, left as public to access.*/
    };
  }
}

#include <datatype/Pair_source.hpp>  // Template Source file

#endif 

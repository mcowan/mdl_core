/*
 * Pair_source.hpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */

#ifndef PAIR_SOURCE_HPP_
#define PAIR_SOURCE_HPP_

using namespace ns_MDL;
using namespace ns_Datatype;

template <typename L, typename R>
Pair<L, R>::Pair()
{
}

template <typename L, typename R>
Pair<L, R>::Pair(L left, R right)
{
  Left = left;
  Right = right;
}

template <typename L, typename R>
Pair<L, R>::~Pair()
{
}

template <typename L, typename R>
Pair<L, R>& Pair<L, R>::operator=(const Pair<L, R>& other)
{
  Left = other.Left;
  Right = other.Right;

  return *this;
}

template <typename L, typename R>
bool Pair<L, R>::operator==(const Pair<L, R>& other) const
{
  return ((Left == other.Left) && (Right == other.Right));
}

template <typename L, typename R>
bool Pair<L, R>::operator!=(const Pair<L, R>& other) const
{
  return !(*this == other);
}

#endif

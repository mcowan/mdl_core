/// @file datatype/CString.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details Source code for the CString class.



#include <datatype/CString.hpp>
#include <common/MemOperations.hpp>
#include <stdio.h>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief General Datatype namespace
  ///
  namespace ns_Datatype
  {

    CString::CString()
    : m_pCString(0)
    {
      m_pCString = new char[1];
      m_pCString[0] = '\0';
    }

    CString::CString(const char* other)
    : m_pCString(0)
    {
      size_t length = StrLen(other);
      m_pCString = new char[length + 1]; // offset for NULL
      MemCopy(m_pCString, other, length);
      m_pCString[length] = '\0';
    }

    CString::CString(const CString& other)
    : m_pCString(0)
    {
      size_t length = StrLen(other.c_str());
      m_pCString = new char[length + 1]; // offset for NULL
      MemCopy(m_pCString, other.c_str(), length);
      m_pCString[length] = '\0';
    }

    CString::~CString()
    {
      delete[] m_pCString;
    }

    size_t
    CString::size() const
    {
      return StrLen(m_pCString);
    }

    void
    CString::clear()
    {
      if (StrLen(m_pCString) > 0) {
        delete[] m_pCString; // Remove the old string
        m_pCString = new char[1]; // Create a new empty string.
        m_pCString[0] = '\0';
      }
    }

    bool
    CString::empty() const
    {
      return StrLen(m_pCString) == 0;
    }

    const char*
    CString::c_str() const
    {
      return m_pCString;
    }

    void
    CString::swap(size_t index1, size_t index2, ERROR* err)
    {
      size_t length = StrLen(m_pCString);
      if (index1 < length && index2 < length) {
        // Swap Characters
        char temp = m_pCString[index1];
        m_pCString[index1] = m_pCString[index2];
        m_pCString[index2] = temp;

        // No Error
        SetErr(err, ERR_NONE);
      }
      else {
        // Invalid Index
        SetErr(err, ERR_RANGE);
      }
    }

    void
    CString::append(const CString& str, ERROR* err)
    {
      SetErr(err, ERR_NONE);

      // get string length
      size_t length = size() + str.size();

      // create temporary memory space
      char* temp = new char[length + 1];

      // create string
      snprintf(temp, length + 1, "%s%s", m_pCString, str.c_str());

      // Store string
      delete[] m_pCString;
      m_pCString = temp;
    }

    void
    CString::append(const char* str, ERROR* err)
    {
      if (str != 0) {
        SetErr(err, ERR_NONE);

        // get string length
        size_t length = size() + StrLen(str);

        // create temporary memory space
        char* temp = new char[length + 1];

        // create string
        snprintf(temp, length + 1, "%s%s", m_pCString, str);

        // Store string
        delete[] m_pCString;
        m_pCString = temp;
      }
      else {
        SetErr(err, ERR_INVALID);
      }
    }

    void
    CString::append(const char c, ERROR* err)
    {
      SetErr(err, ERR_NONE);

      // get string length
      size_t length = size() + 1;

      // create temporary memory space
      char* temp = new char[length + 1];

      // create string
      snprintf(temp, length + 1, "%s%c", m_pCString, c);

      // Store string
      delete[] m_pCString;
      m_pCString = temp;
    }

    void
    CString::assign(const CString& str, ERROR* err)
    {
      SetErr(err, ERR_NONE);
      // get string length
      size_t length = str.size();

      // create new string
      char* temp = new char[length + 1]; // offset for NULL
      MemCopy(temp, str.c_str(), length);
      temp[length] = '\0';

      // remove the old string
      delete[] m_pCString;

      // set new string
      m_pCString = temp;
    }

    void
    CString::assign(const char* str, ERROR* err)
    {
      if (str != 0) {
        SetErr(err, ERR_NONE);
        // get string length
        size_t length = StrLen(str);

        // create new string
        char* temp = new char[length + 1]; // offset for NULL
        MemCopy(temp, str, length);
        temp[length] = '\0';

        // remove the old string
        delete[] m_pCString;

        // set new string
        m_pCString = temp;
      }
      else {
        SetErr(err, ERR_INVALID);
      }
    }

    void
    CString::assign(const char c, ERROR* err)
    {
      // create new string
      char* temp = new char[2]; // offset for NULL and single character
      temp[0] = c;
      temp[1] = '\0';

      // remove the old string
      delete[] m_pCString;

      // set new string
      m_pCString = temp;
    }

    char& CString::operator[](size_t index)
    {
      if (index < size()) {
        return m_pCString[index];
      }
      else {
        return m_pCString[0];
      }
    }

    const char& CString::operator[](size_t index) const
    {
      if (index < size()) {
        return m_pCString[index];
      }
      else {
        return m_pCString[0];
      }
    }

    void
    CString::erase(size_t pos, ERROR* err)
    {
      if (pos < size()) {
        size_t length = size();
        char* temp = new char[length + 1];

        // Copy First Part
        for (size_t i = 0; i < pos; i++) {
          temp[i] = m_pCString[i];
        }

        // Copy End Part
        for (size_t i = pos; i < length; i++) {
          temp[i] = m_pCString[i + 1];
        }

        delete[] m_pCString;
        m_pCString = temp;

        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_RANGE);
      }
    }

    void
    CString::erase(size_t beg, size_t end, ERROR* err)
    {
      if (beg < end) {
        if ((beg < size() + 1) && (end < size() + 1)) {
          size_t length = size() - (end - beg) + 1; // adjust and remove the extra characters
          char* temp = new char[length + 1];
          size_t pos = 0;

          // Copy First Part
          for (size_t i = 0; i < beg; i++) {
            temp[pos] = m_pCString[i];
            pos++;
          }

          // Copy End Part
          for (size_t i = end; i < length; i++) {
            temp[pos] = m_pCString[i];
            pos++;
          }

          delete[] m_pCString;
          m_pCString = temp;
          m_pCString[pos] = '\0';

          SetErr(err, ERR_NONE);
        }
        else {
          SetErr(err, ERR_RANGE);
        }
      }
      else {
        SetErr(err, ERR_INVALID);
      }
    }

    void
    CString::insert(size_t pos, const CString& str, ERROR* err)
    {
      insert(pos, str.c_str(), err);
    }

    void
    CString::insert(size_t pos, const char* str, ERROR* err)
    {
      if (pos <= size()) {
        size_t length = size() + StrLen(str);
        char* temp = new char[length];

        MemCopy(&temp[0], &m_pCString[0], pos);
        MemCopy(&temp[pos], str, StrLen(str));
        MemCopy(&temp[pos + StrLen(str)], &m_pCString[pos], size() - pos);
        temp[length] = '\0';

        delete[] m_pCString;
        m_pCString = temp;
        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_RANGE);
      }
    }

    void
    CString::insert(size_t pos, const char c, ERROR* err)
    {
      if (pos <= size()) {
        size_t length = size() + 1;
        char* temp = new char[length];

        MemCopy(&temp[0], &m_pCString[0], pos);
        temp[pos] = c;
        MemCopy(&temp[pos + 1], &m_pCString[pos], size() - pos);
        temp[length] = '\0';

        delete[] m_pCString;
        m_pCString = temp;
        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_RANGE);
      }
    }

    CString& CString::operator+=(const CString& str)
    {
      append(str);
      return (*this);
    }

    CString& CString::operator+=(const char* str)
    {
      append(str);
      return (*this);
    }

    CString& CString::operator+=(char c)
    {
      append(c);
      return (*this);
    }

    CString CString::operator+(const CString& rhs)
    {
      CString temp(m_pCString);
      temp.append(rhs);
      return temp;
    }

    CString CString::operator+(const char* rhs)
    {
      CString temp(m_pCString);
      temp.append(rhs);
      return temp;
    }

    CString CString::operator+(const char rhs)
    {
      CString temp(m_pCString);
      temp.append(rhs);
      return temp;
    }

    CString& CString::operator=(const CString& str)
    {
      assign(str);
      return (*this);
    }

    CString& CString::operator=(const char* str)
    {
      assign(str);
      return (*this);
    }

    CString& CString::operator=(char c)
    {
      assign(c);
      return (*this);
    }

    CString
    CString::substr(size_t pos, size_t len, ERROR* err) const
    {
      CString temp;
      if (len < size()) {
        char str[len + 1];
        ns_MDL::MemSet(str, 0, len + 1);
        StrCopy(str, &m_pCString[pos], len);
        temp = str;
        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_RANGE);
      }
      return temp;
    }

    size_t
    CString::find(const CString& str, size_t pos, ERROR* err) const
    {
      size_t toReturn = 0;

      if (pos < size() && size() >= str.size()) {
        size_t cmpLength = str.size();
        size_t stopPoint = size() - str.size();
        SetErr(err, ERR_NOT_FOUND);

        for (size_t index = pos; index <= stopPoint; index++) {
          if (MemCompare(&m_pCString[index], str.c_str(), cmpLength) == 0) {
            SetErr(err, ERR_NONE);
            toReturn = index;
            break;
          }
        }
      }
      else {
        SetErr(err, ERR_RANGE);
      }
      return toReturn;
    }

    size_t
    CString::find(const char* str, size_t pos, ERROR* err) const
    {
      size_t toReturn = 0;
      if (pos < size() && size() > StrLen(str)) {
        size_t cmpLength = StrLen(str);
        size_t stopPoint = size() - StrLen(str);
        SetErr(err, ERR_NOT_FOUND);

        for (size_t index = pos; index <= stopPoint; index++) {
          if (MemCompare(&m_pCString[index], str, cmpLength) == 0) {
            SetErr(err, ERR_NONE);
            toReturn = index;
            break;
          }
        }
      }
      else {
        SetErr(err, ERR_RANGE);
      }
      return toReturn;
    }

    size_t
    CString::find(char c, size_t pos, ERROR* err) const
    {
      size_t toReturn = 0;
      if (pos < size() && size() > 0) {
        size_t stopPoint = size() - 1;
        SetErr(err, ERR_NOT_FOUND);

        for (size_t index = pos; index <= stopPoint; index++) {
          if (m_pCString[index] == c) {
            SetErr(err, ERR_NONE);
            toReturn = index;
            break;
          }
        }
      }
      else {
        SetErr(err, ERR_RANGE);
      }
      return toReturn;
    }

  };
};


/// @file HW_GPIO.hpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A header file describing functions to access system variables 
/// pertaining to the hardware.


#ifndef _CLI_HPP_
#define _CLI_HPP_

#include <Types.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{

  class CLI
  {
  public:
    CLI();

    ~CLI();

  private:

  };

};

#endif


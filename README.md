# README #

This README would normally document whatever steps are necessary to get your application up and running.

# What is this repository for? #

##Quick summary
This is a set of c++ libraries and development configurations for multiple operating systems.  Its main design is for embedded platforms such as an Arduino.  

##Version
1.0.0


# How do I get set up? #
Downloading Source

> git clone https://mcowan@bitbucket.org/mcowan/mdl_core.git

## Compiling an Executable.
 
   1. Change to an app directory and run "make".
   2. Application will compile and executable will be found at "dst/app/<application_name>.out".


## Compiling a Library

   1. Change to a src directory and run "make".
   2. Object files will be found at "dst/obj".
   3. Static Libraries will be found at "dst/lib".

## Running Tests on a Library

   1. Change to a src directory and run "make test".
   2. Code will compile and tests will be executed on a successful compile.  
   3. An ".xml" test results file will be generated and can be found at "dst/test-results".
   4. A test executable can be found at "dst/tcreateest".

## Generating a Library code coverage.
   1. Change to a src directory and run "make coverage".
   2. Code will compile and tests will be executed on a successful compile.
   3. Coverage files will be generated using lcov and genhtml to an html interface displaying the line, branch, and function coverage.
   4. Running this agaist multiple libraries will combine the results.
   5. The information can be accessed at "dst/test-results/HTML/index.html"


# Dependencies #
This code repository is being developed on MacOsX; however, it is also being developed for Linux.  At some point in the future an OS abstraction layer for Windows and Arduino will be developed. 

   1. Database configuration
   2. How to run tests
   3. Deployment instructions

# Contribution guidelines #
General Guidelines for contributing code.

* Writing tests

Any code that is added to this repository should have the appropriate tests added to verify its integrity.  The standard practice for development applied to these libraries is [Test Driven Development (TDD)](http://en.wikipedia.org/wiki/Test-driven_development).  The point of these tests is to verify that when new code is added, existing code is not broke.

* Other guidelines

All tests added to the repository should also be verified using the code coverage tools to verify test completeness, and minimize any redundancies.

### Who do I talk to? ###

* Repo admin -- Micheal Cowan -- mjcmicrodevel@hotmail.com


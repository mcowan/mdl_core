/*
 * Clock.cpp
 *
 *  Created on: Jan 9, 2015
 *  Author: simplymac
 *  Description: An implementation of a templated Queue Container.  Based
 *  on the basic container interface.
 *
 */


#include <Clock.hpp>
#include <common/Error.hpp>
#include <ctime>
#include <stdio.h>

namespace ns_MDL
{

  const char* Clock::TimeFormatString = "%02i:%02i:%02i"; // HH:MM:SS
  const char* Clock::DateFormatString = "%04i:%02i:%02i"; // YYYY:MM:DD
  const size_t Clock::TimeFormatSize = sizeof (Clock::TimeFormatString) + 1;
  const size_t Clock::DateFormatSize = sizeof (Clock::DateFormatString) + 1;
  const size_t Clock::DateTimeStringSize = Clock::DateFormatSize + Clock::TimeFormatSize;

  Clock::Clock(TimeFormat format)
  : m_Format(format)
  {
  }

  Clock::~Clock()
  {
  }

  DateTimeStruct
  Clock::Now(ERROR* err)
  {
    DateTimeStruct current;

    time_t t = time(0); // get time now
    struct tm * now = localtime(& t);

    if (now != 0) {
      current.year = now->tm_year + 1900;
      current.month = now->tm_mon + 1;
      current.day = now->tm_mday;

      // Set Hour
      if (m_Format == Standard && now->tm_hour > 12) {
        current.hour = now->tm_hour - 12;
      }
      else {
        current.hour = now->tm_hour;
      }

      current.min = now->tm_min;
      current.sec = now->tm_sec;

      // No Error
      SetErr(err, ERR_NONE);
    }
    else {
      SetErr(err, ERR_UNSPEC);
    }
    return current;
  }

  void
  Clock::DateTime(char* str, size_t bufferSize, ERROR* err)
  {
    // YYYY:MM:DD  -- nine characters for buffer
    if (bufferSize >= DateTimeStringSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, DateFormatString, current.year, current.month, current.day);
        str[sizeof (DateFormatString) + 2] = ' ';
        snprintf(&str[sizeof (DateFormatString) + 3], bufferSize, TimeFormatString, current.hour, current.min, current.sec);
      }
      else {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_UNSPEC);
    }
  }

  void
  Clock::Date(char* str, size_t bufferSize, ERROR* err)
  {
    // YYYY:MM:DD  -- nine characters for buffer
    if (bufferSize >= DateFormatSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, DateFormatString, current.year, current.month, current.day);
      }
      else {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_UNSPEC);
    }
  }

  void
  Clock::Time(char* str, size_t bufferSize, ERROR* err)
  {
    // HH:MM:SS  -- nine characters for buffer
    if (bufferSize >= TimeFormatSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, TimeFormatString, current.hour, current.min, current.sec);
      }
      else {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_UNSPEC);
    }
  }
} /* End of MDL */


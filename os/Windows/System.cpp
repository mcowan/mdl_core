/// @file MacOs/System.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A mac implementation of accessing system variables pertaining
/// to the hardware.

#include <Types.hpp>
#include <common/Error.hpp>

#include <System.hpp>
#include <stdio.h>

using namespace ns_MDL;

namespace ns_MDL
{
#define SYSCTL_BIG_ENDIAN 4321
#define SYSCTL_LITTLE_ENDIAN 1234

  unsigned long
  getTotalSystemMemory(ERROR* err)
  {
    return 0;
  }

  void
  getOSDescription(char* buffer, size_t bufferSize, ERROR* err)
  {
  }

  void
  getHardwareDescription(char* buffer, size_t bufferSize, ERROR* err)
  {
  }

  unsigned long
  getNumberOfCPUs(ERROR* err)
  {
    return 0;
  }

  unsigned long
  getCPUFrequency(ERROR* err)
  {
    return 0;
  }

  bool
  isBigEndian(ERROR* err)
  {
    return false;
  }
};



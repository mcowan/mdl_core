/*
 * Clock.cpp
 *
 *  Created on: Jan 9, 2015
 *  Author: simplymac
 *  Description: An implementation of a templated Queue Container.  Based
 *  on the basic container interface.
 *
 */

#include <Stopwatch.hpp>
#include <common/Error.hpp>

namespace ns_MDL
{

  Stopwatch::Stopwatch()
  {
    m_Start.tv_sec = 0;
    m_Start.tv_usec = 0;
    m_Running = false;
  }

  Stopwatch::~Stopwatch()
  {
    m_Start.tv_sec = 0;
    m_Start.tv_usec = 0;
    m_Running = false;
  }

  bool
  Stopwatch::isRunning()
  {
    return m_Running;
  }

  void
  Stopwatch::Start()
  {
    m_Running = true;
    gettimeofday(&m_Start, NULL);
  }

  double
  Stopwatch::Stop()
  {
    double toReturn = 0.0;
    if (m_Running) {
      m_Running = false;
      timeval temp;
      gettimeofday(&temp, NULL);

      toReturn = (temp.tv_sec - m_Start.tv_sec) + ((temp.tv_usec - m_Start.tv_usec) / 1000000.0);
    }
    return toReturn;
  }

  double
  Stopwatch::Time()
  {
    double toReturn = 0.0;
    if (m_Running) {
      timeval temp;

      gettimeofday(&temp, NULL);
      toReturn = (temp.tv_sec - m_Start.tv_sec) + ((temp.tv_usec - m_Start.tv_usec) / 1000000.0);
    }
    return toReturn;
  }
} /* End of MDL */


/// @file File.hpp
///
/// @brief Defines the File interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An interface to write and read from files, also
/// inherits the DeviceIF as a base class.  Allows it to be
/// tied to the Logger.

#ifndef _FILE_HPP_
#define _FILE_HPP_

#include <DeviceIF.hpp>
#include <Types.hpp>
#include <datatype/CString.hpp>

class FileHandlerImplementation;

using namespace ns_Datatype;

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief An interface to handle writing to a File.
  ///

  class FileIF : public DeviceIF
  {
  public:

    enum FileMode
    {
      Read = 0,
      Write = 1,
      ReadWrite = 2,
    };

  public:
    FileIF();
    ~FileIF();

    void open(const char* name, FileMode mode = FileIF::ReadWrite, bool binary = false, bool truncate = true, ERROR* error = 0);
    void open(CString name, FileMode mode = FileIF::ReadWrite, bool binary = false, bool truncate = true, ERROR* error = 0);
    void close(ERROR* error = 0);
    bool isOpen(ERROR* error = 0);

    void write(const char* str, size_t buffer_size, ERROR* error = 0);
    void write(CString str, ERROR* error = 0);
    void write(unsigned short value, ERROR* error = 0);
    void write(unsigned int value, ERROR* error = 0);

    size_t read(char* buf, size_t bufferSize, ERROR* error = 0);
    CString readLine(ERROR* error = 0);

    void setReadPosition(size_t pos, ERROR* error = 0);
    size_t getReadPosition(ERROR* error = 0);
    void setWritePosition(size_t pos, ERROR* error = 0);
    size_t getWritePosition(ERROR* error = 0);
    size_t size(ERROR* error = 0);

    CString getFileName();
    CString getFullPath();
    CString getFileExtension();
    bool isBinary(ERROR* error = 0);
    bool isTruncate(ERROR* error = 0);
    FileMode getMode(ERROR* error = 0);

  private:
    FileHandlerImplementation* m_pImp;
  };

  class Directory
  {
  public:
    // Methods to determine if the requested file path exists.
    static bool Exists(const char* filepath, ERROR* error = 0);
    static bool Exists(CString filepath, ERROR* error = 0);

    // Create the requested directory hierarchy -- if intermediate 
    // directories do not exist they will be created.
    static bool Mkdir(const char* filepath, ERROR* error = 0);
    static bool Mkdir(CString filepath, ERROR* error = 0);

    // Delete the file.
    static bool Remove(const char* filepath, ERROR* error = 0);
    static bool Remove(CString filepath, ERROR* error = 0);

    static bool Rmdir(const char* filepath, ERROR* error = 0);
    static bool Rmdir(CString filepath, ERROR* error = 0);

    static CString Cwd(ERROR* error = 0);

    static void CopyFile(const char* srcFile, const char* dstFile, ERROR* error = 0);
    static void MoveFile(const char* srcFile, const char* dstFile, ERROR* error = 0);
  };
};

#endif

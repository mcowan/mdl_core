/** 
 * @file types.hpp
 * @breif Defines all of the basic types to be included.
 */


// include size_t
#include <stddef.h>

// include standard datatype sizes
#include <stdint.h>

#ifndef __TYPES_HPP__
#define __TYPES_HPP__

/// @brief Create a type specific for Errors.
typedef int32_t ERROR;

#ifndef NULL
#define NULL 0
#endif

#endif
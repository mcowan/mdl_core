/// @file container/Array.hpp
///
/// @brief Defines the Array interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of a templated Array class.

#ifndef __CONSOLE_HPP__
#define __CONSOLE_HPP__

#include <DeviceIF.hpp>
#include <Types.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief An interface to handle writing to the console.
  ///

  class Console : public DeviceIF
  {
  public:
    Console();
    ~Console();

    void write(const char* str, size_t buffer_size, ERROR* error = 0);

    size_t read(char* buf, size_t bufferSize, ERROR* error = 0);
  };
};

#endif

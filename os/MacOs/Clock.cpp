/*
 * @file Clock.cpp
 * @brief An implementation of a templated Queue Container.  Based on the basic
 * container interface.
 */


#include <Clock.hpp>
#include <common/Error.hpp>
#include <ctime>
#include <stdio.h>

namespace ns_MDL
{
  const char* Clock::TimeFormatCString = "%02i:%02i:%02i"; // HH:MM:SS
  const char* Clock::DateFormatCString = "%04i/%02i/%02i"; // YYYY:MM:DD
  const size_t Clock::TimeFormatSize = 9;
  const size_t Clock::DateFormatSize = 11;
  const size_t Clock::DateTimeCStringSize = Clock::DateFormatSize + Clock::TimeFormatSize;

  Clock::Clock(TimeFormat format)
  : m_Format(format)
  {
  }

  Clock::~Clock()
  {
  }

  DateTimeStruct
  Clock::Now(ERROR* error)
  {
    DateTimeStruct current;

    time_t t = time(0); // get time now
    struct tm * now = localtime(& t);

    if (now != 0) {
      current.year = now->tm_year + 1900;
      current.month = now->tm_mon + 1;
      current.day = now->tm_mday;

      // Set Hour
      if (m_Format == Standard && now->tm_hour > 12) {
        current.hour = now->tm_hour - 12;
      }
      else {
        current.hour = now->tm_hour;
      }

      current.min = now->tm_min;
      current.sec = now->tm_sec;

      // No Error
      SetErr(error, ERR_NONE);
    }
    else {
      SetErr(error, ERR_UNSPEC);
    }
    return current;
  }

  void
  Clock::DateTime(char* str, size_t bufferSize, ERROR* error)
  {
    // YYYY:MM:DD  -- nine characters for buffer
    if (bufferSize >= DateTimeCStringSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, DateFormatCString, current.year, current.month, current.day);
        str[sizeof (DateFormatCString) + 2] = ' ';
        snprintf(&str[sizeof (DateFormatCString) + 3], bufferSize, TimeFormatCString, current.hour, current.min, current.sec);
      }
      else {
        SetErr(error, ERR_UNSPEC);
      }
    }
    else {
      SetErr(error, ERR_UNSPEC);
    }
  }

  void
  Clock::Date(char* str, size_t bufferSize, ERROR* error)
  {
    // YYYY:MM:DD  -- nine characters for buffer
    if (bufferSize >= DateFormatSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, DateFormatCString, current.year, current.month, current.day);
      }
      else {
        SetErr(error, ERR_UNSPEC);
      }
    }
    else {
      SetErr(error, ERR_UNSPEC);
    }
  }

  void
  Clock::Time(char* str, size_t bufferSize, ERROR* error)
  {
    // HH:MM:SS  -- nine characters for buffer
    if (bufferSize >= TimeFormatSize) {
      if (str != 0) {
        DateTimeStruct current = Now();
        snprintf(str, bufferSize, TimeFormatCString, current.hour, current.min, current.sec);
      }
      else {
        SetErr(error, ERR_UNSPEC);
      }
    }
    else {
      SetErr(error, ERR_UNSPEC);
    }
  }

  CString
  Clock::DateTime(ERROR* error)
  {
    // YYYY:MM:DD  -- nine characters for buffer
    return CString(Date(error) + " " + Time(error));
  }

  CString
  Clock::Date(ERROR* error)
  {
    char str[DateFormatSize];
    DateTimeStruct current = Now(error);
    snprintf(str, DateFormatSize, DateFormatCString, current.year, current.month, current.day);
    return CString(str);
  }

  CString
  Clock::Time(ERROR* error)
  {
    char str[TimeFormatSize];

    DateTimeStruct current = Now(error);

    snprintf(str, TimeFormatSize, TimeFormatCString, current.hour, current.min, current.sec);

    return CString(str);
  }

} /* End of MDL */


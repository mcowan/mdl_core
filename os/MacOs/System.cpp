/*
 * @file MacOs/System.cpp
 * @brief A mac implementation of accessing system variables pertaining
 * to the hardware.
 */

#include <sys/sysctl.h>
#include <Types.hpp>
#include <common/Error.hpp>

#include <System.hpp>
#include <stdio.h>

using namespace ns_MDL;

namespace ns_MDL
{
#define SYSCTL_BIG_ENDIAN 4321
#define SYSCTL_LITTLE_ENDIAN 1234

  unsigned long
  getTotalSystemMemory(ERROR* error)
  {
    int mib [] = {CTL_HW, HW_MEMSIZE};
    unsigned long value = 0;
    size_t length = sizeof (value);

    if (-1 == sysctl(mib, 2, &value, &length, NULL, 0)) {
      SetErr(error, ERR_UNSPEC);
    }
    else {
      SetErr(error, ERR_NONE);
    }

    return value;
  }

  void
  getOSDescription(char* buffer, size_t bufferSize, ERROR* error)
  {
    SetErr(error, ERR_INVALID);
    size_t length = 0;
    size_t pos = 0;

    if (buffer != 0) {
      SetErr(error, ERR_NONE);
      int HW_Arch[] = {CTL_HW, HW_MACHINE}; // x86_64
      int Kern_OsType[] = {CTL_KERN, KERN_OSTYPE}; // Darwin
      int Kern_OsRelease[] = {CTL_KERN, KERN_OSRELEASE}; // 14.0.0

      // Get the OS Type
      if (-1 == sysctl(Kern_OsType, 2, NULL, &length, NULL, 0)) {
        SetErr(error, ERR_UNSPEC);
      }

      if (length <= bufferSize) {
        if (-1 == sysctl(Kern_OsType, 2, &buffer[pos], &length, NULL, 0)) {
          SetErr(error, ERR_UNSPEC);
        }
        pos += length - 1;
        buffer[pos++] = ' ';
      }
      else {
        SetErr(error, ERR_RANGE);
      }

      // Get the Hardware Architecture
      if (-1 == sysctl(Kern_OsRelease, 2, NULL, &length, NULL, 0)) {
        SetErr(error, ERR_UNSPEC);
      }

      if (length <= bufferSize) {
        if (-1 == sysctl(Kern_OsRelease, 2, &buffer[pos], &length, NULL, 0)) {
          SetErr(error, ERR_UNSPEC);
        }
        pos += length - 1;
        buffer[pos++] = ' ';
      }
      else {
        SetErr(error, ERR_RANGE);
      }

      // Get the Hardware Architecture
      if (-1 == sysctl(HW_Arch, 2, NULL, &length, NULL, 0)) {
        SetErr(error, ERR_UNSPEC);
      }

      if (length <= bufferSize) {
        if (-1 == sysctl(HW_Arch, 2, &buffer[pos], &length, NULL, 0)) {
          SetErr(error, ERR_UNSPEC);
        }
        pos += length - 1;
      }
      else {
        SetErr(error, ERR_RANGE);
      }
    }
  }

  void
  getHardwareDescription(char* buffer, size_t bufferSize, ERROR* error)
  {
    SetErr(error, ERR_INVALID);
    size_t length = 0;
    if (buffer != 0) {
      int HW_Model [] = {CTL_HW, HW_MODEL};


      if (-1 == sysctl(HW_Model, 2, NULL, &length, NULL, 0)) {
        SetErr(error, ERR_UNSPEC);
      }

      if (length <= bufferSize) {
        if (-1 == sysctl(HW_Model, 2, buffer, &length, NULL, 0)) {
          SetErr(error, ERR_UNSPEC);
        }
        else {
          SetErr(error, ERR_NONE);
        }
      }
      else {
        SetErr(error, ERR_RANGE);
      }
    }
  }

  unsigned long
  getNumberOfCPUs(ERROR* error)
  {
    int mib [] = {CTL_HW, HW_NCPU};
    unsigned long value = 0;
    size_t length = sizeof (value);

    if (-1 == sysctl(mib, 2, &value, &length, NULL, 0)) {
      SetErr(error, ERR_UNSPEC);
    }
    else {
      SetErr(error, ERR_NONE);
    }

    return value;
  }

  unsigned long
  getCPUFrequency(ERROR* error)
  {
    int mib [] = {CTL_HW, HW_CPU_FREQ};
    unsigned long value = 0;
    size_t length = sizeof (value);

    if (-1 == sysctl(mib, 2, &value, &length, NULL, 0)) {
      SetErr(error, ERR_UNSPEC);
    }
    else {
      SetErr(error, ERR_NONE);
    }

    return value;
  }

  bool
  isBigEndian(ERROR* error)
  {
    int mib [] = {CTL_HW, HW_BYTEORDER};
    unsigned long value = 0;
    size_t length = sizeof (value);

    if (-1 == sysctl(mib, 2, &value, &length, NULL, 0)) {
      SetErr(error, ERR_UNSPEC);
    }
    else {
      SetErr(error, ERR_NONE);
    }

    return (value == SYSCTL_BIG_ENDIAN);
  }
};
/// @file HW_GPIO.hpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A header file describing functions to access system variables 
/// pertaining to the hardware.


#ifndef HW_GPIO_HPP_
#define HW_GPIO_HPP_

#include <Types.hpp>
#include <container/Vector.hpp>

#ifndef SIM_NUM_PINS
#define SIM_NUM_PINS 255
#endif

#ifndef DIGITAL_HIGH
#define DIGITAL_HIGH 1
#endif

#ifndef DIGITAL_LOW
#define DIGITAL_LOW 0
#endif

#ifndef ANALOG_READ_HIGH
#define ANALOG_READ_HIGH 1023
#endif

#ifndef ANALOG_READ_LOW
#define ANALOG_READ_LOW 0
#endif

#ifndef ANALOG_WRITE_HIGH
#define ANALOG_WRITE_HIGH 255
#endif

#ifndef ANALOG_WRITE_LOW
#define ANALOG_WRITE_LOW 0
#endif

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{

  enum PIN_MODE
  {
    AnalogInput = 0, // Analag reads.
    AnalogOutput = 1, // Analog writes.
    DigitalInput = 2, // Digital reads.
    DigitalOutput = 3, // Digital writing.
    SerialLock = 4 // Locks the pin for Serial Device use.
  };

  class Pin
  {
  public:
    Pin();

    Pin(size_t pinNumber, PIN_MODE mode);
    ~Pin();

    void setPin(size_t pinNumber, ERROR* error = 0);
    size_t getPin(ERROR* error = 0);

    PIN_MODE getMode(ERROR* error = 0);
    void setMode(PIN_MODE mode, ERROR* error = 0);

    void write(unsigned short value, ERROR* error = 0);
    unsigned short read(ERROR* error = 0);
    void initialize();

  private:
    PIN_MODE m_Mode;
    size_t m_PinNumber;
    unsigned short m_Value;
    bool m_Initialized;
  };

  class HW_GPIO
  {
  public:
    HW_GPIO();

    ~HW_GPIO();

    void initialize();

    Vector<Pin> Pins;
  };
};

#endif


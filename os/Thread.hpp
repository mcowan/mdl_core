/*
 * @file Thread.hpp
 * @brief Defines a multi-thread implementation.
 */


#ifndef THREAD_HPP_
#define THREAD_HPP_

#include <Types.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{

  class Thread
  {
  public:
    Thread();

    ~Thread();

    static void Sleep(size_t milliseconds);

  private:

  };

};

#endif


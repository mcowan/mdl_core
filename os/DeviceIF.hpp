/*
 * @file DeviceIF.hpp
 * @brief This file contains a set of common methods that all collections will
 * inherit and define. 
 */

#ifndef DEVICE_IF_HPP_
#define DEVICE_IF_HPP_

#include <common/Error.hpp>
#include <Types.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{

  class DeviceIF
  {
  public:

    DeviceIF()
    {
    };

    virtual ~DeviceIF()
    {
    };

    virtual void write(const char* str, size_t buffer_size, ERROR* error = 0) = 0;

    virtual size_t read(char* buf, size_t bufferSize, ERROR* error = 0) = 0;
  };
};

#endif


#include <algorithm>
#include <iostream>
#include <iterator>
#include <FileIF.hpp>
#include <stdio.h>
#include <common/Error.hpp>
#include <fstream>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <cstring>

using namespace ns_Datatype;
using namespace ns_MDL;
using namespace std;


// Converts the Systems Errno error codes to Common Error Codes.

void
ProcessErrno(ERROR* err)
{
  int error = errno;

  switch (error) {
  case EROFS:
  case EPERM:
  case EACCES:
  {
    // The parent directory resides on a read-only file system.
    // Search permission is denied on a component of the path prefix, or write permission is denied on the parent directory of the directory to be created.
    SetErr(err, ERR_ACCESS);
    break;
  }
  case EEXIST:
  {
    //The named file exists.
    SetErr(err, ERR_DUPLICATE);
    break;
  }
  case ENAMETOOLONG:
  case EMLINK:
  {
    //The length of the path argument exceeds {PATH_MAX} or a pathname component is longer than {NAME_MAX}.
    //The link count of the parent directory would exceed {LINK_MAX}.
    SetErr(err, ERR_FULL);
    break;
  }
  case ELOOP:
  case ENOTDIR:
  case ENOENT:
  case EFAULT:
  case EISDIR:
  {
    //A component of the path prefix is not a directory.
    //A component of the path prefix specified by path does not name an existing directory or path is an empty string.
    SetErr(err, ERR_INVALID);
    break;
  }
  case ENOSPC:
  case ENOMEM:
  {
    //The file system does not contain enough space to hold the contents of the new directory or to extend the parent directory of the new directory.
    SetErr(err, ERR_RESOURCES);
    break;
  }
  case EBUSY:
  {
    SetErr(err, ERR_BUSY);
    break;
  }
  case EIO:
  default:
  {
    SetErr(err, ERR_UNSPEC);
    break;
  }
  }
}

class FileHandlerImplementation
{
public:

  FileHandlerImplementation()
  : m_Binary(false)
  , m_Truncate(false)
  {
    m_Name.clear();
    m_Mode = FileIF::ReadWrite;
    m_Binary = false;
    m_Truncate = false;
  };

  ~FileHandlerImplementation()
  {
    close();
  };

  void
  close()
  {
    m_fileHandler.close();
    m_Name.clear();
  }

  void
  open(const char* str, FileIF::FileMode mode, bool binary, bool truncate, ERROR* err = 0)
  {
    SetErr(err, ERR_NONE);
    if (isOpen()) {
      close();
    }

    ios_base::openmode OpenMode;

    switch (mode) {
    case FileIF::Read:
    {
      OpenMode = fstream::in;
      break;
    }
    case FileIF::Write:
    {
      OpenMode = fstream::out;
      break;
    }
    case FileIF::ReadWrite:
    {
      OpenMode = fstream::in | fstream::out;
      break;
    }
    };

    if (binary) {
      OpenMode |= fstream::binary;
    }

    // Read File will fail if trunc or ate flags are set.
    if (mode != FileIF::Read) {
      if (truncate) {
        OpenMode |= fstream::trunc;
      }
      else {
        OpenMode |= fstream::ate;
      }
    }

    m_fileHandler.open(str, OpenMode);

    if (!isOpen()) {
      SetErr(err, ERR_NO_FILE_OPEN);
      m_Name.clear();
      m_Mode = FileIF::ReadWrite;
      m_Binary = false;
      m_Truncate = false;
    }
    else {
      SetErr(err, ERR_NONE);
      m_Name.assign(str);
      m_Mode = mode;
      m_Binary = binary;
      m_Truncate = truncate;
    }

    // Clear file handler bits
    m_fileHandler.clear();
  };

  void
  write(const char* str, size_t bufferSize, ERROR* err)
  {
    if (!isOpen()) {
      SetErr(err, ERR_NO_FILE_OPEN);
    }
    else {
      SetErr(err, ERR_NONE);
      m_fileHandler.write(str, bufferSize);
    }
    // Force the stream to the file
    m_fileHandler.flush();
  }

  size_t
  read(char* str, size_t bufferSize, ERROR* err)
  {
    size_t CharactersRead = 0;
    if (isOpen()) {
      // clear output buffer
      memset(str, 0, bufferSize);

      m_fileHandler.read(str, bufferSize);

      if (m_fileHandler.eof()) {
        SetErr(err, ERR_END_OF_FILE);
        CharactersRead = strlen(str);
        ;
      }
      else if (!m_fileHandler.good()) {
        CharactersRead = strlen(str);
        SetErr(err, ERR_READ);
      }
      else {
        SetErr(err, ERR_NONE);
        CharactersRead = bufferSize;
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return CharactersRead;
  }

  bool
  isOpen()
  {
    return m_fileHandler.is_open();
  }

  CString
  getName()
  {
    return m_Name;
  }

  size_t
  getWritePosition(ERROR* err)
  {
    size_t pos = 0;

    if (isOpen()) {
      long temp = m_fileHandler.tellp();

      if (temp != -1) {
        pos = temp;
        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return pos;
  }

  size_t
  getReadPosition(ERROR* err)
  {
    size_t pos = 0;

    if (isOpen()) {
      long temp = m_fileHandler.tellg();
      if (temp != -1) {
        pos = temp;
        SetErr(err, ERR_NONE);
      }
      else {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return pos;
  }

  void
  setWritePosition(size_t pos, ERROR* err)
  {
    if (isOpen()) {
      m_fileHandler.clear();
      SetErr(err, ERR_NONE);
      m_fileHandler.seekp(pos);

      if (m_fileHandler.bad()) {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }
  }

  void
  setReadPosition(size_t pos, ERROR* err)
  {
    if (isOpen()) {
      m_fileHandler.clear();
      SetErr(err, ERR_NONE);
      m_fileHandler.seekg(pos);

      if (m_fileHandler.bad()) {
        SetErr(err, ERR_UNSPEC);
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }
  }

  size_t
  getFileSize(ERROR* err)
  {
    size_t toReturn = 0;
    size_t currentPos = 0;

    int iErr = ERR_NONE;

    currentPos = getReadPosition(&iErr);

    if (iErr == ERR_NONE) {
      // Set Reader at end of file
      m_fileHandler.seekg(0, ios_base::end);

      toReturn = getReadPosition(&iErr);

      if (iErr == ERR_NONE) {
        // reset reader to original position
        m_fileHandler.seekg(currentPos);
      }
    }

    SetErr(err, iErr);
    return toReturn;
  }

  bool
  isBinary(ERROR* err)
  {
    if (isOpen()) {
      SetErr(err, ERR_NONE);
      return m_Binary;
    }
    SetErr(err, ERR_NO_FILE_OPEN);
    return false;
  }

  bool
  isTruncate(ERROR* err)
  {
    if (isOpen()) {
      SetErr(err, ERR_NONE);
      return m_Truncate;
    }
    SetErr(err, ERR_NO_FILE_OPEN);
    return false;
  }

  FileIF::FileMode
  getMode(ERROR* err)
  {
    if (isOpen()) {
      SetErr(err, ERR_NONE);
      return m_Mode;
    }
    SetErr(err, ERR_NO_FILE_OPEN);
    return m_Mode;
  }

private:
  // Private Variable implementation
  fstream m_fileHandler;
  CString m_Name;
  FileIF::FileMode m_Mode;
  bool m_Binary;
  bool m_Truncate;
};

FileIF::FileIF()
{
  m_pImp = new FileHandlerImplementation();
}

FileIF::~FileIF()
{
  delete m_pImp;
}

void
FileIF::write(const char* str, size_t bufferSize, ERROR* err)
{
  m_pImp->write(str, bufferSize, err);
}

void
FileIF::write(CString str, ERROR* err)
{
  m_pImp->write(str.c_str(), str.size(), err);
}

void
FileIF::write(unsigned short value, ERROR* err)
{
  m_pImp->write(reinterpret_cast<char*> (&value), 2, err);
}

void
FileIF::write(unsigned int value, ERROR* err)
{
  m_pImp->write(reinterpret_cast<char*> (&value), 4, err);
}

size_t
FileIF::read(char* buf, size_t bufferSize, ERROR* err)
{
  return m_pImp->read(buf, bufferSize, err);
}

CString
FileIF::readLine(ERROR* err)
{
  CString temp;
  char ch = 0;
  int iErr = ERR_NONE;

  do {
    m_pImp->read(&ch, 1, &iErr);

    if (iErr == ERR_NONE) {
      temp += ch;
    }
  }
  while ((ch != '\n') && iErr == ERR_NONE);

  // TODO add support for windows return line endings "/r/n";

  SetErr(err, iErr);
  return temp;
}

void
FileIF::open(const char* str, FileMode mode, bool binary, bool truncate, ERROR* err)
{
  m_pImp->open(str, mode, binary, truncate, err);
}

void
FileIF::open(CString str, FileMode mode, bool binary, bool truncate, ERROR* err)
{
  open(str.c_str(), mode, binary, truncate, err);
}

void
FileIF::close(ERROR* err)
{
  if (isOpen()) {
    SetErr(err, ERR_NONE);
    m_pImp->close();
  }
  else {
    SetErr(err, ERR_NO_FILE_OPEN);
  }
}

bool
FileIF::isOpen(ERROR* err)
{
  SetErr(err, ERR_NONE);
  return m_pImp->isOpen();
}

void
FileIF::setReadPosition(size_t pos, ERROR* err)
{
  m_pImp->setReadPosition(pos, err);
}

size_t
FileIF::getReadPosition(ERROR* err)
{
  return m_pImp->getReadPosition(err);
}

void
FileIF::setWritePosition(size_t pos, ERROR* err)
{
  m_pImp->setWritePosition(pos, err);
}

size_t
FileIF::getWritePosition(ERROR* err)
{
  return m_pImp->getWritePosition(err);
}

size_t
FileIF::size(ERROR* err)
{
  return m_pImp->getFileSize(err);
}

CString
FileIF::getFullPath()
{
  return m_pImp->getName();
}

CString
FileIF::getFileName()
{
  size_t pos = 0;
  CString temp = m_pImp->getName();

  for (int i = temp.size() - 1; i >= 0; i--) {
    if (temp[i] == '/' || temp[i] == '\\') {
      pos = i + 1;
      break;
    }
  }

  return temp.substr(pos, temp.size() - 1);
}

CString
FileIF::getFileExtension()
{
  size_t pos = 0;
  CString temp = m_pImp->getName();
  bool found = false;
  for (int i = temp.size() - 1; i >= 0; i--) {
    if (temp[i] == '.') {
      found = true;
      pos = i;
      break;
    }
  }
  if (!found) {
    temp.clear();
  }
  else {
    temp.assign(temp.substr(pos + 1, temp.size() - 1));
  }

  return temp;
}

bool
FileIF::isBinary(ERROR* err)
{
  return m_pImp->isBinary(err);
}

bool
FileIF::isTruncate(ERROR* err)
{
  return m_pImp->isTruncate(err);
}

FileIF::FileMode
FileIF::getMode(ERROR* err)
{
  return m_pImp->getMode(err);
}

bool
Directory::Exists(const char* filepath, ERROR* err)
{
  struct stat st_buf;

  if (stat(filepath, &st_buf) != 0) {
    ProcessErrno(err);
    return false;
  }

  SetErr(err, ERR_NONE);
  if (S_ISREG(st_buf.st_mode) || S_ISDIR(st_buf.st_mode) || S_ISFIFO(st_buf.st_mode)) {
    return true;
  }
  else {
    return false;
  }
  return false;
}

bool
Directory::Exists(CString filepath, ERROR* err)
{
  return Directory::Exists(filepath.c_str(), err);
}

bool
Directory::Mkdir(const char* filepath, ERROR* err)
{
  if (mkdir(filepath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0) {
    SetErr(err, ERR_NONE);
    return true;
  }
  else {
    ProcessErrno(err);
    return false;
  }
}

bool
Directory::Mkdir(CString filepath, ERROR* err)
{
  return Directory::Mkdir(filepath.c_str(), err);
}

bool
Directory::Remove(const char* filepath, ERROR* err)
{
  if (unlink(filepath) == 0) {
    SetErr(err, ERR_NONE);
    return true;
  }
  else {
    ProcessErrno(err);
    return false;
  }
}

bool
Directory::Remove(CString filepath, ERROR* err)
{
  return Directory::Remove(filepath.c_str(), err);
}

bool
Directory::Rmdir(const char* filepath, ERROR* err)
{
  if (rmdir(filepath) == 0) {
    SetErr(err, ERR_NONE);
    return true;
  }
  else {
    ProcessErrno(err);
    return false;
  }
}

bool
Directory::Rmdir(CString filepath, ERROR* err)
{
  return Directory::Rmdir(filepath.c_str(), err);
}

CString
Directory::Cwd(ERROR* err)
{
  char temp [ PATH_MAX ];

  if (getcwd(temp, PATH_MAX) != 0) {
    SetErr(err, ERR_NONE);
    return CString(temp);
  }

  ProcessErrno(err);

  return CString();
}

void
Directory::CopyFile(const char* srcFile, const char* dstFile, ERROR* err)
{
  ifstream is(srcFile, ios::in | ios::binary);
  ofstream os;

  if (is.is_open()) {
    os.open(dstFile, ios::out | ios::binary);
  }
  else {
    SetErr(err, ERR_NO_FILE_OPEN);
    return;
  }

  if (!os.is_open()) {
    SetErr(err, ERR_NO_FILE_OPEN);
    return;
  }

  copy(istream_iterator<char>(is), istream_iterator<char>(), ostream_iterator<char>(os));

  SetErr(err, ERR_NONE);
}

void
Directory::MoveFile(const char* srcFile, const char* dstFile, ERROR* err)
{
  CopyFile(srcFile, dstFile, err);

  Directory::Remove(srcFile, err);
}








#include <Console.hpp>
#include <stdio.h>
#include <common/Error.hpp>

using namespace ns_MDL;

Console::Console()
{
};

Console::~Console()
{
};

void
Console::write(const char* str, size_t bufferSize, ERROR* err)
{
  SetErr(err, ERR_NONE);
  if (bufferSize > 0) {
    printf("%s", str);
  }
};

size_t
Console::read(char* buf, size_t bufferSize, ERROR* err)
{
  SetErr(err, ERR_NOT_IMPLEMENTED);
  if (buf != 0) {
    return bufferSize;
  }
  else {
    return 0;
  }
};




#include <cstring>
#include <SerialDevice.hpp>
#include <common/Error.hpp>
#include <termios.h>
#include <fcntl.h>
#include <unistd.h>
#include <common/Log.hpp>
#include <datatype/CString.hpp>

using namespace ns_Datatype;

using namespace ns_MDL;

class SerialDeviceImplementation
{
public:

  SerialDeviceImplementation()
  {
    m_Device.clear();
    m_fd = 0;
    memset(&m_Config, 0, sizeof (m_Config));
  }

  ~SerialDeviceImplementation()
  {
    m_Device.clear();
    m_fd = 0;
    memset(&m_Config, 0, sizeof (m_Config));
    close(m_fd);
  }

  bool
  openSerialDevice(const char* device, SerialDevice::SerialConfig config, SerialDevice::BaudRate br, ERROR* err)
  {
    int configFlags = 0;

    // Close the connection if it is open
    close(m_fd);

    // setup configuration flags
    switch (config) {
    case SerialDevice::CONFIG_5N1:
    {
      configFlags |= CS5;
      break;
    }
    case SerialDevice::CONFIG_6N1:
    {
      configFlags |= CS6;
      break;
    }
    case SerialDevice::CONFIG_7N1:
    {
      configFlags |= CS7;
      break;
    }
    case SerialDevice::CONFIG_8N1: //DEFAULT
    {
      configFlags |= CS8;
      break;
    }
    case SerialDevice::CONFIG_5N2:
    {
      configFlags |= CS5;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_6N2:
    {
      configFlags |= CS6;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_7N2:
    {
      configFlags |= CS7;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_8N2:
    {
      configFlags |= CS8;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_5E1:
    {
      configFlags |= CS5;
      configFlags |= PARENB;
      break;
    }
    case SerialDevice::CONFIG_6E1:
    {
      configFlags |= CS6;
      configFlags |= PARENB;
      break;
    }
    case SerialDevice::CONFIG_7E1:
    {
      configFlags |= CS7;
      configFlags |= PARENB;
      break;
    }
    case SerialDevice::CONFIG_8E1:
    {
      configFlags |= CS8;
      configFlags |= PARENB;
      break;
    }
    case SerialDevice::CONFIG_5E2:
    {
      configFlags |= CS5;
      configFlags |= PARENB;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_6E2:
    {
      configFlags |= CS6;
      configFlags |= PARENB;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_7E2:
    {
      configFlags |= CS7;
      configFlags |= PARENB;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_8E2:
    {
      configFlags |= CS8;
      configFlags |= PARENB;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_5O1:
    {
      configFlags |= CS5;
      configFlags |= PARENB | PARODD;
      break;
    }
    case SerialDevice::CONFIG_6O1:
    {
      configFlags |= CS6;
      configFlags |= PARENB | PARODD;
      break;
    }
    case SerialDevice::CONFIG_7O1:
    {
      configFlags |= CS7;
      configFlags |= PARENB | PARODD;
      break;
    }
    case SerialDevice::CONFIG_8O1:
    {
      configFlags |= CS8;
      configFlags |= PARENB | PARODD;
      break;
    }
    case SerialDevice::CONFIG_5O2:
    {
      configFlags |= CS5;
      configFlags |= PARENB | PARODD;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_6O2:
    {
      configFlags |= CS6;
      configFlags |= PARENB | PARODD;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_7O2:
    {
      configFlags |= CS7;
      configFlags |= PARENB | PARODD;
      configFlags |= CSTOPB;
      break;
    }
    case SerialDevice::CONFIG_8O2:
    {
      configFlags |= CS8;
      configFlags |= PARENB | PARODD;
      configFlags |= CSTOPB;
      break;
    }
    default:
    {
      // Clear
      CString temp("Invalid Serial Configuration for Device: ");
      temp += device;
      LOG::Error(temp);
      SetErr(err, ERR_INVALID);
      m_Device.clear();
      m_fd = 0;
      memset(&m_Config, 0, sizeof (m_Config));
      m_isOpen = false;
      return false;
    }
    }

    m_fd = open(device, O_RDWR | O_NOCTTY | O_NDELAY);

    if (m_fd == -1) {
      LOG::Error("failed to open port\n");
      SetErr(err, ERR_UNSPEC);
      m_isOpen = false;
      return false;
    }

    memset(&m_Config, 0, sizeof (m_Config));
    m_Config.c_iflag = 0;
    m_Config.c_oflag = 0;
    m_Config.c_cflag = configFlags | CREAD | CLOCAL;
    m_Config.c_lflag = 0;
    m_Config.c_cc[VMIN] = 1;
    m_Config.c_cc[VTIME] = 5;

    cfsetospeed(&m_Config, br);
    cfsetispeed(&m_Config, br);

    tcsetattr(m_fd, TCSANOW, &m_Config);

    m_Device.assign(device);
    m_BR = br;
    m_SerialConfig = config;
    SetErr(err, ERR_NONE);

    m_isOpen = true;
    return true;
  }

  void
  closeSerialDevice()
  {
    m_Device.clear();
    m_fd = -1;
    memset(&m_Config, 0, sizeof (m_Config));
    close(m_fd);
    m_isOpen = false;
  }

  void
  serialWrite(const char* str, size_t bufferSize, ERROR* err)
  {
    if (m_isOpen) {
      if (write(m_fd, str, bufferSize) == -1) {
        SetErr(err, ERR_WRITE);
      }
      else {
        tcdrain(m_fd);
        SetErr(err, ERR_NONE);
      }
    }
    else {
      SetErr(err, ERR_HARDWARE);
    }
  }

  size_t
  serialRead(char* buf, size_t bufferSize, ERROR* err, size_t timeout)
  {
    char ch = 0;
    size_t charRead = 0;
    size_t tempCharRead = 0;
    size_t totalNapTime = 0;

    if (m_isOpen) {
      do {
        tempCharRead = read(m_fd, &ch, 1);

        if (tempCharRead == 1) {
          buf[charRead] = ch;
          charRead++;
        }
        else {
          totalNapTime += 10;
          usleep(1000);
        }
      }
      while ((totalNapTime < timeout) && (charRead < bufferSize));


      if (charRead == bufferSize) {
        SetErr(err, ERR_NONE);
      }
      else if (totalNapTime >= timeout) {
        SetErr(err, ERR_TIMEOUT);
      }
      else {
        SetErr(err, ERR_READ);
      }
    }
    else {
      SetErr(err, ERR_HARDWARE);
    }

    return charRead;
  }

  SerialDevice::SerialConfig
  getConfig(ERROR* err)
  {
    if (!m_isOpen) {
      SetErr(err, ERR_HARDWARE);
      return SerialDevice::CONFIG_INVALID;
    }

    SetErr(err, ERR_NONE);
    return m_SerialConfig;
  }

  SerialDevice::BaudRate
  getBaudRate(ERROR* err)
  {
    if (!m_isOpen) {
      SetErr(err, ERR_HARDWARE);
      return SerialDevice::BR_INVALID;
    }

    SetErr(err, ERR_NONE);
    return m_BR;
  }

private:
  bool m_isOpen;
  CString m_Device;
  int m_fd;
  termios m_Config;
  SerialDevice::BaudRate m_BR;
  SerialDevice::SerialConfig m_SerialConfig;
};

SerialDevice::SerialDevice(HW_GPIO* hw)
{
  m_pImp = new SerialDeviceImplementation();
}

SerialDevice::~SerialDevice()
{
  delete m_pImp;
}

bool
SerialDevice::open(const char* device, SerialConfig config, BaudRate br, ERROR* err)
{
  return m_pImp->openSerialDevice(device, config, br, err);
}

void
SerialDevice::close()
{
  m_pImp->closeSerialDevice();
}

void
SerialDevice::write(const char* str, size_t bufferSize, ERROR* err)
{
  m_pImp->serialWrite(str, bufferSize, err);
}

void
SerialDevice::write(CString str, ERROR* err)
{
  m_pImp->serialWrite(str.c_str(), str.size(), err);
}

size_t
SerialDevice::read(char* buf, size_t bufferSize, int timeout, ERROR* err)
{
  return m_pImp->serialRead(buf, bufferSize, err, timeout);
}

size_t
SerialDevice::read(char* buf, size_t bufferSize, ERROR* err)
{
  return m_pImp->serialRead(buf, bufferSize, err, 1000);
}

SerialDevice::SerialConfig
SerialDevice::getConfig(ERROR* err)
{
  return m_pImp->getConfig(err);
}

SerialDevice::BaudRate
SerialDevice::getBaudRate(ERROR* err)
{
  return m_pImp->getBaudRate(err);
}


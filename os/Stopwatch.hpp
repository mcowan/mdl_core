/*
 * Stopwatch.hpp
 *
 *  Created on: Jan 9, 2015
 *  Author: simplymac
 *  Description: An implementation of a stopwatch.  Used for calculating in
 *  nanoseconds the time between events.
 */

#ifndef STOPWATCH_HPP_
#define STOPWATCH_HPP_

#include <Types.hpp>
#include <sys/time.h>

/*
 * @brief General MDL Library Wrapper.
 */
namespace ns_MDL
{

  class Stopwatch
  {
  public:
    /*
     * @brief Constructor
     */
    Stopwatch();

    /*
     * @brief Destructor
     */
    ~Stopwatch();

    /*
     * @brief Check if stopwatch is running.
     *
     * @return True if running; False, otherwise.
     */
    bool isRunning();

    /*
     * Start the Stopwatch and set the initial
     * time point to measure time from.  If called
     * a second time it will restart the stopwatch
     * from the new start time.
     */
    void Start();

    /*
     * @brief Stop the stopwatch, allowing it to
     * be started again with a new start point.
     *
     * @return Returns the time between the start
     * and now.
     */
    double Stop();

    /*
     * @brief Gets the time difference between when
     * the watch was started and now.
     *
     * @return The time difference.
     */
    double Time();

  private:
    timeval m_Start; ///> Start Point.
    bool m_Running; ///> Running Flag.
  };
};


#endif

/// @file MacOs/System.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A mac implementation of accessing system variables pertaining
/// to the hardware.


#include <System.hpp>
#include <common/Error.hpp>
#include <common/Convert.hpp>
#include <cstring>
#include <fstream>

#include <sys/utsname.h>

using namespace ns_MDL;
using namespace std;

namespace ns_MDL
{
#define SYSCTL_BIG_ENDIAN 4321
#define SYSCTL_LITTLE_ENDIAN 1234

  static const size_t MAX_LINE_SIZE = 256;
  static const char* MemInfoFile = "/proc/meminfo";
  static const char* CpuInfoFile = "/proc/cpuinfo";

  unsigned long
  getTotalSystemMemory(ERROR* err)
  {
    ifstream fin;
    fin.open(MemInfoFile);

    if (fin.is_open()) {
      char str[MAX_LINE_SIZE] = {0};
      unsigned long MemTotal = 0;

      while (!fin.eof()) {
        fin.clear(); // clear any bits
        fin.getline(str, MAX_LINE_SIZE);
        if (sscanf(str, "MemTotal:\t%lu", &MemTotal) == 1) {
          fin.close();
          SetErr(err, ERR_NONE);
          return MemTotal*METRIC_FROM_KILO;
        }
      }

      SetErr(err, ERR_NOT_FOUND);
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return 0;
  }

  void
  getOSDescription(char* buffer, size_t bufferSize, ERROR* err)
  {
    utsname utsnameData;

    if (uname(&utsnameData) == 0) {
      size_t pos = 0;

      // Operating system name (e.g., "Linux")
      memcpy(&buffer[pos], utsnameData.sysname, strlen(utsnameData.sysname));
      pos = strlen(utsnameData.sysname);
      buffer[pos++] = ' ';
      // OS release (e.g., "2.6.28") 
      memcpy(&buffer[pos], utsnameData.release, strlen(utsnameData.release));
      pos = strlen(utsnameData.release);
      buffer[pos++] = ' ';
      // Hardware identifier
      memcpy(&buffer[pos], utsnameData.machine, strlen(utsnameData.machine));

      SetErr(err, ERR_NONE);
    }
    else {
      SetErr(err, ERR_UNSPEC);
    }
  }

  void
  getHardwareDescription(char* buffer, size_t bufferSize, ERROR* err)
  {
    ifstream fin;
    fin.open(CpuInfoFile);

    if (fin.is_open()) {
      char str[MAX_LINE_SIZE] = {0};
      char hwDescription[1024] = {0};

      while (!fin.eof()) {
        fin.clear(); // A Fail bit sometimes gets tripped.
        fin.getline(str, MAX_LINE_SIZE - 1);

        if (sscanf(str, "model name\t:%s", hwDescription) == 1) {
          if (strlen(&str[13]) <= bufferSize) {
            memcpy(buffer, &str[13], strlen(&str[13]));
          }
          else {
            SetErr(err, ERR_RANGE);
          }

          fin.close();
          return;
        }
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }
  }

  unsigned long
  getNumberOfCPUs(ERROR* err)
  {
    ifstream fin;
    fin.open(CpuInfoFile);
    unsigned long CpuCount = 0;

    if (fin.is_open()) {
      char str[MAX_LINE_SIZE] = {0};
      unsigned int CPU = 0;
      while (!fin.eof()) {
        fin.clear(); // A Fail bit sometimes gets tripped.
        fin.getline(str, MAX_LINE_SIZE - 1);

        if (sscanf(str, "processor\t:%i", &CPU) == 1) {
          CpuCount++;
        }
      }

      fin.close();
      if (CpuCount == 0) {
        SetErr(err, ERR_NOT_FOUND);
      }
      else {
        SetErr(err, ERR_NONE);
      }
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return CpuCount;
  }

  unsigned long
  getCPUFrequency(ERROR* err)
  {
    ifstream fin;
    fin.open(CpuInfoFile);
    unsigned long Frequency = 0;

    if (fin.is_open()) {
      char str[MAX_LINE_SIZE] = {0};

      while (!fin.eof()) {
        fin.clear(); // A Fail bit sometimes gets tripped.
        fin.getline(str, MAX_LINE_SIZE - 1);

        if (sscanf(str, "cpu MHz\t:%lu", &Frequency) == 1) {
          SetErr(err, ERR_NONE);
          return Frequency*METRIC_FROM_MEGA;
        }
      }

      fin.close();
      SetErr(err, ERR_NOT_FOUND);
    }
    else {
      SetErr(err, ERR_NO_FILE_OPEN);
    }

    return Frequency;
  }

  bool
  isBigEndian(ERROR* err)
  {
    int number = 0x1;
    char *numPtr = (char*) &number;
    return (numPtr[0] == 0);
  }
};



/*
 * @file Linux/Thread.cpp
 * @brief Linux implementation of thread.cpp 
 */


#include <Thread.hpp>
#include <Types.hpp>
#include <unistd.h>

/// @brief General MDL Library Wrapper.
///
using namespace ns_MDL;

Thread::Thread()
{
};

Thread::~Thread()
{
};

void
Thread::Sleep(size_t milliseconds)
{
  usleep(milliseconds * 1000);
};


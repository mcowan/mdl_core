/*
 * TEST_File.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: mjc
 */

#include "gtest/gtest.h"
#include <SerialDevice.hpp>
#include <datatype/CString.hpp>
#include <Thread.hpp>

using namespace ns_Datatype;
using namespace ns_MDL;


// CString Test Fixture

class Test_SerialDevice : public testing::Test
{
protected:

  // Called before each test is run.

  virtual void
  SetUp()
  {
    device = new SerialDevice();
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    delete device;
  }

  SerialDevice* device;
};

#ifndef ENABLE_SERIAL_TEST

TEST_F(Test_SerialDevice, TestsAreDisabled)
{
  printf("==============================================================\n");
  printf("SerialDevice Tests are Currently Disabled.\n");
  printf("\n");
  printf("To enable tests run 'make test ENABLETEST=SERIAL\n");
  printf("\n");
  printf("A simple loopback device can be created if you use a \n");
  printf("FTDI chip connected to the usb with the tx and rx wired\n");
  printf("together.\n");
  printf("===============================================================\n");
}

#else

static const char* SerialDeviceStr = "/dev/ttyUSB0";

TEST_F(Test_SerialDevice, WriteCString)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    ASSERT_EQ(SerialDevice::CONFIG_INVALID, device->getConfig(&err));
    ASSERT_EQ(err, ERR_HARDWARE);
    ASSERT_EQ(SerialDevice::BR_INVALID, device->getBaudRate(&err));
    ASSERT_EQ(err, ERR_HARDWARE);

    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR300, &err);
    ASSERT_EQ(err, ERR_NONE);

    ASSERT_EQ((SerialDevice::SerialConfig)config, device->getConfig(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(SerialDevice::BR300, device->getBaudRate(&err));
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str, &err);
    ASSERT_EQ(err, ERR_NONE);


    // Read
    device->read(readCString, 22, 500, &err);
    ASSERT_STREQ(readCString, str.c_str());
    ASSERT_EQ(err, ERR_NONE);

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, TimeoutError)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[24];

  ASSERT_EQ(SerialDevice::CONFIG_INVALID, device->getConfig(&err));
  ASSERT_EQ(err, ERR_HARDWARE);
  ASSERT_EQ(SerialDevice::BR_INVALID, device->getBaudRate(&err));
  ASSERT_EQ(err, ERR_HARDWARE);

  // Open Device
  device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR300, &err);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ((SerialDevice::SerialConfig)config, device->getConfig(&err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(SerialDevice::BR300, device->getBaudRate(&err));
  ASSERT_EQ(err, ERR_NONE);

  // Clear Array
  memset(readCString, 0, 24);

  // Write
  device->write(str, &err);
  ASSERT_EQ(err, ERR_NONE);


  // Read
  device->read(readCString, 23, 500, &err);
  ASSERT_STREQ(readCString, str.c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close Device
  device->close();
}

TEST_F(Test_SerialDevice, BR300)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    ASSERT_EQ(SerialDevice::CONFIG_INVALID, device->getConfig(&err));
    ASSERT_EQ(err, ERR_HARDWARE);
    ASSERT_EQ(SerialDevice::BR_INVALID, device->getBaudRate(&err));
    ASSERT_EQ(err, ERR_HARDWARE);

    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR300, &err);
    ASSERT_EQ(err, ERR_NONE);

    ASSERT_EQ((SerialDevice::SerialConfig)config, device->getConfig(&err));
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_EQ(SerialDevice::BR300, device->getBaudRate(&err));
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_STREQ(readCString, str.c_str());
    ASSERT_EQ(err, ERR_NONE);

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, BR600)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR600, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, BR1200)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR1200, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, BR2400)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR2400, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, BR4800)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR4800, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR9600)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR9600, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }

};

TEST_F(Test_SerialDevice, BR14400)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR14400, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR19200)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR19200, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR28800)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR28800, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR31250)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR31250, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR38400)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR38400, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR57600)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR57600, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, BR115200)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];


  for (int config = (int) SerialDevice::CONFIG_5N1; config <= (int) SerialDevice::CONFIG_8O2; config++) {
    // Open Device
    device->open(SerialDeviceStr, (SerialDevice::SerialConfig)config, SerialDevice::BR115200, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Clear Array
    memset(readCString, 0, 23);

    // Write
    device->write(str.c_str(), str.size(), &err);
    ASSERT_EQ(err, ERR_NONE);
    usleep(100000);

    // Read
    device->read(readCString, 22, &err);
    usleep(100000);
    ASSERT_EQ(err, ERR_NONE);
    ASSERT_STREQ(readCString, str.c_str());

    // Close Device
    device->close();
  }
};

TEST_F(Test_SerialDevice, IvalidConfig)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  char readCString[23];

  // Open Device -- invalid configuration
  device->open(SerialDeviceStr, (SerialDevice::SerialConfig)9999, SerialDevice::BR115200, &err);
  ASSERT_EQ(err, ERR_INVALID);

  // Clear Array
  memset(readCString, 0, 23);

  // Write
  device->write(str.c_str(), str.size(), &err);
  ASSERT_EQ(err, ERR_HARDWARE);
  usleep(100000);

  // Read
  device->read(readCString, 22, &err);
  usleep(100000);
  ASSERT_EQ(err, ERR_HARDWARE);
  ASSERT_STREQ(readCString, "");

  // Close Device
  device->close();
};

#endif


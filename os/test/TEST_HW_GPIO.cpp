/*
 * TEST_HW_GPIO.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: mjc
 */

#include "gtest/gtest.h"
#include <HW_GPIO.hpp>
#include <common/Error.hpp>

using namespace ns_MDL;

// String Test Fixture

class HW_GPIO_Test : public testing::Test
{
protected:

  // Called before each test is run.

  virtual void
  SetUp()
  {
    err = ERR_NONE;
    m_GPIO = new HW_GPIO();
    m_AnalogPinInput = new Pin(0, AnalogInput);
    m_AnalogPinOutput = new Pin(0, AnalogOutput);
    m_DigitalPinInput = new Pin(0, DigitalInput);
    m_DigitalPinOutput = new Pin(0, DigitalOutput);
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    err = ERR_NONE;
    delete m_DigitalPinOutput;
    delete m_DigitalPinInput;
    delete m_AnalogPinOutput;
    delete m_AnalogPinInput;
    delete m_GPIO;
  }

  int err;
  HW_GPIO* m_GPIO;
  Pin* m_AnalogPinInput;
  Pin* m_AnalogPinOutput;
  Pin* m_DigitalPinInput;
  Pin* m_DigitalPinOutput;
};

TEST_F(HW_GPIO_Test, AnalogPinInput)
{
  // Initialize Pin
  m_AnalogPinInput->initialize();

  ASSERT_EQ(m_AnalogPinInput->getMode(), AnalogInput);

  // Min to Max
  for (size_t value = ANALOG_WRITE_LOW; value <= ANALOG_WRITE_HIGH; value++) {
    // Writing to an input will return a hardware error.
    // The simulated Pins will allow it.
    m_AnalogPinInput->write(value, &err);
    ASSERT_EQ(err, ERR_HARDWARE);

    // Reading will generate no error.
    ASSERT_EQ(value, m_AnalogPinInput->read(&err));
    ASSERT_EQ(err, ERR_NONE);
  }

  // Range Error
  m_AnalogPinInput->write(ANALOG_WRITE_HIGH + 1, &err);
  ASSERT_EQ(err, ERR_RANGE);

  // Reading will return last correct set value.
  ASSERT_EQ(ANALOG_WRITE_HIGH, m_AnalogPinInput->read(&err));
  ASSERT_EQ(err, ERR_NONE);
};

TEST_F(HW_GPIO_Test, AnalogPinOutput)
{
  ASSERT_EQ(m_AnalogPinOutput->getMode(), AnalogOutput);

  // Initialize Pin
  m_AnalogPinOutput->initialize();

  ASSERT_EQ(m_AnalogPinOutput->getMode(), AnalogOutput);

  // Min to Max
  for (size_t value = ANALOG_WRITE_LOW; value <= ANALOG_WRITE_HIGH; value++) {
    // Writing to an input will return a hardware error.
    // The simulated Pins will allow it.
    m_AnalogPinOutput->write(value, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Reading will generate no error.
    ASSERT_EQ(value, m_AnalogPinOutput->read(&err));
    ASSERT_EQ(err, ERR_HARDWARE);
  }

  // Range Error
  m_AnalogPinOutput->write(ANALOG_WRITE_HIGH + 1, &err);
  ASSERT_EQ(err, ERR_RANGE);

  // Reading will return last correct set value.
  ASSERT_EQ(ANALOG_WRITE_HIGH, m_AnalogPinOutput->read(&err));
  ASSERT_EQ(err, ERR_HARDWARE);
};

TEST_F(HW_GPIO_Test, DigitalPinInput)
{
  // Initialize Pin
  m_DigitalPinInput->initialize();

  ASSERT_EQ(m_DigitalPinInput->getMode(), DigitalInput);

  // Min to Max
  for (size_t value = DIGITAL_LOW; value <= DIGITAL_HIGH; value++) {
    // Writing to an input will return a hardware error.
    // The simulated Pins will allow it.
    m_DigitalPinInput->write(value, &err);
    ASSERT_EQ(err, ERR_HARDWARE);

    // Reading will generate no error.
    ASSERT_EQ(value, m_DigitalPinInput->read(&err));
    ASSERT_EQ(err, ERR_NONE);
  }

  // Range Error
  m_DigitalPinInput->write(DIGITAL_HIGH + 1, &err);
  ASSERT_EQ(err, ERR_RANGE);

  // Reading will return last correct set value.
  ASSERT_EQ(DIGITAL_HIGH, m_DigitalPinInput->read(&err));
  ASSERT_EQ(err, ERR_NONE);
};

TEST_F(HW_GPIO_Test, DigitalPinOutput)
{
  // Initialize Pin
  m_DigitalPinOutput->initialize();

  ASSERT_EQ(m_DigitalPinOutput->getMode(), DigitalOutput);

  // Min to Max
  for (size_t value = DIGITAL_LOW; value <= DIGITAL_HIGH; value++) {
    // Writing to an input will return a hardware error.
    // The simulated Pins will allow it.
    m_DigitalPinOutput->write(value, &err);
    ASSERT_EQ(err, ERR_NONE);

    // Reading will generate no error.
    ASSERT_EQ(value, m_DigitalPinOutput->read(&err));
    ASSERT_EQ(err, ERR_HARDWARE);
  }

  // Range Error
  m_DigitalPinOutput->write(DIGITAL_HIGH + 1, &err);
  ASSERT_EQ(err, ERR_RANGE);

  // Reading will return last correct set value.
  ASSERT_EQ(DIGITAL_HIGH, m_DigitalPinOutput->read(&err));
  ASSERT_EQ(err, ERR_HARDWARE);
};

TEST_F(HW_GPIO_Test, DefaultHardware)
{
  // Verify Number of Pins
  ASSERT_EQ(m_GPIO->Pins.size(), SIM_NUM_PINS);

  // Verify Pin Number
  for (size_t i = 0; i < SIM_NUM_PINS; i++) {
    ASSERT_EQ(m_GPIO->Pins[i].getPin(&err), i);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Verify Default Pin Mode
  for (size_t i = 0; i < SIM_NUM_PINS; i++) {
    ASSERT_EQ(m_GPIO->Pins[i].getMode(&err), DigitalOutput);
    ASSERT_EQ(err, ERR_NONE);
  }

  // Initialize
  m_GPIO->initialize();

  // Verify Pin Number
  for (size_t i = 0; i < SIM_NUM_PINS; i++) {
    m_GPIO->Pins[i].setPin(i, &err);
    ASSERT_EQ(err, ERR_INVALID);
  }

  // Verify Default Pin Mode
  for (size_t i = 0; i < SIM_NUM_PINS; i++) {
    m_GPIO->Pins[i].setMode(DigitalOutput, &err);
    ASSERT_EQ(err, ERR_INVALID);
  }
};
/*
 * TEST_Clock.cpp
 *
 *  Created on: Jan 25, 2014
 *  Author: mjc
 */


#include "gtest/gtest.h"
#include <Stopwatch.hpp>
#include <common/Error.hpp>


using ::testing::Test;
using namespace ns_MDL;

TEST(Stopwatch, Constructor)
{
  Stopwatch sw;
}

TEST(Stopwatch, Time)
{
  Stopwatch sw;
  // Stopwatch is stopped.
  ASSERT_FALSE(sw.isRunning());
  ASSERT_DOUBLE_EQ(sw.Time(), 0.0);

  // Start StopWatch
  sw.Start();
  ASSERT_TRUE(sw.isRunning());

  // TODO: Replace with a thread::sleep
  for (int i = 0; i < 10000; i++) {
    for (int j = 0; j < 10000; j++) {
    }
  }

  double time1 = sw.Time();

  ASSERT_TRUE(time1 > 0.0);

  // TODO: Replace with a thread::sleep
  for (int i = 0; i < 10000; i++) {
    for (int j = 0; j < 10000; j++) {
    }
  }

  double time2 = sw.Stop();

  ASSERT_TRUE(time2 > time1);
}
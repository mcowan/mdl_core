/*
 * TEST_File.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: mjc
 */

#include "gtest/gtest.h"
#include <Console.hpp>
#include <datatype/CString.hpp>

using namespace ns_Datatype;
using namespace ns_MDL;

// CString Test Fixture

class Test_Console : public testing::Test
{
protected:

  // Called before each test is run.

  virtual void
  SetUp()
  {
    device = new Console();
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    delete device;
  }

  Console* device;

};

TEST_F(Test_Console, ConsoleWrite)
{
  int err = ERR_NONE;
  CString str("Testing Console Write\n");
  device->write(str.c_str(), str.size(), &err);
  ASSERT_EQ(err, ERR_NONE);
};









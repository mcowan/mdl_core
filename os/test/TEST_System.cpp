/// @file test/TEST_System.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details A test file for the System.hpp functions.

#include "gtest/gtest.h"
#include <System.hpp>
#include <common/Error.hpp>
#include <common/Convert.hpp>

using ::testing::Test;
using namespace ns_MDL;

TEST(System, Memory)
{
  char HardwareDescription[1024] = {0};
  char OSDescription[1024] = {0};
  int err = ERR_NONE;

  getHardwareDescription(HardwareDescription, 1024, &err);
  ASSERT_EQ(err, ERR_NONE);
  getOSDescription(OSDescription, 1024, &err);
  ASSERT_EQ(err, ERR_NONE);

  printf("\n");
  printf("------------------------------------------------------------------\n");
  printf("** There isnt a really solid way to test these System functions **\n");
  printf("** aside from physically verifying that these tests match your  **\n");
  printf("** systems values by hand and verifying no errors.              **\n");
  printf("------------------------------------------------------------------\n");
  printf("\n");
  printf("System Statistics: \n");
  printf("------------------------------------------------------------------\n");
  printf("  HW Description: %s \n", HardwareDescription);
  printf("  OS Description: %s \n", OSDescription);
  printf("  Number of CPUs: %lu \n", getNumberOfCPUs(&err));
  ASSERT_EQ(err, ERR_NONE);
  printf("  CPU Frequency: %lu (Hz) \n", getCPUFrequency(&err));
  ASSERT_EQ(err, ERR_NONE);
  printf("  CPU Frequency: %0.2f (GHz) \n", getCPUFrequency(&err)*1.0 / 1000000000.0);
  ASSERT_EQ(err, ERR_NONE);
  printf("  Total System Memory: %lu (bytes) \n", getTotalSystemMemory(&err));
  ASSERT_EQ(err, ERR_NONE);
  printf("  Total System Memory: %0.2f (GB) \n", getTotalSystemMemory(&err) * BYTES_TO_GIGABYTES);
  ASSERT_EQ(err, ERR_NONE);
  printf("  Endianess: %s \n", isBigEndian(&err) ? "Big Endian" : "Little Endian");
  ASSERT_EQ(err, ERR_NONE);
  printf("\n");
}

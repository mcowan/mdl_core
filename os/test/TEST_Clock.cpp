/*
 * TEST_Clock.cpp
 *
 *  Created on: Jan 25, 2014
 *      Author: simplymac
 */


#include "gtest/gtest.h"
#include <Clock.hpp>
#include <common/Error.hpp>


using ::testing::Test;
using namespace ns_MDL;

TEST(DateTimeStruct, Constructor)
{
  DateTimeStruct datetime;

  ASSERT_EQ(datetime.year, (unsigned int) 0);
  ASSERT_EQ(datetime.month, (unsigned int) 0);
  ASSERT_EQ(datetime.day, (unsigned int) 0);
  ASSERT_EQ(datetime.hour, (unsigned int) 0);
  ASSERT_EQ(datetime.min, (unsigned int) 0);
  ASSERT_EQ(datetime.sec, (unsigned int) 0);
}

TEST(Clock, Constructor)
{
  Clock clock;
}

TEST(Clock, Now)
{
  Clock clock;

  DateTimeStruct datetime = clock.Now();

  printf("Year:  %i \n", datetime.year);
  printf("Month: %i \n", datetime.month);
  printf("Day:   %i \n", datetime.day);

  printf("Hour:   %i \n", datetime.hour);
  printf("Minute: %i \n", datetime.min);
  printf("Second: %i \n", datetime.sec);
}

TEST(Clock, Time)
{
  Clock clock;
  char TimeString[10];
  memset(TimeString, 0, 10);
  int err = ERR_NONE;

  clock.Time(TimeString, 10, &err);
  ASSERT_EQ(err, ERR_NONE);
  printf("Time String: %s\n", TimeString);
}

TEST(Clock, Date)
{
  Clock clock;
  char DateString[11];
  memset(DateString, 0, 11);
  int err = ERR_NONE;

  clock.Date(DateString, 11, &err);
  ASSERT_EQ(err, ERR_NONE);
  printf("Date String: %s\n", DateString);
}

TEST(Clock, DateTime)
{
  Clock clock;
  char DateTimeString[21];
  memset(DateTimeString, 0, 21);
  int err = ERR_NONE;

  clock.DateTime(DateTimeString, 21, &err);
  ASSERT_EQ(err, ERR_NONE);
  printf("DateTime String: %s\n", DateTimeString);
}
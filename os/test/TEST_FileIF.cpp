/*
 * TEST_File.cpp
 *
 *  Created on: Feb 27, 2015
 *      Author: mjc
 */

#include "gtest/gtest.h"
#include <FileIF.hpp>
#include <common/Error.hpp>
#include <cstdlib>
#include <fstream>

using namespace ns_MDL;
using namespace std;

// CString Test Fixture

class Test_FileIF : public testing::Test
{
public:
  /// @brief Generates the path to the test directory depending
  /// on where the app test are run from.

  CString
  TestDirectory()
  {
    int err = ERR_NONE;
    CString currentDirectory = Directory::Cwd();
    CString toReturn;
    // Check if we are in os directory
    currentDirectory.find("/os", 0, &err);

    if (err == ERR_NONE) {
      toReturn = currentDirectory + "/../dst/test/testFiles";
      return toReturn;
    }

    currentDirectory.find("/dst/test", 0, &err);

    if (err == ERR_NONE) {
      toReturn = currentDirectory + "/testFiles";
      return toReturn;
    }

    toReturn = "testFiles";
    return toReturn;
  }

protected:

  // Called before each test is run.

  virtual void
  SetUp()
  {
    TestDir = new CString(TestDirectory());
    AsciiDefaultTxt = new CString(*TestDir + "/Default.txt");
    AsciiDefaultCCString = new CString(*TestDir + "/Default-CCString.txt");
    AsciiWriteTxt = new CString(*TestDir + "/Write.txt");
    AsciiReadWriteTxt = new CString(*TestDir + "/ReadWrite.txt");
    BinaryWriteBin = new CString(*TestDir + "/Write.bin");
    BinaryReadWriteBin = new CString(*TestDir + "/ReadWrite.bin");

    // Create Test Directory
    Directory::Mkdir(TestDirectory());


    // Create Test Files in Directory
    DefaultFileIF = new FileIF();
    DefaultFileCCStringIF = new FileIF();
    TxtWriteFileIF = new FileIF();
    TxtReadWriteFileIF = new FileIF();
    BinWriteFileIF = new FileIF();
    BinReadWriteFileIF = new FileIF();

    DefaultFileIF->open(*AsciiDefaultTxt, FileIF::Write);
    DefaultFileCCStringIF->open(AsciiDefaultCCString->c_str());
    TxtWriteFileIF->open(AsciiWriteTxt->c_str(), FileIF::Write);
    TxtReadWriteFileIF->open(*AsciiReadWriteTxt, FileIF::ReadWrite);
    BinWriteFileIF->open(*BinaryWriteBin, FileIF::Write, true);
    BinReadWriteFileIF->open(*BinaryReadWriteBin, FileIF::ReadWrite, true);
  }

  // Called after each test is run

  virtual void
  TearDown()
  {
    // Delete File Handlers
    delete DefaultFileIF;
    delete DefaultFileCCStringIF;
    delete TxtWriteFileIF;
    delete TxtReadWriteFileIF;
    delete BinWriteFileIF;
    delete BinReadWriteFileIF;

    // remove files
    Directory::Remove(*AsciiDefaultTxt);
    Directory::Remove(*AsciiWriteTxt);
    Directory::Remove(*AsciiReadWriteTxt);
    Directory::Remove(*BinaryWriteBin);
    Directory::Remove(*BinaryReadWriteBin);
    Directory::Remove(*AsciiDefaultCCString);

    // Remove Directory
    Directory::Rmdir(*TestDir);

    // Delete CStrings
    delete TestDir;
    delete AsciiDefaultCCString;
    delete AsciiDefaultTxt;
    delete AsciiWriteTxt;
    delete AsciiReadWriteTxt;
    delete BinaryWriteBin;
    delete BinaryReadWriteBin;
  }

  // CString File Locations
  CString* TestDir;
  CString* AsciiDefaultTxt;
  CString* AsciiDefaultCCString;
  CString* AsciiWriteTxt;
  CString* AsciiReadWriteTxt;
  CString* BinaryWriteBin;
  CString* BinaryReadWriteBin;

  // File Interfaces
  FileIF* DefaultFileIF;
  FileIF* DefaultFileCCStringIF;
  FileIF* TxtWriteFileIF;
  FileIF* TxtReadWriteFileIF;
  FileIF* BinWriteFileIF;
  FileIF* BinReadWriteFileIF;
};

TEST_F(Test_FileIF, VerifyOpenFiles)
{
  // Check DefaultFileIF
  ASSERT_FALSE(DefaultFileIF->isBinary());
  ASSERT_TRUE(DefaultFileIF->isTruncate());
  ASSERT_EQ(FileIF::Write, DefaultFileIF->getMode());

  // Check AsciiDefualtCCString
  ASSERT_FALSE(DefaultFileCCStringIF->isBinary());
  ASSERT_TRUE(DefaultFileCCStringIF->isTruncate());
  ASSERT_EQ(FileIF::ReadWrite, DefaultFileCCStringIF->getMode());

  // Check AsciiWriteTxt
  ASSERT_FALSE(TxtWriteFileIF->isBinary());
  ASSERT_TRUE(TxtWriteFileIF->isTruncate());
  ASSERT_EQ(FileIF::Write, TxtWriteFileIF->getMode());

  // Check AsciiReadWriteTxt
  ASSERT_FALSE(TxtReadWriteFileIF->isBinary());
  ASSERT_TRUE(TxtReadWriteFileIF->isTruncate());
  ASSERT_EQ(FileIF::ReadWrite, TxtReadWriteFileIF->getMode());

  // Check BinaryWriteBin
  ASSERT_TRUE(BinWriteFileIF->isBinary());
  ASSERT_TRUE(BinWriteFileIF->isTruncate());
  ASSERT_EQ(FileIF::Write, BinWriteFileIF->getMode());

  // Check BinaryReadWriteBin
  ASSERT_TRUE(BinReadWriteFileIF->isBinary());
  ASSERT_TRUE(BinReadWriteFileIF->isTruncate());
  ASSERT_EQ(FileIF::ReadWrite, BinReadWriteFileIF->getMode());
}

TEST_F(Test_FileIF, isOpen)
{
  ASSERT_TRUE(DefaultFileCCStringIF->isOpen());
  ASSERT_TRUE(DefaultFileIF->isOpen());
  ASSERT_TRUE(TxtWriteFileIF->isOpen());
  ASSERT_TRUE(TxtReadWriteFileIF->isOpen());
  ASSERT_TRUE(BinWriteFileIF->isOpen());
  ASSERT_TRUE(BinReadWriteFileIF->isOpen());
}

TEST_F(Test_FileIF, Open)
{
  // Reopen already open files
  DefaultFileCCStringIF->open(*AsciiDefaultCCString);
  DefaultFileIF->open(*AsciiDefaultTxt);
  TxtWriteFileIF->open(*AsciiWriteTxt, FileIF::Write);
  TxtReadWriteFileIF->open(*AsciiReadWriteTxt, FileIF::ReadWrite);
  BinWriteFileIF->open(*BinaryWriteBin, FileIF::Write);
  BinReadWriteFileIF->open(*BinaryReadWriteBin, FileIF::ReadWrite);

  // Check if files are open	
  ASSERT_TRUE(DefaultFileCCStringIF->isOpen());
  ASSERT_TRUE(DefaultFileIF->isOpen());
  ASSERT_TRUE(TxtWriteFileIF->isOpen());
  ASSERT_TRUE(TxtReadWriteFileIF->isOpen());
  ASSERT_TRUE(BinWriteFileIF->isOpen());
  ASSERT_TRUE(BinReadWriteFileIF->isOpen());

  // Verify Names of open files
  ASSERT_STREQ(AsciiDefaultCCString->c_str(), DefaultFileCCStringIF->getFullPath().c_str());
  ASSERT_STREQ(AsciiDefaultTxt->c_str(), DefaultFileIF->getFullPath().c_str());
  ASSERT_STREQ(AsciiWriteTxt->c_str(), TxtWriteFileIF->getFullPath().c_str());
  ASSERT_STREQ(AsciiReadWriteTxt->c_str(), TxtReadWriteFileIF->getFullPath().c_str());
  ASSERT_STREQ(BinaryWriteBin->c_str(), BinWriteFileIF->getFullPath().c_str());
  ASSERT_STREQ(BinaryReadWriteBin->c_str(), BinReadWriteFileIF->getFullPath().c_str());
}

TEST_F(Test_FileIF, NoFileOpen)
{
  // Open a read only file that doesn't exist
  FileIF badFile;
  int err = ERR_NONE;

  badFile.open("BadFile.txt", FileIF::Read, false, true, &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  // File didnt open so these should be defaulted.
  err = ERR_NONE;
  ASSERT_FALSE(badFile.isBinary(&err));
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  ASSERT_FALSE(badFile.isTruncate(&err));
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  ASSERT_EQ(badFile.getMode(&err), FileIF::ReadWrite);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  ASSERT_EQ(0, badFile.getReadPosition(&err));
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  ASSERT_EQ(0, badFile.getWritePosition(&err));
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  badFile.setWritePosition(0, &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  err = ERR_NONE;
  badFile.setReadPosition(0, &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
}

TEST_F(Test_FileIF, Close)
{
  int err = ERR_NONE;

  // Close files and get no error
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Close already close file and get an error code
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
  TxtWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
  TxtReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
  BinWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
  BinReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
}

TEST_F(Test_FileIF, Directory_Exists)
{
  // CString
  ASSERT_TRUE(Directory::Exists(*TestDir));
  ASSERT_TRUE(Directory::Exists(*AsciiDefaultTxt));
  ASSERT_TRUE(Directory::Exists(*AsciiWriteTxt));
  ASSERT_TRUE(Directory::Exists(*AsciiReadWriteTxt));
  ASSERT_TRUE(Directory::Exists(*BinaryWriteBin));
  ASSERT_TRUE(Directory::Exists(*BinaryReadWriteBin));

  // C-CString	
  ASSERT_TRUE(Directory::Exists(TestDir->c_str()));
  ASSERT_TRUE(Directory::Exists(AsciiDefaultTxt->c_str()));
  ASSERT_TRUE(Directory::Exists(AsciiWriteTxt->c_str()));
  ASSERT_TRUE(Directory::Exists(AsciiReadWriteTxt->c_str()));
  ASSERT_TRUE(Directory::Exists(BinaryWriteBin->c_str()));
  ASSERT_TRUE(Directory::Exists(BinaryReadWriteBin->c_str()));

  // Invalid File
  ASSERT_FALSE(Directory::Exists("InvalidDirectory"));
}

TEST_F(Test_FileIF, getFullPath)
{
  int err = ERR_NONE;
  // Verfiy open files name
  ASSERT_STREQ(DefaultFileIF->getFullPath().c_str(), AsciiDefaultTxt->c_str());
  ASSERT_STREQ(TxtWriteFileIF->getFullPath().c_str(), AsciiWriteTxt->c_str());
  ASSERT_STREQ(TxtReadWriteFileIF->getFullPath().c_str(), AsciiReadWriteTxt->c_str());
  ASSERT_STREQ(BinWriteFileIF->getFullPath().c_str(), BinaryWriteBin->c_str());
  ASSERT_STREQ(BinReadWriteFileIF->getFullPath().c_str(), BinaryReadWriteBin->c_str());

  // Close files and get no error
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert Name is empty
  ASSERT_STREQ(DefaultFileIF->getFullPath().c_str(), "");
  ASSERT_STREQ(TxtWriteFileIF->getFullPath().c_str(), "");
  ASSERT_STREQ(TxtReadWriteFileIF->getFullPath().c_str(), "");
  ASSERT_STREQ(BinWriteFileIF->getFullPath().c_str(), "");
  ASSERT_STREQ(BinReadWriteFileIF->getFullPath().c_str(), "");
}

TEST_F(Test_FileIF, getFileName)
{
  int err = ERR_NONE;

  // Verify the file names
  ASSERT_STREQ(DefaultFileIF->getFileName().c_str(), "Default.txt");
  ASSERT_STREQ(TxtWriteFileIF->getFileName().c_str(), "Write.txt");
  ASSERT_STREQ(TxtReadWriteFileIF->getFileName().c_str(), "ReadWrite.txt");
  ASSERT_STREQ(BinWriteFileIF->getFileName().c_str(), "Write.bin");
  ASSERT_STREQ(BinReadWriteFileIF->getFileName().c_str(), "ReadWrite.bin");

  // Close files and get no error
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert Name is empty
  ASSERT_STREQ(DefaultFileIF->getFileName().c_str(), "");
  ASSERT_STREQ(TxtWriteFileIF->getFileName().c_str(), "");
  ASSERT_STREQ(TxtReadWriteFileIF->getFileName().c_str(), "");
  ASSERT_STREQ(BinWriteFileIF->getFileName().c_str(), "");
  ASSERT_STREQ(BinReadWriteFileIF->getFileName().c_str(), "");
}

TEST_F(Test_FileIF, getFileExtension)
{
  int err = ERR_NONE;

  // Verify the file extensions
  ASSERT_STREQ(DefaultFileIF->getFileExtension().c_str(), "txt");
  ASSERT_STREQ(TxtWriteFileIF->getFileExtension().c_str(), "txt");
  ASSERT_STREQ(TxtReadWriteFileIF->getFileExtension().c_str(), "txt");
  ASSERT_STREQ(BinWriteFileIF->getFileExtension().c_str(), "bin");
  ASSERT_STREQ(BinReadWriteFileIF->getFileExtension().c_str(), "bin");

  // Close files and get no error
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  TxtReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);
  BinReadWriteFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert Name is empty
  ASSERT_STREQ(DefaultFileIF->getFileExtension().c_str(), "");
  ASSERT_STREQ(TxtWriteFileIF->getFileExtension().c_str(), "");
  ASSERT_STREQ(TxtReadWriteFileIF->getFileExtension().c_str(), "");
  ASSERT_STREQ(BinWriteFileIF->getFileExtension().c_str(), "");
  ASSERT_STREQ(BinReadWriteFileIF->getFileExtension().c_str(), "");
};

TEST_F(Test_FileIF, CopyFile)
{
  int err = ERR_NONE;

  CString CopyTestDir(TestDir->c_str());
  CopyTestDir += "/../CopyTestDir/";
  Directory::Mkdir(CopyTestDir);

  // Generate copy file strings
  CString AsciiDefaultTxtCopy(CopyTestDir);
  AsciiDefaultTxtCopy += DefaultFileIF->getFileName();
  CString AsciiReadWriteTxtCopy(CopyTestDir);
  AsciiReadWriteTxtCopy += TxtWriteFileIF->getFileName();
  CString BinaryWriteBinCopy(CopyTestDir);
  BinaryWriteBinCopy += BinWriteFileIF->getFileName();
  CString BinaryReadWriteBinCopy(CopyTestDir);
  BinaryReadWriteBinCopy += BinReadWriteFileIF->getFileName();

  // Copy the Files
  Directory::CopyFile(DefaultFileIF->getFullPath().c_str(), AsciiDefaultTxtCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(TxtWriteFileIF->getFullPath().c_str(), AsciiReadWriteTxtCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(BinWriteFileIF->getFullPath().c_str(), BinaryWriteBinCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(BinReadWriteFileIF->getFullPath().c_str(), BinaryReadWriteBinCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert the files exist
  ASSERT_TRUE(Directory::Exists(CopyTestDir));
  ASSERT_TRUE(Directory::Exists(AsciiDefaultTxtCopy));
  ASSERT_TRUE(Directory::Exists(AsciiReadWriteTxtCopy));
  ASSERT_TRUE(Directory::Exists(BinaryWriteBinCopy));
  ASSERT_TRUE(Directory::Exists(BinaryReadWriteBinCopy));

  // remove files
  Directory::Remove(BinaryReadWriteBinCopy);
  Directory::Remove(BinaryWriteBinCopy);
  Directory::Remove(AsciiReadWriteTxtCopy);
  Directory::Remove(AsciiDefaultTxtCopy);

  // Remove Directory
  Directory::Rmdir(CopyTestDir);

  // Bad Copy
  Directory::CopyFile(BinaryReadWriteBinCopy.c_str(), "Bad File Copy.bin", &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
};

TEST_F(Test_FileIF, MoveFile)
{
  int err = ERR_NONE;

  CString CopyTestDir(TestDir->c_str());
  CopyTestDir += "/../CopyTestDir/";

  CString MoveTestDir(TestDir->c_str());
  MoveTestDir += "/../MoveTestDir";

  Directory::Mkdir(CopyTestDir);
  Directory::Mkdir(MoveTestDir);

  // Generate copy file strings
  CString AsciiDefaultTxtCopy(CopyTestDir);
  AsciiDefaultTxtCopy += DefaultFileIF->getFileName();
  CString AsciiReadWriteTxtCopy(CopyTestDir);
  AsciiReadWriteTxtCopy += TxtWriteFileIF->getFileName();
  CString BinaryWriteBinCopy(CopyTestDir);
  BinaryWriteBinCopy += BinWriteFileIF->getFileName();
  CString BinaryReadWriteBinCopy(CopyTestDir);
  BinaryReadWriteBinCopy += BinReadWriteFileIF->getFileName();

  // Generate move file strings
  CString AsciiDefaultTxtMove(MoveTestDir);
  AsciiDefaultTxtMove += DefaultFileIF->getFileName();
  CString AsciiReadWriteTxtMove(MoveTestDir);
  AsciiReadWriteTxtMove += TxtWriteFileIF->getFileName();
  CString BinaryWriteBinMove(MoveTestDir);
  BinaryWriteBinMove += BinWriteFileIF->getFileName();
  CString BinaryReadWriteBinMove(MoveTestDir);
  BinaryReadWriteBinMove += BinReadWriteFileIF->getFileName();

  // Copy the Files
  Directory::CopyFile(DefaultFileIF->getFullPath().c_str(), AsciiDefaultTxtCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(TxtWriteFileIF->getFullPath().c_str(), AsciiReadWriteTxtCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(BinWriteFileIF->getFullPath().c_str(), BinaryWriteBinCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::CopyFile(BinReadWriteFileIF->getFullPath().c_str(), BinaryReadWriteBinCopy.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert the files exist
  ASSERT_TRUE(Directory::Exists(CopyTestDir));
  ASSERT_TRUE(Directory::Exists(AsciiDefaultTxtCopy));
  ASSERT_TRUE(Directory::Exists(AsciiReadWriteTxtCopy));
  ASSERT_TRUE(Directory::Exists(BinaryWriteBinCopy));
  ASSERT_TRUE(Directory::Exists(BinaryReadWriteBinCopy));

  // Move the files
  Directory::MoveFile(AsciiDefaultTxtCopy.c_str(), AsciiDefaultTxtMove.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::MoveFile(AsciiReadWriteTxtCopy.c_str(), AsciiReadWriteTxtMove.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::MoveFile(BinaryWriteBinCopy.c_str(), BinaryWriteBinMove.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);
  Directory::MoveFile(BinaryReadWriteBinCopy.c_str(), BinaryReadWriteBinMove.c_str(), &err);
  ASSERT_EQ(err, ERR_NONE);

  // Assert the files exist
  ASSERT_TRUE(Directory::Exists(MoveTestDir));
  ASSERT_TRUE(Directory::Exists(AsciiDefaultTxtMove));
  ASSERT_TRUE(Directory::Exists(AsciiReadWriteTxtMove));
  ASSERT_TRUE(Directory::Exists(BinaryWriteBinMove));
  ASSERT_TRUE(Directory::Exists(BinaryReadWriteBinMove));

  // remove files
  Directory::Remove(BinaryReadWriteBinCopy);
  Directory::Remove(BinaryWriteBinCopy);
  Directory::Remove(AsciiReadWriteTxtCopy);
  Directory::Remove(AsciiDefaultTxtCopy);

  // remove files
  Directory::Remove(BinaryReadWriteBinMove);
  Directory::Remove(BinaryWriteBinMove);
  Directory::Remove(AsciiReadWriteTxtMove);
  Directory::Remove(AsciiDefaultTxtMove);

  // Remove Directory
  Directory::Rmdir(CopyTestDir);
  Directory::Rmdir(MoveTestDir);
};

TEST_F(Test_FileIF, Read)
{
  int err = ERR_NONE;
  char str[14] = {0};
  memset(str, 0, 14);

  // Read past the end of the file
  DefaultFileIF->readLine(&err).c_str();
  ASSERT_EQ(err, ERR_END_OF_FILE);

  // Close files and get no error
  DefaultFileIF->close(&err);
  ASSERT_EQ(err, ERR_NONE);

  // Read from closed file
  DefaultFileIF->read(str, 12, &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);

  ASSERT_STREQ("", DefaultFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
};

TEST_F(Test_FileIF, WriteCharCString)
{
  CString Hello("Hello World\n");

  int err = ERR_NONE;

  DefaultFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtWriteFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtReadWriteFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NONE);

  BinWriteFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NONE);

  BinReadWriteFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NONE);

  // Attempt to write to a closed file
  BinReadWriteFileIF->close();
  BinReadWriteFileIF->write(Hello.c_str(), Hello.size(), &err);
  ASSERT_EQ(err, ERR_NO_FILE_OPEN);
};

TEST_F(Test_FileIF, WriteCString)
{
  CString Hello("Hello World\n");
  int err = ERR_NONE;
  char str[13] = {0};
  memset(str, 0, 13);
  size_t CStringSize = 12;

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  DefaultFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->close();
  DefaultFileIF->open(*AsciiDefaultTxt, FileIF::Read);

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), DefaultFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->setReadPosition(0); // Beginning of file

  // Read Line and Check
  DefaultFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, DefaultFileIF->size());

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  TxtWriteFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  TxtWriteFileIF->close();
  TxtWriteFileIF->open(*AsciiWriteTxt, FileIF::Read);

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), TxtWriteFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  TxtWriteFileIF->close();
  TxtWriteFileIF->open(*AsciiWriteTxt, FileIF::Read);

  // Read Line and Check
  TxtWriteFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, TxtWriteFileIF->size());

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  TxtReadWriteFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  TxtReadWriteFileIF->close();
  TxtReadWriteFileIF->open(*AsciiReadWriteTxt, FileIF::Read);

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), TxtReadWriteFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  TxtReadWriteFileIF->close();
  TxtReadWriteFileIF->open(*AsciiReadWriteTxt, FileIF::Read);

  // Read Line and Check
  TxtReadWriteFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, TxtReadWriteFileIF->size());

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  BinWriteFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  BinWriteFileIF->close();
  BinWriteFileIF->open(*BinaryWriteBin, FileIF::Read, true);

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), BinWriteFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  BinWriteFileIF->close();
  BinWriteFileIF->open(*BinaryWriteBin, FileIF::Read);

  // Read Line and Check
  BinWriteFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, BinWriteFileIF->size());

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  BinReadWriteFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  BinReadWriteFileIF->close();
  BinReadWriteFileIF->open(*BinaryReadWriteBin, FileIF::Read, true);

  // Read File and Check
  ASSERT_STREQ(Hello.c_str(), BinReadWriteFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  BinReadWriteFileIF->close();
  BinReadWriteFileIF->open(*BinaryReadWriteBin, FileIF::Read);

  // Read Line and Check
  BinReadWriteFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, BinReadWriteFileIF->size());
};

TEST_F(Test_FileIF, WriteShort)
{
  unsigned short s = 42;
  int err = ERR_NONE;

  DefaultFileIF->write(s, &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtWriteFileIF->write(s, &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtReadWriteFileIF->write(s, &err);
  ASSERT_EQ(err, ERR_NONE);

  BinWriteFileIF->write(s, &err);
  ASSERT_EQ(err, ERR_NONE);

  BinReadWriteFileIF->write(s, &err);
  ASSERT_EQ(err, ERR_NONE);
};

TEST_F(Test_FileIF, WriteInt)
{
  unsigned int i = 42;
  int err = ERR_NONE;

  DefaultFileIF->write(i, &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtWriteFileIF->write(i, &err);
  ASSERT_EQ(err, ERR_NONE);

  TxtReadWriteFileIF->write(i, &err);
  ASSERT_EQ(err, ERR_NONE);

  BinWriteFileIF->write(i, &err);
  ASSERT_EQ(err, ERR_NONE);

  BinReadWriteFileIF->write(i, &err);
  ASSERT_EQ(err, ERR_NONE);
}

TEST_F(Test_FileIF, Truncate)
{
  CString Hello("Hello World\n");
  int err = ERR_NONE;
  char str[13] = {0};
  memset(str, 0, 13);
  size_t CStringSize = 12;

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  DefaultFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->close();
  DefaultFileIF->open(*AsciiDefaultTxt, FileIF::Read);

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), DefaultFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->setReadPosition(0); // Beginning of file

  // Read Line and Check
  DefaultFileIF->read(str, CStringSize, &err);
  ASSERT_STREQ(Hello.c_str(), str);
  ASSERT_EQ(err, ERR_NONE);

  // Verify Size of file
  ASSERT_EQ(CStringSize, DefaultFileIF->size());

  DefaultFileIF->close();

  // Reopen and truncate the file
  DefaultFileIF->open(*AsciiDefaultTxt, FileIF::ReadWrite, false, true, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(0, DefaultFileIF->size());
}

TEST_F(Test_FileIF, ReadPosition)
{
  CString Hello("Hello World\n");
  int err = ERR_NONE;
  char str[13] = {0};
  memset(str, 0, 13);

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  DefaultFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->close();
  DefaultFileIF->open(*AsciiDefaultTxt, FileIF::Read);

  ASSERT_EQ(0, DefaultFileIF->getReadPosition());

  // Read Line and Check
  ASSERT_STREQ(Hello.c_str(), DefaultFileIF->readLine(&err).c_str());
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(12, DefaultFileIF->getReadPosition());

  // Close File and Read File
  DefaultFileIF->setReadPosition(0); // Beginning of file
  ASSERT_EQ(0, DefaultFileIF->getReadPosition());
}

TEST_F(Test_FileIF, WritePosition)
{
  CString Hello("Hello World\n");
  CString Test("Test ");
  int err = ERR_NONE;
  char str[18] = {0};
  memset(str, 0, 18);

  /////////////////////////////////////////////////////////////////////////////
  // Write to File
  ASSERT_EQ(0, DefaultFileIF->getWritePosition());
  DefaultFileIF->write(Hello, &err);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ(12, DefaultFileIF->getWritePosition(&err));
  ASSERT_EQ(err, ERR_NONE);

  // Close File and Read File
  DefaultFileIF->close();
  DefaultFileIF->open(*AsciiDefaultTxt, FileIF::ReadWrite, false, false, &err);
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_TRUE(DefaultFileIF->isOpen());

  // Check Write Position is at end of file
  ASSERT_EQ(12, DefaultFileIF->getWritePosition(&err));
  ASSERT_EQ(12, DefaultFileIF->getReadPosition());
  ASSERT_EQ(err, ERR_NONE);

  // Write "Test " to end of file
  DefaultFileIF->write(Test, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Verify updated Write Position
  ASSERT_EQ(17, DefaultFileIF->getWritePosition(&err));

  // Read position shifted after the write as well.
  ASSERT_EQ(17, DefaultFileIF->getReadPosition());


  // Set Write Position to beginning of file
  DefaultFileIF->setWritePosition(0, &err);
  ASSERT_EQ(err, ERR_NONE);

  ASSERT_EQ(0, DefaultFileIF->getWritePosition(&err));
  ASSERT_EQ(err, ERR_NONE);
  ASSERT_EQ(0, DefaultFileIF->getReadPosition(&err));
  ASSERT_EQ(err, ERR_NONE);

  DefaultFileIF->write(Test, &err);
  ASSERT_EQ(err, ERR_NONE);

  // Over wrote the beginning words
  ASSERT_EQ(17, DefaultFileIF->size());

  // Read First line of file
  DefaultFileIF->setReadPosition(0, &err);
  CString temp(DefaultFileIF->readLine(&err));
  ASSERT_STREQ(temp.c_str(), "Test  World\n");
}





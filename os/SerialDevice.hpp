/// @file os/Serial.hpp
///
/// @brief Defines the Serial Interface.
///
/// @authors mjc
///
/// @date 01-13-15
///
/// @details An implementation of the Serial Interface.  

#ifndef _SERIAL_DEVICE_HPP_
#define _SERIAL_DEVICE_HPP_

#include <DeviceIF.hpp>
#include <Types.hpp>
#include <datatype/CString.hpp>
#include <HW_GPIO.hpp>

class SerialDeviceImplementation;

/// @brief General MDL Library Wrapper.
///
using namespace ns_Datatype;

namespace ns_MDL
{
  /// @brief An interface to handle writing to the console.
  ///

  class SerialDevice : public DeviceIF
  {
  public:

    enum SerialConfig
    {
      CONFIG_5N1 = 0,
      CONFIG_6N1 = 1,
      CONFIG_7N1 = 2,
      CONFIG_8N1 = 3, // DEFAULT
      CONFIG_5N2 = 4,
      CONFIG_6N2 = 5,
      CONFIG_7N2 = 6,
      CONFIG_8N2 = 7,
      CONFIG_5E1 = 8,
      CONFIG_6E1 = 9,
      CONFIG_7E1 = 10,
      CONFIG_8E1 = 11,
      CONFIG_5E2 = 12,
      CONFIG_6E2 = 13,
      CONFIG_7E2 = 14,
      CONFIG_8E2 = 15,
      CONFIG_5O1 = 16,
      CONFIG_6O1 = 17,
      CONFIG_7O1 = 18,
      CONFIG_8O1 = 19,
      CONFIG_5O2 = 20,
      CONFIG_6O2 = 21,
      CONFIG_7O2 = 22,
      CONFIG_8O2 = 23,
      CONFIG_INVALID = 24
    };

    /** Bits Per Second **/
    enum BaudRate
    {
      BR300 = 300,
      BR600 = 600,
      BR1200 = 1200,
      BR2400 = 2400,
      BR4800 = 4800,
      BR9600 = 9600,
      BR14400 = 14400,
      BR19200 = 19200,
      BR28800 = 28800,
      BR31250 = 31250,
      BR38400 = 38400,
      BR57600 = 57600,
      BR115200 = 115200,
      BR_INVALID = 0
    };

  public:
    SerialDevice(HW_GPIO* hw = 0);
    ~SerialDevice();

    bool open(const char* device, SerialConfig config = CONFIG_8N1, BaudRate br = BR9600, ERROR* error = 0);

    void close();

    void write(const char* str, size_t buffer_size, ERROR* error = 0);

    void write(CString str, ERROR* error = 0);

    size_t read(char* buf, size_t bufferSize, ERROR* error = 0);

    size_t read(char* buf, size_t bufferSize, int timeout, ERROR* error = 0);

    SerialConfig getConfig(ERROR* error = 0);

    BaudRate getBaudRate(ERROR* error = 0);

  private:
    SerialDeviceImplementation* m_pImp;
    HW_GPIO* p_hardware;
  };
};

#endif

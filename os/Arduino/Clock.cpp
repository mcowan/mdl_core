/*
 * Clock.cpp
 *
 *  Created on: Jan 9, 2015
 *  Author: simplymac
 *  Description: An implementation of a templated Queue Container.  Based
 *  on the basic container interface.
 *
 */


#include <Clock.hpp>
#include <common/Error.hpp>

using namespace ns_Datatype;

namespace ns_MDL
{

  const char* Clock::TimeFormatCString = "%02i:%02i:%02i"; // HH:MM:SS
  const char* Clock::DateFormatCString = "%04i/%02i/%02i"; // YYYY:MM:DD
  const size_t Clock::TimeFormatSize = 9;
  const size_t Clock::DateFormatSize = 11;
  const size_t Clock::DateTimeCStringSize = Clock::DateFormatSize + Clock::TimeFormatSize;

  Clock::Clock(TimeFormat format)
  : m_Format(format)
  {
  }

  Clock::~Clock()
  {
  }

  DateTimeStruct
  Clock::Now(ERROR* err)
  {
    DateTimeStruct current;

    SetErr(err, ERR_NOT_IMPLEMENTED);

    return current;
  }

  void
  Clock::DateTime(char* str, size_t bufferSize, ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
  }

  CString
  Clock::DateTime(ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
    return CString("Not Implemented\n");
  }

  void
  Clock::Date(char* str, size_t bufferSize, ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
  }

  CString
  Clock::Date(ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
    return CString("Not Implemented\n");
  }

  void
  Clock::Time(char* str, size_t bufferSize, ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
  }

  CString
  Clock::Time(ERROR* err)
  {
    SetErr(err, ERR_NOT_IMPLEMENTED);
    return CString("Not Implemented\n");
  }

} /* End of MDL */


#include <Console.hpp>
#include <common/Error.hpp>

using namespace ns_MDL;

Console::Console()
{
};

Console::~Console()
{
};

void
Console::write(const char* str, size_t bufferSize, ERROR* err)
{
  SetErr(err, ERR_NOT_IMPLEMENTED);
};

size_t
Console::read(char* buf, size_t bufferSize, ERROR* err)
{
  SetErr(err, ERR_NOT_IMPLEMENTED);
  return 0;
};





#include <SerialDevice.hpp>
#include <common/Error.hpp>
#include <Arduino.h>

using namespace ns_MDL;

class SerialDeviceImplementation
{
public:

  SerialDeviceImplementation()
  : m_isOpen(false)
  {
    // Close the device if we are trying to instantiate multiple devices.
    // Arduios can support multiple serial devices, but I am only configuring
    // the first.
    closeSerialDevice();
  }

  ~SerialDeviceImplementation()
  {
    closeSerialDevice();
  }

  bool
  openSerialDevice(const char* device, SerialDevice::SerialConfig config, SerialDevice::BaudRate br, ERROR* err)
  {
    int configFlags = 0;

    // Close the device if it is already open
    closeSerialDevice();

    // setup configuration flags
    switch (config) {
    case SerialDevice::CONFIG_5N1:
    {
      configFlags = SERIAL_5N1;
      break;
    }
    case SerialDevice::CONFIG_6N1:
    {
      configFlags = SERIAL_6N1;
      break;
    }
    case SerialDevice::CONFIG_7N1:
    {
      configFlags = SERIAL_7N1;
      break;
    }
    case SerialDevice::CONFIG_8N1: //DEFAULT
    {
      configFlags = SERIAL_8N1;
      break;
    }
    case SerialDevice::CONFIG_5N2:
    {
      configFlags = SERIAL_5N2;
      break;
    }
    case SerialDevice::CONFIG_6N2:
    {
      configFlags = SERIAL_6N2;
      break;
    }
    case SerialDevice::CONFIG_7N2:
    {
      configFlags = SERIAL_7N2;
      break;
    }
    case SerialDevice::CONFIG_8N2:
    {
      configFlags = SERIAL_8N2;
      break;
    }
    case SerialDevice::CONFIG_5E1:
    {
      configFlags = SERIAL_5E1;
      break;
    }
    case SerialDevice::CONFIG_6E1:
    {
      configFlags = SERIAL_6E1;
      break;
    }
    case SerialDevice::CONFIG_7E1:
    {
      configFlags = SERIAL_7E1;
      break;
    }
    case SerialDevice::CONFIG_8E1:
    {
      configFlags = SERIAL_8E1;
      break;
    }
    case SerialDevice::CONFIG_5E2:
    {
      configFlags = SERIAL_5E2;
      break;
    }
    case SerialDevice::CONFIG_6E2:
    {
      configFlags = SERIAL_6E2;
      break;
    }
    case SerialDevice::CONFIG_7E2:
    {
      configFlags = SERIAL_7E2;
      break;
    }
    case SerialDevice::CONFIG_8E2:
    {
      configFlags = SERIAL_8E2;
      break;
    }
    case SerialDevice::CONFIG_5O1:
    {
      configFlags = SERIAL_5O1;
      break;
    }
    case SerialDevice::CONFIG_6O1:
    {
      configFlags = SERIAL_6O1;
      break;
    }
    case SerialDevice::CONFIG_7O1:
    {
      configFlags = SERIAL_7O1;
      break;
    }
    case SerialDevice::CONFIG_8O1:
    {
      configFlags = SERIAL_8O1;
      break;
    }
    case SerialDevice::CONFIG_5O2:
    {
      configFlags = SERIAL_5O2;
      break;
    }
    case SerialDevice::CONFIG_6O2:
    {
      configFlags = SERIAL_6O2;
      break;
    }
    case SerialDevice::CONFIG_7O2:
    {
      configFlags = SERIAL_7O2;
      break;
    }
    case SerialDevice::CONFIG_8O2:
    {
      configFlags == SERIAL_8O2;
      break;
    }
    default:
    {
      SetErr(err, ERR_INVALID);
      m_Device.clear();
      m_isOpen = false;
      return false;
    }
    }

    Serial.begin(br, configFlags);

    m_Device.assign(device);
    m_BR = br;
    m_SerialConfig = config;
    SetErr(err, ERR_NONE);

    m_isOpen = true;
    return true;
  }

  void
  closeSerialDevice()
  {
    Serial.end();
    m_isOpen = false;
  }

  void
  serialWrite(const char* str, size_t bufferSize, ERROR* err)
  {
    if (m_isOpen) {
      if (Serial.write(str, bufferSize) == 0) {
        SetErr(err, ERR_WRITE);
      }
      else {
        Serial.flush();
        SetErr(err, ERR_NONE);
      }
    }
    else {
      SetErr(err, ERR_HARDWARE);
    }
  }

  size_t
  serialRead(char* buf, size_t bufferSize, ERROR* err, int timeout)
  {
    size_t charRead = 0;


    if (m_isOpen) {
      Serial.setTimeout(timeout);

      charRead = Serial.readBytes(buf, bufferSize);

      if (charRead == 0) {
        SetErr(err, ERR_TIMEOUT);
      }
      else {
        SetErr(err, ERR_NONE);
      }
    }
    else {
      SetErr(err, ERR_HARDWARE);
    }

    return charRead;
  }

  SerialDevice::SerialConfig
  getConfig(ERROR* err)
  {
    if (!m_isOpen) {
      SetErr(err, ERR_HARDWARE);
      return SerialDevice::CONFIG_INVALID;
    }

    SetErr(err, ERR_NONE);
    return m_SerialConfig;
  }

  SerialDevice::BaudRate
  getBaudRate(ERROR* err)
  {
    if (!m_isOpen) {
      SetErr(err, ERR_HARDWARE);
      return SerialDevice::BR_INVALID;
    }

    SetErr(err, ERR_NONE);
    return m_BR;
  }

private:
  bool m_isOpen;
  CString m_Device;
  int m_fd;
  SerialDevice::BaudRate m_BR;
  SerialDevice::SerialConfig m_SerialConfig;
};

SerialDevice::SerialDevice(HW_GPIO* hw)
: p_hardware(hw)
{
  m_pImp = new SerialDeviceImplementation();

  p_hardware->Pins[0].setMode(SerialLock);
  p_hardware->Pins[1].setMode(SerialLock);
}

SerialDevice::~SerialDevice()
{
  delete m_pImp;
}

bool
SerialDevice::open(const char* device, SerialConfig config, BaudRate br, ERROR* err)
{
  return m_pImp->openSerialDevice(device, config, br, err);
}

void
SerialDevice::close()
{
  m_pImp->closeSerialDevice();
}

void
SerialDevice::write(const char* str, size_t bufferSize, ERROR* err)
{
  m_pImp->serialWrite(str, bufferSize, err);
}

void
SerialDevice::write(CString str, ERROR* err)
{
  m_pImp->serialWrite(str.c_str(), str.size(), err);
}

size_t
SerialDevice::read(char* buf, size_t bufferSize, int timeout, ERROR* err)
{
  Serial.flush();
  return m_pImp->serialRead(buf, bufferSize, err, timeout);
}

size_t
SerialDevice::read(char* buf, size_t bufferSize, ERROR* err)
{
  Serial.flush();
  return m_pImp->serialRead(buf, bufferSize, err, 1000);
}

SerialDevice::SerialConfig
SerialDevice::getConfig(ERROR* err)
{
  return m_pImp->getConfig(err);
}

SerialDevice::BaudRate
SerialDevice::getBaudRate(ERROR* err)
{
  return m_pImp->getBaudRate(err);
}


/// @file Arduino/Thread.cpp
///
/// @date Jan 16, 2015
/// @Authors mjc
/// @details Arduino Thread Source Code.


#include <Thread.hpp>
#include <Types.hpp>
#include <Arduino/Arduino.h>

/// @brief General MDL Library Wrapper.
///
using namespace ns_MDL;

Thread::Thread()
{
};

Thread::~Thread()
{
};

void
Thread::Sleep(size_t milliseconds)
{
  delay(milliseconds);
};

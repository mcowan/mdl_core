/*
 * HW_GPIO.cpp
 *
 *  Created on: Feb 27, 2015
 *  Author: mjc
 *  Description: An abstraction layer for the GPIO.
 *
 */


#include <common/Error.hpp>
#include <HW_GPIO.hpp>

#include <Arduino/Arduino.h>

namespace ns_MDL
{

  Pin::Pin()
  : m_Mode(DigitalOutput)
  , m_PinNumber(0)
  , m_Value(0)
  , m_Initialized(false)
  {
  };

  Pin::Pin(size_t pinNumber, PIN_MODE mode)
  : m_Mode(mode)
  , m_PinNumber(pinNumber)
  , m_Value(0)
  , m_Initialized(false)
  {
  };

  Pin::~Pin()
  {
    m_PinNumber = 0;
    m_Value = 0;
    m_Initialized = false;
  };

  void
  Pin::setPin(size_t pinNumber, ERROR* err)
  {
    if (m_Initialized) {
      SetErr(err, ERR_INVALID);
    }

    m_PinNumber = pinNumber;
    SetErr(err, ERR_NONE);
  };

  size_t
  Pin::getPin(ERROR* err)
  {
    SetErr(err, ERR_NONE);
    return m_PinNumber;
  };

  PIN_MODE
  Pin::getMode(ERROR* err)
  {
    SetErr(err, ERR_NONE);
    return m_Mode;
  };

  void
  Pin::setMode(PIN_MODE mode, ERROR* err)
  {
    if (m_Initialized) {
      SetErr(err, ERR_INVALID);
      return;
    }

    SetErr(err, ERR_NONE);
    m_Mode = mode;
  };

  void
  Pin::write(unsigned short value, ERROR* err)
  {
    if (!m_Initialized) {
      SetErr(err, ERR_NOT_INITIALIZED);
      return;
    }

    switch (m_Mode) {
    case DigitalInput:
    case AnalogInput:
    {
      SetErr(err, ERR_HARDWARE);
      break;
    }
    case AnalogOutput:
    {
      if (value <= ANALOG_WRITE_HIGH && value >= ANALOG_WRITE_LOW) {
        SetErr(err, ERR_NONE);
        m_Value = value;
        analogWrite(m_PinNumber, value);
      }
      else {
        SetErr(err, ERR_RANGE);
      }

      break;
    }
    case DigitalOutput:
    {
      if (value <= DIGITAL_HIGH && value >= DIGITAL_LOW) {
        SetErr(err, ERR_NONE);
        m_Value = value;
        digitalWrite(m_PinNumber, value);
      }
      else {
        SetErr(err, ERR_RANGE);
      }

      break;
    }
    };
  };

  unsigned short
  Pin::read(ERROR* err)
  {
    unsigned short toReturn = 0;

    if (!m_Initialized) {
      SetErr(err, ERR_NOT_INITIALIZED);
      return 0;
    }

    switch (m_Mode) {
    case DigitalInput:
    {
      toReturn = digitalRead(m_PinNumber);
      SetErr(err, ERR_NONE);
      break;
    }
    case AnalogInput:
    {
      toReturn = analogRead(m_PinNumber);
      SetErr(err, ERR_NONE);
      break;
    }
    case DigitalOutput:
    case AnalogOutput:
    {
      SetErr(err, ERR_HARDWARE);
      toReturn = m_Value;
      break;
    }
    };

    return toReturn;
  };

  void
  Pin::initialize()
  {
    switch (m_Mode) {
    case DigitalInput:
    case AnalogInput:
    {
      pinMode(m_PinNumber, INPUT);
      m_Initialized = true;
      break;
    }
    case AnalogOutput:
    case DigitalOutput:
    {
      pinMode(m_PinNumber, OUTPUT);
      m_Initialized = true;
      break;
    };
    };
  };

  HW_GPIO::HW_GPIO()
  {
    for (int i = 0; i < NUM_DIGITAL_PINS; i++) {
      Pins.push_back(Pin(i, DigitalOutput));
    }
  };

  HW_GPIO::~HW_GPIO()
  {
  };

  void
  HW_GPIO::initialize()
  {
    for (int i = 0; i < NUM_DIGITAL_PINS; i++) {
      Pins[i].initialize();
    }
  };

};





/*
 * @file System.hpp
 * @brief Defines a series of functions that allow the user to query the systems
 * functionality and capability.
 */


#ifndef __SYSTEM_HPP__
#define __SYSTEM_HPP__

#include <Types.hpp>
#include <common/Error.hpp>

/// @brief General MDL Library Wrapper.
///
namespace ns_MDL
{
  /// @brief Get the total systems Physical Memory;
  ///
  /// @param[out] err Optional Error Argument.
  ///
  /// @return The number of bytes of RAM.
  ///
  unsigned long getTotalSystemMemory(ERROR* error = 0);

  /// @brief Hardware Machine Name
  ///
  /// @param[out] buffer      String return.
  /// @param[in]  bufferSize  Total Size of the buffer.
  /// @param[out] err         Optional Error Argument.
  ///
  void getHardwareDescription(char* buffer, size_t bufferSize, ERROR* error = 0);

  /// @brief Hardware Machine Model
  ///
  /// @param[out] buffer      String return.
  /// @param[in]  bufferSize  Total Size of the buffer.
  /// @param[out] err         Optional Error Argument.
  ///
  void getOSDescription(char* buffer, size_t bufferSize, ERROR* error = 0);

  /// @brief Get the total number of CPUs.
  ///
  /// @param[out] err Optional Error Argument.
  ///
  /// @return The number of CPU's.
  ///
  unsigned long getNumberOfCPUs(ERROR* error = 0);

  /// @brief Get the Frequency of the CPU in Hertz.
  ///
  /// @param[out] err Optional Error Argument.
  ///
  /// @return Frequency in Hertz.
  ///
  unsigned long getCPUFrequency(ERROR* error = 0);

  /// @brief Get if the system is big or little endian.
  ///
  /// @param[out] err Optional Error Argument.
  ///
  /// @return Endianess of the system.
  ///
  bool isBigEndian(ERROR* error = 0);
};

#endif 

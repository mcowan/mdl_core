/*
 * @file Clock.hpp
 * @brief An implementation of a time interface. Great for timestamps.
 */

#ifndef __Clock_HPP__
#define __Clock_HPP__

#include <Types.hpp>
#include <datatype/CString.hpp>

using namespace ns_Datatype;

/*
 * @brief General MDL Library Wrapper.
 */
namespace ns_MDL
{

  struct DateTimeStruct
  {
    unsigned int year;
    unsigned int month;
    unsigned int day;

    unsigned int hour;
    unsigned int min;
    unsigned int sec;

    DateTimeStruct()
    : year(0)
    , month(0)
    , day(0)
    , hour(0)
    , min(0)
    , sec(0)
    {
    };
  };

  class Clock
  {
  public:

    enum TimeFormat
    {
      Military,
      Standard
    };

    static const size_t TimeFormatSize;
    static const size_t DateFormatSize;
    static const size_t DateTimeCStringSize;

  public:
    /*
     * @brief Create an instance of the clock.
     */
    Clock(TimeFormat format = Standard);

    /*
     * @brief Destringuctor
     */
    ~Clock();

    /*
     * @brief Get the current time as a DateTime object.
     *
     * @param[out] error Optional error code.
     * 
     * @return DateTime stringucture
     */
    DateTimeStruct Now(ERROR* error = 0);

    /*
     * @brief Return the DateTime as a c-string.
     *
     * @param[out] string DateTime string.
     * @param[in]  bufferSize The size of the buffer being passed in.
     * @param[out] error Optional error code.
     */
    void DateTime(char* string, size_t bufferSize, ERROR* error = 0);

    /*
     * @brief Return the DateTime as a CString.
     *
     * @param[out] error Optional error code.
     *
     * @return CString.
     */
    CString DateTime(ERROR* error = 0);

    /*
     * @brief Return the Date as a CString.
     *
     * @param[out] error Optional error code.
     *
     * @return CString.
     */
    void Date(char* string, size_t bufferSize, ERROR* error = 0);

    /*
     * @brief Return the Date as a CString.
     *
     * @param[out] error Optional error code.
     *
     * @return CString.
     */
    CString Date(ERROR* error = 0);

    /*
     * @brief Return the Time as a CString.
     *
     * @param[out] string Time string.
     * @param[in]  bufferSize The size of the buffer being passed in.
     * @param[out] error Optional error code.
     */
    void Time(char* string, size_t bufferSize, ERROR* error = 0);

    /*
     * @brief Return the Time as a CString.
     *
     * @param[out] error Optional error code.
     *
     * @return CString.
     */
    CString Time(ERROR* error = 0);

  private:
    static const char* TimeFormatCString; // HH:MM:SS
    static const char* DateFormatCString; // YYYY:MM:DD
    TimeFormat m_Format; // User selected time format
  }; // End of Clock.hpp
}; /* End of MDL */

#endif 

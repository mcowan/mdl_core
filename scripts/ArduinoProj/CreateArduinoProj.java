
// File IO
import java.io.File;
import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.io.FileOutputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * The HelloWorldApp class implements an application that
 * simply displays "Hello World!" to the standard output.
 */
public class CreateArduinoProj
{
		private static String projectName;
		private static String baseDirectory;
		
		public static void CreateProjectDirectory()
		{
				File file = new File(baseDirectory + projectName );
				if (!file.exists())
				{
					if (file.mkdir())
					{
						System.out.println(projectName + " Directory is created!");
					}
					else
					{
						System.out.println("Failed to create directory!");
					}
				}		
		};
		
		public static void CreateProjectMakefile()
		{
				Writer writer = null;
				
				try
				{
						writer = new BufferedWriter(new OutputStreamWriter(
									new FileOutputStream(baseDirectory + projectName + "/Makefile"), "utf-8"));
						
						System.out.println(baseDirectory + projectName + "/Makefile");
						writer.write("# Override Super Directory \n");
						writer.write("BASEDIR=../../\n");
						writer.write("\n");
						writer.write("TARGET = UNO\n");
						writer.write("\n");
						writer.write("LINKLIBS+= arduinomain \\\n");
						writer.write("           common \n");
						writer.write("\n");
						writer.write("include $(BASEDIR)Framework/DefaultConfig.mak\n\n");
				}
				catch (IOException ex)
				{
					// report
				}
				finally
				{
					 try {writer.close();} catch (Exception ex) {}
				}
		};
		
				public static void CreateProjectFile()
		{
				Writer writer = null;
				
				try
				{
						Date date = new Date();
						String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);

						writer = new BufferedWriter(new OutputStreamWriter(
									new FileOutputStream(baseDirectory + projectName + "/" + projectName + ".cpp"), "utf-8"));
						
												System.out.println(baseDirectory + projectName + "/" + projectName + ".cpp");

						
						writer.write("/*************************************************************\n");
						writer.write("* @file " + projectName +".cpp \n");
						writer.write("*\n");
						writer.write("* @date " + modifiedDate + "\n");
						writer.write("*\n");
						writer.write("* @description: A basic Blink Arduino project. TODO: Update\n");
						writer.write("**************************************************************/\n");
						writer.write("\n");
						writer.write("#include <Arduino/Arduino.h> \n");
						writer.write("#include <Thread.hpp>\n");
						writer.write("#include <HW_GPIO.hpp>\n");
						writer.write("\n");
						writer.write("using namespace ns_MDL;\n");
						writer.write("\n");
						writer.write("// Define Pins and Hardware\n");
						writer.write("HW_GPIO Hardware;\n");
						writer.write("int led = 13;");
						writer.write("// the setup routine runs once when you press reset:\n");
						writer.write("void setup() \n");
						writer.write("{ \n");
						writer.write("  // Configure the hardware");
						writer.write("\n");
						writer.write("  // Initialize Hardware\n");
						writer.write("  Hardware.initialize();\n");
						writer.write("};\n");
						writer.write("\n");
						writer.write("// the loop routine runs over and over again forever: \n");
						writer.write("void loop() \n");
						writer.write("{\n");
						writer.write("  Hardware.Pins[led].write(DIGITAL_HIGH);\n");
						writer.write("  Thread::sleep(1000);\n");
						writer.write("\n");
						writer.write("  Hardware.Pins[led].write(DIGITAL_LOW);\n");
						writer.write("  Thread::sleep(1000);\n");
						writer.write("};\n");
						writer.write("\n");
						writer.write("\n");
				}
				catch (IOException ex)
				{
					// report
				}
				finally
				{
					 try {writer.close();} catch (Exception ex) {}
				}
		};
		
    public static void main(String[] args)
    {
				// Declare Variables
				baseDirectory = args[0] + "app/";

        System.out.println("Please enter a project name... [Default: NewProj]");
				System.out.print(" > ");
				try
				{
						projectName = System.console().readLine();
				}
				catch(Exception e)
				{
						projectName = "NewProj";
				}
				
				if(projectName.isEmpty())
				{
						projectName = "NewProj";
				}
				
				System.out.println(projectName);
				
				// Create the project directory
				CreateProjectDirectory();
				
				// Write Makefile
				CreateProjectMakefile();
				
				// Write the main Arduino File
				CreateProjectFile();
    };
};
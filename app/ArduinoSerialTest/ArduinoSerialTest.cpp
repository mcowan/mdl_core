/*************************************************************
* @file ArduinoSerialTest.cpp 
*
* @date 2015-05-21
*
* @description: A basic Blink Arduino project. TODO: Update
**************************************************************/

#include <Arduino/Arduino.h> 
#include <Thread.hpp>
#include <SerialDevice.hpp>
#include <datatype/CString.hpp>
#include <stdio.h>

using namespace ns_MDL;
using namespace ns_Datatype;

// Define Pins and Hardware
int led = 13;// the setup routine runs once when you press reset:
int timeout = 500;
//SerialDevice device;

HW_GPIO* Hardware;
SerialDevice* device;
char str[255];
int charRead = 0;
int err = ERR_NONE;
CString str2;

void setup() 
{
	Hardware = new HW_GPIO();
	device = new SerialDevice();
	
	device->open("Arduino USB");
  // Configure the hardware
  // Initialize Hardware
  Hardware->initialize();
};

// the loop routine runs over and over again forever: 
void loop() 
{
	str2.clear();
	
	charRead = device->read(str, 255, &err);
	
	if(charRead != 0 && err == ERR_NONE)
	{
		Hardware->Pins[led].write(DIGITAL_HIGH);
		device->write(str, charRead, &err);
		Thread::sleep(timeout);
		device->write("Read Data\n");
	}
	if(charRead == 0)
	{
		device->write("No Data \n");
	}
	else
	{
		device->write("Data Found \n");
	}
	Thread::sleep(250);
	charRead = 0;
	err = ERR_NONE;
	Hardware->Pins[led].write(DIGITAL_LOW);
};





#include <stdio.h>
#include <Stopwatch.hpp>
#include <Clock.hpp>
#include <common/CopyrightLogo.hpp>
#include <container/Vector.hpp>
#include <vector>
#include <container/Pair.hpp>



using namespace std;
using namespace ns_MDL;
using namespace ns_Container;
using namespace ns_MDL::ns_Container;


#define STL_Version 1
#define MY_Version 0

typedef unsigned char TestDataType;
static const int MAX_TEST_SIZES = 9;
static const size_t TestSizes[MAX_TEST_SIZES] = { 10,
                                                  100,
                                                  1000,
                                                  10000,
                                                  100000,
                                                  1000000,
                                                  10000000,
                                                  100000000,
                                                  1000000000};
                                                  
int MaxRunTests = MAX_TEST_SIZES;


static double ResultsArray[MAX_TEST_SIZES][2];

// Test Pointer
typedef void TEST(size_t);


Vector< Pair<const char*, Pair<TEST*, TEST*> > > TestList; 



void PrintResults(const char* Test)
{
  printf("\n\n%s\n", Test);
  printf(" Power |      Mine      |    Standard   |   Difference   |   Percent   \n");
  for(int i = 0; i < MaxRunTests; i++)
  {
    double difference = ResultsArray[i][MY_Version]-ResultsArray[i][STL_Version];
    double average = (ResultsArray[i][MY_Version]+ResultsArray[i][STL_Version])/2.0;
    double percentDifference = difference/average*100;
    
    printf("-------+----------------+----------------+---------------|---------------\n");
    printf(" 10^%02i | %14.7f | %14.7f | %14.7f | %05.2f\n", i+1, ResultsArray[i][MY_Version], ResultsArray[i][STL_Version], difference, percentDifference);
  }
}


void STL_Vector_Push_Back(size_t Elements)
{
  vector<TestDataType> stdVector;
  
  for(size_t j = 0; j < Elements; j++)
  {
    stdVector.push_back(j);
  } 
}

void MY_Vector_Push_Back(size_t Elements)
{
  Vector<TestDataType> myVector;
  
  for(size_t j = 0; j < Elements; j++)
  {
    myVector.push_back(j);
  } 
}

void ExecuteTests(void)
{
  Stopwatch timer;
  double stdTime;
  double myTime;
  
  // Push Back
  for(size_t i = 0; i < TestList.size(); i++)
  {
    double TotalTime = 0.0;
    printf("Executing Test [%lu/%lu]\n", i+1, TestList.size());
    for(int j = 0; j < MaxRunTests; j++)
    {
      printf("Executing SubTest[%i/%i]...", j+1, MaxRunTests);
      
      // Standard Vector
      timer.Start();
      TestList[i].Right.Right(TestSizes[j]);
      stdTime = timer.Stop();
      
      // My Vector
      timer.Start();
      TestList[i].Right.Left(TestSizes[j]);
      myTime = timer.Stop();
      
      // Store Results
      ResultsArray[j][MY_Version] = myTime;
      ResultsArray[j][STL_Version] = stdTime;
      TotalTime+= myTime + stdTime;
          
      printf(" done! [%.7f Seconds]\n", myTime+stdTime);
    }
    printf("Complete!! [%.7f Seconds]\n", TotalTime);
    
    // Print push back results
    PrintResults(TestList[i].Left);
  }
  
  
  /*
  // Push Front
  for(int i = 0; i < MaxRunTests; i++)
  {
    printf("Executing Test[%i/%i]...", i, MaxRunTests);
    vector<TestDataType> stdVector;
    Vector<TestDataType> myVector;

    // Standard Vector
    timer.Start();
    for(size_t j = 0; j < TestSizes[i]; j++)
    {
      stdVector.insert(stdVector.begin(), j); 
    }
    
    stdTime = timer.Stop();
    
    // My Vector
    timer.Start();
    for(size_t j = 0; j < TestSizes[i]; j++)
    {
      myVector.push_front(j);
    }
    myTime = timer.Stop();
  
    // Store Results
    ResultsArray[i][0] = myTime;
    ResultsArray[i][1] = stdTime;
    
    // Clear Vectors
    stdVector.clear();
    myVector.clear();
    printf(" done!\n");
  }
  PrintResults("Vector -- insert at beginning");
  */
}



int main()
{
  Clock clock;
  Stopwatch ProgramTimer;
  char TimeStamp[20];
  
  clock.DateTime(TimeStamp, sizeof(TimeStamp));
  MicroDevelLogo("Perfomance Test Suite", 2015);
  printf("Started On: %s\n", TimeStamp);
  ProgramTimer.Start();
  
  
  //// Compile List of Tests
  TestList.push_back(Pair<const char*, Pair<TEST*, TEST*> >("Vector - back", Pair<TEST*, TEST*>(&MY_Vector_Push_Back, &STL_Vector_Push_Back)));
  
  //// Call Execute All Tests
  ExecuteTests();
  
  //PerformanceTestList();
  
  
  // End of Program
  clock.DateTime(TimeStamp, sizeof(TimeStamp));
  printf("\n\n==============================================\n");
  printf("Finished At: %s\n", TimeStamp);
  printf("Total Runtime: %f Seconds\n", ProgramTimer.Stop());
  return 0; 
}





/*
  Blink
  Turns on an LED on for one second, then off for one second, repeatedly.
 
  This example code is in the public domain.
 */
 
 #include <Thread.hpp>
 #include <HW_GPIO.hpp>
 #include <Arduino/Arduino.h>
 using namespace ns_MDL;

int time = 1000;
 
// Pin 13 has an LED connected on most Arduino boards.
// give it a name:
//Pin led(13, DigitalOutput);
//HW_GPIO Hardware;
int led = 13;
// the setup routine runs once when you press reset:
void setup()
{                
  // Configure any pins here before the initialization.
  
  
  Hardware.initialize();
}

// the loop routine runs over and over again forever:
void loop()
{  
  Hardware.Pins[led].write(DIGITAL_HIGH);
  Thread::sleep(time);
  
  Hardware.Pins[led].write(DIGITAL_LOW);
  Thread::sleep(time);
}


# override Super Directory


include Framework/DefaultConfig.mak



SOURCE_TEST_LIBS = \
        gtest      \
	common     \
	container  \
	math       \
        datatype  
	
	


##########################################
# Target: test
#
# Make Target in the parent directory will compile all of the tests in 
# all of the source directories except for those pertaining to the 
# google test directories which fail at this time.
#
test:
		# Test the OS Abstraction Layer
		$(MAKE) -C $(OSDIR)
		
		# Loop through and Test all of the tests listed in the Source TEST Libs
		@for lib in $(SOURCE_TEST_LIBS) ; do \
				echo "Compiling Tests: $$lib" ; \
				$(MAKE) -C $(addprefix $(SRCDIR)/,$$lib) test; \
		done


